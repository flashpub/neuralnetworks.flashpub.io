import styled from 'styled-components';

import { Color, Font, Media, Spacing } from 'assets/styles';
import { FPButton } from 'ui/elements';
import { StyledFlexWrapper } from 'ui/elements/styles';

export const StyledCreateAccountButton = styled(FPButton)`
    font-size: 16px;
    font-weight: 200;
    margin-bottom: 0;
    color: ${Color.grey};
    font-family: ${Font.text};
    &:hover {
        color: ${Color.flashpub};
    }
`;
export const StyledResetPassword = styled(FPButton)`
    margin: 0;
    font-size: 12px;
    height: fit-content;
    color: ${Color.midgrey};
    font-family: ${Font.text};
    &:hover {
        color: ${Color.flashpub};
    }
`;
export const StyledNextButton = styled(FPButton)`
    display: block;
    font-size: 28px;
    font-weight: 200;
    font-family: ${Font.text};
    margin: ${Spacing._12} auto;
    color: ${({ disabled, isLoading }) => (disabled ? Color.midgrey : isLoading ? Color.midgrey : Color.flashpub)};
`;
export const StyledAccessTitle = styled.h2`
    margin-top: 0;
    font-size: 28px;
    font-weight: 200;
    text-align: center;
    color: ${Color.grey};
    font-family: ${Font.text};
    margin-bottom: ${Spacing._24};
`;
export const StyledAccessText = styled.p`
    font-size: 17px;
    font-weight: 300;
    line-height: 20px;
    text-align: justify;
    color: ${Color.grey};
    font-family: ${Font.text};
`;
export const StyledAccessFormWrapper = styled(StyledFlexWrapper)`
    width: 100%;
    font-family: ${Font.text};
`;
export const StyledAccessFormFieldWrapper = styled(StyledFlexWrapper)`
    width: 100%;
    input {
        height: 36px;
    }
`;
export const StyledConnectOrcidButton = styled(StyledNextButton)`
    font-size: 20px;
    ${Media.tablet} {
    
    }
`;
export const StyledGetOrcidButton = styled(StyledCreateAccountButton)``;
export const StyledOpenOrcidProfile = styled(FPButton)`
    color: ${Color.success};
    font-family: ${Font.text};
    div {
        font-size: 14px;
    }
    &:hover {
        color: ${Color.flashpub};
    }
`;
