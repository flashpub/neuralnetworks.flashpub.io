import * as React from 'react';

import { FPEditIcon } from 'ui/elements';
import { StyledPublicationSheetTitleWrapper, StyledPublicationSheetTitle } from 'ui/Publish/styles';

type Props = {
    editMode: boolean;
    pub: PubDefinition;
    onBlur(value: string): void;
    onChange(value: string): void;
};

export const PublicationTitle: React.FC<Props> = ({ pub, editMode, onChange, onBlur }) => {
    return (
        <StyledPublicationSheetTitleWrapper>
            <StyledPublicationSheetTitle
                counter
                required
                chars={150}
                onBlur={onBlur}
                value={pub.title}
                onChange={onChange}
                disabled={!editMode}
                placeholder="Publication title..."
            />
            {editMode && <FPEditIcon />}
        </StyledPublicationSheetTitleWrapper>
    );
};
