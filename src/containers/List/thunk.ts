import { Dispatch } from 'redux';

import * as API from 'api';
import { AppThunk } from 'store';
import * as List from 'containers/List';
import { firebase } from 'configs/firebase';
import * as Feed from 'containers/Feed/Feed.container';

type Snapshot = firebase.firestore.QueryDocumentSnapshot<firebase.firestore.DocumentData>;

export function fetchDrafts(uid: string): AppThunk {
    return async (dispatch: Dispatch) => {
        dispatch(List.status.loading());
        try {
            const list: PubDefinition[] = [];
            const snapshot = await API.pubs.drafts(uid);
            snapshot.forEach((snap: Snapshot) => list.push(snap.data() as PubDefinition));
            dispatch(List.populate(list.sort((a, b) => b.metadata.datePublished - a.metadata.datePublished)));
            dispatch(List.status.success());
        } catch (err) {
            dispatch(List.status.failure(err.message));
        }
    };
}

export function fetchBookmarked(uid: string): AppThunk {
    return async (dispatch: Dispatch) => {
        dispatch(List.status.loading());
        try {
            const list: PubDefinition[] = [];
            const snapshot = await API.pubs.bookmarked(uid);
            snapshot.forEach((snap: Snapshot) => list.push(snap.data() as PubDefinition));
            dispatch(List.populate(list));
            dispatch(List.status.success());
        } catch (err) {
            dispatch(List.status.failure(err.message));
        }
    };
}

export function fetchListed(list: string[]): AppThunk {
    return async (dispatch: Dispatch) => {
        dispatch(List.status.loading());
        try {
            const pubs: PubDefinition[] = [];
            for (const id of list) {
                const resp = await API.pub.load(id);
                if (resp) {
                    pubs.push(resp.data() as PubDefinition);
                }
            }
            dispatch(List.populate(pubs.filter((p) => !p.metadata.draft)));
            dispatch(List.status.success());
        } catch (err) {
            dispatch(List.status.failure(err.message));
        }
    };
}
