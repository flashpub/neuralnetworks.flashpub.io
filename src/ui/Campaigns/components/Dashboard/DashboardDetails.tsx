import dayjs from 'dayjs';
import lowerCase from 'lodash/lowerCase';
import * as React from 'react';
import { useNavigate } from '@reach/router';
import { LeftOutlined, CloseOutlined } from '@ant-design/icons';
import { ResponsiveSwarmPlot } from '@nivo/swarmplot';

import {
    StyledDashboardDetails,
    StyledDashboardTile,
    StyledDashboard,
} from 'ui/Campaigns/styles';
import { Color, Spacing } from 'assets/styles';
import { FPButton } from 'ui/elements';
import { CampaignTitle } from 'ui/Campaigns/components/CampaignTitle';
import { StyledPubRecordTag } from 'ui/Feed/styles';

interface Props {
    contentTypesDetails: MidasCampaignData;
}

export const DashboardDetails: React.FC<Props> = ({ contentTypesDetails }) => {
    const navigate = useNavigate();
    
    const [selectedPub, setSelectedPub] = React.useState<MidasCampaignPubsData | null>(null);
    const graphData = contentTypesDetails.pubs.map((p) => {
        return {
            id: p.title,
            group: 'US',
            volume: 10,
            isEndorsed: p.isEndorsed,
            keyDetails: p.keyDetails,
            pubURL: p.pubURL,
            publishedDate: p.publishedDate,
            title: p.title,
            value: p.value,
            valueError: p.valueError,
        };
    });
    
    const onNodeClick = (node: MidasCampaignPubsData | null) => setSelectedPub(node);
    const handleOnURL = () => window.open(selectedPub?.pubURL);
    
    const renderKeyDetails = selectedPub && Object.entries(selectedPub.keyDetails as MidasCampaignKeyDetails).map(([key, value]) => {
        if (key.toLowerCase().includes('data')) {
            return <span key={key} style={{ marginRight: Spacing._12 }}><strong>{lowerCase(key)}</strong>: {dayjs(value).format('M/D/YYYY')}</span>;
        }
        
        return (
            <span key={key} style={{ marginRight: Spacing._12 }}><strong>{lowerCase(key)}</strong>: {value}</span>
        )
    });
    
    return (
        <>
        <StyledDashboardDetails.Container>
            <CampaignTitle />
            <StyledDashboardDetails.NavigationWrapper>
                <FPButton
                    fontSize={14}
                    buttonType="link"
                    icon={<LeftOutlined />}
                    label="return to the dashboard"
                    onClick={() => navigate(-1)}
                />
            </StyledDashboardDetails.NavigationWrapper>
            <StyledDashboardDetails.Wrapper>
                <StyledDashboardDetails.HeaderWrapper>
                    <StyledDashboardDetails.HeaderTopWrapper>
                        <StyledDashboardDetails.HeaderTopTitle>
                            {contentTypesDetails.title}
                        </StyledDashboardDetails.HeaderTopTitle>
                        <StyledDashboardDetails.HeaderIndicators>
                            <StyledDashboardTile.Indicator>{contentTypesDetails.value}</StyledDashboardTile.Indicator>
                            <StyledDashboardTile.IndicatorSub>
                                ±{contentTypesDetails.valueError}
                            </StyledDashboardTile.IndicatorSub>
                        </StyledDashboardDetails.HeaderIndicators>
                    </StyledDashboardDetails.HeaderTopWrapper>
                    <StyledDashboardDetails.HeaderBottomWrapper>
                        (click data point to see source article details)
                    </StyledDashboardDetails.HeaderBottomWrapper>
                </StyledDashboardDetails.HeaderWrapper>
                {<StyledDashboardDetails.GraphWrapper>
                    <StyledDashboardDetails.LegendWrapper>
                        <StyledDashboardDetails.LegendItemWrapper>
                            <StyledDashboardDetails.LegendItemEndorsed />
                            Endorsed
                        </StyledDashboardDetails.LegendItemWrapper>
                        <StyledDashboardDetails.LegendItemWrapper>
                            <StyledDashboardDetails.LegendItemInReview />
                            In review
                        </StyledDashboardDetails.LegendItemWrapper>
                    </StyledDashboardDetails.LegendWrapper>
                    <ResponsiveSwarmPlot
                        data={graphData}
                        groups={['US', 'Europe', 'China', 'South America', 'Russia']}
                        value="value"
                        valueScale={{type: 'linear', min: 0, reverse: false}}
                        size={{key: 'volume', values: [4, 20], sizes: [6, 20]}}
                        forceStrength={4}
                        // @ts-ignore
                        renderNode={props => {
                            const active = props.node.data.title === selectedPub?.title;
                            const fill = props.node.data.isEndorsed ? Color.flashpub : '#fff';
                            return (
                                <circle
                                    stroke={props.borderColor}
                                    strokeWidth={active ? 2 : 1}
                                    strokeOpacity={active ? .8 : 1}
                                    r={props.size / (active ? 1.2 : 2)}
                                    fill={active ? Color.community_t(.4) : fill}
                                    onClick={() => onNodeClick(active ? null : props.node.data)}
                                    transform={`translate(${props.x},${props.y}) scale(${props.scale})`}
                                />
                            );
                        }}
                        simulationIterations={100}
                        borderWidth={1}
                        borderColor={Color.flashpub}
                        margin={{top: 80, right: 100, bottom: 80, left: 100}}
                        axisBottom={{
                            orient: 'bottom',
                            tickSize: 10,
                            tickPadding: 5,
                            tickRotation: 0,
                            legendPosition: 'middle',
                            legendOffset: 46,
                        }}
                        axisLeft={{
                            orient: 'left',
                            tickSize: 25,
                            tickPadding: 5,
                            tickRotation: 0,
                            legend: 'IFR (deaths/cases)',
                            legendPosition: 'middle',
                            legendOffset: -76
                        }}
                        axisTop={null}
                        axisRight={null}
                        motionStiffness={50}
                        motionDamping={10}
                        enableGridY={false}
                    />
                </StyledDashboardDetails.GraphWrapper>}
            </StyledDashboardDetails.Wrapper>
            {selectedPub && <StyledDashboardDetails.PubDetailsWrapper>
                {!selectedPub.isEndorsed && (
                    <StyledPubRecordTag>IN REVIEW</StyledPubRecordTag>
                )}
              <CloseOutlined
                onClick={() => onNodeClick(null)}
                style={{ position: 'absolute', top: 2, left: 2, fontSize: 14 }} />
              <StyledDashboardDetails.Date>
                  {dayjs(selectedPub.publishedDate).format('M/D/YYYY')}
              </StyledDashboardDetails.Date>
              <StyledDashboardDetails.PubDetailsInfoWrapper>
                <StyledDashboardDetails.PubDetailsTitle>
                    {selectedPub.title}
                </StyledDashboardDetails.PubDetailsTitle>
                <StyledDashboardDetails.PubKeyDetails>
                    {renderKeyDetails}
                </StyledDashboardDetails.PubKeyDetails>
                <StyledDashboardDetails.PubDetailsURL onClick={handleOnURL}>
                  {!selectedPub.isEndorsed ? 'Review the publication now' : 'Go to the publication now'}
                </StyledDashboardDetails.PubDetailsURL>
              </StyledDashboardDetails.PubDetailsInfoWrapper>
              <StyledDashboardDetails.PubDetailsDataWrapper>
                <StyledDashboardDetails.DataTitle>{contentTypesDetails.title}</StyledDashboardDetails.DataTitle>
                <StyledDashboardTile.IndicatorWrapper style={{ minWidth: 130 }}>
                  <StyledDashboardTile.Indicator>{selectedPub.value}</StyledDashboardTile.Indicator>
                  <StyledDashboardTile.IndicatorSub>±{selectedPub.valueError}</StyledDashboardTile.IndicatorSub>
                </StyledDashboardTile.IndicatorWrapper>
              </StyledDashboardDetails.PubDetailsDataWrapper>
            </StyledDashboardDetails.PubDetailsWrapper>}
        </StyledDashboardDetails.Container>
        <StyledDashboard.PartnershipWrapper>
            {/*in partnership with*/}
            {/*<StyledDashboard.PartnershipLogo src="https://firebasestorage.googleapis.com/v0/b/flashpub-staging-489e3.appspot.com/o/partnerships%2Fmidas_logo.jpg?alt=media&token=41a5ce51-9516-43a8-8f78-f1efbed8d465" />*/}
        </StyledDashboard.PartnershipWrapper>
        </>
    );
};
