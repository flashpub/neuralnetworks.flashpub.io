import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router-dom';

import { RootState } from 'app/store';
import { ReduxProps } from 'app/types';
import { PortalView } from 'app/utils';
import * as AppAction from 'app/actions';

import { LeadersList } from 'feed/components';
import { PubsList } from 'app/components/Shared'

import { DictionariesContainer } from 'dictionaries/components';

import * as PublicationAction from 'publication/actions';
import * as PublicationAsyncAction from 'publication/async-actions';
import Campaigns from '../../campaigns/components/Campaigns';


const mapDispatch = {
    setPortalView: AppAction.SET_PORTAL_VIEW.use,
    setPickedPub: PublicationAction.SET_PICKED_PUBLICATION.use,

    getPublication: PublicationAsyncAction.get,
};
const mapState = (state: RootState) => ({
    pubs: state.feed.pubs,
    claims: state.feed.claims,
    feedStatus: state.feed.status,
});

type Props = ReduxProps<typeof mapState, typeof mapDispatch> & RouteComponentProps;

class FeedContainer extends React.Component<Props> {
    handleSetPickedPub = (id: string) => {
        this.props.getPublication(id);
        this.props.setPickedPub({ id });
    };
    handleSetPortalView = (view: PortalView) => this.props.setPortalView({ view });

    render() {
        return (
            <>
                <DictionariesContainer />
                <Campaigns />
                <LeadersList
                    pubs={this.props.pubs}
                    feedStatus={this.props.feedStatus}
                />
                <PubsList
                    type="top"
                    pubs={this.props.pubs}
                    claims={this.props.claims}
                    feedStatus={this.props.feedStatus}
                    setPickedPub={this.handleSetPickedPub}
                    setPortalView={this.handleSetPortalView}
                />
                <PubsList
                    type="recent"
                    pubs={this.props.pubs}
                    claims={this.props.claims}
                    feedStatus={this.props.feedStatus}
                    setPickedPub={this.handleSetPickedPub}
                    setPortalView={this.handleSetPortalView}
                />
            </>
        );
    }
}

// @ts-ignore
export default connect(mapState, mapDispatch)(withRouter(FeedContainer));
