import { Dispatch } from 'redux';

import * as API from 'api';
import { AppThunk } from 'store';
import { firebase } from 'configs/firebase';
import * as Campaigns from 'containers/Campaigns';
import { project } from 'configs';

type Snapshot = firebase.firestore.QueryDocumentSnapshot<firebase.firestore.DocumentData>;

export function getCampaigns(): AppThunk {
    return async (dispatch: Dispatch) => {
        dispatch(Campaigns.status.loading());
        try {
            const list: CampaignDefinition[] = [];
            const snapshot = await API.campaigns.load(project.community.id);
            snapshot.forEach((snap: Snapshot) => list.push(snap.data() as CampaignDefinition));
            dispatch(Campaigns.populate(list));
            dispatch(Campaigns.status.success());
        } catch (err) {
            dispatch(Campaigns.status.failure(err.message));
        }
    };
}


export function getCampaignData(campaign: string): AppThunk {
    return async (dispatch: Dispatch) => {
        dispatch(Campaigns.status.loading());
        try {
            const list: CampaignData[] = [];
            const snapshot = await API.campaigns.data(campaign);
            snapshot.forEach((snap: Snapshot) => list.push(snap.data() as CampaignData));
            dispatch(Campaigns.list(list));
            dispatch(Campaigns.status.success());
        } catch (err) {
            dispatch(Campaigns.status.failure(err.message));
        }
    };
}

export function getCampaignDataPerPubCount(campaign: string, pubCount: number): AppThunk {
    return async (dispatch: Dispatch) => {
        dispatch(Campaigns.status.loading());
        try {
            const list: CampaignData[] = [];
            const snapshot = await API.campaigns.dataPerPubCount(campaign, pubCount);
            snapshot.forEach((snap: Snapshot) => list.push(snap.data() as CampaignData));
            dispatch(Campaigns.list(list));
            dispatch(Campaigns.status.success());
        } catch (err) {
            dispatch(Campaigns.status.failure(err.message));
        }
    };
}

export function getCampaignDataPerCaseCount(campaign: string, caseCount: number): AppThunk {
    return async (dispatch: Dispatch) => {
        dispatch(Campaigns.status.loading());
        try {
            const list: CampaignData[] = [];
            const snapshot = await API.campaigns.dataPerCaseCount(campaign, caseCount);
            snapshot.forEach((snap: Snapshot) => list.push(snap.data() as CampaignData));
            dispatch(Campaigns.list(list));
            dispatch(Campaigns.status.success());
        } catch (err) {
            dispatch(Campaigns.status.failure(err.message));
        }
    };
}