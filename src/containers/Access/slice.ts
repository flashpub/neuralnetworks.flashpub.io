import { createGenericSlice } from 'helpers';

const initialState = {
    data: {
        step: 'LOBBY',
        isOpen: false,
    },
    status: { type: 'PENDING', message: null },
};

export const slice = createGenericSlice({
    name: 'access',
    initialState: initialState as GenericState<AccessContainerState>,
    reducers: {
        set: (state, action) => {
            state.data.isOpen = action.payload;
        },
        step: (state, action) => {
            state.data.step = action.payload;
        },
    },
});

export const { set, step, ...status } = slice.actions;

export default slice.reducer;
