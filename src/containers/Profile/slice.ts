import { createGenericSlice } from 'helpers';

const initialState = {
    data: {
        user: null,
    },
    status: { type: 'PENDING', message: null },
};

export const slice = createGenericSlice({
    name: 'profile',
    initialState: initialState as GenericState<ProfileContainerState>,
    reducers: {
        populate: (state, action) => {
            state.data.user = action.payload;
        },
        update: (state: any, action) => {
            const { form, field, value } = action.payload;
            const split = field.split('.');
            const depth = split.length;

            switch (depth) {
                case 1:
                    state.data[form][split] = value;
                    break;
                case 2:
                    state.data[form][split[0]][split[1]] = value;
                    break;
                case 3:
                    state.data[form][split[0]][split[1]][split[2]] = value;
                    break;
                case 4:
                    state.data[form][split[0]][split[1]][split[2]][split[3]] = value;
                    break;
                default:
                    throw new Error("Field value doesn't meet the requirements");
            }
        },
    },
});

export const { populate, update, ...status } = slice.actions;

export default slice.reducer;
