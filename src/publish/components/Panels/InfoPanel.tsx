import * as React from 'react';
import styled from 'styled-components';
import { useSelector } from 'react-redux';


import { Content                        } from 'app/types';
import { RootState                      } from 'app/store';
import { project                        } from 'app/config';
import { Color, Spacing, Font           } from 'app/assets';
import { GenericPub, ClinicalCaseReport } from 'app/assets/graphics';
import { Button, Title  } from 'publish/components/Elements';
import { PublishAction, usePublishDispatch } from 'publish';
import { FUIPanel } from 'FUI/components';


export const InfoPanel: React.FC = () => {
    const dispatch = usePublishDispatch();
    const content = useSelector<RootState, Content | null>(state => state.publish.contentType);

    if (content) {
        const toNextStep = () => dispatch(PublishAction.SetPublishPanel({
            order: 1,
            title: 'Claim',
        }));

        return (
            <FUIPanel
                title=""
                header={false}
            >
                <Title>{content.info.title}</Title>
                <DiagramBox>
                    {project.name === 'clinic' ? <ClinicalCaseReportPic /> : <GenericPubPic />}
                </DiagramBox>
                <Description>{content.info.description}</Description>
                <Button type="button" text="Next Step" buttonType="text" onClick={toNextStep}/>
            </FUIPanel>
        );
    }
    return null;
};


const DiagramBox = styled.div`
    margin-bottom: ${Spacing.XXXL};
`;
const GenericPubPic = styled(GenericPub)`
    width: 100%;
    height: 250px;
`;
const ClinicalCaseReportPic = styled(ClinicalCaseReport)`
    width: 100%;
    height: 250px;
`;
const Description = styled.p`
    font-size: 20px;
    font-weight: 200;
    text-align: center;
    color: ${Color.grey};
    font-family: ${Font.text};
    margin-bottom: ${Spacing.XXXL};
`;
