import * as React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { RouteComponentProps } from 'react-router-dom'

import store, { RootState } from 'app/store';
import { AlertType, ReduxProps } from 'app/types';
import * as AppAction from 'app/actions';
import { PortalView, sleep, Status } from 'app/utils';

import { AccessStep } from 'access/utils';
import * as AccessAction from 'access/actions';
import * as AccessAsyncAction from 'access/async-actions';

import MenuDesktop from './MenuDesktop';
import MenuResponsive from './MenuResponsive';


const mapDispatch = {
    logoutUser: AccessAsyncAction.logoutUser,
    setAppStatus: AppAction.SET_APP_STATUS.use,
    setPortalView: AppAction.SET_PORTAL_VIEW.use,
    setAccessStep: AccessAction.SET_ACCESS_STEP.use,
};

const mapState = (state: RootState) => ({
    user: state.access.user,
});

type MenuContainerProps = {
    isMobile: boolean,
    logoutUser(): void,
    handleUserMenu(state: boolean): void,
} & any

type Props = ReduxProps<typeof mapState, typeof mapDispatch> & MenuContainerProps & RouteComponentProps & any;

class MenuContainer extends React.Component<Props> {
    goToLibrary = () => {
        this.props.handleUserMenu(false);
        this.props.setPortalView({ view: PortalView.LIBRARY });
        this.props.history.push('/library');
    };

    goToDrafts = () => {
        console.log('nav to drafts');
        store.dispatch(AppAction.ADD_ALERT.use({
            type: AlertType.INFO,
            message: 'This feature will be available soon',
        }));
        this.props.handleUserMenu(false);
    };

    goToHelp = () => {
        const email = 'mailto:hello@flashpub.io?Subject=Help';
        window.open(email);
        this.props.handleUserMenu(false);
    };

    goToSettings = () => {
        console.log('nav to settings');
        store.dispatch(AppAction.ADD_ALERT.use({
            type: AlertType.INFO,
            message: 'This feature will be available soon',
        }));
        this.props.handleUserMenu(false);
    };

    goToOrcid = () => {
        console.log('nav to orcid');
        this.props.handleUserMenu(false);
    };

    logout = () => {
        this.props.setAppStatus({ status: Status.LOADING });
        this.props.handleUserMenu(false);
        this.props.logoutUser();
        sleep(500).then(() => this.props.setAppStatus({ status: Status.SUCCESS }));
    };

    handleOpenOrcidConnection = () => {
        this.props.setPortalView({ view: PortalView.ACCESS });
        this.props.setAccessStep({ step: AccessStep.ORCID });
        this.props.handleUserMenu(false);
    };

    menuList: { title: string, func: () => void }[] = [
        { title: 'library', func: this.goToLibrary },
        { title: 'drafts', func: this.goToDrafts },
        { title: 'settings', func: this.goToSettings },
        { title: 'help', func: this.goToHelp },
        { title: 'logout', func: this.logout },
    ];

    render() {
        return this.props.isMobile
            ? <MenuResponsive
                user={this.props.user}
                goToOrcid={this.goToOrcid}
                menuList={this.menuList}
                handleUserMenu={this.props.handleUserMenu}
                openOrcidConnection={this.handleOpenOrcidConnection}
            />
            : <MenuDesktop
                user={this.props.user}
                menuList={this.menuList}
                goToOrcid={this.goToOrcid}
                handleUserMenu={this.props.handleUserMenu}
                openOrcidConnection={this.handleOpenOrcidConnection}
            />
    }
}

// @ts-ignore
export default withRouter(connect(mapState, mapDispatch)(MenuContainer));
