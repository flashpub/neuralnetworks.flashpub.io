import { Col, Row } from 'antd';
import { Link } from '@reach/router';
import styled, { css } from 'styled-components';
import { SlackOutlined, MediumOutlined, TwitterOutlined } from '@ant-design/icons';

import { Color, Font, MaxWidth, Media, Radius, Spacing, TABLET } from 'assets/styles';
import { StyledFlexWrapper } from 'ui/elements/styles';

export const StyledFooterWrapper = styled(StyledFlexWrapper)`
    width: 100%;
    color: #fff;
    z-index: 101;
    font-weight: 300;
    position: relative;
    height: fit-content;
    font-family: ${Font.text};
    background-color: ${Color.community};
    padding: ${Spacing._24} ${Spacing._12} 0;
    
    ${Media.tablet} {
        z-index: unset;
    }
`;
export const StyledFooterInternalWrapper = styled(Row)`
    width: 100%;
    flex-direction: row;
    max-width: ${MaxWidth};
`;
export const StyledFooterSectionWrapper = styled(Col)`
    width: fit-content;
    position: relative;
    text-align: center;
    align-items: center;
    flex-direction: column;
    justify-content: center;
    margin-bottom: ${Spacing._24};

    ${Media.desktop} {
        margin-bottom: 0;
        text-align: left;
        align-items: flex-start;
    }
`;
export const StyledFooterBrandTitle = styled.div`
    font-size: 20px;
    line-height: 10px;
    margin-top: ${Spacing._12};
    font-family: ${Font.header};
`;
export const StyledFooterBrandUrl = styled.a`
    color: #fff;
    opacity: 0.5;
    font-size: 12px;
    font-weight: 100;
    font-family: ${Font.text};
    :hover {
        opacity: 1;
        color: #fff;
    }
`;
export const StyledFooterISSN = styled.p`
    font-size: 12px;
    font-weight: 300;
    font-family: ${Font.text};
    margin: ${Spacing._8} 0 0;
`;
export const StyledFooterSectionTitle = styled.div`
    font-size: 18px;
    font-weight: 400;
    font-family: ${Font.text};
    margin-bottom: ${Spacing._12};
`;
export const StyledFooterListItem = styled(Link)`
    color: #fff;
    display: block;
    font-weight: 100;
    margin: ${Spacing._8} 0;
    font-family: ${Font.text};
`;
export const StyledFooterListItemLink = styled.a`
    color: #fff;
    display: block;
    font-weight: 100;
    line-height: 1.2;
    margin: ${Spacing._8} 0;
    font-family: ${Font.text};
`;
export const StyledFooterSocialWrapper = styled(StyledFlexWrapper)`
    align-items: center;
    flex-direction: row;
    justify-content: center;
    ${Media.desktop} {
        align-items: flex-end;
        justify-content: flex-start;
    }
`;
export const StyledSocialMediaIcon = css`
    width: 40px;
    height: 40px;
    display: flex;
    font-size: 34px;
    align-items: center;
    background-color: #fff;
    justify-content: center;
    border-radius: ${Radius};
    color: ${Color.community};
    margin-right: ${Spacing._12};
`;
export const StyledSlackIcon = styled(SlackOutlined)`
    ${StyledSocialMediaIcon};
`;
export const StyledMediumIcon = styled(MediumOutlined)`
    ${StyledSocialMediaIcon};
`;
export const StyledTwitterIcon = styled(TwitterOutlined)`
    ${StyledSocialMediaIcon};
`;
export const StyledImprintWrapper = styled(StyledFooterInternalWrapper)`
    height: 80px;
    text-align: center;
    align-items: center;
    justify-content: center;
    ${Media.tablet} {
        height: 60px;
    }
`;
export const StyledLeftSide = styled(Col)`
    display: flex;
    align-items: center;
    justify-content: center;
`;
export const StyledRightSide = styled(Col)`
    display: flex;
    align-items: center;
    justify-content: center;
    span:last-child {
        a {
            color: #fff;
            font-size: 20px;
            font-family: ${Font.header};
            margin-right: ${Spacing._12};
        }
    }
    span:first-child {
        font-size: 8px;
        font-family: ${Font.text};
        margin-right: ${Spacing._12};
        padding-top: ${Spacing._x(6)};
    }
`;
export const StyledTNC = styled(Link)`
    color: #fff;
    font-weight: 100;
    font-family: ${Font.text};
    margin-right: ${Spacing._24};
`;
