import * as React from 'react';
import { find, kebabCase } from 'lodash';
import { useLocation, useNavigate } from '@reach/router';

import { StyledDashboardTile } from 'ui/Campaigns';
import { useTypedSelector } from 'helpers';

interface Props {
    tileData: MidasCampaignData;
}

export const DashboardTile: React.FC<Props> = ({ tileData }) => {
    const navigate = useNavigate();
    const location = useLocation();
    
    const selectedCampaign = useTypedSelector((state) => state.campaigns.data.selected);
    const campaignData: CampaignData[] = useTypedSelector((state) => state.campaigns.data.list);
    
    const comingSoon = tileData.pubs ? tileData.pubs.length : 0;
    
    const handleOnTileClick = () => {
        const contentType = Object.entries(selectedCampaign?.contentTypes as ContentTypes).map(([key, value]) => {
            if (key === tileData.title) {
                return value;
            } else return null;
        })[0];
        const campaignDoc = find(campaignData, (value) => value.title === tileData.title && value);
        
        navigate(`${location.pathname}/${kebabCase(contentType?.contentTypeLabel)}`, { state: campaignDoc as any });
    };
    
  return (
     <StyledDashboardTile.Container>
         <StyledDashboardTile.Wrapper disabled={comingSoon === 0}>
             <StyledDashboardTile.DataWrapper onClick={handleOnTileClick}>
                 <StyledDashboardTile.Title>{tileData.title}</StyledDashboardTile.Title>
                 {tileData.pubCount > 0 && <>
                    <StyledDashboardTile.SubTitle>{tileData.subtitle}</StyledDashboardTile.SubTitle>
                     <StyledDashboardTile.IndicatorWrapper>
                         <StyledDashboardTile.Indicator>{tileData.value}</StyledDashboardTile.Indicator>
                         <StyledDashboardTile.IndicatorSub>±{tileData.valueError}</StyledDashboardTile.IndicatorSub>
                     </StyledDashboardTile.IndicatorWrapper>
                     <StyledDashboardTile.FooterWrapper>
                     <div><strong>{tileData.pubCount}</strong> {tileData.pubCount === 1 ? 'publication' : 'publications'}</div>
                     </StyledDashboardTile.FooterWrapper>
                 </>}
             </StyledDashboardTile.DataWrapper>
             {comingSoon === 0 &&
                 <StyledDashboardTile.SoonOverlay>
                   <StyledDashboardTile.SoonOverlayText>coming soon...</StyledDashboardTile.SoonOverlayText>
                 </StyledDashboardTile.SoonOverlay>
             }
         </StyledDashboardTile.Wrapper>
     </StyledDashboardTile.Container>
  );
};
