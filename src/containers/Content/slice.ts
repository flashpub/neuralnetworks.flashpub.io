import { createGenericSlice } from 'helpers';

const initialState = {
    data: {
        type: null,
        source: null,
    },
    status: { type: 'PENDING', message: null },
};

export const slice = createGenericSlice({
    name: 'content',
    initialState: initialState as GenericState<ContentContainerState>,
    reducers: {
        populate: (state, action) => {
            state.data.type = action.payload;
        },
        source: (state, action) => {
            state.data.source = action.payload;
        },
    },
});

export const { populate, source, ...status } = slice.actions;

export default slice.reducer;
