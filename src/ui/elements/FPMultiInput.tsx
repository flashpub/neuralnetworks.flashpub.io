import { Select } from 'antd';
import * as React from 'react';
import styled from 'styled-components';

import { Color, Font, Radius, Spacing } from 'assets/styles';

interface Props {
    placeholder: string;
    onInputChange(value: any): void;
}

export const FPMultiInput: React.FC<Props> = ({ onInputChange, placeholder }) => (
    <StyledMultiInput
        mode="tags"
        placeholder={placeholder}
        onChange={onInputChange}
        dropdownStyle={{ display: 'none' }}
    />
);

const StyledMultiInput = styled(Select)`
    width: 100%;
    border-radius: ${Radius};
    border: 1px solid ${Color.midgrey};
    
    .ant-select-selector {
        border: 0;
        text-align: center;
        border-radius: ${Radius};
        padding: 0 ${Spacing._4};
        background-color: ${Color.notwhite};
    }
    .ant-select-selection-search {
        width: 0;
        height: 32px;
    }
    .ant-select-selection-placeholder {
        font-style: italic;
        font-family: ${Font.text};
    }
    .ant-select-selection-search-input {
        margin: 0;
        padding: 0;
        font-family: ${Font.text};
        padding-bottom: ${Spacing._x(2)};
    }
    .ant-select-selection-item {
        font-family: ${Font.text};
    }
`;
