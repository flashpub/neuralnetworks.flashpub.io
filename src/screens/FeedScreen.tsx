import * as React from 'react';
import { RouteComponentProps } from '@reach/router';

import { useTypedSelector, useWindowWidth } from 'helpers';
import { FeedContainer } from 'containers/Feed';
import { StyledSpacer } from 'ui/elements/styles';
import { Section } from 'ui/Feed';
import { StyledFeedContent, StyledFeedWrapper, StyledTopCampaignWrapper, StyledWrapper } from 'ui/Feed/styles';
import { DictionaryBar } from 'ui/Feed/DictionaryBar';
import { Viewport } from 'assets/styles/responsive';
import * as CampaignsState from 'containers/Campaigns';
import { useDispatch } from 'react-redux';

export const FeedScreen: React.FC<RouteComponentProps> = () => {
    const dispatch = useDispatch();
    
    const isTablet = useWindowWidth() <= Viewport.DESKTOP;
    const campaigns = useTypedSelector((state) => state.campaigns.data.campaigns);
    
    React.useEffect(() => {
        dispatch(CampaignsState.list([]));
    }, [dispatch]);
    
    return (
        <FeedContainer>
            <StyledWrapper>
                <StyledFeedWrapper>
                    <DictionaryBar />
                    <StyledSpacer height={20} />
                    <StyledFeedContent>
                        {isTablet && campaigns.length > 0 && <Section type="campaigns" />}
                        <StyledTopCampaignWrapper>
                            <Section type="top" />
                            {!isTablet && campaigns.length > 0 && <Section type="campaigns" />}
                        </StyledTopCampaignWrapper>
                        <StyledSpacer height={20} />
                        <Section type="leaders" />
                        <StyledSpacer height={20} />
                        <Section type="recent" />
                    </StyledFeedContent>
                    <StyledSpacer height={40} />
                </StyledFeedWrapper>
            </StyledWrapper>
        </FeedContainer>
    );
};
