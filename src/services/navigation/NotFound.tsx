import { Result } from 'antd';
import * as React from 'react';
import { RouteComponentProps } from '@reach/router';

import { useTypedSelector } from 'helpers';
import { StyledFlexWrapper } from 'ui/elements/styles';
import { GlobalLoader } from 'ui/Overlays';

export const NotFound: React.FC<RouteComponentProps> = () => {
    const isAuth = useTypedSelector((state) => state.auth.data);
    const authStatus = useTypedSelector((state) => state.auth.status.type);

    const isLoading = authStatus === 'LOADING';
    const isFailure = authStatus === 'FAILURE';

    const renderScreen = () => {
        if (isLoading) return <GlobalLoader loading={isLoading} />;
        if (isFailure) {
            return <Result status="500" title="500" subTitle="Sorry, something went wrong." />;
        }
        if (!isAuth) {
            return <Result status="403" title="403" subTitle="Sorry, you are not authorized to access this page." />;
        }
        return <Result status="404" title="404" subTitle="Sorry, the page you visited does not exist." />;
    };

    return <StyledFlexWrapper style={{ minHeight: 700 }}>{renderScreen()}</StyledFlexWrapper>;
};
