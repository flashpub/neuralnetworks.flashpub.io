import * as React from 'react';


const usePrevious = (value: any) => {
    const ref = React.useRef();
    React.useEffect(() => {
        if (!ref.current) {
            ref.current = value;
        }

        return () => { ref.current = undefined }
    }, [value]);

    return ref.current;
};

export function useCompareStates<T>(value: T): {
    previousState: T | undefined,
    isStateChanged: boolean,
} {
    const previousState = usePrevious(value);
    const isStateChanged = previousState !== value;
    return {
        previousState,
        isStateChanged,
    }
}
