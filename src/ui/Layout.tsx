import React from 'react';

import { Header } from 'ui/Header';
import { Contents } from 'ui/Contents';
import { Footer } from 'ui/Footer';
import { Overlays } from 'ui/Overlays';
import { StyledSpacer } from 'ui/elements/styles';
import { useTypedSelector } from 'helpers';

export const Layout: React.FC = () => {
    const editMode = useTypedSelector((state) => state.pub.data.edit);
    
    return (
        <>
            <Header />
            <StyledSpacer height={20} />
            <Contents />
            {!editMode && <Footer/>}
            
            <Overlays />
        </>
    );
}
