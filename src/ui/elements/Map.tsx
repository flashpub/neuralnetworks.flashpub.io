import L from 'leaflet';
import dayjs from 'dayjs';
import * as React from 'react';
import styled from 'styled-components/macro';
import { RouteComponentProps, withRouter } from 'react-router';
import { Map as LeafMap, Marker, Popup, TileLayer } from 'react-leaflet';

import { Spacing, Font, Color } from '../../assets';
import { Position, Tooltip } from '@blueprintjs/core';
import { GlobalStyle } from '../../assets/global';

type Props = {
    markers: {
        id: string;
        city: string;
        dataDate: number;
        hospitalizationCount: number;
        latitude: number;
        longitude: number;
        patientCountIncreasing: boolean;
        puburl: string;
        tippingPointDate: number;
        tippingPointDaysFromNow: number;
        hospitalCapacity: number;
    }[],
    updatedOn: number;
    pubs: number;
} & RouteComponentProps;


const Map: React.FC<Props> = ({ markers, updatedOn, pubs }) => {
    const [activeMarker, setActiveMarker] = React.useState<string>('');
    const [mapMode, setMapMode] = React.useState(false);

    React.useEffect(() => {
        const grabContent = document.querySelector('#content');
        grabContent && grabContent.classList.add('content-main');
        setMapMode(true);

        return () => {
            setMapMode(false);
            grabContent && grabContent.classList.remove('content-main');
        }
    });

    const renderMarkers = markers.map(m => {
        const count = m.hospitalizationCount;
        return (
            <Marker
                key={m.id}
                icon={FUIIcon(m, activeMarker)}
                position={[m.latitude, m.longitude]}
            >
                <StyledPopup
                    placement={popupPlacement(count)}
                    onOpen={() => setActiveMarker(m.id)}
                    onClose={() => setActiveMarker('')}
                >
                    <div onClick={() => window.location.assign(m.puburl)} style={{ cursor: 'pointer' }}>
                        <Update>updated at: {dayjs(m.dataDate).format('D/MM/YY')}</Update>
                        <Content>
                            <City>{m.city}</City>
                            <Data>Hospitalizations: <strong>{m.hospitalizationCount}</strong></Data>
                            {m.hasOwnProperty('hospitalCapacity') && <Data>Capacity: <strong>{m.hospitalCapacity}</strong> beds</Data>}
                            <Data>
                                Predicted date of reaching <br/> hospital capacity:<br/>
                                <strong id="tip">{dayjs(m.tippingPointDate).format('D/MM/YY')}</strong>
                            </Data>
                            <Tooltip position={Position.RIGHT} content="Number of connected pubs">
                                <Data>({pubs})</Data>
                            </Tooltip>
                        </Content>
                    </div>
                </StyledPopup>
            </Marker>
        );
    });

    return (
        <MapWrapper>
            <GlobalStyle mapMode={mapMode} />
            <StyledLeafMap center={[38.50, -100.00]} zoom={5} active={activeMarker}>
                <UpdatedOn>last update: {dayjs(updatedOn).format('YYYY-MM-DD h:mm:ssa ')}</UpdatedOn>
                <TileLayer
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
                />
                {renderMarkers}
            </StyledLeafMap>
        </MapWrapper>
    );
};

const StyledPopup = styled(Popup)<{ placement: number}>` && {
    font-family: ${Font.text};
    bottom: ${({ placement }) => `${placement}px !important`};
    .leaflet-popup-content-wrapper {
        padding: 0;
        border-radius: 10px;
    }
    .leaflet-popup-content {
        margin: 0;
        display: flex;
        align-items: center;
        flex-direction: column;
    }
}`;
const Content = styled.div`
    display: flex;
    align-items: center;
    flex-direction: column;
    padding: 0 ${Spacing.S};
`;
const City = styled.div`
    font-size: 16px;
    text-align: center;
    color: ${Color.grey};
    margin: ${Spacing.ML} 0;
`;
const Update = styled.div`
    font-size: 10px;
    width: 100%;
    text-align: center;
    color: ${Color.grey};
    font-family: ${Font.text};
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
    padding: ${Spacing.S} ${Spacing.M};
    background-color: ${Color.lightgrey};
`;
const Data = styled.div`
    font-size: 12px;
    text-align: center;
    color: ${Color.grey};
    margin-bottom: ${Spacing.M};
    strong {
        color: ${Color.error};
    }
    strong#tip {
        font-size: 20px;
        margin-top: ${Spacing.M};
    }
`;

const MapWrapper = styled.div`
    z-index: 1;
    width: 100%;
    height: 74vh;
    display: flex;
    position: relative;
    margin-top: ${Spacing.XXL};
`;
const StyledLeafMap = styled(LeafMap)<{ active: string }>` && {
    height: 100%;
    width: 100%;
    .leaflet-div-icon {
        border: none;
        background: transparent;
    }
    
    .leaflet-marker-pane { z-index: ${({ active }) => active ? 'unset' : 600 }}
    .active { z-index: 1001 !important }
    .marker-icon-20 { width: 20px !important; height: 20px !important; margin-left: -10px !important; margin-top: -10px !important; }
    .marker-icon-26 { width: 26px !important; height: 26px !important; margin-left: -13px !important; margin-top: -13px !important; }
    .marker-icon-32 { width: 32px !important; height: 32px !important; margin-left: -16px !important; margin-top: -16px !important; }
    .marker-icon-38 { width: 38px !important; height: 38px !important; margin-left: -19px !important; margin-top: -19px !important; }
    .marker-icon-44 { width: 44px !important; height: 44px !important; margin-left: -22px !important; margin-top: -22px !important; }
    .marker-icon-50 { width: 50px !important; height: 50px !important; margin-left: -25px !important; margin-top: -25px !important; }
    .marker-icon-56 { width: 56px !important; height: 56px !important; margin-left: -28px !important; margin-top: -28px !important; }
    .leaflet-popup-close-button { display: none }
}`;
const UpdatedOn = styled.div`
    top: 0;
    right: 0;
    z-index: 1000;
    font-size: 10px;
    position: absolute;
`;


const iconSize = (m: number) => {
    if (m < 30) { return 20 }
    if (m < 70) { return 25 }
    if (m < 100) { return 32 }
    if (m < 300) { return 38 }
    if (m < 500) { return 44 }
    if (m < 1000) { return 50 }
    return 56;
};
const popupPlacement = (m: number) => {
    if (m < 30) { return 5 }
    if (m < 70) { return 8 }
    if (m < 100) { return 11 }
    if (m < 300) { return 14 }
    if (m < 500) { return 17 }
    if (m < 1000) { return 20 }
    return 23;
};

const ArrowIcon = (m: any) => {
    const tip = m.tippingPointDaysFromNow;
    const color = tip > 60 ? 'rgba(103, 172, 91, .7)' : tip > 15 ? 'rgba(246, 194, 68, .7)' : 'rgba(236, 95, 89, .7)';
    const rotate = m.patientCountIncreasing ? 'unset' : 'rotate(180deg)';

    return `<svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" style="fill: ${color}; transform: ${rotate}">
        <defs>
            <style>.cls-1{fill:#fff;}</style>
        </defs>
        <path d="M20,10A10,10,0,1,1,10,0,10,10,0,0,1,20,10Z" />
        <path class="cls-1" d="M14.32,9.27a.73.73,0,0,1-.52.21.76.76,0,0,1-.54-.22L10.75,6.69v8.64a.75.75,0,0,1-1.5,0V6.64L6.65,9.19a.77.77,0,0,1-.53.21.76.76,0,0,1-.54-.22.75.75,0,0,1,0-1.06l3.88-3.8s0,0,.06-.05a1.08,1.08,0,0,1,.18-.11l.17,0,.11,0h0a.71.71,0,0,1,.28.06,1,1,0,0,1,.24.16h0l3.8,3.88A.75.75,0,0,1,14.32,9.27Z"/>
    </svg>`
};

const FUIIcon = (m: any, activeMarker: string) => {
    const size = iconSize(m.hospitalizationCount);
    const active = m.id === activeMarker;
    const className = `marker-icon-${size} ${active && 'active'}`;
    return L.divIcon({
        html: ArrowIcon(m),
        iconAnchor: [0, 0],
        popupAnchor: [0, 0],
        className,
    });
};

export default withRouter(Map);
