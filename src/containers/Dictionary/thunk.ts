import { Dispatch } from 'redux';

import * as API from 'api';
import { AppThunk } from 'store';
import { firebase } from 'configs/firebase';
import * as Dictionary from 'containers/Dictionary';

type Snapshot = firebase.firestore.QueryDocumentSnapshot<firebase.firestore.DocumentData>;

export function getDictionary(): AppThunk {
    return async (dispatch: Dispatch) => {
        dispatch(Dictionary.status.loading());
        try {
            const list: TermDefinition[] = [];
            const snapshot = await API.dictionary.load();
            snapshot.forEach((snap: Snapshot) => list.push(snap.data() as TermDefinition));
            dispatch(Dictionary.populate(list));
            dispatch(Dictionary.status.success());
        } catch (err) {
            dispatch(Dictionary.status.failure(err.message));
        }
    };
}

export function searchDictionary(filter: string, input: string): AppThunk {
    return async (dispatch: Dispatch) => {
        dispatch(Dictionary.status.loading());
        try {
            const list: TermDefinition[] = [];
            const snapshot = await API.dictionary.search(filter, input);
            snapshot.forEach((snap: Snapshot) => list.push(snap.data() as TermDefinition));
            dispatch(Dictionary.search(list));
            dispatch(Dictionary.status.success());
        } catch (err) {
            dispatch(Dictionary.status.failure(err.message));
        }
    };
}
