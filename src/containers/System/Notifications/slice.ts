import { createGenericSlice } from 'helpers';

const initialState = {
    data: {
        update: null,
    },
    status: { type: 'PENDING', message: null },
};

export const slice = createGenericSlice({
    name: 'notifications',
    initialState: initialState as GenericState<NotificationsContainerState>,
    reducers: {
        populate: (state, action) => {
            state.data = action.payload;
        },
    },
});

export const { populate, ...status } = slice.actions;

export default slice.reducer;
