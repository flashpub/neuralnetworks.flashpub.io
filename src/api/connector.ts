import axios from 'axios';
import { envDetector } from 'helpers';

export const connector = axios.create({
    baseURL: envDetector('HOST'),
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
    },
});
