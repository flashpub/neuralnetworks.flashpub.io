import { createGlobalStyle } from 'styled-components';

export const MOBILE = '576px';
export const TABLET = '768px';
export const Radius = '0.33rem';
export const MaxWidth = '1100px';
export const GlobalStyle = createGlobalStyle`
    body, html {
        font-size: 16px;
        min-width: 320px;
        box-sizing: border-box;
    }
    a, a:link, a:visited, a:hover {
        text-decoration: none;
    }
`;
