import * as React from 'react';
import { useDispatch } from 'react-redux';
import { RightCircleOutlined } from '@ant-design/icons';
import { RouteComponentProps, useNavigate, useLocation } from '@reach/router';

import { useTypedSelector, matchStepperToUrl } from 'helpers';
import * as Publish from 'containers/Publish';
import {
    StyledStepTitle,
    StyledStepWrapper,
    StyledDiagramWrapper,
    StyledStepDescription,
    StyledStepNextButton,
} from 'ui/Publish/styles';
import { GenericPub } from 'assets/graphics';

type Props = RouteComponentProps<{ contentType: string }>;

export const Introduction: React.FC<Props> = ({ contentType }) => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const location = useLocation();
    const step = useTypedSelector((state) => state.publish.data.step);
    const selectedContent = useTypedSelector((state) => state.content.data.type);

    React.useEffect(() => {
        const match = matchStepperToUrl(location.pathname.split(`/${contentType}/`)[1]);
        dispatch(Publish.step({ order: match.order, type: match.type }));
    }, [contentType, location.pathname, dispatch]);

    const handleToNextStep = () => {
        dispatch(Publish.step({ order: step.order + 1, type: 'CLAIM' }));
        navigate(`${location.origin}/publish/${contentType}/claim`);
    };

    if (selectedContent) {
        return (
            <StyledStepWrapper>
                <StyledStepTitle>{selectedContent.info.title}</StyledStepTitle>
                <StyledDiagramWrapper>
                    <GenericPub />
                </StyledDiagramWrapper>
                <StyledStepDescription>{selectedContent.info.description}</StyledStepDescription>
                <StyledStepNextButton
                    iconSide="right"
                    label="Next Step"
                    buttonType="link"
                    onClick={handleToNextStep}
                    icon={<RightCircleOutlined />}
                />
            </StyledStepWrapper>
        );
    } else return null;
};
