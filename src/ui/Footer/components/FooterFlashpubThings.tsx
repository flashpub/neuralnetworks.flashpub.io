import * as React from 'react';
import { StyledFooterSectionTitle, StyledFooterListItemLink } from 'ui/Footer/styles';


export const FooterFlashpubThings: React.FC = () => {
    const onSupportScience = () => {
        const email = 'mailto:hello@flashpub.io?Subject=Donate to Outbreak Science Research';
        window.open(email);
    };
    return (
        <>
            <StyledFooterSectionTitle>Flashpub</StyledFooterSectionTitle>
            <StyledFooterListItemLink target="_blank" href="https://www.flashpub.io/about">
                What is a Micropublication?
            </StyledFooterListItemLink>
            <StyledFooterListItemLink onClick={onSupportScience}>Support outbreak science</StyledFooterListItemLink>
        </>
    );
};
