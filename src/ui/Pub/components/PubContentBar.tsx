import * as React from 'react';

import { StyledPubContentBarWrapper } from 'ui/Pub/styles';

type Props = {
    pub: PubDefinition;
};

export const PubContentBar: React.FC<Props> = ({ pub }) => {
    return (
        <StyledPubContentBarWrapper>
            Originally published on {pub.contentOrigin.source}:
            <a href={pub.contentOrigin.url} target="_blank" rel="noopener noreferrer">{pub.contentOrigin.url}</a>
        </StyledPubContentBarWrapper>
    );
};
