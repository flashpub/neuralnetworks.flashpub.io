import * as React from 'react';
import { connect } from 'react-redux';
import includes from 'lodash/includes';
import styled from 'styled-components';
import { RouteComponentProps } from 'react-router'

import { RootState } from 'app/store';
import { PortalView } from 'app/utils';
import { ReduxProps } from 'app/types';
import * as AppAction from 'app/actions';
import { Color, Spacing, Font } from 'app/assets'

import { Library } from 'library/components'

import * as PublicationAction from 'publication/actions';
import * as PublicationAsyncAction from 'publication/async-actions';

import { FUIButton } from 'FUI/elements';

const mapDispatch = {
    setPortalView: AppAction.SET_PORTAL_VIEW.use,
    setPickedPub: PublicationAction.SET_PICKED_PUBLICATION.use,
    getPublication: PublicationAsyncAction.get,
};
const mapState = (state: RootState) => ({
    pubs: state.feed && state.feed.pubs && state.feed.pubs.filter(pub => includes(pub.bookmarkedBy, state.access && state.access.user && state.access.user.uid) ? pub : null),
    claims: state.feed.claims,
    feedStatus: state.feed.status,
    portalView: state.app.view,
    user: state.access.user
});

type Props = ReduxProps<typeof mapState, typeof mapDispatch> & RouteComponentProps;

class LibraryContainer extends React.Component<Props> {
    handleSetPickedPub = (id: string) => {
        this.props.getPublication(id);
        this.props.setPickedPub({ id });
    };

    handleSetPortalView = (view: PortalView) => this.props.setPortalView({ view });

    handleButtonClick = () => {
        this.props.history.push('/');
        this.handleSetPortalView(PortalView.DEFAULT);
    };

    render() {
        return (
            <>
                <LibraryNavigation>
                    <ReturnButton
                        type="button"
                        text="Back to home page"
                        onClick={this.handleButtonClick}
                    />
                </LibraryNavigation>
                <Library
                    type="library"
                    pubs={this.props.pubs}
                    claims={this.props.claims}
                    portalView={this.props.portalView}
                    feedStatus={this.props.feedStatus}
                    setPickedPub={this.handleSetPickedPub}
                    setPortalView={this.handleSetPortalView}
                />
            </>
        );
    }
}

const ReturnButton = styled(FUIButton)`
    font-weight: 300;
    margin-top: ${Spacing.L};
    border: none;
    background-color: unset;
    color: ${Color.midgrey};
    &:hover {
        color: ${Color.grey};
        background-color: unset;
    }
`;

const LibraryNavigation = styled.div`
    position: relative;
    color: ${Color.midgrey};
    margin-bottom: ${Spacing.XL};
    font-family: ${Font.pub_text};
    display: flex;
    justify-content: center;
`;

// @ts-ignore
export default connect(mapState, mapDispatch)(LibraryContainer);
