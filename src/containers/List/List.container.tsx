import * as React from 'react';
import { capitalize } from 'lodash';
import { useDispatch } from 'react-redux';
import { useLocation } from '@reach/router';

import { useTypedSelector } from 'helpers';
import * as List from 'containers/List';

type Props = {
    type: ListType;
};

export const ListContainer: React.FC<Props> = ({ type, children }) => {
    const dispatch = useDispatch();
    const location = useLocation();
    
    const user = useTypedSelector((state) => state.profile.data.user);
    const pubListStatus = useTypedSelector((state) => state.list.status.type);

    // TODO filter drafts per community
    React.useEffect(() => {
        if (user) {
            if (type === 'drafts') {
                dispatch(List.fetchDrafts(user.uid));
            }
            if (type === 'library') {
                dispatch(List.fetchBookmarked(user.uid));
            }
            dispatch(List.title(capitalize(type)));
        }
    }, [dispatch, type, user]);
    
    React.useEffect(() => {
        const parseQuery = (query: string) => {
            return query
                ? (/^[?#]/.test(query) ? query.slice(1) : query)
                    .split('&')
                    .reduce((params: any, param) => {
                            const [key, value] = param.split('=');
                            params[key] = value ? decodeURIComponent(value.replace(/\+/g, ' ')) : '';
                            return params;
                        }, {}
                    )
                : {};
        };
        const queryParams = parseQuery(location.search);
        
        const getListedPubs = async () => {
            const pubIdsList = queryParams.pubids.split(',');
            dispatch(List.fetchListed(pubIdsList));
        };
        
        if (pubListStatus !== 'LOADING' && location.search && location.pathname.includes('/list')) {
            dispatch(List.title(queryParams.title));
            getListedPubs();
        }
    }, [dispatch, location.search]);
    
    return <>{children}</>;
};
