import * as React from 'react';
import { Tooltip } from 'antd';
import styled from 'styled-components';
import { EditOutlined } from '@ant-design/icons';

import { Color } from 'assets/styles';

export const FPEditIcon: React.FC = () => (
    <Tooltip title="you can edit this field" placement="right" mouseEnterDelay={0} mouseLeaveDelay={0}>
        <StyledEditIcon />
    </Tooltip>
);

const StyledEditIcon = styled(EditOutlined)`
    && {
        top: 50%;
        width: 20px;
        right: -24px;
        height: 20px;
        position: absolute;
        border-radius: 100%;
        background-color: #fff;
        transform: translateY(-50%);
        box-shadow: 0px 0px 10px 0px ${Color.community_t75};

        svg {
            width: 16px;
            height: 14px;
            margin-top: 2px;
            fill: ${Color.grey};
        }
    }
`;
