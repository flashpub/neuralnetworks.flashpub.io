import store from 'app/store';
import * as AppAction from 'app/actions';
import api, { API_VERSION } from 'app/api';
import { AlertType } from 'app/types';

import * as CampaignsAction from 'campaigns/actions';

const version = API_VERSION.content;

const alertHandler = (type: AlertType, alert: Error | { message: string }) => {
    store.dispatch(AppAction.ADD_ALERT.use({
        type: type,
        message: alert.message,
    }));
    store.dispatch(CampaignsAction.CAMPAIGN_OPERATION_FAIL.use());
};

export const campaigns = {
    getAll(): Promise<any> {
        return api
            .get(`/content/${version}/campaigns`)
            .then(response => response.data)
            .catch(error => alertHandler(AlertType.ERROR, error.response.data));
    },
    get(id: string): Promise<any> {
        return api
            .get(`/content/${version}/campaigns/${id}`)
            .then(response => response.data)
            .catch(error => alertHandler(AlertType.ERROR, error.response.data));
    },
};
