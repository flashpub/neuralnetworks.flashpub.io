import * as React from 'react';
import { Icon, IconName } from '@blueprintjs/core';
import styled, { keyframes } from 'styled-components';

import { AlertDefinition, AlertType } from 'app/types';
import { Color, Font, Radius, Spacing } from 'app/assets';


type Props = {
    equalWidth: boolean,
    alert: AlertDefinition,
    removeAlert: (id?: string) => void,
    autoRemoveAlert: (id?: string) => void,
}


const Alert: React.FC<Props> = ({ alert, removeAlert, autoRemoveAlert, equalWidth }) => {
    React.useEffect(() => {
        autoRemoveAlert(alert.id);
    });

    if (!alert.message) return null;

    const alertType = (type: string) => {
        switch (type) {
            case AlertType.ERROR:
                return { color: Color.error, icon: 'error' };
            case AlertType.WARNING:
                return { color: Color.warning, icon: 'warning-sign' };
            case AlertType.INFO:
                return { color: Color.info, icon: 'info-sign' };
            default:
                return { color: Color.success, icon: 'tick-circle' };
        }
    };

    return (
        <AlertRecord
            equalWidth={equalWidth}
            onClick={() => removeAlert(alert.id)}
            alertType={alertType(alert.type).color}
        >
            <AlertMessage>{alert.message}</AlertMessage>
            <AlertIcon>
                <Icon icon={alertType(alert.type).icon as IconName} iconSize={34} />
            </AlertIcon>
        </AlertRecord>
    );
};


const slideDown = keyframes`
    0% {
        opacity: 0;
        -webkit-transform: translate3d(0, -100%, 0);
        transform: translate3d(0, -100%, 0);
    }
    100% {
        opacity: 1;
        -webkit-transform: none;
        transform: none;
    }
`;
const AlertRecord = styled.div<{ alertType: string, equalWidth: boolean }>`
    margin: 5px;
    display: flex;
    cursor: pointer;
    min-height: 40px;
    align-items: center;
    white-space: pre-line;
    background-color: #fff;
    padding: ${Spacing.LXL};
    justify-content: center;
    border-radius: ${Radius};
    font-family: ${Font.text};
    transition: width .2s ease;
    animation: ${slideDown} .2s both;
    color: ${props => props.alertType};
    width: ${props => props.equalWidth ? '100%' : 'fit-content'};
    position: relative;
    z-index: 10000;
    
    border-width: 1px;
    border-style: solid;
    border-left-width: 10px;
    border-color: ${props => props.alertType};
    
    @media (max-width: 576px) {
        margin-left: 20px;
        margin-right: 20px;
    }
`;
const AlertMessage = styled.span`
    max-width: 300px;
    padding-right: ${Spacing.L};
`;
const AlertIcon = styled.span`
    padding-left: ${Spacing.S}
`;


export default Alert;
