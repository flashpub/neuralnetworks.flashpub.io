import { Dispatch } from 'redux';

import * as API from 'api';
import { AppThunk } from 'store';
import * as Profile from 'containers/Profile';

export function getUser(uid: string): AppThunk {
    return (dispatch: Dispatch) => {
        dispatch(Profile.status.loading());
        return API.user
            .get(uid)
            .then((res) => {
                dispatch(Profile.populate(res.data()));
                dispatch(Profile.status.success());
            })
            .catch((err) => {
                dispatch(Profile.status.failure(err.message));
            });
    };
}
