import * as React from 'react';
import styled from 'styled-components';
import { useDebounce } from 'react-use';
import { useSelector } from 'react-redux';
import { Icon, Position, Tooltip } from '@blueprintjs/core';

import { Content, PubDefinition } from 'app/types';
import { RootState } from 'app/store';
import { Color, Font, Media, Spacing } from 'app/assets';
import { PublishAction, usePublish } from 'publish';
import { Authors, Carousel, Description, Metadata } from 'publish/components';

import { FUITextAreaRefs, FUITextArea, FUIClaim } from 'FUI/elements';


type Props = {
    pubDesc: string;
    pubTitle: string;
    editMode?: boolean;
    pub: PubDefinition;
    pubClaim: JSX.Element | null;
};


export const PublicationSheet: React.FC<Props> = ({ pubTitle, editMode = false, pubClaim, pubDesc, pub}) => {
    const [, dispatch] = usePublish();
    const [edit, ] = React.useState<boolean>(editMode);
    const [desc, setDesc] = React.useState<string>(pubDesc);
    const [title, setTitle] = React.useState<string>(pubTitle);
    const [isTitleChanged, setIsTitleChanged] = React.useState<boolean>(false);
    const content = useSelector<RootState, Content | null>(state => state.publish.contentType);

    const titleRef = React.useRef<FUITextAreaRefs>(null);

    const [] = useDebounce(
        () => dispatch(PublishAction.SetPublishFormValue({ name: 'title', value: title })),
        500,
        [title]
    );
    const [] = useDebounce(
        () => dispatch(PublishAction.SetPublishFormValue({ name: 'description', value: desc })),
        500,
        [desc]
    );

    const storedTitle = localStorage.getItem('title') as string;
    const description = (content && content.contentTypeLabel) === 'diagnosis'
        ? 'Diagnosis...'
        : 'Write your report and reference figure panels (a, b, c ...) here.';

    React.useEffect(() => {
        if (title !== pubTitle) { setIsTitleChanged(true) }
        if (title === storedTitle) { setIsTitleChanged(false) }
        else { setIsTitleChanged(true) }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [title, pubTitle]);

    const onDescChange = (value: string) => setDesc(value);
    const onTitleChange = (value: string) => setTitle(value);

    return (
        <Form>
            <TitleWrapper>
                <TitleTextArea
                    counter
                    required
                    chars={150}
                    value={title}
                    ref={titleRef}
                    disabled={!edit}
                    onChange={onTitleChange}
                    placeholder="Publication title..."
                />
                {isTitleChanged && <ResetTitle onClick={() => {
                    setTitle(storedTitle);
                    setIsTitleChanged(false);
                    titleRef.current!.setAreaValue(storedTitle);
                }}>
                    <Tooltip content="Reset the title" position={Position.LEFT}>
                        <Icon icon="reset" />
                    </Tooltip>
                </ResetTitle>}
            </TitleWrapper>
            <ClaimWrapper>{pubClaim}</ClaimWrapper>
            <Authors />
            <Carousel edit={edit}/>
            <DescriptionWrapper>
                <Description
                    counter
                    required
                    pub={pub}
                    chars={3000}
                    disabled={!editMode}
                    onChange={onDescChange}
                    placeholder={description}
                />
            </DescriptionWrapper>
            <Metadata />
        </Form>
    );
};


const Form = styled.form`
    margin-bottom: 70px;
    *:focus { outline: none }
`;
const ResetTitle = styled.div`
    top: -2px;
    left: 3px;
    cursor: pointer;
    position: absolute;
    visibility: hidden;
    svg {
        width: 10px;
        height: 10px;
        color: ${Color.grey};
    }
`;
const TitleWrapper = styled.div`
    position: relative;
    height: fit-content;
    
    &:hover ${ResetTitle} { visibility: visible }
`;
const TitleTextArea = styled(FUITextArea)` && {
    font-size: 28px;
    text-align: center;
    color: ${Color.community};
    font-family: ${Font.feed_title};
    ${Media.desktop} { text-align: start }
    &:focus + ${ResetTitle} { visibility: visible; }
}`;
const ClaimWrapper = styled(FUIClaim)` && {
    text-align: center;
}`;
const DescriptionWrapper = styled.div`
    position: relative;
    height: fit-content;
    margin-bottom: ${Spacing.XL};
`;
