import { Row, Col } from 'antd';
import styled from 'styled-components';

import { Color, Font, MOBILE, Spacing, MaxWidth, Radius, Media } from 'assets/styles';
import { StyledFlexWrapper } from 'ui/elements/styles';
import { FPButton } from 'ui/elements';

export const StyledHeaderWrapper = styled(StyledFlexWrapper)`
    width: 100%;
    height: 60px;
    font-size: 20px;
    color: ${Color.community};
    padding: 0 ${Spacing._12};
    font-family: ${Font.header};
    box-shadow: 0px 2px 10px 2px ${Color.community_t15};
    @media (min-width: ${MOBILE}) {
        height: 80px;
        font-size: 24px;
    }
`;
export const StyledHeaderInternalWrapper = styled(Row)`
    width: 100%;
    position: relative;
    align-items: center;
    max-width: ${MaxWidth};
    justify-content: space-between;
    
    ${Media.tablet} {
        justify-content: unset;
    }
`;
export const StyledIconWrapper = styled(Col)`
    font-size: 20px;
    color: ${Color.midgrey};
    font-family: ${Font.text};
    
    div {
      width: fit-content;
    }
`;
export const StyledCommunityNameWrapper = styled.div`
    a {
        color: ${Color.community};
    }
    
    ${Media.tablet} {
        top: 50%;
        left: 50%;
        z-index: 10;
        position: absolute;
        transform: translate(-50%, -50%);
    }
`;
export const StyledHeaderActionsWrapper = styled(Col)`
    display: flex;
    align-items: center;
    justify-content: flex-end;

    div:last-child {
        margin: 0 !important;
    }
`;
export const StyledMenuWrapper = styled(StyledFlexWrapper)`
    width: 100%;
    background-color: #fff;
    padding: ${Spacing._12};
    border-radius: ${Radius};
    font-family: ${Font.text};
    justify-content: flex-start;
    border: 1px solid ${Color.midgrey};
    box-shadow: 0px 0px 10px 5px ${Color.community_t(0.35)};
    @media (min-width: ${MOBILE}) {
        width: 140px;
    }
    a {
        margin-bottom: ${Spacing._12};
    }
    a:last-child {
        margin: 0;
    }
`;
export const StyledMenuUserName = styled.div`
    line-height: 1;
    font-size: 14px;
    text-align: center;
    color: ${Color.grey};
`;
export const StyledUserButtonWrapper = styled.div<{ showDot?: boolean }>`
    font-size: 20px;
    position: relative;
    margin-right: ${Spacing._12};
    :before {
        left: 50%;
        width: 4px;
        height: 4px;
        bottom: -10px;
        position: absolute;
        border-radius: 50%;
        transform: translateX(-50%);
        background-color: ${Color.flashpub};
        box-shadow: 0 0 5px 3px ${Color.flashpub_t75};
        content: ${({ showDot }) => showDot ? "''" : ''};
    }
`;
export const StyledPublishItem = styled.div<{ showDot?: boolean }>`
    display: flex;
    cursor: pointer;
    line-height: 16px;
    position: relative;
    text-align: center;
    align-items: center;
    color: ${Color.midgrey};
    padding: 0 ${Spacing._8};
    :hover {
        color: ${Color.grey};
    }
    ::before {
        left: -8px;
        width: 4px;
        height: 4px;
        position: absolute;
        border-radius: 50%;
        background-color: ${Color.flashpub};
        box-shadow: 0 0 5px 3px ${Color.flashpub_t75};
        content: ${({ showDot }) => showDot ? "''" : ''};
    }
`;
export const StyledPublishButton = styled(FPButton)`
    font-size: 16px;
    font-family: ${Font.text};
`;
