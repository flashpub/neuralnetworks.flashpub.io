import * as React from 'react';
import { Router } from '@reach/router';

import { FeedScreen, PubScreen, OrcidScreen, PublishScreen, CampaignsScreen, DashboardDetailsScreen } from 'screens';
import { NotFound } from 'services/navigation';
import { ListScreen } from 'screens/ListScreen';

export const Authenticated: React.FC = () => {
    return (
        <Router primary={false}>
            <NotFound default />
            <FeedScreen path="/" />
            <PubScreen path="/pub/:id" />
            <OrcidScreen path="/orcid" />
            <FeedScreen path="/feed/:name" />
            <PublishScreen path="/publish/:contentType/*" />
            <CampaignsScreen path="/campaigns/:id" />
            <DashboardDetailsScreen path="/campaigns/:id/:contentType" />
            <ListScreen path="/list" />
            <ListScreen path="/profile/drafts" />
            <ListScreen path="/profile/library" />
            {/*<ProfileSettingsScreen path="/profile/settings" />*/}
        </Router>
    );
};
