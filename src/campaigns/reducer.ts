import { Status, reducer } from 'app/utils';

import * as CampaignsAction from 'campaigns/actions';
import { project } from '../app/config';

type CampaignsState = any;
export const initialState: CampaignsState = {
    list: null,
    current: null,
    status: Status.PENDING,
};

const campaignsReducer = reducer<CampaignsState>(initialState,
    CampaignsAction.CAMPAIGN_OPERATION_START.handle((state) => {
        state.status = Status.LOADING;
    }),

    CampaignsAction.CAMPAIGN_OPERATION_FAIL.handle((state) => {
        state.status = Status.FAILURE;
    }),

    CampaignsAction.CAMPAIGN_OPERATION_SUCCESS.handle((state) => {
        state.status = Status.SUCCESS;
    }),

    CampaignsAction.SET_CAMPAIGNS.handle((state, payload) => {
        state.list = payload.reverse().filter(p => p.community === project.id);
    }),

    CampaignsAction.SET_CURRENT_CAMPAIGN.handle((state, payload) => {
        state.current = payload.current;
    }),
);

export default campaignsReducer;
