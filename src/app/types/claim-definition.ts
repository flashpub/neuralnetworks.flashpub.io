export type ClaimItem = {
    id: string,
    name: string,
}

export interface ClaimDefinition {
    readonly [key: string]: any;
    readonly id: string;
    readonly linkedPubIds: Array<string>;
    readonly relationship: string | null;
    readonly allDictionaryIds: Array<string>;
    readonly relationshipType: 'positive' | 'negative' | 'independent' | null;
    readonly claimItems: {
        readonly firstItem: ClaimItem | null;
        readonly secondItem: ClaimItem | null;
    };
}
