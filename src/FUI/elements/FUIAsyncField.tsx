import * as React from 'react';
import { Styles } from 'react-select';
import styled from 'styled-components/macro';
import Creatable from 'react-select/creatable';
import { ValueType } from 'react-select/src/types';
import { SelectComponents } from 'react-select/src/components';
import AsyncPaginate, { AsyncPaginateProps } from 'react-select-async-paginate';

import { sleep } from 'app/utils';
import { Color, Radius } from 'app/assets';


type Option = { value: string, label: string }
type OrArray<T> = T[] extends undefined ? T[] : T
type FUIAsyncFieldState = OrArray<Option>
type MyAsyncPaginateProps = Omit<AsyncPaginateProps<any>, 'loadOptions'>
interface FUIAsyncFieldProps extends MyAsyncPaginateProps {
    list: any;
    placeholder: string;
    
    field?: string;
    isMulti?: boolean;
    required?: boolean;
    defaultValue?: any;
    isCreatable?: boolean;
    isSearchable?: boolean;
    styles?: Partial<Styles>;
    components?: SelectComponents<any>;
    onChange?(field: string, value: FUIAsyncFieldState): void;
}


export const FUIAsyncField: React.FC<FUIAsyncFieldProps> = (
    { list, placeholder, isSearchable, isMulti, components, styles, isCreatable, onChange, field, defaultValue, required }
) => {
    const [value, setValue] = React.useState<FUIAsyncFieldState>(defaultValue);

    const handleOnChange = (value: ValueType<Option>) => {
        setValue(value as Option);
        onChange && onChange(field as string, value as Option);
    };

    const loadOptions = async (input: string, prevOptions: Option[]) => {
        await sleep(1000);
        const filteredOptions = await list(input.toLowerCase());
        const hasMore = filteredOptions.length > prevOptions.length + 10;
        const slicedOptions = filteredOptions.slice(prevOptions.length, prevOptions.length + 10);
        return { options: slicedOptions, hasMore };
    };
    const selectComponent = isCreatable ? Creatable as unknown as JSX.Element : undefined;

    const isRequired = required && !value;

    return (
        <FieldWrapper required={isRequired}>
            <AsyncPaginate
                cacheOptions
                label={false}
                value={value}
                defaultOptions
                onChange={handleOnChange}
                placeholder={placeholder}
                loadOptions={loadOptions}
                debounceTimeout={500}
                isMulti={isMulti || false}
                SelectComponent={selectComponent}
                isSearchable={isSearchable || true}

                styles={{
                    placeholder: () => ({
                        fontWeight: 200,
                        fontStyle: 'italic',
                        color: Color.midgrey,
                    }),
                    indicatorsContainer: () => ({
                        right: 0,
                        position: 'absolute',
                    }),
                    singleValue: () => ({
                        fontWeight: 400,
                        color: Color.grey,
                        textAlign: 'center',
                    }),
                    menuList: () => ({
                        paddingTop: 4,
                        maxHeight: 300,
                        paddingBottom: 4,
                        overflowY: 'auto',
                        textAlign: 'center',
                        boxSizing: 'border-box',
                        fontSize: 14,
                    }),
                    menu: () => ({
                        top: '100%',
                        backgroundColor: 'hsl(0,0%,100%)',
                        borderRadius: 4,
                        boxShadow: '0 0 0 1px hsla(0,0%,0%,0.1),0 4px 11px hsla(0,0%,0%,0.1)',
                        marginBottom: 8,
                        marginTop: 8,
                        position: 'absolute',
                        width: '100%',
                        zIndex: 100,
                        boxSizing: 'border-box',
                        fontSize: 14,
                    }),
                    ...styles,
                }}

                components={{
                    DropdownIndicator: null,
                    ...components
                }}
            />
        </FieldWrapper>
    );
};


const FieldWrapper = styled.div<{ required?: boolean }>`
    position: relative;
    background-color: #fff;
    
    &::before {
        top: -2px;
        left: -2px;
        content: "";
        position: absolute;
        width: calc(100% + 4px);
        height: calc(100% + 4px);
        border-radius: ${Radius};
    }
    
    &::after {
        top: 2px;
        left: 4px;
        position: absolute;
        color: ${Color.error};
        content: ${({ required }) => required ? "'*'" : "''"};
    }
`;