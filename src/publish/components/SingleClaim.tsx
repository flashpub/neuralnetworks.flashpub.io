import * as React from 'react';
import uuid from 'uuid/v4';
import { Styles } from 'react-select';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import { Col as UICol } from 'react-styled-flexboxgrid';

import { Content } from 'app/types';
import { RootState } from 'app/store';
import { usePublish, PublishAction } from 'publish';
import { Label, Term } from 'publish/components/Elements';
import { DictionaryDefinition } from 'dictionaries/types';
import { FUIAsyncField } from 'FUI/elements';


type Option = { value: string, label: string }
type OrArray<T> = T[] extends undefined ? T[] : T


export const SingleClaim: React.FC = () => {
    const [{ form }, dispatch] = usePublish();

    const content = useSelector<RootState, Content | null>(state => state.publish.contentType);
    const dictionaries = useSelector<RootState, DictionaryDefinition[] | null>(state => state.dictionaries.list);

    const firstItem = form.claim.firstItem;
    const firstItemId = firstItem.id;
    const clearAncestors = () => dispatch(PublishAction.ReplacePublishFormValue({ name: 'claim.firstItemAncestors', value: [] }));

    React.useState(() => {
        const forcedTerm = content && content.claim.item1.setFilterAsItem;
        if (forcedTerm && dictionaries) {
            const item = dictionaries.filter(d => d.id === (content && content.claim.item1.filter))[0];
            dispatch(PublishAction.SetPublishFormValue({ name: 'claim.firstItem', value: { id: item.id, name: item.name }}));
        }
    });

    if (content && dictionaries) {
        const item1 = content.claim.item1;
        const isSingle = content.claim.type === 'single';
        const forcedTerm = content.claim.item1.setFilterAsItem;
        const isNew = !dictionaries.filter(d => d.id === firstItemId)[0];
        const firstItemDictionaryAncestors = dictionaries.filter(d => d.ancestors.includes(item1.filter));

        const filterNames = (input: string) => firstItemDictionaryAncestors
            .filter(d => d.longName.toLowerCase().includes(input.toLowerCase()))
            .map(d => ({ value: d.id, label: d.longName }));

        const onFieldChange = (field: string, value: OrArray<Option>) => {
            if (value) {
                if (Array.isArray(value)) {
                    const update = value.map(o => o.value);
                    dispatch(PublishAction.ReplacePublishFormValue({ name: field, value: update }));
                } else {
                    const exists = dictionaries.filter(d => d.id === value.value)[0];
                    if (exists) { clearAncestors() }
                    dispatch(PublishAction.SetPublishFormValue({
                        name: field,
                        value: { id: exists ? value.value : uuid(), name: exists ? exists.name : value.label },
                    }));
                }
            }
        };

        const setDefaultValue = (name: string) => {
            const item = form.claim[name];

            if (item) {
                if (Array.isArray(item)) {
                    return item
                        .map(x => dictionaries.filter(d => d.id === x)[0])
                        .map(x => ({ value: x.id, label: x.longName }));
                } else {
                    const exists = dictionaries.filter(d => d.id === item.id)[0];
                    if (forcedTerm) return dictionaries.filter(d => d.id === content.claim.item1.filter).map(d => ({ value: d.id, label: d.longName }))[0];
                    return exists
                        ? dictionaries.filter(d => d.id === item.id).map(d => ({ value: d.id, label: d.longName }))[0]
                        : item.id && ({ value: item.id, label: item.name});
                }
            } return null
        };

        return (
            <Col xs={12} sm>
                <Term single={isSingle}>
                    <Label>{item1.label}</Label>
                    <FUIAsyncField
                        required
                        styles={styles}
                        list={filterNames}
                        field="claim.firstItem"
                        onChange={onFieldChange}
                        isCreatable={item1.newTermAllowed}
                        placeholder="pick a term or type in something"
                        defaultValue={setDefaultValue('firstItem')}
                    />
                </Term>
                {firstItemId && isNew &&
                    <Term single={isSingle}>
                        <FUIAsyncField
                            isMulti
                            styles={styles}
                            list={filterNames}
                            onChange={onFieldChange}
                            placeholder="add ancestors"
                            field="claim.firstItemAncestors"
                            defaultValue={setDefaultValue('firstItemAncestors')}
                        />
                    </Term>
                }
            </Col>
        );
    } return null;
};


const Col = styled(UICol)` && {
    padding: 0;
}`;
const styles: Styles = {
    valueContainer: () => ({
        flex: 1,
        display: 'flex',
        flexWrap: 'wrap',
        padding: '2px 8px',
        overflow: 'hidden',
        position: 'relative',
        alignItems: 'center',
        boxSizing: 'border-box',
        justifyContent: 'center',
    }),
};
