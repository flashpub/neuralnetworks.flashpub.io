import { FirebaseUser } from 'types/firebase';

export const userData = (user: FirebaseUser) =>
    user && {
        uid: user.uid,
        email: user.email,
        photoURL: user.photoURL,
        isAnonymous: user.isAnonymous,
        phoneNumber: user.phoneNumber,
        displayName: user.displayName,
        emailVerified: user.emailVerified,
    };
