import * as React from 'react';
import { RouteComponentProps } from '@reach/router';

import { PubContainer } from 'containers/Pub';
import { Pub } from 'ui/Pub';

export const PubScreen: React.FC<RouteComponentProps> = () => {
    return (
        <PubContainer>
            <Pub />
        </PubContainer>
    );
};
