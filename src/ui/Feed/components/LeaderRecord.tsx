import * as React from 'react';
import sortBy from 'lodash/sortBy';
import { FileTextFilled } from '@ant-design/icons';

import {
    StyledLeaderWrapper,
    StyledLeaderMetadata,
    StyledLeaderIndicators,
    StyledLeaderName,
    StyledLeaderSpec,
    StyledLeaderWork,
    StyledLeaderAvatarPlaceholder, StyledIndicatorWrapper,
} from 'ui/Feed/styles';
import { Color } from 'assets/styles';
import { useTypedSelector } from 'helpers';

type Props = {
    pubs: PubDefinition[];
};

export const LeaderRecord: React.FC<Props> = ({ pubs }) => {
    const onLeaderRecordClick = (orcid: string) => {
        const url = `https://orcid.org/${orcid}`;
        orcid && window.open(url, "_blank");
    };
    
    const printAuthorsSortedPerPubCount = () => {
        if (pubs) {
            const authors: {[key: string]: any}  = {};
            const leaders: Leader[] = [];
            
            pubs.map((pub: PubDefinition) => {
                const pubId = pub.id;
                const authorId = pub.author.id;
                const authorName = pub.author.name;
                const authorOrcid = pub.author.orcid;
                const split = pub.author.email !== undefined ? pub.author.email.split('@') : "";
                const authorAffiliation = '@'.concat(split[split.length - 1]);
                const authorInitials = authorName.match(/\b\w/g)!.join('');
                
                if (authors[authorOrcid]) {
                    // const bookmarked = find(pub.bookmarkedBy, (id) => id === authors[authorId].id || id === authors[authorId].orcid);
                    // const endorsed = find(pub.review.acceptedBy, (id) => id === authors[authorId].orcid);
                    
                    authors[authorOrcid] = {
                        ...authors[authorOrcid],
                        pubs: [...authors[authorOrcid].pubs, pubId],
                    };
                    // if (bookmarked) {
                    //     authors[authorId] = {
                    //         ...authors[authorId],
                    //         bookmarks: [...authors[authorId].bookmarks, pubId],
                    //     };
                    // }
                    // if (endorsed) {
                    //     authors[authorId] = {
                    //         ...authors[authorId],
                    //         endorsements: [...authors[authorId].endorsements, pubId],
                    //     };
                    // }
                } else {
                    authors[authorOrcid] = {
                        ...authors[authorOrcid],
                        id: authorId,
                        pubs: [pubId],
                        name: authorName,
                        initials: authorInitials,
                        affiliation: authorAffiliation,
                        orcid: authorOrcid,
                        bookmarks: [],
                        endorsements: [],
                    };
                    // pub.bookmarkedBy.map((id) => {
                    //     if (id === authorId || id === authorOrcid) {
                    //         return authors[authorId] = {
                    //             ...authors[authorId],
                    //             bookmarks: [pubId],
                    //         };
                    //     } else {
                    //         return authors[id] = {
                    //             ...authors[id],
                    //             bookmarks: [pubId],
                    //         };
                    //     }
                    // });
                    // pub.review.acceptedBy.map((id) => {
                    //     if (id !== authorId) {
                    //         return authors[id] = {
                    //             ...authors[id],
                    //             endorsements: [pubId],
                    //         };
                    //     } else {
                    //         return authors[id] = {
                    //             ...authors[id],
                    //             endorsements: [],
                    //         };
                    //     }
                    // });
                }
            });
            
            Object.values(authors).map(author => leaders.push(author as Leader));
            
            return sortBy(leaders, leader => leader.pubs.length)
                .reverse()
                .splice(0, 5)
                .map((leader: Leader, i) => (
                    <StyledLeaderWrapper
                        key={i}
                        onClick={() => onLeaderRecordClick(leader.orcid)}
                    >
                        <StyledLeaderMetadata>
                            <StyledLeaderAvatarPlaceholder>
                                {leader.initials}
                            </StyledLeaderAvatarPlaceholder>
                            <StyledLeaderIndicators>
                                <StyledIndicatorWrapper>
                                    <FileTextFilled style={{ fontSize: 20, color: Color.community }} />
                                    <div>{leader.pubs.length}</div>
                                </StyledIndicatorWrapper>
                            </StyledLeaderIndicators>
                        </StyledLeaderMetadata>
                        <StyledLeaderName>{leader.name}</StyledLeaderName>
                        <StyledLeaderSpec>{leader.affiliation}</StyledLeaderSpec>
                        <StyledLeaderWork></StyledLeaderWork>
                    </StyledLeaderWrapper>
                ));
        }
    };
    
    if (pubs) {
        return (
            <>
                {printAuthorsSortedPerPubCount()}
            </>
        );
    } else return null;
};
