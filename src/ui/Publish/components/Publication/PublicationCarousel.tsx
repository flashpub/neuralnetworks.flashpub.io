import * as React from 'react';
import Slider from 'react-slick';
import { useHover } from 'react-use';
import styled from 'styled-components/macro';
import { FileAddOutlined, LoadingOutlined } from '@ant-design/icons';

import { useTypedSelector } from 'helpers';
import { Color, Spacing } from 'assets/styles';
import { FPEditIcon } from 'ui/elements';
import { StyledFlexWrapper } from 'ui/elements/styles';
import { PublicationDropzone } from 'ui/Publish';

type Props = {
    editMode: boolean;
    pub: PubDefinition;
};

export const PublicationCarousel: React.FC<Props> = ({ pub, editMode }) => {
    const uploadStatus = useTypedSelector((state) => state.files.status.type);
    const settings = {
        dots: true,
        speed: 500,
        arrows: false,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dotsClass: 'slick-dots custom-thumb',
        customPaging: (i: number) => renderCustomPaging(i),
    };

    const renderCustomPaging = (i: number) => {
        return (
            <StyledCustomPagingWrapper>
                {uploadStatus === 'LOADING' ? (
                    <LoadingOutlined />
                ) : pub.figures[i] ? (
                    <StyledCustomPaging src={pub.figures[i].url} />
                ) : (
                    <FileAddOutlined />
                )}
            </StyledCustomPagingWrapper>
        );
    };

    const renderSlides = (isHovering: boolean) => {
        const end = Object.values(pub.figures).map((s) => s).length + 1;
        const array: number[] = [];
        (editMode ? [...pub.figures, undefined as any] : pub.figures).slice(0, end).map((_, i) => array.push(i));

        return array
            .slice(0, 5)
            .map((s) => (
                <PublicationDropzone key={s} pub={pub} label={Number(s)} editMode={editMode} hovered={isHovering} />
            ));
    };

    const slider = (isHovering: boolean) => (
        <StyledCarouselWrapper>
            <Slider {...settings}>{renderSlides(isHovering)}</Slider>
            {editMode && <FPEditIcon />}
        </StyledCarouselWrapper>
    );

    const [carousel] = useHover(slider);

    return carousel;
};

export const StyledCarouselWrapper = styled(StyledFlexWrapper)`
    width: 100%;
    max-width: 700px;
    position: relative;

    .slick-slider {
        width: inherit;
    }
    .custom-thumb {
        height: 30px;
        bottom: -35px;
        align-items: center;
        justify-content: center;
        display: flex !important;
        li {
            width: 45px;
            height: 30px;
            overflow: hidden;
            align-items: center;
            filter: grayscale(100%);
            justify-content: center;
            display: flex !important;
            border: 1px solid ${Color.midgrey};
        }
        li.slick-active {
            filter: grayscale(0);
            border: 1px solid ${Color.community};
        }
    }
`;
export const StyledCustomPagingWrapper = styled.a`
    width: 45px;
    height: 30px;
    span {
        font-size: 20px;
        color: ${Color.grey};
        margin-top: ${Spacing._x(5)};
    }
`;
export const StyledCustomPaging = styled.img`
    height: 100%;
`;
