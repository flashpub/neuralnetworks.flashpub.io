import store from 'app/store';
import * as AppAction from 'app/actions';
import api, { API_VERSION } from 'app/api';
import { AlertType, ClaimDefinition, PubDefinition } from 'app/types';

import * as OntologiesAction from 'dictionaries/actions';

const version = API_VERSION.content;

const alertHandler = (type: AlertType, alert: Error | { message: string }) => {
    store.dispatch(AppAction.ADD_ALERT.use({
        type: type,
        message: alert.message,
    }));
    store.dispatch(OntologiesAction.DICTIONARIES_OPERATION_FAIL.use());
};

export const pubs = {
    get(id: string, page?: number, size?: number): Promise<Array<PubDefinition>> {
        const params = { page, size };
        return api
            .get(`/content/${version}/feed_new/${id}`, { params })
            .then(response => response.data)
            .catch(error => alertHandler(AlertType.ERROR, error.response.data));
    },
};

export const claims = {
    get(): Promise<Array<ClaimDefinition>> {
        return api
            .get(`/content/${version}/claims`)
            .then(response => response.data)
            .catch(error => alertHandler(AlertType.ERROR, error.response.data));
    },
};
