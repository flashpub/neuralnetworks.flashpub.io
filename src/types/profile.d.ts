interface ProfileContainerState {
    user: UserDefinition | null;
};
type AccessGrants = {
    editorOf: string;
    canReview: boolean;
    canPublish: boolean;
};
interface UserDefinition {
    bio: string;
    uid: string;
    name: string;
    email: string;
    orcid: string;
    avatar?: string;
    initials: string;
    affiliation: string;
    isVerified: boolean;
    access: AccessGrants;
    metrics: UserMetrics;
    communities: string[];
}
type Activity = {
    lastLogin: number;
    lastReview: number;
};
type UserMetrics = {
    activity: Activity[];
    authoredPubIds: string[];
    reviewedPubIds: string[];
    bookmarkedPubIds: string[];
};
