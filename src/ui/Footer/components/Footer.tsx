import * as React from 'react';
import { Divider } from 'antd';

import { StyledSpacer } from 'ui/elements/styles';
import {
    StyledLeftSide,
    StyledRightSide,
    StyledFooterWrapper,
    StyledImprintWrapper,
    StyledFooterSectionWrapper,
    StyledFooterInternalWrapper,
    StyledTNC,
} from 'ui/Footer/styles';
import { FooterAbout, FooterBrand, FooterResources, FooterFlashpubThings, FooterSocial } from 'ui/Footer';

export const Footer: React.FC = () => {
    return (
        <StyledFooterWrapper id="footer">
            <StyledFooterInternalWrapper gutter={[8, 8]}>
                <StyledFooterSectionWrapper xs={24} md={4}>
                    <FooterBrand />
                </StyledFooterSectionWrapper>
                <StyledFooterSectionWrapper xs={24} md={6}>
                    <FooterFlashpubThings />
                </StyledFooterSectionWrapper>
                <StyledFooterSectionWrapper xs={24} md={6}>
                    <FooterResources />
                </StyledFooterSectionWrapper>
                <StyledFooterSectionWrapper xs={24} md={4}>
                    <FooterAbout />
                </StyledFooterSectionWrapper>
                <StyledFooterSectionWrapper xs={24} md={4}>
                    <FooterSocial />
                </StyledFooterSectionWrapper>
            </StyledFooterInternalWrapper>
            <StyledSpacer height={20} />
            <Divider style={{ margin: 0, background: '#fff' }} />
            <StyledImprintWrapper>
                <StyledLeftSide xs={24} md={12}>
                    <span>© 2020 FLASHPUB</span>
                </StyledLeftSide>
                <StyledRightSide xs={24} md={12}>
                    <StyledTNC to={''}>Terms & Conditions</StyledTNC>
                    <StyledTNC to={''}>All content CC - BY</StyledTNC>
                </StyledRightSide>
            </StyledImprintWrapper>
        </StyledFooterWrapper>
    );
};
