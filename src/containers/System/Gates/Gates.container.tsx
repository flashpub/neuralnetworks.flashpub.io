import * as React from 'react';
import { useDispatch } from 'react-redux';

import * as Gates from 'containers/System/Gates';

export const GatesContainer: React.FC = ({ children }) => {
    const dispatch = useDispatch();

    React.useEffect(() => {
        dispatch(Gates.populateSystemGates());
    }, []);

    return <>{children}</>;
};
