import * as React from 'react';
import { find, pull } from 'lodash';
import { useDispatch } from 'react-redux';
import { RightCircleOutlined } from '@ant-design/icons';
import { RouteComponentProps, useLocation, useNavigate } from '@reach/router';
import Checkbox, { CheckboxChangeEvent } from 'antd/lib/checkbox';

import { matchStepperToUrl, useTypedSelector } from 'helpers';
import * as Pub from 'containers/Pub';
import * as Publish from 'containers/Publish';
import {
    StyledStepTitle,
    StyledLegalLabel,
    StyledStepWrapper,
    StyledStepNextButton,
    StyledSubmitCheckboxCertify,
    StyledSubmitSuggestedReviewer,
    StyledSubmitSuggestedReviewers,
} from 'ui/Publish/styles';
import { GlobalLoader } from 'ui/Overlays';

export const Submit: React.FC<RouteComponentProps<{ contentType: string }>> = ({ contentType }) => {
    const dispatch = useDispatch();
    const location = useLocation();
    const navigate = useNavigate();

    const draft = useTypedSelector((state) => state.pub.data.draft);
    const claim = useTypedSelector((state) => state.claim.data.claim);
    const selectedContent = useTypedSelector((state) => state.content.data.type);
    const community = useTypedSelector((state) => state.community.data.community);
    const dictionaryList = useTypedSelector((state) => state.dictionary.data.list);
    const dictionaryTerm1 = useTypedSelector((state) => state.dictionary.data.term1);
    const dictionaryTerm2 = useTypedSelector((state) => state.dictionary.data.term2);
    const publishState = useTypedSelector((state) => state.publish.status.type);

    const [checked, setChecked] = React.useState<boolean>(draft.metadata.isDeIdentified);
    const [reviewers, setReviewers] = React.useState({ _0: '', _1: '', _2: '', _3: '' });

    React.useEffect(() => {
        const match = matchStepperToUrl(location.pathname.split(`/${contentType}/`)[1]);
        dispatch(Publish.step({ order: match.order, type: match.type }));
    }, [contentType, location.pathname, dispatch]);
    
    if (community && draft && selectedContent) {
        const legalExists = community.hasOwnProperty('legal') && community.legal;
        const isSubmitDisabled = !!(legalExists && !checked);
        const legalDesc = legalExists && Object.values(community.legal!).filter((c) => c.description)[0].description;

        const handleOnSubmit = () => {
            const revs = Object.values(reviewers).map((r) => r);
            const fetchId = `${draft.title.split(' ').join('-').toLowerCase()}_${draft.id}`;
            localStorage.setItem('fetchId', fetchId);
            dispatch(Pub.form.set({ form: 'pub', field: 'review.suggestedReviewers', value: pull(revs, '') }));

            const submitPub = async () => {
                const firstTermExists = find(dictionaryList, (d) => d.name === draft.claim.terms.firstItem);
                const secondTermExists = find(dictionaryList, (d) => d.name === draft.claim.terms.secondItem);
                const isSingle = selectedContent.claim.type === 'single';
                
                const pubFromDraft = ({
                   ...draft,
                    metadata: {
                       ...draft.metadata,
                        draft: null,
                    },
                });

                if (firstTermExists && !secondTermExists && isSingle) {
                    await dispatch(
                        Publish.createPub(
                            {
                                pub: pubFromDraft,
                                claim,
                            },
                            undefined,
                            () => navigate('/'),
                        )
                    );
                }
                if (!firstTermExists && !secondTermExists && isSingle) {
                    await dispatch(
                        Publish.createPub(
                            {
                                pub: pubFromDraft,
                                claim,
                                newDictionary1: dictionaryTerm1,
                            },
                            undefined,
                            () => navigate('/'),
                        )
                    );
                }
                if (firstTermExists && secondTermExists && !isSingle) {
                    await dispatch(
                        Publish.createPub(
                            {
                                pub: pubFromDraft,
                                claim,
                            },
                            undefined,
                            () => navigate('/'),
                        )
                    );
                }
                if (firstTermExists && !secondTermExists && !isSingle) {
                    await dispatch(
                        Publish.createPub(
                            {
                                pub: pubFromDraft,
                                claim,
                                newDictionary2: dictionaryTerm2,
                            },
                            undefined,
                            () => navigate('/'),
                        )
                    );
                }
                if (!firstTermExists && secondTermExists && !isSingle) {
                    await dispatch(
                        Publish.createPub(
                            {
                                pub: pubFromDraft,
                                claim,
                                newDictionary1: dictionaryTerm1,
                            },
                            undefined,
                            () => navigate('/'),
                        )
                    );
                }
                if (!firstTermExists && !secondTermExists && !isSingle) {
                    await dispatch(
                        Publish.createPub(
                            {
                                pub: pubFromDraft,
                                claim,
                                newDictionary1: dictionaryTerm1,
                                newDictionary2: dictionaryTerm2,
                            },
                            undefined,
                            () => navigate('/'),
                        )
                    );
                }
                return null;
            };
            submitPub();
        };

        const handleOnCheck = (e: CheckboxChangeEvent) => {
            const value = e.target.checked;
            setChecked(value);
            dispatch(Pub.form.set({ form: 'pub', field: 'metadata.isDeIdentified', value }));
        };

        const onMoreInfo = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
            if (legalExists) {
                const legalMoreInfo = Object.values(community.legal!).filter((c) => c.moreInfo)[0].moreInfo;
                e.preventDefault();
                window.open(legalMoreInfo);
            }
        };

        const label = (
            <StyledLegalLabel>
                {legalDesc}
                <br />(<span onClick={onMoreInfo}>more info</span>)
            </StyledLegalLabel>
        );
        const renderReviewerInputs = [0, 1, 2, 3].map((r) => {
            const handleOnChange = (e: string) => setReviewers({ ...reviewers, [`_${r}`]: e });
            return <StyledSubmitSuggestedReviewer key={r} placeholder="email address..." onChange={handleOnChange} />;
        });
        
        if (publishState === 'LOADING') {
            return <GlobalLoader slogan="We're submitting your publication" />;
        } else {
            return (
                <StyledStepWrapper>
                    <StyledStepTitle>Ready to Submit?</StyledStepTitle>
                    {legalExists && (
                        <StyledSubmitCheckboxCertify>
                            <Checkbox checked={checked} onChange={handleOnCheck}>
                                {label}
                            </Checkbox>
                        </StyledSubmitCheckboxCertify>
                    )}
                    <StyledSubmitSuggestedReviewers>
                        <p>Suggested reviewers</p>
                        {renderReviewerInputs}
                    </StyledSubmitSuggestedReviewers>
                    <StyledStepNextButton
                        label="Submit"
                        iconSide="right"
                        buttonType="link"
                        onClick={handleOnSubmit}
                        disabled={isSubmitDisabled}
                        icon={<RightCircleOutlined />}
                    />
                </StyledStepWrapper>
            );
        }
    }
    return null;
};
