import * as React from 'react';

import { useTypedSelector, useWindowWidth } from 'helpers';
import { PubActionButtons, PubContentBar, PubMobileBar, PubReviewBar, PubReviewPanel } from 'ui/Pub';
import { PublicationSheet } from 'ui/Publish';
import { StyledSpacer } from 'ui/elements/styles';
import { FPPlaceholder } from 'ui/elements';
import { Viewport } from 'assets/styles';
import { StyledPubWrapper } from 'ui/Pub/styles';

export const Pub: React.FC = () => {
    const [reviewPanel, setReviewPanel] = React.useState(false);
    
    const isMobile = useWindowWidth() <= Viewport.TABLET;
    const isTablet = useWindowWidth() <= Viewport.DESKTOP;
    
    const pub = useTypedSelector((state) => state.pub.data.pub);
    const draft = useTypedSelector((state) => state.pub.data.draft);
    const editMode = useTypedSelector((state) => state.pub.data.edit);
    
    const pubData = editMode ? draft : pub;

    const handleSetReviewPanel = (show: boolean) => setReviewPanel(show);

    return (
        <StyledPubWrapper editMobile={isMobile && editMode}>
            {!pub.contentOrigin.originalContent && <PubContentBar pub={pubData}/>}
            {!pub.description
                ? (
                    <div style={{ height: 'fit-content', maxWidth: 700, width: '100%' }}>
                        <FPPlaceholder height={40} />
                    </div>
                ) : pub.review.isPubAccepted ? null : <PubReviewBar pub={pubData} />
            }
            <PublicationSheet pub={pubData} editMode={editMode} cb={handleSetReviewPanel} />
            {!isMobile && (
                <PubActionButtons
                  pub={pubData}
                  editMode={editMode}
                  showPanel={reviewPanel}
                  setReviewPanel={handleSetReviewPanel}
                />
            )}
            {!isTablet && <PubReviewPanel pub={pubData} editMode={editMode} showPanel={reviewPanel && !isTablet} />}
            {isTablet && (
                <>
                    <StyledSpacer height={40} />
                    <PubReviewPanel editMode={editMode} pub={pub} showPanel={reviewPanel && isTablet} />
                </>
            )}
            <StyledSpacer height={80} />
            {isMobile && <PubMobileBar pub={pubData} editMode={editMode} showPanel={reviewPanel} setReviewPanel={handleSetReviewPanel} />}
        </StyledPubWrapper>
    );
};
