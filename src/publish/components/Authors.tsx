import * as React from 'react';
import shortid from 'shortid';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import { Icon, Popover, Position, Tooltip } from '@blueprintjs/core';

import { RootState } from 'app/store';
import { Color, Font, Radius, Spacing } from 'app/assets';
import { UserDefinition } from 'access/types';

import { FUIButton, FUIInput, FUIInputRefs } from 'FUI/elements';
import { usePublish, PublishAction } from 'publish';


const initialContributor = { name: '', email: '', orcid: '' };

export const Authors: React.FC = () => {
    const [{ form }, dispatch] = usePublish();
    const [contributor, setContributor] = React.useState<typeof initialContributor>(initialContributor);
    const user = useSelector<RootState, UserDefinition | null>(state => state.access.user);

    const nameRef = React.useRef<FUIInputRefs>(null);
    const emailRef = React.useRef<FUIInputRefs>(null);
    const orcidRef = React.useRef<FUIInputRefs>(null);
    const contributors: Author[] = form.contributors;

    const onNameChange = (value: string) => setContributor({...contributor, name: value});
    const onEmailChange = (value: string) => setContributor({...contributor, email: value});
    const onOrcidChange = (value: string) => setContributor({...contributor, orcid: value});

    const onAddContributor = () => {
        const user: Author = {
            id: shortid(),
            name: contributor.name,
            email: contributor.email,
            orcid: contributor.orcid,
        };
        dispatch(PublishAction.SetPublishFormValue({ name: 'contributors', value: user }));
        setContributor(initialContributor);
        nameRef.current && nameRef.current.setInputValue('');
        emailRef.current && emailRef.current.setInputValue('');
        orcidRef.current && orcidRef.current.setInputValue('');
    };

    if (user) {
        const renderAddContributorForm = () => {
            const isDisabled = !contributor.name || !contributor.email;
            return (
                <FormWrapper>
                    <Title>Add a contributor:</Title>
                    <ContributorName ref={nameRef} placeholder="name" onChange={onNameChange} required />
                    <ContributorEmail ref={emailRef} placeholder="email" onChange={onEmailChange} required />
                    <ContributorOrcid ref={orcidRef} placeholder="orcid" onChange={onOrcidChange} />
                    <AddContributor text="add" type="button" disabled={isDisabled} buttonType="outlined" onClick={onAddContributor} />
                </FormWrapper>
            );
        };

        const printContributors = contributors.map((c, i) => {
            const removeContributor = () => dispatch(PublishAction.CutPublishFormValue({ name: 'contributors', id: c.id }));
            return (
                <div key={i} style={{ display: 'inline-block' }}>
                    ,
                    <Tooltip content="remove" position={Position.RIGHT}>
                        <Author onClick={removeContributor} co>
                            <Name>{c.name}</Name>
                            <RemoveIcon />
                        </Author>
                    </Tooltip>
                </div>
            );
        });

        return (
            <AuthorsWrapper>
                by<Author>{user.name}</Author>
                {printContributors}
                <Popover
                    autoFocus={true}
                    canEscapeKeyClose={true}
                    position={Position.RIGHT}
                    content={renderAddContributorForm()}
                >
                    <Tooltip content="add contributor" position={Position.RIGHT}>
                        <AddAuthor icon="add" />
                    </Tooltip>
                </Popover>
            </AuthorsWrapper>
        );
    } return null;
};

const Name = styled.span``;
const RemoveIcon = styled.div`
    top: 50%;
    left: 50%;
    width: 97%;
    height: 1px;
    margin: 0 3px;
    position: absolute;
    visibility: hidden;
    background-color: #000;
    transform: translate(-50%, -50%);
    :focus { outline: none };
`;
const Author = styled.div<{ co?: boolean }>`
    position: relative;
    font-style: italic;
    display: inline-block;
    padding: 2px 0 2px 4px;
    border-radius: ${Radius};
    &:hover {
        cursor: ${({ co }) => co && 'pointer'};
        color: ${({ co }) => co && Color.midgrey};
        background-color: ${({ co }) => co && Color.lightgrey};
    }
    &:hover ${RemoveIcon} { visibility: visible }
`;
const AuthorsWrapper = styled.div`
    font-size: 14px;
    padding: ${Spacing.ML} ${Spacing.M};
`;
const AddAuthor = styled(Icon)` && {
    cursor: pointer;
    color: ${Color.midgrey};
    margin-left: ${Spacing.SM};
    &:hover { color: ${Color.grey} }
    *:focus { outline: none }
}`;
const FormWrapper = styled.div`
    width: 300px;
    padding: ${Spacing.ML};
`;
const Title = styled.div`
    font-size: 14px;
    text-align: center;
    color: ${Color.grey};
    margin: 0 0 ${Spacing.M};
    font-family: ${Font.text};
`;
const ContributorField = styled(FUIInput)` && {
    height: 26px;
    font-size: 12px;
    margin: 0 0 ${Spacing.M};
}`;
const ContributorName = styled(ContributorField)``;
const ContributorEmail = styled(ContributorField)``;
const ContributorOrcid = styled(ContributorField)``;
const AddContributor = styled(FUIButton)<{ disabled?: boolean }>` && {
    width: 100%;
    height: 26px;
    font-size: 14px;
    font-weight: 400;
    font-family: ${Font.pub_text};
    padding: ${Spacing.S} ${Spacing.L} ${Spacing.SM};
    cursor: ${({ disabled }) => disabled && 'not-allowed'};
    background-color: ${({ disabled }) => disabled && Color.lightgrey};
}`;
