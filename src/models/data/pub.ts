import { v4 as uuid_v4 } from 'uuid';
import ClaimDataModel from 'models/data/claim';
import { community } from 'configs/project';
import { PEER_REVIEW_PERIOD } from 'configs/settings';

const PubDataModel: PubDefinition = {
    id: uuid_v4(),
    title: 'Pub title not yet created',
    contentOrigin: {
        url: community.url,
        source: `${community.name}.flashpub.io`,
        originalContent: true,
    },
    acceptedNotification: false,
    author: {
        id: '',
        name: '',
        orcid: '',
        email: '',
    },
    contributors: [],
    description: '',
    claim: {
        id: ClaimDataModel.id,
        conditions: [],
        terms: {
            firstItem: '',
            secondItem: null,
            relationship: null,
            type: null,
        },
    },
    figures: [],
    metadata: {
        protocols: [],
        code: [],
        datasets: [],
        references: [],
        datePublished: Date.now(),
        dateUpdated: Date.now(),
        doi: '',
        fetchId: '',
        isDeIdentified: false,
        isPrivate: false,
        draft: null,
        allDictionaryIds: [],
    },
    review: {
        isPubAccepted: false,
        isPubRetracted: false,
        discussionId: [],
        deadline: Number(new Date().setDate(new Date().getDate() + PEER_REVIEW_PERIOD).toString()),
        acceptedBy: [],
        reportedBy: [],
        suggestedReviewers: [],
        quickReviews: {},
    },
    bookmarkedBy: [],
};

export default PubDataModel;
