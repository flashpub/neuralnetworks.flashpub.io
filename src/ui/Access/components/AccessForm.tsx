import * as React from 'react';

import { useTypedSelector } from 'helpers';
import { FPInput } from 'ui/elements';
import {
    StyledAccessFormFieldWrapper,
    StyledAccessFormWrapper,
    StyledNextButton,
    StyledResetPassword
} from 'ui/Access/styles';

type Props = {
    label: string;
    step: AccessStep;
    isLoading: boolean;
    dispatch: (value: string) => void;
    onSubmit: (creds: Credentials) => void;
};

export const AccessForm: React.FC<Props> = ({ onSubmit, isLoading, label, dispatch, step }) => {
    const isAccessOpen = useTypedSelector((state) => state.access.data.isOpen);

    const [name, setName] = React.useState<string>('');
    const [email, setEmail] = React.useState<string>('');
    const [password, setPassword] = React.useState<string>('');

    React.useEffect(
        () => () => {
            const clearForm = () => {
                setName('');
                setEmail('');
                setPassword('');
            };
            !isAccessOpen && clearForm();
        },
        [isAccessOpen]
    );

    const onNameChange = (value: string) => setName(value);
    const onEmailChange = (value: string) => setEmail(value);
    const onPasswordChange = (value: string) => setPassword(value);
    const onPasswordReset = () => dispatch('RESET_PASSWORD');
    const onSubmitClick = () => onSubmit({ email, password, name });

    const isDisabled = !email || (step !== 'RESET_PASSWORD' && !password) || (step === 'REGISTER' && !name);

    return (
        <StyledAccessFormWrapper>
            {step === 'REGISTER' && (
                <StyledAccessFormFieldWrapper>
                    <FPInput
                        required
                        type="name"
                        value={name}
                        placeholder="full name"
                        onChange={onNameChange}
                        disabled={isLoading}
                    />
                </StyledAccessFormFieldWrapper>
            )}
            <StyledAccessFormFieldWrapper>
                <FPInput required type="email" value={email} placeholder="email" onChange={onEmailChange} />
            </StyledAccessFormFieldWrapper>
            {step !== 'RESET_PASSWORD' && (
                <StyledAccessFormFieldWrapper>
                    <FPInput
                        required
                        type="password"
                        name="password"
                        value={password}
                        disabled={isLoading}
                        placeholder="password"
                        autocomplete="new-password"
                        onChange={onPasswordChange}
                    />
                </StyledAccessFormFieldWrapper>
            )}
            {step === 'LOBBY' && (
                <StyledResetPassword fontSize={12} buttonType="link" label="RESET PASSWORD" onClick={onPasswordReset} />
            )}
            <StyledNextButton
                label={label}
                buttonType="link"
                disabled={isDisabled}
                isLoading={isLoading}
                onClick={onSubmitClick}
            />
        </StyledAccessFormWrapper>
    );
};
