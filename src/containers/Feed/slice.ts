import { createGenericSlice } from 'helpers';

const initialState = {
    data: [] as PubDefinition[],
    status: { type: 'PENDING', message: null },
};

export const slice = createGenericSlice({
    name: 'feed',
    initialState: initialState as GenericState<PubDefinition[]>,
    reducers: {
        populate: (state, action) => {
            state.data = action.payload;
        },
    },
});

export const { populate, ...status } = slice.actions;

export default slice.reducer;
