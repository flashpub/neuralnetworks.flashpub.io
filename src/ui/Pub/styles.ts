import styled, { css, keyframes } from 'styled-components/macro';

import { Color, Font, Media, Radius, Spacing } from 'assets/styles';
import { StyledFlexWrapper } from 'ui/elements/styles';
import { FPButton } from 'ui/elements';

export const StyledPubWrapper = styled(StyledFlexWrapper)<{ editMobile?: boolean }>`
    padding: 0 24px;
`;
export const StyledPubActionButtonsWrapper = styled.div`
    width: 100%;
    z-index: 1000;
    display: flex;
    align-items: center;
    flex-direction: row;
    padding: ${Spacing._12};
    border-radius: ${Radius};
    justify-content: center;
    
    ${Media.tablet} {
        top: 50%;
        width: 84px;
        position: fixed;
        flex-direction: column;
        transform: translateY(-50%);
    }
`;
export const StyledEditModeOverlay = styled.div`
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 10;
    position: fixed;
    background-color: rgba(0, 0, 0, 0.8);
`;
export const StyledEditModeOverlayLoading = styled(StyledEditModeOverlay)`
    z-index: 10000;
    background-color: transparent;
`;
const StyledIconButton = css`
    width: 40px;
    height: 40px;
    display: flex;
    cursor: pointer;
    align-items: center;
    border-radius: 100%;
    background-color: #fff;
    justify-content: center;
    transition: all 0.2s ease;
    
    svg {
        width: 24px;
        height: 24px;
    }
    
    &:hover {
        transform: scale(1.1);
        background-color: ${Color.community_t15};
    }
    
    ${Media.tablet} {
        box-shadow: 0px 0px 10px 2px ${Color.community_t75};
    }
`;
export const StyledShareWrapper = styled(StyledFlexWrapper)<{ mobile?: boolean }>`
    flex-direction: ${({ mobile }) => mobile ? 'row' : 'column'};
    background-color: transparent;
`;
export const StyledActionIcon = styled.div<{ editMode?: boolean }>`
    ${StyledIconButton};
    width: ${({ editMode }) => editMode && '60px'};
    height: ${({ editMode }) => editMode && '60px'};
    svg {
        width: ${({ editMode }) => editMode && '36px'};
        height: ${({ editMode }) => editMode && '36px'};
    }
    &:hover {
        transform: ${({ editMode }) => editMode && 'scale(1)'};
    }
    span {
        color: ${Color.community};
    }
`;
export const StyledReviewPanelWrapper = styled(StyledFlexWrapper)<{ show: boolean }>`
    width: 100%;
    background-color: #fff;
    padding: ${Spacing._12};
    align-items: flex-start;
    border-radius: ${Radius};
    transition: all 0.2s ease;
    font-family: ${Font.text};
    opacity: ${({ show }) => (show ? '1' : '0')};
    box-shadow: 0px 0px 10px -1px ${Color.community_t75};
    visibility: ${({ show }) => (show ? 'visible' : 'hidden')};
    
    ${Media.tablet} {
        max-width: 230px;
        min-width: 150px;
    }
    ${Media.desktop} {
        top: 50%;
        right: 100px;
        z-index: 1000;
        position: fixed;
        transform: translateY(-50%);
        visibility: ${({ show }) => (show ? 'visible' : 'hidden')};
    }

    .ant-badge {
        width: 100%;
    }
`;
export const StyledReviewPanelBlocker = styled.div`
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 100;
    position: absolute;
    cursor: not-allowed;
    border-radius: ${Radius};
    background-color: rgba(255,255,255,0.4);
`;
export const StyledReviewPanelTitle = styled.div`
    width: 100%;
    padding-bottom: ${Spacing._4};
    border-bottom: 1px solid ${Color.community_t75};
`;
export const StyledReviewsListWrapper = styled.div``;
export const StyledCheckboxWrapper = styled.div`
    margin-bottom: ${Spacing._4};
    .ant-checkbox-checked .ant-checkbox-inner {
        border-color: ${Color.community};
        background-color: ${Color.community};
    }
    .ant-checkbox-checked::after,
    .ant-checkbox-wrapper:hover .ant-checkbox-inner {
        border-color: ${Color.community};
    }
`;
export const StyledEndorseButton = styled(FPButton)`
    padding: 0;
    height: 34px;
`;
export const StyledCommentButton = styled(StyledEndorseButton)`
  height: 26px;
`;
const blink = keyframes`
  50% {
    opacity: 0;
  }
`;
export const StyledReviewBarWrapper = styled(StyledFlexWrapper)`
    width: 100%;
    height: 40px;
    font-size: 14px;
    max-width: 700px;
    font-weight: 600;
    flex-direction: column;
    font-family: ${Font.text};
    background-color: ${Color.flashpub_t15};
    span {
        margin-right: ${Spacing._4};
    }
    
    ${Media.tablet} {
        flex-direction: row;
    }
`;
export const StyledReviewBarTop = styled(StyledFlexWrapper)`
    flex-direction: row;
    div:first-child {
        width: 14px;
        height: 14px;
        border-radius: 50%;
        margin-right: ${Spacing._8};
        background-color: ${Color.flashpub};
        animation: ${blink} 1s step-start infinite;
    }
`;
export const StyledReviewBarBottom = styled(StyledFlexWrapper)``;
export const StyledPubMobileBarWrapper = styled(StyledFlexWrapper)`
    bottom: 0;
    width: 100%;
    height: 60px;
    z-index: 100;
    position: fixed;
    background-color: ${Color.notwhite};
    box-shadow: 0px -2px 10px -2px ${Color.community_t75};
`;

export const StyledPubContentBarWrapper = styled(StyledFlexWrapper)`
    width: 100%;
    font-size: 13px;
    max-width: 700px;
    color: ${Color.grey};
    padding: ${Spacing._4} 0;
    border-radius: ${Radius};
    font-family: ${Font.text};
    background-color: ${Color.lightgrey};
    
    a {
      color: ${Color.community};
    }
`;
