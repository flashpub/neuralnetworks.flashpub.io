import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/storage';
import 'firebase/firestore';

import { envDetector } from 'helpers';

const devConfig = {
    apiKey: process.env.REACT_APP_DEV_FIREBASE_API_KEY,
    authDomain: process.env.REACT_APP_DEV_FIREBASE_AUTH_DOMAIN,
    databaseURL: process.env.REACT_APP_DEV_FIREBASE_DATABASE_URL,
    projectId: process.env.REACT_APP_DEV_FIREBASE_PROJECT_ID,
    storageBucket: process.env.REACT_APP_DEV_FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.REACT_APP_DEV_FIREBASE_MESSAGING_SENDER_ID,
    appId: process.env.REACT_APP_DEV_APP_ID,
};

const testConfig = {
    apiKey: process.env.REACT_APP_TEST_FIREBASE_API_KEY,
    authDomain: process.env.REACT_APP_TEST_FIREBASE_AUTH_DOMAIN,
    databaseURL: process.env.REACT_APP_TEST_FIREBASE_DATABASE_URL,
    projectId: process.env.REACT_APP_TEST_FIREBASE_PROJECT_ID,
    storageBucket: process.env.REACT_APP_TEST_FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.REACT_APP_TEST_FIREBASE_MESSAGING_SENDER_ID,
    appId: process.env.REACT_APP_TEST_APP_ID,
};

const prodConfig = {
    apiKey: process.env.REACT_APP_PROD_FIREBASE_API_KEY,
    authDomain: process.env.REACT_APP_PROD_FIREBASE_AUTH_DOMAIN,
    databaseURL: process.env.REACT_APP_PROD_FIREBASE_DATABASE_URL,
    projectId: process.env.REACT_APP_PROD_FIREBASE_PROJECT_ID,
    storageBucket: process.env.REACT_APP_PROD_FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.REACT_APP_PROD_FIREBASE_MESSAGING_SENDER_ID,
    appId: process.env.REACT_APP_PROD_APP_ID,
};

const env = envDetector();

const config = env === 'DEV'
    ? devConfig
    : env === 'TEST'
        ? testConfig
        : prodConfig;

if (!firebase.apps.length) {
    firebase.initializeApp(config);
}

let db: firebase.firestore.Firestore;
let auth: firebase.auth.Auth;
let storage: firebase.storage.Storage;

if (typeof window !== 'undefined') {
    auth = firebase.auth();
    db = firebase.firestore();
    storage = firebase.storage();
}

export { db, auth, storage, firebase };
