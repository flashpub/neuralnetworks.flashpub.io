import { SliceCaseReducers, ValidateSliceCaseReducers } from '@reduxjs/toolkit';
import { uniq, sortBy } from 'lodash';

import { createGenericSlice } from 'helpers';
import ClaimDataModel from 'models/data/claim';
import TermDataModel from 'models/data/term';
import PubDataModel from 'models/data/pub';

export const createFormGenericSlice = <T, Reducers extends SliceCaseReducers<GenericState<T>>>({
    name = '',
    initialState,
    reducers,
}: {
    name: string;
    initialState: GenericState<T>;
    reducers: ValidateSliceCaseReducers<GenericState<T>, Reducers>;
}) => {
    return createGenericSlice({
        name,
        initialState,
        reducers: {
            // TODO figure out how to type state properly
            set: (state: any, action) => {
                const { form, field, value } = action.payload;
                const split = field.split('.');
                const depth = split.length;
                let lastField;

                if (field === 'figures') {
                    lastField = state.data[form][split];
                    state.data[form][split] = sortBy(uniq([...lastField, value]), (f) => f.order);
                    return;
                }

                switch (depth) {
                    case 1:
                        lastField = state.data[form][split];
                        state.data[form][split] = Array.isArray(lastField) ? uniq([...lastField, value]) : value;
                        break;
                    case 2:
                        lastField = state.data[form][split[0]][split[1]];
                        state.data[form][split[0]][split[1]] = Array.isArray(lastField)
                            ? uniq([...lastField, value])
                            : value;
                        break;
                    case 3:
                        lastField = state.data[form][split[0]][split[1]][split[2]];
                        state.data[form][split[0]][split[1]][split[2]] = Array.isArray(lastField)
                            ? uniq([...lastField, value])
                            : value;
                        break;
                    case 4:
                        lastField = state.data[form][split[0]][split[1]][split[2]][split[3]];
                        state.data[form][split[0]][split[1]][split[2]][split[3]] = Array.isArray(lastField)
                            ? uniq([...lastField, value])
                            : value;
                        break;
                    default:
                        throw new Error("Field value doesn't meet the requirements");
                }
            },
            sub: (state: any, action) => {
                const { form, field, value } = action.payload;
                const split = field.split('.');
                const depth = split.length;

                switch (depth) {
                    case 1:
                        state.data[form][split] = value;
                        break;
                    case 2:
                        state.data[form][split[0]][split[1]] = value;
                        break;
                    case 3:
                        state.data[form][split[0]][split[1]][split[2]] = value;
                        break;
                    case 4:
                        state.data[form][split[0]][split[1]][split[2]][split[3]] = value;
                        break;
                    default:
                        throw new Error("Field value doesn't meet the requirements");
                }
            },
            cut: (state: any, action) => {
                const { form, field, value } = action.payload;
                const split = field.split('.');
                const depth = split.length;
                let lastField;
                let initialField;

                switch (depth) {
                    case 1:
                        lastField = state.data[form][split];
                        initialField = (initialState as any).data[form][split];
                        state.data[form][split] = Array.isArray(lastField)
                            ? lastField.filter((v) => v.id !== value.key).filter((v) => v.id !== value)
                            : initialField;
                        break;
                    case 2:
                        lastField = state.data[form][split[0]][split[1]];
                        initialField = (initialState as any).data[form][split[0]][split[1]];
                        state.data[form][split[0]][split[1]] = Array.isArray(lastField)
                            ? lastField.filter((v) => v.id !== value.key).filter((v) => v.id !== value)
                            : initialField;
                        break;
                    case 3:
                        lastField = state.data[form][split[0]][split[1]][split[2]];
                        initialField = (initialState as any).data[form][split[0]][split[1]][split[2]];
                        state.data[form][split[0]][split[1]][split[2]] = Array.isArray(lastField)
                            ? lastField
                                .filter((v) => v && v.id ? (v.id !== value.key) : (v !== value.key))
                                .filter((v) => v && v.id ? (v.id !== value) : (v !== value))
                            : initialField;
                        break;
                    case 4:
                        lastField = state.data[form][split[0]][split[1]][split[2]][split[3]];
                        initialField = (initialState as any).data[form][split[0]][split[1]][split[2]][split[3]];
                        state.data[form][split[0]][split[1]][split[2]][split[3]] = Array.isArray(lastField)
                            ? lastField.filter((v) => v.id !== value.key).filter((v) => v.id !== value)
                            : initialField;
                        break;
                    default:
                        throw new Error("Field value doesn't meet the requirements");
                }
            },
            clear: (state: any, action) => {
                switch (action.payload) {
                    case 'claim':
                        state.data.claim = ClaimDataModel;
                        break;
                    case 'term1':
                        state.data.term1 = TermDataModel;
                        break;
                    case 'term2':
                        state.data.term2 = TermDataModel;
                        break;
                    case 'pub':
                        state.data.pub = PubDataModel;
                        break;
                    case 'draft':
                        state.data.draft = PubDataModel;
                        break;
                    default:
                        throw new Error("Field value doesn't meet the requirements");
                }
            },

            ...reducers,
        },
    });
};
