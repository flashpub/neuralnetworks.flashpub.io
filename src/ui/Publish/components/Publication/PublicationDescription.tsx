import * as React from 'react';

import { TextEditor } from 'ui/components';
import { FPEditIcon } from 'ui/elements';
import { StyledPublicationSheetDescriptionWrapper } from 'ui/Publish/styles';

type Props = {
    pub: PubDefinition;
    editMode: boolean;
    onBlur?(value: string): void;
    onChange?(value: string): void;
};

export const PublicationDescription: React.FC<Props> = ({ pub, editMode, onBlur, onChange }) => {
    return (
        <StyledPublicationSheetDescriptionWrapper>
            <TextEditor
                required
                pub={pub}
                onBlur={onBlur}
                onChange={onChange}
                editMode={editMode}
                placeholder="Write your report and reference figure panels (a, b, c ...) here."
            />
            {editMode && <FPEditIcon />}
        </StyledPublicationSheetDescriptionWrapper>
    );
};
