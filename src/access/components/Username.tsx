import * as React from 'react';
import { Field, FieldProps, FormikProps } from 'formik';
import styled from 'styled-components';

import { Color, Spacing } from 'app/assets';

import { AccessStep } from 'access/utils';
import { ReginCredentials } from 'access/types';
import { Button, Title } from 'access/components/elements';
import { FUIInput } from '../../app/components/Shared';


type Props = {
    formikBag: FormikProps<ReginCredentials>,
    setAccessStep(accessStep: AccessStep): void,
};

const Username: React.FunctionComponent<Props> = (props: Props) => {
    const handleSetAccessStep = () => props.setAccessStep(AccessStep.REGISTER);
    const disableButton = props.formikBag.values.name === '';
    return (
        <>
            <Title>What's your name?</Title>
            <Field name="name" render={({ field }: FieldProps<ReginCredentials>) =>
                <FUIInput placeholder="name" type="text" {...field}/>
            }/>
            <ContinueButton
                text="Next"
                buttonType="text"
                disabled={disableButton}
                onClick={handleSetAccessStep}
            />
        </>
    );
};

const ContinueButton = styled(Button)<{ disabled?: boolean }>` && {
    margin-top: ${Spacing.XXL};
    cursor: ${props => props.disabled ? 'not-allowed' : 'pointer'};
    color: ${props => props.disabled ? Color.midgrey : Color.flashpub};
}`;

export default Username;
