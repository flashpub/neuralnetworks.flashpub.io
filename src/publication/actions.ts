import { action } from 'app/utils';
import { ClaimDefinition, PubDefinition } from 'app/types';

export const PUBLICATION_OPERATION_FAIL = action('publication/PUBLICATION_OPERATION_FAIL');
export const PUBLICATION_OPERATION_START = action('publication/PUBLICATION_OPERATION_START');
export const PUBLICATION_OPERATION_SUCCESS = action('publication/PUBLICATION_OPERATION_SUCCESS');

export const SET_PUB_CLAIM = action<ClaimDefinition>('publication/SET_PUB_CLAIM');
export const GET_PUBLICATION = action<PubDefinition>('publication/GET_PUBLICATION');
export const SET_PICKED_PUBLICATION = action<{ id: string }>('publication/SET_PICKED_PUBLICATION');

export const UPDATE_PUBLICATION_AB = action<{ update: any }>('publication/UPDATE_PUBLICATION_AB');
export const UPDATE_PUBLICATION_QR = action<{ update: any }>('publication/UPDATE_PUBLICATION_QR');
export const UPDATE_PUBLICATION_BKMRK = action<{ update: any }>('publication/UPDATE_PUBLICATION_BKMRK');

export const EDIT_OPERATION_FAIL = action('publication/EDIT_OPERATION_FAIL');
export const EDIT_OPERATION_START = action('publication/EDIT_OPERATION_START');
export const EDIT_OPERATION_SUCCESS = action('publication/EDIT_OPERATION_SUCCESS');
