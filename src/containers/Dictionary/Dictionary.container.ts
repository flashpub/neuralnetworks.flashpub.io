import * as React from 'react';
import { useDispatch } from 'react-redux';

import * as Dictionary from 'containers/Dictionary';

export const DictionaryContainer = () => {
    const dispatch = useDispatch();

    React.useEffect(() => {
        dispatch(Dictionary.getDictionary());
    }, [dispatch]);

    return null;
};
