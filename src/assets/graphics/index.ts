// @ts-ignore
import { ReactComponent as GenericPub } from 'assets/graphics/genericpub.svg';
// @ts-ignore
import { ReactComponent as ClinicalCaseReport } from 'assets/graphics/clinicalcasereport.svg';

export {
    GenericPub,
    ClinicalCaseReport,
};
