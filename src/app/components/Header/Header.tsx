import * as React from 'react';
import styled from 'styled-components';
import { RouteComponentProps, withRouter } from 'react-router';
import { Grid as UIGrid, Col as UICol, Row as UIRow } from 'react-styled-flexboxgrid';

import { project } from 'app/config';
import { Color, Font } from 'app/assets';
import { PortalView, useWindowWidth, Viewport } from 'app/utils';

import { PublishStep } from 'publish/utils';
import { UserDefinition } from 'access/types';
import { SetDictionary } from 'dictionaries/types';

import HeaderButtons from './HeaderButtons';
import { CommunityDefinition, Content, PubDefinition, SystemGatesDefinition } from '../../types';


type Props = {
    isAuth: boolean,
    community: CommunityDefinition | null,
    campaign: any,
    campaigns: any,
    gates: SystemGatesDefinition | null,
    user: Optional<UserDefinition> | null,
    publishStep: PublishStep | null,

    setContentType(type: Content): void,
    setPortalView(view: PortalView): void,
    setPublishStep(step: PublishStep | null): void,
    setCurrentDictionary(current: SetDictionary | null): void,
    setCurrentPub(pub: PubDefinition | null): void,
} & RouteComponentProps;

const Header: React.FunctionComponent<Props> = (props: Props) => {
    const isMobile = useWindowWidth() <= Viewport.MOBILE;
    const goToHomepage = () => {
        props.history.push('/');
        props.setPublishStep(null);
        props.setCurrentPub(null);
        props.setPortalView(PortalView.DEFAULT);
        props.setCurrentDictionary({
            id: project.id,
            name: project.name,
        });
        localStorage.clear();
    };
    const goToFlashpub = () => window.open('https://flashpub.io');

    return (
        <HeaderWrapper>
            <Grid fluid>
                <UIRow bottom="xs">
                    <Logo xs={isMobile ? 2 : 3} onClick={goToFlashpub}>
                        <h2>{isMobile ? 'fp' : 'flashpub'}</h2>
                    </Logo>

                    <Domain xs={isMobile ? 8 : 6}>
                        <h1 onClick={goToHomepage}>{project.name}</h1>
                    </Domain>

                    <Actions xs={isMobile ? 2 : 3}>
                        <HeaderButtons
                            user={props.user}
                            isMobile={isMobile}
                            gates={props.gates}
                            isAuth={props.isAuth}
                            community={props.community}
                            campaign={props.campaign}
                            campaigns={props.campaigns}
                            publishStep={props.publishStep}
                            setPortalView={props.setPortalView}
                            setPublishStep={props.setPublishStep}
                            setContentType={props.setContentType}
                        />
                    </Actions>
                </UIRow>
            </Grid>
        </HeaderWrapper>
    );
};


const HeaderWrapper = styled.header`
    padding: 20px 0 24px;
    border-bottom: 1px dashed ${Color.flashpub_t15};
`;
const Grid = styled(UIGrid)` && {
    z-index: 10; 
    max-width: 1100px;
    position: relative;
}`;
const Logo = styled(UICol)` && {
    padding: 0;
    cursor: pointer;
    h2 {
        margin: 0;
        font-weight: 500;
        padding-bottom: 1px;
        color: ${Color.grey};
        font-family: ${Font.heading};

        @media (max-width: 576px) {
            font-size: 30px;
        }
    }
}`;
const Domain = styled(UICol)` && {
    padding: 0;
    h1 {
        padding: 0;
        margin: 0 auto;
        cursor: pointer;
        font-weight: 500;
        width: fit-content;
        text-align: center;
        color: ${Color.community};
        font-family: ${Font.heading};

        @media (max-width: 576px) {
            font-size: 24px;
            padding-bottom: 3px;
        }
    }
}`;
const Actions = styled(UICol)` && {
    padding: 0;
    text-align: right;
}`;

export default withRouter(Header);
