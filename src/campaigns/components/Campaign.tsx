import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../app/store';
import { campaigns } from '../client';
import * as CampaignsAction from '../actions';
import styled from 'styled-components/macro';
import { Color, Font, Spacing } from '../../app/assets';
import { Popover, Position } from '@blueprintjs/core';
import { FUIButton } from '../../FUI/elements';
import { Map } from '../../app/components/Shared';
import { PublishContainer } from '../../publish';
import { Content } from '../../app/types';
import { PortalView } from '../../app/utils';
import snakeCase from 'lodash/snakeCase';
import * as PublishAction from '../../publish/actions';
import * as AppAction from '../../app/actions';
import { RouteComponentProps } from 'react-router-dom';


type Props = {} & RouteComponentProps;

export const Campaign: React.FC<Props> = ({ history }) => {
    const [isOpen, setIsOpen] = React.useState(false);
    const storeDispatch = useDispatch();
    const user = useSelector<RootState, any>(state => state.access.user);
    const isAuth = useSelector<RootState, any>(state => state.app.isAuth);
    const currentCampaign = useSelector<RootState, any>(state => state.campaigns.current);

    const onSetContentType = (type: Content) => {
        if (isAuth && user) {
            const label = type.contentTypeLabel;
            storeDispatch(PublishAction.SET_CONTENT_TYPE.use(type));
            storeDispatch(AppAction.SET_PORTAL_VIEW.use({ view: PortalView.PUBLISH }));
            history.push(`/publish/${snakeCase(label)}`);
            localStorage.removeItem('conditions');
        } else {
            window.alert('You need to log in to continue');
        }
    };

    React.useEffect(() => {
        if (!currentCampaign) {
            const path = window.location.pathname.split('/campaigns/')[1];

            (async () => {
                try {
                    const resp = await campaigns.get(path);
                    storeDispatch(CampaignsAction.SET_CURRENT_CAMPAIGN.use({ current: resp[0] }));
                } catch (err) {
                    storeDispatch(CampaignsAction.CAMPAIGN_OPERATION_FAIL.use());
                }
            })();
        }
    }, [currentCampaign]);

    if (currentCampaign) {
        const handleDropdown = (state: boolean) => setIsOpen(state);
        const listResources = currentCampaign.resources.map((c: any, i: number) => (
            <MenuItem key={i}>
                <a href={c.url} target="_blank">{c.title}</a>
            </MenuItem>
        ));
        const campaignTypes =
            <List>
                <MenuItem>Resources:</MenuItem>
                {listResources}
            </List>;
        const listCampaignContents = Object.values(currentCampaign.contentTypes).map((t: any, i) =>
            <PublishCaseButton key={i} text={`Publish a ${t.contentTypeLabel}`} onClick={() => onSetContentType(t)} />
        );

        return (
            <CampaignWrapper>
                <Popover
                    isOpen={isOpen}
                    onInteraction={handleDropdown}
                    popoverClassName="pt-popover-content-sizing"
                    content={campaignTypes}
                    position={Position.BOTTOM}
                >
                    <Title>{currentCampaign.title}</Title>
                </Popover>
                <CampaignDescription>{currentCampaign.description}</CampaignDescription>
                <MapWrapper>
                    <MapBox>
                        <Map
                            updatedOn={currentCampaign.updatedOn}
                            markers={currentCampaign.campaignData}
                            pubs={currentCampaign.metrics.pubCount}
                        />
                    </MapBox>
                    <PublishButtons>
                        <PublishContainer>
                            {listCampaignContents}
                        </PublishContainer>
                    </PublishButtons>
                </MapWrapper>
            </CampaignWrapper>
        )
    } else return null;
};

const CampaignWrapper = styled.div`
    display: flex;
    align-items: center;
    flex-direction: column;
    margin: ${Spacing.XXL} 0;
    font-family: ${Font.text};
`;
const MapWrapper = styled.div`
    height: 100%;
    width: 100vw;
    position: relative;
`;
const MapBox = styled.div`
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
`;
const Title = styled.div`
    cursor: pointer;
    font-size: 30px;
    text-align: center;
    color: ${Color.community};
    font-family: ${Font.pub_title};
`;
const CampaignDescription = styled.div`
    font-size: 16px;
    font-weight: 200;
    text-align: center;
    color: ${Color.grey};
    font-family: ${Font.text};
    margin: ${Spacing.XXXL} 0 ${Spacing.XL};
`;
const PublishButtons = styled.div`
    top: 20px;
    left: 50%;
    z-index: 1;
    display: flex;
    position: absolute;
    flex-direction: row;
    transform: translateX(-50%);
`;
const List = styled.div`
    min-width: 110px;
    text-align: center;
    padding: 0 ${Spacing.M};
`;
const MenuItem = styled.div`
    font-size: 14px;
    position: relative;
    list-style-type: none;
    font-family: ${Font.pub_title};
    color: ${Color.grey};
    margin:${Spacing.L};
    a {
        :hover {
            cursor: pointer;
            color: ${Color.grey};
        };
    }
`;
const PublishCaseButton = styled(FUIButton)` && {
    margin:${Spacing.XXL};
    width: fit-content;
    
    &:hover {
        background-color: ${Color.community_t75};
    }
}`;
