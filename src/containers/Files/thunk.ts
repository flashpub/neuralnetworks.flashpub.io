import { message } from 'antd';
import { Dispatch } from 'redux';

import { AppThunk } from 'store';
import { firebase } from 'configs';
import * as Files from 'containers/Files';
import * as Pub from 'containers/Pub';
import * as API from 'api';

export function uploadFile(file: File, pubId: string, figure: Figure, replace: Figure): AppThunk {
    return (dispatch: Dispatch) => {
        dispatch(Files.status.loading());
        const id = replace ? replace.id : figure.id;
        const uploadTask = firebase.storage.ref().child(`figures/${pubId}/${id}`).put(file);
        return uploadTask.on(
            'state_changed',
            (snap) => {
                const progress = (snap.bytesTransferred / snap.totalBytes) * 100;
                dispatch(Files.progress(progress));
            },
            (err) => {
                dispatch(Files.status.failure(err.message));
                message.error('Something went wrong during image upload');
            },
            () =>
                uploadTask.snapshot.ref
                    .getDownloadURL()
                    .then((url) => {
                        dispatch(Files.url(url));
                        return url;
                    })
                    .then((url) => {
                        const fig = {
                            ...figure,
                            url,
                            id,
                        };
                        if (replace) {
                            dispatch(Pub.form.cut({ form: 'draft', field: 'figures', value: replace.id }));
                        }
                        dispatch(Pub.form.set({ form: 'draft', field: 'figures', value: fig }));
                        dispatch(Files.status.success());
                        message.success('Image uploaded successfully');
                    })
        );
    };
}
export function deleteFileFolder(pubId: string): AppThunk | Promise<string> {
    return (dispatch: Dispatch) => {
        return API.file.deleteAll(pubId).then(() => 'OK').catch((err) => dispatch(Files.status.failure(err.message)));
    };
}
export function deleteFile(fileName: string, pubId: string): AppThunk | Promise<string> {
    return (dispatch: Dispatch) => {
        return API.file.delete(fileName, pubId).then(() => 'OK').catch((err) => dispatch(Files.status.failure(err.message)));
    };
}
