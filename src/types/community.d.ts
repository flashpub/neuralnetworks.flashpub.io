interface CommunityContainerState {
    community: CommunityDefinition | null;
}
type CommunityMetrics = {
    readonly pubCount: number;
    readonly userCount: number;
    readonly subCommunityCount: number;
};
type Legal = {
    readonly description: string;
    readonly moreInfo: string;
};
type ContentTypes = {
    [key: string]: CommunityContentType;
};
interface CommunityDefinition {
    name: string;
    dictionaryId: string;
    legal?: {
        [key: string]: Legal;
    } | null;
    createdOn: number;
    metrics: CommunityMetrics;
    contentTypes: ContentTypes;
    loadingTaglines: string[];
    topSubCommunities: string[];
    quickReviewTemplates: string[];
}
