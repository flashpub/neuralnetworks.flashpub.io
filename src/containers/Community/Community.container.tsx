import * as React from 'react';
import { useDispatch } from 'react-redux';

import * as API from 'api';
import * as Community from 'containers/Community';
import { project } from 'configs';

export const CommunityContainer = () => {
    const dispatch = useDispatch();

    React.useEffect(() => {
        const populateCommunity = async () => {
            dispatch(Community.status.loading());
            try {
                const resp = await API.community.load(project.community.name);
                if (resp.exists) {
                    dispatch(Community.populate(resp.data()));
                    dispatch(Community.status.success());
                }
            } catch (err) {
                dispatch(Community.status.failure(err.message));
            }
        };
        populateCommunity();
    }, [dispatch]);

    return null;
};
