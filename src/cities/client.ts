import api, { API_VERSION } from 'app/api';

const version = API_VERSION.content;

export const cities = {
    getForPrefix(namePrefix: string): Promise<any> {
        return api
            .get(`/content/${version}/cities?namePrefix=${namePrefix}`)
            .then(response => response.data);
            //.catch(error => alertHandler(AlertType.ERROR, error.response.data));
    },
};
