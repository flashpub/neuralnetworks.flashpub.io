import React, { useState, useRef } from 'react';
import styled from 'styled-components';
import capitalize from 'lodash/capitalize';
import { Field, FieldProps, FormikProps } from 'formik';
import { Grid as UIGrid, Col as UICol, Row as UIRow } from 'react-styled-flexboxgrid';
import { Classes, Icon, Popover, Tooltip, Position } from '@blueprintjs/core';

import { Asset, PubDefinition, Assets } from 'app/types';
import { Color, Font, Media, Spacing } from 'app/assets';
import { FUIButton, FUIInput } from 'app/components/Shared/Elements';


type Props = {
    assets: Assets,
    editMode: boolean,
    formikBag?: FormikProps<any>,
    publication?: PubDefinition,
    assetsChanged?: {
        code: boolean,
        datasets: boolean,
        protocols: boolean,
        references: boolean
    },
    setAssetsChanged?(assetsChanged: {
        code: boolean,
        datasets: boolean,
        protocols: boolean,
        references: boolean
    }): void,
    removeAsset(type: string, id: string | undefined): void,
    addAsset(type: string, asset: { name: string; url: string }): void,
};

const Metadata: React.FC<Props> = (
    { formikBag, addAsset, assets, removeAsset, publication, editMode, assetsChanged, setAssetsChanged }
) => {
    const renderFieldInput = (field: any, placeholder: string) => (
        <FUIInput placeholder={placeholder} type="text" {...field} />
    );
    const renderReferencesSelect = (field: any, placeholder: string) => (
        <select placeholder={placeholder} {...field}>
            <option value="" disabled selected>{placeholder}</option>
            <option value="supports">Supports</option>
            <option value="refutes">Refutes</option>
            <option value="extends">Extends</option>
        </select>
    );

    const [assetName, setAssetName] = useState('');
    const [assetUrl, setAssetUrl] = useState('');
    const editModeSelect = useRef<HTMLSelectElement>(null);
    const editModeInputOne = useRef<HTMLInputElement>(null);
    const editModeInputTwo = useRef<HTMLInputElement>(null);

    const addAssetsForm = (type: string) => {
        let pub: any;
        if (formikBag !== undefined) {
            pub = formikBag.values.pub;
        } else {
            pub = publication;
            pub.asset = { name: assetName, url: assetUrl };
        }
        const isDisabled = !pub.asset.name || !pub.asset.url;

        const bySource = () => {
            let name = 'name', url = 'url';
            switch (type) {
                case 'protocols':
                    name = 'protocol name';
                    url = 'e.g. doi from protocols.io';
                    break;
                case 'code':
                    name = 'codebase name';
                    url = 'e.g. url from codeocean.com';
                    break;
                case 'datasets':
                    name = 'dataset name';
                    url = 'e.g. doi from figshare.com';
                    break;
                case 'references':
                    name = 'choose a reference';
                    url = 'doi';
                    break;
                default:
                    return { name, url };
            }
            return { name, url };
        };
        const onAddAsset = () => {
            addAsset(type, {
                name: pub.asset.name,
                url: pub.asset.url,
            });

            if (!editMode) {
                pub.asset = { name: '', url: '' };
            } else {
                assetsChanged && setAssetsChanged && setAssetsChanged({ ...assetsChanged, [type]: true })
                setAssetName('');
                setAssetUrl('');

                if (editModeInputOne !== null && editModeInputOne.current !== null)
                    editModeInputOne.current.value = '';

                if (editModeInputTwo !== null && editModeInputTwo.current !== null)
                    editModeInputTwo.current.value = '';

                if (editModeSelect !== null && editModeSelect.current !== null)
                    editModeSelect.current.value = '';
            }
        };

        return (
            <AssetForm>
                {editMode ?
                    <>
                        <FUIInput ref={editModeInputOne} placeholder={bySource().url} type="text" onChange={(e) => setAssetUrl(e.target.value)} />
                        {type === "references" ?
                            <select ref={editModeSelect} defaultValue={""} onChange={(e) => setAssetName(e.target.value)}>
                                <option value="" disabled>{bySource().name}</option>
                                <option value="supports">Supports</option>
                                <option value="refutes">Refutes</option>
                                <option value="extends">Extends</option>
                            </select> :
                            <FUIInput ref={editModeInputTwo} placeholder={bySource().name} type="text" onChange={(e) => setAssetName(e.target.value)} />
                        }
                        <AddAssetButton
                            text="add"
                            type="button"
                            onClick={onAddAsset}
                            disabled={isDisabled}
                            buttonType="outlined"
                        />
                    </> :
                    <>
                        <Field
                            name="pub.asset.url"
                            render={({ field }: FieldProps) => renderFieldInput(field, bySource().url)}
                        />
                        <Field
                            name="pub.asset.name"
                            render={({ field }: FieldProps) => type === 'references'
                                ? renderReferencesSelect(field, bySource().name)
                                : renderFieldInput(field, bySource().name)
                            }
                        />
                        <AddAssetButton
                            text="add"
                            type="button"
                            onClick={onAddAsset}
                            disabled={isDisabled}
                            buttonType="outlined"
                        />
                    </>
                }
            </AssetForm>
        );
    };

    const renderMetadataElement = (type: string) => (
        <>
            <span>{capitalize(type)}</span>
            <Popover
                content={addAssetsForm(type)}
                position={Position.RIGHT}
                popoverClassName={Classes.POPOVER_CONTENT_SIZING}
                canEscapeKeyClose={true}
                autoFocus={true}
            >
                <Tooltip
                    content={`Add ${type}`}
                    position={Position.RIGHT}
                >
                    <AddAssetIcon iconSize={16} icon="add" />
                </Tooltip>
            </Popover>
        </>
    );
    const assetTile = (asset: Asset, action: (id: string | undefined) => void, i: number, type?: string) => {
        const prepareHref = (url: string) => {
            if (url.includes('http')) { return url }
            else { return `https://${url}` }
        };
        return (
            <AssetWrapper key={i}>
                <AssetLink target="_blank" href={prepareHref(asset.url)}>
                    <Tooltip
                        content={type === 'references' ? asset.name : (`${asset.name} | ${asset.url}`)}
                        position={Position.RIGHT}
                    >
                        {type === 'references' ? asset.url : asset.name}
                    </Tooltip>
                </AssetLink>
                <AssetRemoveIcon onClick={() => action(asset.id)} icon="delete" iconSize={14} />
            </AssetWrapper>
        );
    };

    const printAssetTiles = (type: string) => assets[type].map((asset: Asset, i: number) => {

        if (!asset) return null;
        const onRemoveAsset = () => {
            removeAsset(type, asset.id);
            assetsChanged && assetsChanged[type] ?
                assetsChanged && setAssetsChanged && setAssetsChanged({ ...assetsChanged, [type]: false }) :
                assetsChanged && setAssetsChanged && setAssetsChanged({ ...assetsChanged, [type]: true });
        }
        return assetTile(asset, onRemoveAsset, i, type);
    });

    return (
        <Wrapper>
            <Grid fluid>
                <Row>
                    <Col xs={12} sm>
                        {renderMetadataElement('references')}
                        {printAssetTiles('references')}
                    </Col>

                    <Col xs={12} sm>
                        {renderMetadataElement('protocols')}
                        {printAssetTiles('protocols')}
                    </Col>

                    <Col xs={12} sm>
                        {renderMetadataElement('code')}
                        {printAssetTiles('code')}
                    </Col>

                    <Col xs={12} sm>
                        {renderMetadataElement('datasets')}
                        {printAssetTiles('datasets')}
                    </Col>
                </Row>
            </Grid>
        </Wrapper>
    );
};
const AddAssetIcon = styled(Icon)`
    cursor: pointer;
    visibility: hidden;
    color: ${Color.midgrey};
    margin-left: ${Spacing.S};
    :hover {
        color: ${Color.grey};
    }
    :focus {
        outline: none;
    };
`;
const AssetRemoveIcon = styled(Icon)`
    cursor: pointer;
    visibility: hidden;
    color: ${Color.midgrey};
    margin-left: ${Spacing.S};
    margin-bottom: ${Spacing.SM};
    :hover {
        color: ${Color.grey};
    }
    :focus {
        outline: none;
    };
`;
const Grid = styled(UIGrid)` && {
    padding: 0;
}`;
const Row = styled(UIRow)` && {
    padding: 0;
}`;
const Col = styled(UICol)` && {
    padding: 0;
    text-align: start;
    
    &:hover ${AddAssetIcon} {
        visibility: visible;
    }
}`;
const Wrapper = styled.div`
    text-align: center;
    color: ${Color.grey};
    padding: ${Spacing.ML};
    font-family: ${Font.text};
    margin-top: ${Spacing.XL};
    margin-bottom: ${Spacing.XXXL};
`;
const AssetForm = styled.div`
    input, select {
        width: 100%;
        height: 28px;
        font-size: 14px;
        text-align: start;
        color: ${Color.grey};
        margin: ${Spacing.S} 0;
        padding: 0 ${Spacing.M};
        background-color: transparent;
        border: 1px solid ${Color.grey};
    }
`;
const AddAssetButton = styled(FUIButton) <{ disabled: boolean }>` && {
    width: 100%;
    height: 28px;
    font-size: 14px;
    margin: ${Spacing.S} 0;
    cursor: ${props => props.disabled && 'not-allowed'};
}`;
const AssetWrapper = styled.div`
    position: relative;
    border-radius: 4px;
    margin: ${Spacing.M} 0;
    font-family: ${Font.mono};
    
    &:hover ${AssetRemoveIcon} {
        visibility: visible;
    }
`;
const AssetLink = styled.a`
    width: 300px;
    font-weight: 200;
    white-space: nowrap;
    display: inline-block;
    text-overflow: ellipsis;
    color: ${Color.community};
    overflow: hidden !important;
    &:hover {
        color: ${Color.flashpub};
    }
    ${Media.tablet} {
        width: 150px;
    }
`;

export default Metadata;
