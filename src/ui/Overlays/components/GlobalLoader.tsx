import * as React from 'react';
import { useTimeout } from 'react-use';
import { navigate } from '@reach/router';
import styled, { keyframes } from 'styled-components/macro';
// import Particles, { IParticlesParams } from 'react-particles-js';

import { Color, Font, Spacing } from 'assets/styles';

type Props = {
    slogan?: string;
    loading?: boolean;
    cb?: boolean;
    cbURL?: string;
    cbTimer?: number;
};

// const particlesParams: IParticlesParams = {
//     particles: {
//         number: { value: 5 },
//     },
// };

export const GlobalLoader: React.FC<Props> = ({ loading = true, slogan, cbURL, cbTimer = 3000, cb = false }) => {
    const [isReady] = useTimeout(cbTimer);

    React.useEffect(() => {
        if (cb && cbURL) {
            isReady() ? navigate(cbURL) : console.log();
        }
    });

    return loading ? (
        <Overlay>
            <Wrapper>
                <Slogan>{slogan || 'Loading...'}</Slogan>
                <AnimatedBackground>
                    <AnimatedLogo>
                        {/*<Particles canvasClassName="canvas" params={particlesParams} />*/}
                        <Text>fp</Text>
                    </AnimatedLogo>
                </AnimatedBackground>
            </Wrapper>
        </Overlay>
    ) : null;
};

const rotate = keyframes`
  from { transform: rotate(0deg) }
  to { transform: rotate(360deg) }
`;
const Overlay = styled.div`
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    display: flex;
    z-index: 10000;
    position: absolute;
    align-items: center;
    justify-content: center;
    background-color: rgba(255, 255, 255, 0.92);
`;
const Wrapper = styled.div`
    display: flex;
    align-items: center;
    flex-direction: column;
    justify-content: center;
`;
const Slogan = styled.div`
    font-size: 20px;
    font-weight: 200;
    width: fit-content;
    text-align: center;
    color: ${Color.grey};
    font-family: ${Font.text};
    margin: auto auto ${Spacing._24};
`;
const AnimatedBackground = styled.div`
    width: 160px;
    height: 160px;
    display: flex;
    overflow: hidden;
    position: relative;
    align-items: center;
    border-radius: 25px;
    justify-content: center;
    :after {
        width: 150%;
        content: '';
        z-index: -1;
        height: 100px;
        position: absolute;
        transform: translate(-50%, -50%);
        animation: ${rotate} 2s linear infinite;
        background: linear-gradient(90deg, rgba(255, 255, 255, 0.7) 50%, ${Color.flashpub_t75} 100%);
    }
`;
const AnimatedLogo = styled.div`
    width: 150px;
    height: 150px;
    display: flex;
    position: relative;
    align-items: center;
    border-radius: 20px;
    justify-content: center;
    background-color: ${Color.community};

    div[class='canvas']:first-child {
        width: 150px;
        height: 150px;
        border-radius: 20px;
        overflow: hidden;
    }
`;
const Text = styled.div`
    color: #fff;
    font-size: 60px;
    font-weight: 100;
    position: absolute;
    margin-top: -${Spacing._8};
    font-family: ${Font.header};
`;
