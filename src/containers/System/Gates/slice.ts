import { createGenericSlice } from 'helpers';

const initialState = {
    data: null,
    status: { type: 'PENDING', message: null },
};

export const slice = createGenericSlice({
    name: 'gates',
    initialState: initialState as GenericState<GatesContainerState | null>,
    reducers: {
        populate: (state, action) => {
            state.data = action.payload;
        },
    },
});

export const { populate, ...status } = slice.actions;

export default slice.reducer;
