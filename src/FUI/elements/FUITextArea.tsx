import * as React from 'react';
import autosize from 'autosize';
import styled from 'styled-components';
import { useMeasure, useEnsuredForwardedRef } from 'react-use';

import { Color, Radius, Font, Spacing } from 'app/assets';


export interface FUITextAreaRefs {
    getAreaFocus(): void;
    getAreaValue(): string;

    setAreaValue<T>(value: T): void;
}
interface FUITextAreaProps {
    [propName: string]: any;

    rows?: number
    chars?: number;
    counter?: boolean;
    required?: boolean;
    disabled?: boolean;
    placeholder?: string;
    changeHeight?: boolean;
    onChange?(value: string): any;
}
type ChangeEvent = React.ChangeEvent<HTMLTextAreaElement>


const TextArea: React.RefForwardingComponent<FUITextAreaRefs, FUITextAreaProps> = (
    { rows, chars = 300, counter = false, required, disabled, placeholder, changeHeight, onChange, ...props },
    ref
) => {
    const [value, setValue] = React.useState<string>(props.value);
    const [localRef, setLocalRef] = React.useState<HTMLTextAreaElement | null>(null);

    const areaRef = useEnsuredForwardedRef<HTMLTextAreaElement>(null as unknown as React.MutableRefObject<HTMLTextAreaElement>);
    const [wrapperRef, { width }] = useMeasure();

    React.useEffect(() => {
        localRef && autosize(localRef);
    });

    React.useImperativeHandle(ref, () => ({
        getAreaValue: () => value,
        getAreaFocus: () => areaRef.current && areaRef.current.focus(),

        setAreaValue: (value: any) => setValue(value),
    }));

    const onValueChange = (e: ChangeEvent) => {
        setValue(e.target.value);
        onChange && onChange(e.target.value);
    };

    const isRequired = required && !value;

    return (
        <TextAreaWrapper required={isRequired} ref={wrapperRef}>
            <StyledTextArea
                {...props}
                value={value}
                rows={rows || 1}
                maxLength={chars}
                required={isRequired}
                onChange={onValueChange}
                placeholder={placeholder}
                changeHeight={width > 329}
                ref={(c: React.SetStateAction<HTMLTextAreaElement | null>) => {
                    setLocalRef(c);
                    return areaRef;
                }}
            />
            {counter && <Counter>{chars - value.length}</Counter>}
        </TextAreaWrapper>
    )
};
export const FUITextArea = React.forwardRef(TextArea);


const Counter = styled.div`
    top: 1px;
    right: 1px;
    padding: 1px;
    font-size: 14px;
    position: absolute;
    visibility: hidden;
    color: ${Color.midgrey};
    border-radius: ${Radius};
    background-color: rgba(255,255,255,.9);
`;
const TextAreaWrapper = styled.div<FUITextAreaProps>`
    position: relative;
    &::before {
        top: 2px;
        left: 4px;
        position: absolute;
        color: ${Color.error};
        content: ${({ required }) => required ? "'*'" : "''"};
    }
    &:hover ${Counter} { visibility: visible }
`;
const StyledTextArea = styled.textarea<FUITextAreaProps>`
    width: 100%;
    resize: none;
    font-size: 14px;
    box-shadow: none;
    font-weight: 400;
    border-width: 1px;
    text-align: center;
    border-style: solid;
    color: ${Color.grey};
    padding: ${Spacing.M};
    background-color: #fff;
    border-radius: ${Radius};
    font-family: ${Font.text};
    border-color: ${({ required }) => required ? Color.midgrey : '#fff'};
    &:focus {
        outline: none;
        border-color: ${Color.midgrey};
    }
    &:hover {
        border-color: ${({ required, disabled }) => required
            ? Color.midgrey
            : disabled
                ? 'transparent'
                : Color.midgrey
        };
    }
    &:focus + ${Counter} { visibility: visible; }
    &::placeholder {
        font-weight: 200;
        font-style: italic;
        color: ${Color.midgrey};
    }
`;
