import * as React from 'react';
import styled from 'styled-components';
import lowerCase from 'lodash/lowerCase';
import { useSelector } from 'react-redux';

import { Spacing } from 'app/assets';
import { RootState } from 'app/store';
import { ClaimDefinition, Content } from 'app/types';
import { usePublish, PublishAction } from 'publish';
import { Button } from 'publish/components/Elements';
import { DictionaryDefinition } from 'dictionaries/types';

import { FUIPanel, PublicationSheet } from 'FUI/components';
import { FUIClaimItem } from 'FUI/elements';
import { positiveLine, negativeLine, neutralLine } from 'app/assets';
import { Position, Tooltip } from '@blueprintjs/core';


type SyntheticEvent = React.MouseEvent<HTMLSpanElement, MouseEvent>;


export const PublicationPanel: React.FC = () => {
    const [{ form , panelStack}, dispatch] = usePublish();

    const content = useSelector<RootState, Content | null>(state => state.publish.contentType);
    const claims = useSelector<RootState, ClaimDefinition[] | null>(state => state.feed.claims);
    const dictionaries = useSelector<RootState, DictionaryDefinition[] | null>(state => state.dictionaries.list);

    if (content && dictionaries) {
        const { id, title, claim, description } = form;
        const { firstItem, secondItem, relationship, relationshipType } = claim;
        const firstItemId = firstItem && firstItem.id;
        const secondItemId = (secondItem && secondItem.id) || '';
        const figureRequired = content!.figureRequired;
        const single = content!.claim.type === 'single';
        const isButtonDisabled = !form.title
            || (single && !firstItem)
            || (!single && (!firstItem || !secondItem || !relationship))
            || (figureRequired && !(form.figures[0] && form.figures[0].preview))
            || !form.description;

        const getItem = (item: string) => {
            const getFirstItem = dictionaries.filter(d => d.id === firstItemId)[0];
            const getSecondItem = dictionaries.filter(d => d.id === secondItemId)[0];

            const name = item === 'first'
                ? (getFirstItem ? getFirstItem.name : firstItem.name)
                : (getSecondItem ? getSecondItem.name : secondItem && secondItem.name);

            const longName = item === 'first'
                ? (getFirstItem ? getFirstItem.longName : firstItem.name)
                : (getSecondItem ? getSecondItem.longName :  secondItem && secondItem.name);

            return { name, longName };
        };

        const handleOnClaimClick = (e: SyntheticEvent, c: string) => {
            e.stopPropagation();
            console.log(c);
        };

        const renderClaimItems = () => {
            if (claims) {
                let firstItemName: string | null;
                let secondItemName: string | null;
                const thisPub = claims.filter(c => c.linkedPubIds && c.linkedPubIds.includes(id))[0];

                if (thisPub) {
                    const claimItems = thisPub.claimItems;
                    firstItemName = claimItems.firstItem && claimItems.firstItem.name;
                    secondItemName = claimItems.secondItem && claimItems.secondItem.name;
                } else {
                    firstItemName = lowerCase(getItem('first').name).split(' ').join('');
                    secondItemName = lowerCase(getItem('second').name).split(' ').join('');
                }

                return secondItemName
                    ? (
                        <div onClick={(e) => handleOnClaimClick(e, firstItemName!)}>
                            <FUIClaimItem>{firstItemName}</FUIClaimItem>
                            <Tooltip content={lowerCase(relationship)} position={Position.TOP}>
                                {relationshipType === 'positive'
                                    ? positiveLine
                                    : relationshipType === 'neutral'
                                        ? neutralLine
                                        : negativeLine
                                }
                            </Tooltip>
                            <FUIClaimItem>{secondItemName}</FUIClaimItem>
                        </div>
                    ) : <FUIClaimItem onClick={(e) => handleOnClaimClick(e, firstItemName!)}>{firstItemName}</FUIClaimItem>;
            } return null;
        };

        const previousPanel = () => {
            const panel = panelStack[panelStack.length - 2];
            if (panel && panel.order === 0) return panelStack[0];
            return panel;
        };

        const toNextStep = () => dispatch(PublishAction.SetPublishPanel({
            order: 3,
            title: 'Submit',
        }));

        return (
            <FUIPanel
                textAlign="left"
                title="Publication"
                previousPanel={previousPanel()}
                onReturn={() => dispatch(PublishAction.CutPublishPanel())}
            >
                <PublicationSheet
                    editMode
                    pub={form}
                    pubTitle={title}
                    pubDesc={description}
                    pubClaim={renderClaimItems()}
                />
                <ButtonWrapper>
                    <NextButton
                        type="button"
                        text="Next Step"
                        buttonType="text"
                        onClick={toNextStep}
                        disabled={isButtonDisabled}
                    />
                </ButtonWrapper>
            </FUIPanel>
        );
    } return null;
};


const ButtonWrapper = styled.div`
    left: 0;
    right: 0;
    bottom: 0;
    position: fixed;
    height: fit-content;
    padding: ${Spacing.M} 0;
    background-color: rgba(255,255,255,.6);
`;
const NextButton = styled(Button)`
    background-color: #fff;
`;
