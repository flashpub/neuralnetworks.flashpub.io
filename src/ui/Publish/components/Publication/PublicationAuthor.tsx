import shortid from 'shortid';
import * as React from 'react';
import { Tooltip, Popover } from 'antd';
import { useDispatch } from 'react-redux';

import { useTypedSelector } from 'helpers';
import * as Pub from 'containers/Pub';
import {
    StyledAuthorsWrapper,
    StyledAuthor,
    StyledRemoveIcon,
    StyledFormWrapper,
    StyledTitle,
    StyledContributorField,
    StyledAddContributor,
    StyledAddUser,
} from 'ui/Publish/styles';

type Props = {
    editMode: boolean;
    pub: PubDefinition;
};

const initialContributor = { name: '', email: '', orcid: '' };
export const PublicationAuthor: React.FC<Props> = ({ pub, editMode }) => {
    const dispatch = useDispatch();
    const [open, setOpen] = React.useState(false);
    const [contributor, setContributor] = React.useState<typeof initialContributor>(initialContributor);

    const userProfile = useTypedSelector((state) => state.profile.data.user);

    const handleNameChange = (value: string) => setContributor({ ...contributor, name: value });
    const handleEmailChange = (value: string) => setContributor({ ...contributor, email: value });
    const handleOrcidChange = (value: string) => setContributor({ ...contributor, orcid: value });
    const handleFormOpen = (open: boolean) => {
        setOpen(open);
        !open && setContributor(initialContributor);
    };

    const handleAddContributorClick = () => {
        const user: Author = {
            id: shortid(),
            name: contributor.name,
            email: contributor.email,
            orcid: contributor.orcid,
        };
        setOpen(false);
        setContributor(initialContributor);
        dispatch(Pub.form.set({ form: 'draft', field: 'contributors', value: user }));
    };
    
    const handleAuthorClick = (orcid: string) => {
        const url = `https://orcid.org/${orcid}`;
        orcid && window.open(url, "_blank");
    };

    const renderAddContributorForm = () => {
        const isDisabled = !contributor.name || !contributor.email;
        return (
            <StyledFormWrapper>
                <StyledTitle>Add a contributor:</StyledTitle>
                <StyledContributorField
                    placeholder="name"
                    value={contributor.name}
                    onChange={handleNameChange}
                    required
                />
                <StyledContributorField
                    placeholder="email"
                    value={contributor.email}
                    onChange={handleEmailChange}
                    required
                />
                <StyledContributorField placeholder="orcid" value={contributor.orcid} onChange={handleOrcidChange} />
                <StyledAddContributor
                    label="add"
                    buttonType="default"
                    disabled={isDisabled}
                    onClick={handleAddContributorClick}
                />
            </StyledFormWrapper>
        );
    };

    const printContributors = (pub.contributors || []).map((c, i) => {
        const removeContributor = () =>
            editMode && dispatch(Pub.form.cut({ form: 'draft', field: 'contributors', value: c.id }));

        return (
            <div key={i} style={{ display: 'flex', alignItems: 'center', width: 'fit-content' }}>
                <StyledAuthor onClick={removeContributor} co={editMode} id="author">
                    {editMode ? (
                        <Tooltip title="remove" placement="top" mouseEnterDelay={0} mouseLeaveDelay={0}>
                            <div>
                                <span onClick={() => handleAuthorClick(c.orcid ? c.orcid : pub.author?.orcid)}>{c.name}</span>
                                <StyledRemoveIcon />
                            </div>
                        </Tooltip>
                    ) : (
                        <span onClick={() => handleAuthorClick(c.orcid ? c.orcid : pub.author?.orcid)}>{c.name}</span>
                    )}
                </StyledAuthor>
            </div>
        );
    });

    return (
        <StyledAuthorsWrapper>
            <StyledAuthor id="main-author" onClick={() => handleAuthorClick(pub.author?.orcid)}>by {pub.author?.name || userProfile?.name}</StyledAuthor>
            {printContributors}
            {editMode && (
                <Popover
                    visible={open}
                    trigger="click"
                    placement="right"
                    onVisibleChange={handleFormOpen}
                    content={renderAddContributorForm()}
                >
                    <Tooltip
                        mouseEnterDelay={0}
                        mouseLeaveDelay={0}
                        title="add contributor"
                        placement={open ? 'left' : 'right'}
                    >
                        <StyledAddUser />
                    </Tooltip>
                </Popover>
            )}
        </StyledAuthorsWrapper>
    );
};
