import uniq from 'lodash/uniq';
import concat from 'lodash/concat';
import camelCase from 'lodash/camelCase';

import store from 'app/store';
import { Content } from 'app/types';
import { project } from 'app/config';
import { UserDefinition } from 'access/types';


export const preparePubData = (form: PublishStateForm) => {
    const state = store.getState();
    const user: UserDefinition | null = state.access.user;
    const content: Content | null = state.publish.contentType;

    const firstItem = form.claim.firstItem;
    const secondItem = form.claim.secondItem;
    const secondItemId = (secondItem && secondItem.id) || '';
    const isSingle = content!.claim.type === 'single';
    const fetchId: string = `${(form.title).split(' ').join('-').toLowerCase()}_${form.id}`;
    const conditionsList = form.claim.conditions.map(c => {
        return {
            id: c.id,
            name: c.name,
            value: c.value,
            type: c.type,
        };
    });
    const ancestors = concat(form.claim.firstItemAncestors, !isSingle ? form.claim.secondItemAncestors : []);

    return {
        acceptedNotification: false,
        author: {
            id: user && user.uid,
            name: user && user.name,
            orcid: user && user.orcid,
            email: user && user.email,
        },
        contentOrigin: {
            source: project.url,
            originalContent: true,
            url: `https://${project.url}/${fetchId}`,
        },
        claim: {
            id: form.claim.id,
            terms: {
                firstItem: firstItem.name,
                secondItem: secondItem && secondItem.name,
                relationship: form.claim.relationship,
                type: form.claim.relationshipType,
            },
            conditions: conditionsList,
        },
        description: form.description,
        contributors: form.contributors.map(c => ({
            id: c.id,
            name: c.name,
            orcid: c.orcid,
            email: c.email,
        })),
        figures: form.figures.map(f => ({
            id: f.id,
            url: f.url,
            order: f.order,
            type: f.type,
            label: f.label,
            file: f.file,
        })),
        id: form.id,
        isDoiActivated: false,
        metadata: {
            isDeIdentified: form.metadata.isDeIdentified,
            protocols: form.metadata.protocols,
            code: form.metadata.code,
            datasets: form.metadata.datasets,
            references: form.metadata.references,
            datePublished: Date.now(),
            dateUpdated: Date.now(),
            doi: `https://${project.url}/${fetchId}`,
            fetchId,
            allDictionaryIds: isSingle
                ? uniq([...ancestors, project.id, firstItem.id])
                : uniq([...ancestors, project.id, firstItem.id, secondItemId]),
        },
        review: {
            isPubAccepted: false,
            isPubRetracted: false,
            deadline: Number((new Date().setDate(new Date().getDate() + 7)).toString()),
            acceptedBy: [],
            quickReviews: content!.quickReviews.reduce((a,b) => ({ ...a, [camelCase(b)]: [] }), {}),
            reportedBy: [],
            discussionId: [],
            suggestedReviewers: form.suggested,
        },
        bookmarkedBy: [],
        title: form.title,
    }
};
