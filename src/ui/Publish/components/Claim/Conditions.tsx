import dayjs from 'dayjs';
import shortid from 'shortid';
import * as React from 'react';
import { Popover } from 'antd';
import camelCase from 'lodash/camelCase';
import lowerCase from 'lodash/lowerCase';
import { useDispatch } from 'react-redux';
import { PlusCircleOutlined } from '@ant-design/icons';

import { useTypedSelector } from 'helpers';
import {
    StyledAddedConditions, StyledCondition,
    StyledConditionNameInput,
    StyledConditionsContentWrapper,
    StyledConditionsInputWrapper,
    StyledConditionsWrapper,
    StyledConditionValueInputWrapper,
    StyledDefaultCondition,
    StyledRemoveCondition,
    StyledConditionTypeInput,
    StyledConditionValueInput, StyledConditionsEditModeButton, StyledDefaultConditionsWrapper,
} from 'ui/Publish/styles';
import {
    FPButton,
    FPClaim,
    FPDatePicker,
    FPInput,
    FPMultiInput,
    FPNumberInput,
    FPSimpleSelect,
    FPSwitch
} from 'ui/elements';
import * as Pub from 'containers/Pub';


interface Props {
    disabled: boolean;
    editMode?: boolean;
    terms?: PubClaim['terms'];
}

const Conditions: React.FC<Props> = ({ disabled, editMode, terms }) => {
    const dispatch = useDispatch();
    const [open, setOpen] = React.useState(false);
    const [detailType, setDetailType] = React.useState<any>('string');
    const [detailName, setDetailName] = React.useState('');
    const [detailValue, setDetailValue] = React.useState<any>('');
    
    const draft = useTypedSelector((state) => state.pub.data.draft);
    const defaultConditions = useTypedSelector((state) =>
        state.content.data.type ? state.content.data.type.defaultConditions : []
    );
    
    React.useEffect(() => {
        if (detailName === '') {
            clearState();
        }
        if (detailName.toLowerCase().match(/( date)\b/g) || detailName.toLowerCase().match(/( day)\b/g)) {
            return setDetailType('date');
        }
        if (detailName.toLowerCase().match(/( count)\b/g) || detailName.toLowerCase().match(/( number)\b/g)) {
            return setDetailType('number');
        }
    }, [detailName]);
    
    const clearState = () => {
        setDetailName('');
        setDetailType('string');
        setDetailValue('');
    };
    
    const handleFormOpen = (open: boolean) => {
        setOpen(open);
        clearState();
    };
    
    const onDateChange = (date: any) => {
        setDetailValue(dayjs(date).valueOf());
        setDetailType('date');
    };
    const onNameChange = (value: string) => setDetailName(value);
    const onTypeChange = (value: any) => {
        setDetailValue('');
        if (value === 'date') return setDetailType('date');
        if (value === 'number') return setDetailType('number');
        if (value === 'string') return setDetailType('string');
        if (value === 'list' || value === 'string[]') return setDetailType('list');
        if (value === 'boolean') {
            setDetailValue(false);
            return setDetailType('boolean');
        }
    };
    const onValueChange = (value: any) => setDetailValue(value);
    const onAddCondition = () => {
        const condition = {
            id: shortid(),
            value: detailValue,
            name: camelCase(detailName),
            type: detailType,
        };
        dispatch(Pub.form.set({ form: 'draft', field: 'claim.conditions', value: condition }));
        clearState();
    };
    const onRemoveCondition = (id: string) =>
        dispatch(Pub.form.cut({ form: 'draft', field: 'claim.conditions', value: id }));
    
    const setDefaultCondition = (d: CommunityDefaultCondition) => {
        setDetailName(lowerCase(d.name));
        setDetailType(d.type);
    };
    
    const renderDefaultConditions = defaultConditions!.map((d, i) =>
        <StyledDefaultCondition
            key={i}
            onClick={() => setDefaultCondition(d)}
        >
            {lowerCase(d.name)}
        </StyledDefaultCondition>
    );
    
    const renderValueField = () => {
        if (detailType === 'string') {
            return <FPInput placeholder="detail value" onChange={onValueChange} value={detailValue} />;
        }
        if (detailType === 'date') return <FPDatePicker onDateChange={onDateChange} />;
        if (detailType === 'boolean') {
            return <FPSwitch onSwitchChange={onValueChange} />;
        }
        if (detailType === 'number') {
            return <FPNumberInput onChange={onValueChange} placeholder="count/number" />;
        }
        if (detailType === 'list' || detailType === 'string[]') {
            return <FPMultiInput onInputChange={onValueChange} placeholder="list of values" />;
        }
        return <FPInput placeholder="detail value" onChange={onValueChange} value={detailValue} />;
    };
    
    const isAddDisabled = !detailName || (detailValue.length === 0 || !detailValue);
    
    console.log('isAddDisabled', isAddDisabled);
    
    const renderConditionsFormContent = () => {
        return (
            <StyledConditionsContentWrapper>
                <p>Recommended details:</p>
                <StyledDefaultConditionsWrapper>
                    {renderDefaultConditions}
                </StyledDefaultConditionsWrapper>
                <StyledConditionsInputWrapper>
                    <StyledConditionNameInput placeholder="detail name" onChange={onNameChange} value={detailName} />
                    <p>:</p>
                    <StyledConditionValueInputWrapper>
                        <StyledConditionTypeInput>
                            <FPSimpleSelect
                                value={detailType}
                                onChange={onTypeChange}
                                options={['string', 'list', 'date', 'number', 'boolean']}
                            />
                        </StyledConditionTypeInput>
                        <StyledConditionValueInput fluid={detailType === 'list' || detailType === 'string[]'}>
                            {renderValueField()}
                        </StyledConditionValueInput>
                    </StyledConditionValueInputWrapper>
                </StyledConditionsInputWrapper>
                <FPButton
                    block
                    label="add"
                    buttonType="default"
                    onClick={onAddCondition}
                    disabled={detailType === 'boolean' ? false : isAddDisabled}
                />
            </StyledConditionsContentWrapper>
        );
    };
    
    const renderConditionsList = draft.claim.conditions.map((c) => {
        const conditionValue = Array.isArray(c.value)
            ? c.value.toString().split(',').join(', ')
            : c.type === 'date'
                ? dayjs(c.value).format('M/D/YYYY')
                : c.type === 'boolean'
                    ? c.value === true ? 'yes' : 'no'
                    : c.value;
        return (
            <StyledCondition key={c.id} editMode={editMode}>
                <div>
                    <StyledRemoveCondition onClick={() => onRemoveCondition(c.id)} />
                    {lowerCase(c.name)}:
                </div>
                <em>{conditionValue}</em>
            </StyledCondition>
        );
    });
    
    return (
        <StyledConditionsWrapper editMode={editMode}>
            <div style={{ display: 'flex', flexDirection: 'row', width: '100%' }}>
                <FPClaim terms={terms as PubClaim['terms']} onClick={(e) => e.preventDefault()} />
                <div style={{ width: 4 }} />
                <Popover
                    visible={open}
                    trigger="click"
                    placement="top"
                    onVisibleChange={handleFormOpen}
                    content={renderConditionsFormContent()}
                >
                    {editMode
                        ? (
                            <StyledConditionsEditModeButton>
                                Add key details to your claim
                                <PlusCircleOutlined style={{ marginLeft: 8 }}  />
                            </StyledConditionsEditModeButton>
                        ) : (
                            <div style={{ width: '100%' }}>
                                <FPButton block buttonType="default" label="add key details" disabled={disabled} />
                            </div>
                        )
                    }
                </Popover>
            </div>
            <StyledAddedConditions editMode={editMode}>
                {renderConditionsList}
            </StyledAddedConditions>
        </StyledConditionsWrapper>
    );
};

export default Conditions;
