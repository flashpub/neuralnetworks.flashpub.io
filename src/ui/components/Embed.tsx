import * as React from 'react';
// import Embed from 'react-embed';
import styled from 'styled-components';

import { StyledFlexWrapper } from 'ui/elements/styles';
import { FPInput } from 'ui/elements';

export const Embed: React.FC = () => {
    const [embedUrl, setEmbedUrl] = React.useState<string>('');

    return (
        <StyledEmbedWrapper>
            <FPInput value={embedUrl} onChange={setEmbedUrl} placeholder="Paste a url here" />
            {/* <Embed url={embedUrl} /> */}
        </StyledEmbedWrapper>
    );
};

const StyledEmbedWrapper = styled(StyledFlexWrapper)`
    && {
    }
`;
