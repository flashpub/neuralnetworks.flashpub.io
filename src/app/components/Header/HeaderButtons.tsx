import * as React from 'react';
import styled from 'styled-components';
import { Icon as UIIcon } from '@blueprintjs/core';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import { PortalView } from 'app/utils';
import { Color, Spacing, Font } from 'app/assets';

import { UserDefinition } from 'access/types';

import { PublishStep } from 'publish/utils';

import  { HeaderButtonsMenu } from './HeaderButtons.Menu';
import HeaderButtonsPublish from './HeaderButtons.Publish';
import { CommunityDefinition, Content, SystemGatesDefinition } from '../../types';

import { FUIButton } from 'FUI/elements';


type Props = {
    isAuth: boolean,
    isMobile: boolean,
    campaign: any,
    campaigns: any,
    community: CommunityDefinition | null,
    gates: SystemGatesDefinition | null,
    user: Optional<UserDefinition> | null,
    publishStep: PublishStep | null,

    setContentType(type: Content): void,
    setPortalView(view: PortalView): void,
    setPublishStep(step: PublishStep): void,
} & RouteComponentProps;

const HeaderButtons: React.FunctionComponent<Props> = ({ setContentType, isMobile, isAuth, user, setPortalView, setPublishStep, gates, publishStep, community, campaign, campaigns }) => {
    const openAccessDialog = () => setPortalView(PortalView.ACCESS);

    const renderLoginButton = isMobile
        ? <LoginIcon icon="user" iconSize={35} onClick={openAccessDialog}/>
        : <LoginButton text="Login" buttonType="outlined" onClick={openAccessDialog}/>;

    return (
        <ActionsWrapper>
            {isAuth && user && (gates && gates.isPublishOpen) &&
                <HeaderButtonsPublish
                    openPublishView={setPortalView}
                    setContentType={setContentType}
                    setPublishStep={setPublishStep}
                    publishStep={publishStep}
                    community={community}
                    campaign={campaign}
                    campaigns={campaigns}
                />
            }
            {(isAuth && user)
                ? <HeaderButtonsMenu isMobile={isMobile} initials={user.initials} orcid={user.orcid}/>
                : renderLoginButton
            }
        </ActionsWrapper>
    );
};

const ActionsWrapper = styled.div`
    display: flex;
    font-size: 16px;
    padding-bottom: 1px;
    justify-content: flex-end;
`;
const LoginButton = styled(FUIButton)` && {
    color: ${Color.grey};
    display: inline-block;
    border-color: ${Color.grey};
    font-family: ${Font.header_button};
    :hover {
        background-color: ${Color.lightgrey};
    }
}`;
const LoginIcon = styled(UIIcon)` && {
    cursor: pointer;
    color: ${Color.midgrey};
    margin-bottom: ${Spacing.S};
}`;

export default withRouter(HeaderButtons);
