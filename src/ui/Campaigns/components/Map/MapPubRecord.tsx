import dayjs from 'dayjs';
import * as React from 'react';

import {
    StyledMapPubRecordLink,
    StyledMapPubRecordWrapper,
    StyledMapPubRecordTitle,
    StyledMapPubMeta,
} from 'ui/Campaigns/styles';

type Props = {
    pub: {
        author: string;
        date: number;
        fetchId: string;
        id: string;
        title: string;
    };
};

export const MapPubRecord: React.FC<Props> = ({ pub }) => {
    const origin = window.location.origin;
    return (
        <StyledMapPubRecordWrapper>
            <StyledMapPubRecordLink
                href={`${origin}/pub/${pub.fetchId}`}
                target="_blank"
                rel="noopener"
            >
                <StyledMapPubRecordTitle>{pub.title}</StyledMapPubRecordTitle>
                <StyledMapPubMeta>
                    <div>by {pub.author}</div>
                    <div>{dayjs(pub.date).format('MMM DD, YYYY')}</div>
                </StyledMapPubMeta>
            </StyledMapPubRecordLink>
        </StyledMapPubRecordWrapper>
    );
};
