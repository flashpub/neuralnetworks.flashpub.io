import find from 'lodash/find';
import * as React from 'react';
import { useDispatch } from 'react-redux';

import { project } from 'configs';
import { useTypedSelector, useWindowWidth } from 'helpers';
import * as Dictionary from 'containers/Dictionary';
import { DictionaryTerm } from 'ui/Feed/DictionaryBar';
import { StyledDictionaryBar } from 'ui/Feed/DictionaryBar/styles';
import { FPPlaceholder, FPSimpleSelect } from 'ui/elements';
import { Viewport } from 'assets/styles';
import { useNavigate } from '@reach/router';

export const DictionaryBar: React.FC = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const dictionaryStatus = useTypedSelector((state) => state.dictionary.status);
    const dictionaryList = useTypedSelector((state) =>
        state.dictionary.data.list.filter((term) => term.metrics.pubCount > 0)
    );
    const selectedDictionary = useTypedSelector((state) => state.dictionary.data.selected);
    
    const isMobile = useWindowWidth() <= Viewport.TABLET;
    
    const setSelectedTerm = (term: DictionaryContainerState['selected'] | string) => {
        if (typeof term === 'string') {
            const termExists = find(dictionaryList, (d) => d.name === term);
            if (termExists) {
                dispatch(Dictionary.select({ id: termExists.id, name: termExists.name }));
                navigate(`/feed/${termExists.name}`);
            }
        } else {
            dispatch(Dictionary.select(term));
        }
    };

    const mobileBarOptions = dictionaryList.map((d) => d.name);
    
    const renderDictionaryTerms = dictionaryList
        .filter((term) => term.id !== project.community.id)
        .map((term) => (
            <DictionaryTerm
                key={term.id}
                to={`/feed/${term.name}`}
                onClick={() => setSelectedTerm({ id: term.id, name: term.name })}
            >
                {term.name}
            </DictionaryTerm>
        ));

    return (
        <StyledDictionaryBar>
            {isMobile
                ? <FPSimpleSelect
                    mobilebar="true"
                    options={mobileBarOptions}
                    onChange={setSelectedTerm}
                    value={selectedDictionary.name}
                />
                : (
                    <>
                        <DictionaryTerm
                            to="/"
                            onClick={() => setSelectedTerm({ id: project.community.id, name: project.community.name })}
                        >
                            {project.community.name}
                        </DictionaryTerm>
                        {dictionaryStatus.type === 'LOADING' ? <FPPlaceholder height={30} /> : renderDictionaryTerms}
                    </>
                )
            }
        </StyledDictionaryBar>
    );
};
