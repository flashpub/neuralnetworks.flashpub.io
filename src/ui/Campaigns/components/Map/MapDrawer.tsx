import dayjs from 'dayjs';
import { Empty } from 'antd';
import { kebabCase, uniq } from 'lodash';
import * as React from 'react';
import { Link, navigate } from '@reach/router';
import { RightOutlined } from '@ant-design/icons';

import { useTypedSelector } from 'helpers';
import {
    StyledCityStats,
    StyledDeathStats,
    StyledDrawerClose,
    StyledFooterWrapper,
    StyledHeaderTitle,
    StyledHeaderWrapper,
    StyledHeading,
    StyledHeadingWrapper,
    StyledMapDrawer,
    StyledSeeAllButton,
    StyledStats,
} from 'ui/Campaigns/styles';
import { MapPubRecord } from 'ui/Campaigns/components/Map/MapPubRecord';
import { FPButton } from 'ui/elements';
import { StyledMenuWrapper, StyledPublishItem } from 'ui/Header';

type Props = {
    onOpenPublish(c: CommunityContentType): void;
    onCloseDrawer(): void;
    drawerVisible: boolean;
    selectedCity: CampaignData | null;
};

export const MapDrawer: React.FC<Props> = ({ selectedCity, drawerVisible, onOpenPublish, onCloseDrawer }) => {
    const campaignData = useTypedSelector((state) => state.campaigns.data.list);
    const community = useTypedSelector((state) => state.community.data.community);
    const selectedCampaign = useTypedSelector((state) => state.campaigns.data.selected);

    const aCity = selectedCity ? selectedCity.city : 'a city';

    const renderDrawerHeader = selectedCity ? (
        <StyledHeaderWrapper>
            <StyledHeaderTitle>{`${selectedCity.city}, ${selectedCity.state}`}</StyledHeaderTitle>
            {dayjs().format('MMM DD, YYYY')}
            <StyledCityStats>
                <StyledStats>Confirmed Cases: {selectedCity.confirmed}</StyledStats>
                <StyledDeathStats>Deaths: {selectedCity.deaths}</StyledDeathStats>
            </StyledCityStats>
        </StyledHeaderWrapper>
    ) : (
        <StyledHeaderWrapper>
            <StyledHeaderTitle>Click on a marker for city details</StyledHeaderTitle>
            {dayjs().format('MMM DD, YYYY')}
        </StyledHeaderWrapper>
    );
    
    const renderDrawerFooter = (
        <StyledFooterWrapper>
            {community &&
            Object.values(selectedCampaign?.contentTypes!)
                .sort((a, b) => b.order - a.order)
                .map((c) => {
                    const cityLabel = c.contentTypeLabel.toLowerCase().includes('city');
                    if (cityLabel) {
                        return (
                            <FPButton
                                block
                                key={c.order}
                                buttonType="primary"
                                onClick={() => onOpenPublish(c)}
                                label={cityLabel ? `Publish results for ${aCity}` : `Publish a ${c.contentTypeLabel}`}
                            />
                        );
                    } else return null;
                })}
        </StyledFooterWrapper>
    );

    const handleSeeAll = () => {
        if (selectedCity) {
            navigate(selectedCity.pubURL);
        }
    };

    const renderPubList = () => {
        if (selectedCity) {
            if (selectedCity.pubCount > 0) return selectedCity.pubArray.map((p) => <MapPubRecord key={p.id} pub={p} />);
            return <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />;
        } else {
            const defaultList = campaignData
                .map((c) => c.pubCount > 0 && c.pubArray)
                .filter((c) => c)
                .flat();
            return uniq(defaultList).map((p) => <MapPubRecord key={p.id} pub={p} />);
        }
    };

    return (
        <StyledMapDrawer
            closable={false}
            getContainer={false}
            visible={drawerVisible}
            onClose={onCloseDrawer}
            title={renderDrawerHeader}
            footer={renderDrawerFooter}
        >
            <StyledDrawerClose onClick={onCloseDrawer} />
            <StyledHeadingWrapper>
                <StyledHeading>Publications:</StyledHeading>
                {selectedCity && selectedCity.pubCount > 0 && (
                    <StyledSeeAllButton
                        iconSide="right"
                        buttonType="link"
                        onClick={handleSeeAll}
                        icon={<RightOutlined />}
                        label={`See all for ${selectedCity.city}`}
                    />
                )}
            </StyledHeadingWrapper>
            {renderPubList()}
        </StyledMapDrawer>
    );
};
