import * as React from 'react';
import { v4 as uuidv4 } from 'uuid';
import { useDispatch } from 'react-redux';
import { useEffectOnce } from 'react-use';
import { find, lowerCase, startCase, intersectionWith } from 'lodash';

import { project } from 'configs';
import { useTypedSelector } from 'helpers';
import * as Pub from 'containers/Pub';
import * as Claim from 'containers/Claim';
import * as Dictionary from 'containers/Dictionary';
import { FPSelect } from 'ui/elements';
import { StyledTermWrapper, StyledTermLabel, StyledTerm } from 'ui/Publish/styles';

type Props = {
    setDictionaries: (values: string[]) => void;
};

export const Single: React.FC<Props> = ({ setDictionaries }) => {
    const dispatch = useDispatch();

    const [search, setSearch] = React.useState<string>('');

    const claim = useTypedSelector((state) => state.claim.data.claim);
    const selectedContent = useTypedSelector((state) => state.content.data.type);
    const dictionaryStatus = useTypedSelector((state) => state.dictionary.status);
    const dictionaryList = useTypedSelector((state) => state.dictionary.data.list);
    const dictionaryAncestors = useTypedSelector((state) => state.dictionary.data.term1.ancestors);

    const { claimItems } = claim;
    const firstItem = claimItems.firstItem;
    const termExists = (item: string) => find(dictionaryList, (d) => d.id === item);

    useEffectOnce(() => {
        const term = termExists(firstItem.id);
        if (firstItem.id) {
            if (term) {
                setDictionaries([...term.ancestors, firstItem.id, project.community.id]);
            } else {
                setDictionaries([...dictionaryAncestors, firstItem.id, project.community.id]);
            }
        }
    });
    
    const handleOnChange = (value: any, field?: string) => {
        const term = termExists(value.key);
        sessionStorage.removeItem('storedTitle');
        if (field) {
            if (Array.isArray(value)) {
                const valueArray = value.map((v: Option) => v.key);
                // PUT on term1.ancestors array
                dispatch(Dictionary.form.sub({ form: 'term1', field, value: [...valueArray, project.community.id]}));
                setDictionaries([...valueArray, firstItem.id, project.community.id]);
            } else {
                if (term) {
                    dispatch(Dictionary.form.clear('term1'));
                    setDictionaries([...term.ancestors, value.key, project.community.id]);
                }
                
                dispatch(Pub.form.set({ form: 'draft', field: 'claim.terms.firstItem', value: value.value }));
                dispatch(Claim.form.set({ form: 'claim', field, value: { id: value.key, name: value.value } }));
            }
        }
    };
    
    React.useEffect(() => {
        if (selectedContent?.claim.item1.setFilterAsItem) {
            const filter = termExists(selectedContent?.claim.item1.filter);
            const data = {
                value: filter?.name,
                key: filter?.id,
                title: filter?.longName,
            };
            handleOnChange(data, 'claimItems.firstItem');
        }
    }, [selectedContent]);

    // React.useEffect(() => {
    //     if (firstItem.id) {
    //         setDictionaries([]);
    //     }
    // }, [dictionaryAncestors, firstItem.id]);

    if (dictionaryList && selectedContent) {
        const contentItem1 = selectedContent.claim.item1;
        const isSingle = selectedContent.claim.type === 'single';
        const isCreatable = !termExists(lowerCase(search)) && selectedContent.claim.item1.newTermAllowed;

        const fixedTerm = () => {
            const term = termExists(selectedContent.claim.item1.filter);
            return term ? [{ value: term.name, key: term.id, title: term.longName }] : [];
        };
        
        const options: Option[] = selectedContent.claim.item1.setFilterAsItem
            ? fixedTerm()
            : dictionaryList
                .filter((term) => term.name !== project.community.name)
                .map((term) => ({ value: term.name, key: term.id, title: term.longName }));


        const handleOnCreate = (value: any, field?: string) => {
            const termId = uuidv4();
            sessionStorage.removeItem('storedTitle');
            if (field) {
                setDictionaries([termId]);
                dispatch(Dictionary.form.set({ form: 'term1', field: 'id', value: termId }));
                dispatch(Dictionary.form.set({ form: 'term1', field: 'name', value: value.value }));
                dispatch(Dictionary.form.set({ form: 'term1', field: 'longName', value: value.title }));
                dispatch(Dictionary.form.set({ form: 'term1', field: 'ancestors', value: project.community.id }));
                dispatch(Pub.form.set({ form: 'draft', field: 'claim.terms.firstItem', value: value.value }));
                dispatch(Claim.form.set({ form: 'claim', field, value: { id: termId, name: value.value } }));
            }
        };
        const handleOnSearch = (input: string) => {
            if (input.length >= 3) {
                setSearch(input);
                // dispatch(Dictionary.searchDictionary(contentItem1.filter, lowerCase(input)));
            }
        };

        const defaultValue = {
            firstItem: () => {
                const term = termExists(firstItem.id);
                const filter = termExists(selectedContent.claim.item1.filter);
                if (term) {
                    return { value: term.name, key: term.id, title: term.longName };
                } else {
                    if (!selectedContent.claim.item1.setFilterAsItem) {
                        return { value: firstItem.name, key: firstItem.id, title: startCase(firstItem.name) };
                    } else {
                        return { value: filter?.name, key: filter?.id, title: startCase(filter?.name) };
                    }
                }
            },
            ancestors: () => intersectionWith(options, dictionaryAncestors, (o, id) => o.key === id),
        };

        return (
            <StyledTermWrapper>
                <StyledTerm single={isSingle}>
                    <StyledTermLabel>{contentItem1.label || 'Select a term from a dropdown below'}</StyledTermLabel>
                    <FPSelect
                        required
                        searchable
                        options={options}
                        creatable={isCreatable}
                        onChange={handleOnChange}
                        onCreate={handleOnCreate}
                        onSearch={handleOnSearch}
                        field="claimItems.firstItem"
                        defaultValue={defaultValue.firstItem()}
                        loading={dictionaryStatus.type === 'LOADING'}
                        placeholder="pick a term or type in something"
                    />
                </StyledTerm>
                {firstItem.id && !termExists(firstItem.id) && (
                    <StyledTerm single={isSingle}>
                        <FPSelect
                            searchable
                            mode="multiple"
                            field="ancestors"
                            options={options}
                            onChange={handleOnChange}
                            onSearch={handleOnSearch}
                            placeholder="add ancestors"
                            defaultValue={defaultValue.ancestors()}
                        />
                    </StyledTerm>
                )}
            </StyledTermWrapper>
        );
    } else return null;
};
