import styled from 'styled-components/macro';

import { Font, Color, MaxWidth, Spacing, Radius, Media } from 'assets/styles';
import { FPButton } from 'ui/elements';
import { StyledFlexWrapper } from 'ui/elements/styles';

export const StyledWrapper = styled(StyledFlexWrapper)`
    width: 100%;
    margin: 0 auto;
    padding: 0 ${Spacing._24};
    max-width: calc(${MaxWidth} + ${Spacing._24});
    ${Media.tablet} {
        padding: 0 ${Spacing._x(80)};
    }
`;
export const StyledFeedWrapper = styled(StyledFlexWrapper)`
    width: 100%;
    margin: 0 auto;
`;
export const StyledFeedContent = styled(StyledFlexWrapper)`
    width: 100%;
    min-height: 700px;
    position: relative;
    justify-content: flex-start;
`;
export const StyledTopCampaignWrapper = styled(StyledFlexWrapper)`
    width: 100%;
    flex-direction: row;
    align-items: flex-start;
    justify-content: space-between;
`;
export const StyledSectionWrapper = styled(StyledFlexWrapper)<{ type?: any }>`
    align-items: flex-start;
    justify-content: flex-start;
    width: 100%;
    
    ${Media.tablet} {
        min-width: ${({ type }) => (type === 'campaigns' ? '288px' : 'calc(100% - 300px)')};
        margin-left: ${({ type }) => (type === 'campaigns' ? Spacing._12 : 0)};
    }
`;
export const StyledCampaignsSectionWrapper = styled(StyledFlexWrapper)`
    margin: 0;
    color: #fff;
    width: 100%;
    height: fit-content;
    align-items: center;
    justify-content: center;
    
    ${Media.tablet} {
        align-items: flex-start;
        justify-content: flex-start;
    }
`;
export const StyledCampaignWrapper = styled.div`
    width: 100%;
    cursor: pointer;
    border-radius: ${Radius};
    background-color: ${Color.community};
    padding: ${Spacing._x(2)};
    margin-bottom: ${Spacing._8};
    transition: transform 0.15s ease;
    :hover {
        transform: scale(1.01);
    }
    div {
        text-align: center;
        padding: 0 ${Spacing._12};
        font-family: ${Font.title};
    }
`;
export const StyledCampaignImage = styled.img`
    width: 100%;
    margin-top: ${Spacing._4};
    border-bottom-left-radius: ${Radius};
    border-bottom-right-radius: ${Radius};
`;
export const StyledSectionTopRecordsWrapper = styled(StyledFlexWrapper)<{ noData?: boolean }>`
    width: 100%;
    position: relative;
    border-radius: ${({ noData }) => noData && Radius};
    border: ${({ noData }) => noData && `1px dashed ${Color.midgrey}`}
`;
export const StyledSectionLeaderRecordsWrapper = styled(StyledFlexWrapper)`
    width: 100%;
    overflow-x: auto;
    flex-direction: row;
    margin: -${Spacing._8};
    padding: ${Spacing._8};
    align-items: flex-start;
    justify-content: flex-start;
`;
export const StyledSectionTitleWrapper = styled(StyledFlexWrapper)`
    width: 100%;
    height: 50px;
    flex-direction: row;
    justify-content: flex-start;
    
    button {
        padding-right: 0;
    }
`;
export const StyledSectionTitle = styled.div`
    font-size: 24px;
    font-weight: 500;
    color: ${Color.community};
    font-family: ${Font.header};
    margin-right: ${Spacing._12};
    margin-bottom: ${Spacing._x(6)};
`;
export const StyledSectionTitleMore = styled(FPButton)`
    font-size: 12px;
    color: ${Color.midgrey};
    font-family: ${Font.text};
    :hover {
      color: ${Color.grey};
    }
    
    div {
      font-size: 14px;
    }
`;
export const StyledPubRecordWrapper = styled.div`
    width: 100%;
    position: relative;
    height: fit-content;
    background-color: #fff;
    border-radius: ${Radius};
    margin-bottom: ${Spacing._12};
    transition: transform 0.15s ease;
    box-shadow: 0px 0px 10px 5px ${Color.community_t15};
    :hover {
        transform: scale(1.005);
    }
`;
export const StyledPubRecordInnerWrapper = styled(StyledFlexWrapper)`
    width: 100%;
    cursor: pointer;
    flex-direction: row;
    padding: 0 ${Spacing._8};
    justify-content: flex-start;
`;
export const StyledPubRecordContent = styled(StyledFlexWrapper)`
    width: 100%;
    font-weight: 300;
    align-items: flex-start;
    font-family: ${Font.text};
`;
export const StyledPubRecordTag = styled.div`
    top: 30px;
    width: auto;
    left: -48px;
    color: #fff;
    font-size: 12px;
    font-weight: 500;
    text-align: center;
    position: absolute;
    padding: 0 ${Spacing._8};
    border-radius: ${Radius};
    transform: rotate(270deg);
    font-family: ${Font.text};
    background-color: ${Color.flashpub};
`;
export const StyledPubRecordUpper = styled(StyledFlexWrapper)`
    width: 100%;
    min-height: 30px;
    font-weight: 400;
    flex-direction: row;
    align-items: flex-start;
    margin-top: ${Spacing._4};
    font-family: ${Font.title};
    justify-content: space-between;
`;
export const StyledPubRecordTitle = styled.div`
    width: 100%;
    font-size: 18px;
    line-height: 1.2;
    margin-right: ${Spacing._x(60)};
`;
export const StyledPubRecordDate = styled.div`
    margin-left: ${Spacing._4};
`;
export const StyledPubRecordDescription = styled.span`
    width: 100%;
    height: 30px;
    overflow: hidden;
`;
export const StyledPubRecordLower = styled(StyledPubRecordUpper)`
    padding: 0;
    margin-top: 0;
    align-items: center;
    font-family: ${Font.text};
    margin-bottom: ${Spacing._4};
    flex-direction: column;
    
    ${Media.tablet} {
        flex-direction: row;
    }
`;
export const StyledPubRecordClaim = styled.div``;
export const StyledPubRecordAuthor = styled.div`
    font-weight: 300;
`;
export const StyledLeaderWrapper = styled(StyledFlexWrapper)`
    min-width: 150px;
    cursor: pointer;
    padding: ${Spacing._12};
    align-items: flex-start;
    border-radius: ${Radius};
    margin-right: ${Spacing._12};
    transition: transform 0.15s ease;
    box-shadow: 0px 0px 10px 5px ${Color.community_t15};
    :hover {
        transform: scale(1.025);
    }
`;
export const StyledLeaderMetadata = styled(StyledFlexWrapper)`
    width: 100%;
    flex-direction: row;
    justify-content: flex-start;
`;
export const StyledLeaderAvatarPlaceholder = styled(StyledFlexWrapper)`
    width: 40px;
    height: 40px;
    font-size: 20px;
    border-radius: ${Radius};
    font-family: ${Font.header};
    background-color: ${Color.lightgrey};
`;
export const StyledLeaderAvatar = styled.img``;
export const StyledLeaderIndicators = styled(StyledFlexWrapper)`
    flex-direction: row;
    justify-content: space-around;
`;
export const StyledIndicatorWrapper = styled(StyledFlexWrapper)`
    line-height: 1;
    font-family: ${Font.title};
    margin-left: ${Spacing._12};
    
    div {
        padding-top: ${Spacing._x(2)};
    }
`;
export const StyledLeaderName = styled.div`
    font-family: ${Font.title};
`;
export const StyledLeaderSpec = styled.div`
    font-size: 14px;
    font-weight: 200;
    font-family: ${Font.title};
`;
export const StyledLeaderWork = styled.div``;
export const StyledAuthoredDateWrapper = styled(StyledFlexWrapper)`
    flex-direction: row;
    font-size: 12px;
`;
export const StyledPubRecordActionsWrapper = styled(StyledFlexWrapper)`
    top: 0;
    right: 0;
    position: absolute;
    flex-direction: row;
    color: ${Color.community}
`;
export const StyledPubRecordBookmarkIcon = styled.div`
`;
export const StyledPubRecordEndorseIcon = styled.div`
    margin-top: -4px;
    margin-right: ${Spacing._4};
`;
