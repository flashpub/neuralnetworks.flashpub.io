import * as React from 'react';
import isEqual from 'lodash/isEqual';
import shortid from 'shortid';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';

import { RootState } from 'app/store';
import { PortalView } from 'app/utils';
import * as AppAction from 'app/actions';
import { PubDefinition, ReduxProps, Asset, Assets } from 'app/types';

import { Publication } from 'publication/components';
import * as PublicationAction from 'publication/actions';
import * as PublicationAsyncAction from 'publication/async-actions';

import { SetDictionary } from 'dictionaries/types';
import * as DictionariesAction from 'dictionaries/actions';

const mapDispatch = {
    updatePub: PublicationAsyncAction.update,
    setPortalView: AppAction.SET_PORTAL_VIEW.use,
    setCurrentPub: PublicationAction.GET_PUBLICATION.use,
    setCurrent: DictionariesAction.SET_CURRENT_DICTIONARY.use,
    updateBookmark: PublicationAction.UPDATE_PUBLICATION_BKMRK.use,
    updateQuickReview: PublicationAction.UPDATE_PUBLICATION_QR.use,
    updateAcceptButton: PublicationAction.UPDATE_PUBLICATION_AB.use,
    submitEditedPub: PublicationAsyncAction.edit
};
const mapState = (state: RootState) => ({
    user: state.access.user,
    isAuth: state.app.isAuth,
    claims: state.feed.claims,
    pub: state.publication.pub,
    current: state.dictionaries.current,
    origPub: state.publication.origPub,
    publicationStatus: state.publication.status,
    portalView: state.app.view,
});

type Props = ReduxProps<typeof mapState, typeof mapDispatch> & RouteComponentProps;

type State = {
    edit: boolean,
    assets: Assets
}

class PublicationContainer extends React.Component<Props, State> {
    state = {
        edit: false,
        assets: {
            code: (this.props.pub && this.props.pub.metadata) ? this.props.pub.metadata.code : [],
            datasets: (this.props.pub && this.props.pub.metadata) ? this.props.pub.metadata.datasets : [],
            protocols: (this.props.pub && this.props.pub.metadata) ? this.props.pub.metadata.protocols : [],
            references: (this.props.pub && this.props.pub.metadata) ? this.props.pub.metadata.references : []
        }
    }

    componentWillUnmount(): void {
        localStorage.setItem('lastPath', this.props.history.location.pathname.split('/')[1]);
        this.handlePubUpdate();
    }

    handlePubUpdate = () => {
        const { pub, origPub } = this.props;
        const equal = isEqual(pub, origPub);
        return equal ? null : this.props.updatePub(pub!);
    };

    handleAddAsset = (type: string, asset: Asset) => {
        const assetId = shortid()
        this.setState({
            assets: {
                ...this.state.assets,
                [type]: [
                    ...this.state.assets[type],
                    {
                        id: assetId,
                        url: asset.url,
                        name: asset.name
                    }
                ],
            }
        })
        localStorage.setItem('assets', JSON.stringify(({
            ...this.state.assets,
            [type]: [
                ...this.state.assets[type],
                {
                    id: assetId,
                    url: asset.url,
                    name: asset.name
                }
            ]
        })))
    };

    handleRemoveAsset = (type: string, id: string) => {
        this.setState(prevState => ({
            assets: {
                ...prevState.assets,
                [type]: prevState.assets[type].filter((asset: Asset) => asset.id !== id),
            }
        }))

        localStorage.setItem('assets', JSON.stringify(({
            ...this.state.assets,
            [type]: this.state.assets[type].filter((item: Asset) => item.id !== id)
        })))
    };

    handleSetCurrentPub = (pub: PubDefinition | null) => this.props.setCurrentPub(pub!);
    handleUpdateBookmark = (update: any) => this.props.updateBookmark({ update });
    handleSetPortalView = (view: PortalView) => this.props.setPortalView({ view });
    handleUpdateQuickReview = (update: any) => this.props.updateQuickReview({ update });
    handleUpdateAcceptButton = (update: any) => this.props.updateAcceptButton({ update });
    handleSetCurrentDictionary = (current: SetDictionary | null) => this.props.setCurrent({ current: current! });
    handleSetEditMode = () => this.setState(prevState => ({ edit: !prevState.edit }));
    handleSubmitEditedPub = (pub: PubDefinition, editedElements: Array<string>) => this.props.submitEditedPub(pub, editedElements);

    render() {
        return (
            <Publication
                pub={this.props.pub}
                assets={this.state.assets}
                user={this.props.user}
                editMode={this.state.edit}
                claims={this.props.claims}
                isAuth={this.props.isAuth}
                current={this.props.current}
                addAsset={this.handleAddAsset}
                portalView={this.props.portalView}
                removeAsset={this.handleRemoveAsset}
                setEditMode={this.handleSetEditMode}
                setPortalView={this.handleSetPortalView}
                setCurrentPub={this.handleSetCurrentPub}
                updateBookmark={this.handleUpdateBookmark}
                submitEditedPub={this.handleSubmitEditedPub}
                setCurrent={this.handleSetCurrentDictionary}
                publicationStatus={this.props.publicationStatus}
                updateQuickReview={this.handleUpdateQuickReview}
                updateAcceptButton={this.handleUpdateAcceptButton}
            />
        );
    }
}

// @ts-ignore
export default connect(mapState, mapDispatch)(PublicationContainer);
