import * as React from 'react';
import { connect } from 'react-redux';

import { Status } from 'app/utils';
import { project } from 'app/config';
import { RootState } from 'app/store';
import { ReduxProps } from 'app/types';
import * as Shared from 'app/components/Shared';

import * as DictionariesAction from 'dictionaries/actions';
import { SubcommunitiesBar } from 'dictionaries/components';

import { SetDictionary } from '../types';


const mapDispatch = {
    setCurrentDictionary: DictionariesAction.SET_CURRENT_DICTIONARY.use,
};

const mapState = (state: RootState) => ({
    feedStatus: state.feed.status,
    current: state.dictionaries.current,
    dictionaries: state.dictionaries.list,
    dictionariesStatus: state.dictionaries.status,
});

type Props = ReduxProps<typeof mapState, typeof mapDispatch>;

class DictionariesContainer extends React.Component<Props> {
    handleSetCurrentDictionary = (current: SetDictionary | null) => this.props.setCurrentDictionary({ current: current! });

    render() {
        const { dictionaries, current, feedStatus, dictionariesStatus } = this.props;

        if (dictionaries) {
            const clinicDictionaries = dictionaries
                .filter(d => d.ancestors.includes(project.id) || d.id === project.id)
                .filter(d => d.metrics.pubCount > 0)
                .sort((a, b) => b.metrics.pubCount - a.metrics.pubCount);

            return (
                <SubcommunitiesBar
                    current={current}
                    dictionaries={clinicDictionaries}
                    setCurrentDictionary={this.handleSetCurrentDictionary}
                    isFeedLoading={feedStatus === Status.LOADING}
                    isLoading={dictionariesStatus === Status.LOADING}
                />
            );
        } return <Shared.Placeholder height="30px"/>;
    }
}

// @ts-ignore
export default connect(mapState, mapDispatch)(DictionariesContainer);
