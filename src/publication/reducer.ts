import { Status, reducer } from 'app/utils';

import { PublicationState } from 'publication/types';
import * as PublicationAction from 'publication/actions';


export const initialState: PublicationState = {
    pub: null,
    claim: null,
    origPub: null,
    pickedPub: null,
    status: Status.PENDING,
    editStatus: Status.PENDING
};

const publicationsReducer = reducer<PublicationState>(initialState,
    PublicationAction.PUBLICATION_OPERATION_START.handle((state) => {
        state.status = Status.LOADING;
    }),

    PublicationAction.PUBLICATION_OPERATION_FAIL.handle((state) => {
        state.status = Status.FAILURE;
    }),

    PublicationAction.PUBLICATION_OPERATION_SUCCESS.handle((state) => {
        state.status = Status.SUCCESS;
    }),

    PublicationAction.SET_PICKED_PUBLICATION.handle((state, payload) => {
        state.pickedPub = payload.id;
    }),

    PublicationAction.SET_PUB_CLAIM.handle((state, payload) => {
        state.claim = payload;
    }),

    PublicationAction.GET_PUBLICATION.handle((state, payload) => {
        state.pub = payload;
        state.origPub = payload;
    }),

    PublicationAction.UPDATE_PUBLICATION_QR.handle((state, payload) => {
        if (state.pub) {
            state.pub.review.quickReviews[payload.update.key] = payload.update.value;
        }
    }),

    PublicationAction.UPDATE_PUBLICATION_AB.handle((state, payload) => {
        if (state.pub) {
            state.pub.review.acceptedBy = payload.update;
        }
    }),

    PublicationAction.UPDATE_PUBLICATION_BKMRK.handle((state, payload) => {
        if (state.pub) {
            state.pub.bookmarkedBy = payload.update;
        }
    }),
    PublicationAction.EDIT_OPERATION_START.handle((state) => {
        state.editStatus = Status.LOADING;
    }),

    PublicationAction.EDIT_OPERATION_FAIL.handle((state) => {
        state.editStatus = Status.FAILURE;
    }),

    PublicationAction.EDIT_OPERATION_SUCCESS.handle((state) => {
        state.editStatus = Status.SUCCESS;
    }),
);

export default publicationsReducer;
