import * as React from 'react';
import styled from 'styled-components';
import { ErrorMessage, Field, FieldProps, FormikProps } from 'formik';

import { Status } from 'app/utils';
import { Color, Font, Spacing } from 'app/assets';

import { ReginCredentials } from 'access/types';
import { emailValidation, AccessStep } from 'access/utils';
import { Button, Title } from 'access/components/elements';

import { FUIInput } from '../../app/components/Shared';


type Props = {
    isAccessLoading: Status,
    formikBag: FormikProps<ReginCredentials>,
    setAccessStep(accessStep: AccessStep): void,
};

const Register: React.FunctionComponent<Props> = (props: Props) =>  {
    const disableButton = (props.formikBag.values.email === '' || props.formikBag.errors.email === 'Invalid email')
        || props.formikBag.values.password === '';
    const renderFieldInput = ({ field }: FieldProps<ReginCredentials>) => {
        const inputType = field.name !== 'password' ? 'text' : 'password';
        return <FUIInput placeholder={field.name} type={inputType} {...field}/>;
    };

    return (
        <>
            <Title>What's your email?</Title>
            <InputWrapper>
                <Field name="email" render={renderFieldInput} validate={emailValidation}/>
                <ErrorMessage name="email">{error => <Error>{error}</Error>}</ErrorMessage>
            </InputWrapper>
            <Field name="password" render={renderFieldInput}/>
            <RegisterButton
                text="Next"
                type="submit"
                buttonType="text"
                disabled={disableButton}
                loading={props.isAccessLoading}
            />
        </>
    );
};

const InputWrapper = styled.div`
    position: relative;
`;
const Error = styled.div`
    left: 50%;
    font-size: 12px;
    width: fit-content;
    position: absolute;
    bottom: -${Spacing.SM};
    color: ${Color.flashpub};
    font-family: ${Font.text};
    transform: translateX(-50%);

    @media (max-width: 380px) {
        font-size: 10px;
    }
`;
const RegisterButton = styled(Button)<{ disabled?: boolean }>` && {
    margin-top: ${Spacing.XXL};
    cursor: ${props => props.disabled ? 'not-allowed' : 'pointer'};
    color: ${props => props.disabled ? Color.midgrey : Color.flashpub};
}`;

export default Register;
