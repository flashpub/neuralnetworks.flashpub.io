import * as React from 'react';

import { StyledDialogWrapper } from 'ui/Overlays/styles';

type Props = {
    open: boolean;

    width?: number;
    maskStyle?: {};
    maskClosable?: boolean;
    onClose?: (open: boolean) => void;
};

export const Dialog: React.FC<Props> = ({ children, open, onClose, maskStyle, maskClosable = false, width }) => (
    <StyledDialogWrapper
        centered
        width={width}
        footer={null}
        visible={open}
        closable={false}
        maskStyle={maskStyle}
        maskClosable={maskClosable}
        onCancel={() => onClose && onClose(false)}
    >
        {children}
    </StyledDialogWrapper>
);
