import * as React from 'react';
import { useDispatch } from 'react-redux';

import * as Access from 'containers/Access';
import { StyledFlexWrapper } from 'ui/elements/styles';
import { StyledAccessText, StyledAccessTitle, StyledNextButton } from 'ui/Access/styles';

export const AccessFinish: React.FC = () => {
    const dispatch = useDispatch();

    return (
        <StyledFlexWrapper>
            <StyledAccessTitle>Congratulations!</StyledAccessTitle>
            <StyledAccessText>You're ready to start publishing</StyledAccessText>
            <StyledNextButton buttonType="link" label="Finish" onClick={() => dispatch(Access.set(false))} />
        </StyledFlexWrapper>
    );
};
