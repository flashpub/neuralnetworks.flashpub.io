export const Spacing = Object.freeze({
    _4: '4px',
    _8: '8px',
    _12: '12px',
    _24: '24px',
    _48: '48px',
    _x: (x: number) => `${x}px`,
});
