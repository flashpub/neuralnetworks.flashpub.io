import * as React from 'react';
import styled, { keyframes } from 'styled-components';

import { Color, Radius } from 'app/assets';

type Props = {
    height: string,
}

const Placeholder: React.FC<Props> = ({ height }) => (
    <Box height={height}/>
);

const blink = keyframes`
    0% { transform: translate3d(-100%, 0, 0); }
    80% { transform: translate3d(100%, 0, 0); }
    100% { transform: translate3d(100%, 0, 0); }
`;

const Box = styled.div<Props>`
    width: 100%;
    display: flex;
    flex-wrap: wrap;
    overflow: hidden;
    position: relative;
    border-radius: ${Radius};
    height: ${props => props.height};
    background-color: ${Color.lightgrey};
    ::after {
        top: 0;
        right: 0;
        left: 0;
        bottom: 0;
        z-index: 1;
        width: 100%;
        content: '';
        position: absolute;
        animation: ${blink} 1000ms linear infinite;
        background: linear-gradient(to right, rgba(185, 185, 185, 0) 20%, rgba(185, 185, 185, .35) 50%, rgba(185, 185, 185, 0) 54%) 50% 50%;
    }
`;

export default Placeholder;
