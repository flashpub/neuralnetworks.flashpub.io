import * as React from 'react';
import { InputNumber } from 'antd';
import styled from 'styled-components';
import { Color, Font, Radius } from 'assets/styles';


interface Props {
    placeholder: string;
    onChange(value: any): void;
}

export const FPNumberInput: React.FC<Props> = ({ onChange, placeholder }) => (
   <StyledInputNumber placeholder={placeholder} onChange={onChange} type="number">
       [FPNumberInput]
   </StyledInputNumber>
);

const StyledInputNumber = styled(InputNumber)`
    width: 100%;
    height: 100%;
    box-shadow: none;
    font-weight: 200;
    font-style: italic;
    border-radius: ${Radius};
    font-family: ${Font.text};
    border-color: ${Color.midgrey};
    background-color: ${Color.notwhite};
    
    :hover {
        border-color: ${Color.midgrey};
    }
`;
