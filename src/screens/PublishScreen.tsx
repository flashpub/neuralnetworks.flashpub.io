import * as React from 'react';
import { useDispatch } from 'react-redux';
import { useEffectOnce, useUnmount } from 'react-use';
import { includes, kebabCase, find, camelCase } from 'lodash';
import { RouteComponentProps, Router, useLocation, useNavigate } from '@reach/router';

import { useTypedSelector } from 'helpers';
import { NotFound } from 'services/navigation';
import * as Pub from 'containers/Pub';
import * as Content from 'containers/Content';
import { Stepper, Introduction, Publication, Submit, Claim } from 'ui/Publish';
import { StyledPublishWrapper } from 'ui/Publish/styles';
import { GlobalLoader } from 'ui/Overlays';
import { Spacing } from 'assets/styles';

type Props = RouteComponentProps<{ contentType: string }>;

export const PublishScreen: React.FC<Props> = (props) => {
    const dispatch = useDispatch();
    const location = useLocation();
    const navigate = useNavigate();
    const campaigns = useTypedSelector((state) => state.campaigns.data.campaigns);
    const selectedContent = useTypedSelector((state) => state.content.data.type);
    const community = useTypedSelector((state) => state.community.data.community);
    const publishStatus = useTypedSelector((state) => state.publish.status.type);

    const steps = ['', '/', '/claim', '/publication', '/submit'];
    const step = location.pathname.split(`/${props.contentType}`)[1];
    const isOnStep = includes(steps, step);

    useEffectOnce(() => {
        const pass = (location.state as any)?.pass;
        if (pass) {
            navigate(`/publish/${props?.contentType}/claim`);
        } else {
            navigate(`/publish/${props?.contentType}`);
        }
    });

    React.useEffect(() => {
        if ((community && props) || (campaigns && props)) {
            const campaignsContentTypes = campaigns.map((c) => Object.values(c.contentTypes)).flat();
            const communityContentTypes = Object.values(community?.contentTypes || {});
            const contentTypes = [...communityContentTypes, ...campaignsContentTypes];
            const content = find(contentTypes, (c) => kebabCase(c.contentTypeLabel as string) === props.contentType);
            if (!selectedContent) {
                dispatch(Content.populate(content));
            } else {
                const quickReviews = selectedContent.quickReviews.reduce((a, b) => ({ ...a, [camelCase(b)]: [] }), {});
                dispatch(Pub.form.sub({ form: 'draft', field: 'review.quickReviews', value: quickReviews }));
            }
        }
    }, [community, dispatch, selectedContent, props]);
    
    useUnmount(() => {
        dispatch(Pub.form.clear('draft'));
    });

    return (
        <StyledPublishWrapper>
            {isOnStep && <Stepper contentType={props.contentType} />}
            <div style={{ width: '100%', padding: Spacing._24 }}>
                <Router style={{ width: '100%' }}>
                    <NotFound default />
                    <Introduction path="/" />
                    <Claim path="/claim" />
                    <Publication path="/publication" />
                    <Submit path="/submit" />
                </Router>
            </div>
            {publishStatus === 'LOADING' && <GlobalLoader slogan="We're submitting your work" />}
        </StyledPublishWrapper>
    );
};
