import * as React from 'react';
import { useTypedSelector } from 'helpers';
import { Authenticated, Open } from 'services/navigation';

export const Contents: React.FC = () => {
    const auth = useTypedSelector((state) => state.auth.data);

    return auth ? <Authenticated /> : <Open />;
};
