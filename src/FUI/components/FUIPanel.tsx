import * as React from 'react';
import styled from 'styled-components';

import { Color, Font, Radius, Spacing } from 'app/assets';


type FUIPanelStyleProps = {
    textAlign?: 'left' | 'right' | 'center' | 'justify' | string;
}

export interface FUIPanelProps extends FUIPanelStyleProps {
    title: string;
    order?: number;
    header?: boolean;
    children?: React.ReactNode;
    previousPanel?: FUIPanelProps;
    onReturn?: ([string]: any) => void;
}


export const FUIPanel: React.FC<FUIPanelProps> = (
    { textAlign, title, onReturn, children, header = true, previousPanel }
) => {
    const handleOnReturn = () => {
        onReturn && onReturn({});
    };

    return (
        <PanelWrapper
            textAlign={textAlign}
        >
            <PanelHeader>
                <PreviousPanel>
                    {header && previousPanel && (
                        <PreviousButton onClick={handleOnReturn}>
                            <PreviousPanelIcon>{'\u2039'}</PreviousPanelIcon>
                            <PreviousPanelName>{previousPanel && previousPanel.title}</PreviousPanelName>
                        </PreviousButton>
                    )}
                </PreviousPanel>
                <PanelTitle>{title}</PanelTitle>
                <PreviousPanel/>
            </PanelHeader>
            <PanelContent>
                {children}
            </PanelContent>
        </PanelWrapper>
    );
};

const PanelWrapper = styled.div<FUIPanelStyleProps>`
    width: 100%;
    display: flex;
    margin: 0 auto;
    height: fit-content;
    flex-direction: column;
    text-align: ${({ textAlign }) => textAlign || 'center'};
`;
const PanelHeader = styled.header<FUIPanelStyleProps>`
    height: 30px;
    display: flex;
    flex-shrink: 0;
    margin: ${Spacing.XXL} 0;
    text-align: center;
    align-items: center;
    color: ${Color.midgrey};
    font-family: ${Font.mono};
`;
const PreviousPanel = styled.div`
    flex: 1 1;
    display: flex;
    align-items: stretch;
`;
const PreviousButton = styled.div`
    cursor: pointer;
    padding: ${Spacing.S} ${Spacing.M};
    width: fit-content;
    border-radius: ${Radius};
    white-space: nowrap;
    
    &:hover {
        background-color: ${Color.lightgrey};
    }
`;
const PreviousPanelIcon = styled.span`
    margin-right: ${Spacing.S};
`;
const PreviousPanelName = styled.span``;
const PanelTitle = styled.div`
    overflow: hidden;
    word-wrap: normal;
    white-space: nowrap;
    text-overflow: ellipsis;
`;
const PanelContent = styled.div`
    padding: 0 ${Spacing.ML};
`;
