import { Dispatch } from 'redux';

import * as API from 'api';
import { AppThunk } from 'store';
import * as Pub from 'containers/Pub';
import { message } from 'antd';

export function fetchPub(id: string): AppThunk {
    return async (dispatch: Dispatch) => {
        dispatch(Pub.status.loading());
        try {
            const resp = await API.pub.load(id);
            if (resp) {
                dispatch(Pub.populate(resp.data()));
                dispatch(Pub.status.success());
            }
        } catch (err) {
            dispatch(Pub.status.failure(err.message));
        }
    };
}
export function editPub(): AppThunk {
    return async (dispatch: Dispatch, getState) => {
        dispatch(Pub.status.loading());

        const pub = getState().pub.data.pub;
        const draft = getState().pub.data.draft;

        const changes = [];
        const draftsK = Object.keys(draft);
        const draftsV = Object.values(draft);
        const pubK = Object.keys(pub);
        const pubV = Object.values(pub);

        for (let i = 0; i < draftsK.length; i++) {
            for (let j = 0; j < pubK.length; j++) {
                if (draftsK[i] === pubK[j]) {
                    if (draftsV[i] !== pubV[j]) {
                        changes.push(draftsK[i]);
                    }
                }
            }
        }

        if (changes.length > 0) {
            try {
                const resp = await API.pub.update(draft);
                if (resp) {
                    dispatch(Pub.populate(draft));
                    message.success(`Changes applied to ${changes.map((c) => ` ${c}`)}`);
                    dispatch(Pub.status.success());
                }
            } catch (err) {
                dispatch(Pub.status.failure(err.message));
            }
        } else {
            dispatch(Pub.status.success());
            message.info('No changes were made');
        }
    };
}

export function deletePub(id: string): AppThunk | Promise<string> {
    return (dispatch: Dispatch) => {
        return API.pub.delete(id).then(() => 'OK').catch((err) => dispatch(Pub.status.failure(err.message)));
    };
}
