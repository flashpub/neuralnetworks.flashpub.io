import styled from 'styled-components/macro';
import { StyledFlexWrapper } from 'ui/elements/styles';
import { Color, Radius, Spacing, Font } from 'assets/styles';

export const StyledDictionaryBar = styled(StyledFlexWrapper)`
    width: 100%;
    height: 40px;
    flex-direction: row;
    border-radius: ${Radius};
    justify-content: flex-start;
    flex-wrap: nowrap;
`;
export const StyledDictionaryTerm = styled.div`
    display: inline-block;
    font-family: ${Font.title};
    margin-right: ${Spacing._12};
    a {
        overflow: hidden;
        border-bottom: 2px solid;
        :hover {
            color: ${Color.community} !important;
        }
    }
`;
