type GatesContainerState = {
    readonly isApiOpen: boolean;
    readonly isRegOpen: boolean;
    readonly isPublishOpen: boolean;
};
type NotificationsContainerState = {
    readonly update: {
        [key: string]: any;
    } | null;
};
