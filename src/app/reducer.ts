import { generate } from 'shortid';

import { AppState } from 'app/types';
import * as AppAction from 'app/actions';
import { Status, PortalView, reducer } from 'app/utils';

export const initialState: AppState = {
    alerts: [],
    gates: null,
    isAuth: false,
    community: null,
    status: Status.LOADING,
    view: PortalView.DEFAULT,
    sesameOpen: false,
};

const appReducer = reducer<AppState>(initialState,
    AppAction.SET_AUTH.handle((state, payload) => {
        state.isAuth = payload.isAuth;
    }),
    
    AppAction.SET_APP_STATUS.handle((state, payload) => {
        state.status = payload.status;
    }),
    
    AppAction.SET_PORTAL_VIEW.handle((state, payload) => {
        state.view = payload.view;
    }),

    AppAction.SET_MAIN_COMMUNITY.handle((state, payload) => {
        state.community = payload;
    }),

    AppAction.SET_SYSTEM_GATES.handle((state, payload) => {
        state.gates = payload;
    }),
    
    AppAction.ADD_ALERT.handle((state, payload) => {
        state.alerts = [
            ...state.alerts,
            { id: generate(), ...payload },
        ];
    }),

    AppAction.SET_SESAME_OPEN.handle((state, payload) => {
        state.sesameOpen = payload;
    }),
    
    AppAction.REMOVE_ALERT.handle((state, payload) => {
        state.alerts = state.alerts.filter(alert => alert.id !== payload.id);
    }),
);

export default appReducer;
