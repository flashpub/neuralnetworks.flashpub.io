import dayjs from 'dayjs';
import * as React from 'react';
import { Collapse } from 'antd';
import styled from 'styled-components';
import lowerCase from 'lodash/lowerCase';
import { DownOutlined } from '@ant-design/icons';

import { Color, Font, Spacing } from 'assets/styles';
import { StyledFlexWrapper } from 'ui/elements/styles';

interface Props {
    headerTitle: string | JSX.Element;
    conditions: PubClaimCondition[];
}

export const FPCollapse: React.FC<Props> = ({ conditions, headerTitle }) => {
    return (
        <StyledCollapse
            bordered={false}
            expandIconPosition="right"
            className="site-collapse-custom-collapse"
            expandIcon={({ isActive }) => <DownOutlined rotate={isActive ? 180 : 0} style={{ fontSize: 10 }} />}
        >
            <StyledCollapsePanel header={headerTitle} key="1" className="site-collapse-custom-panel">
                {conditions.map((c) => {
                    const conditionValue = Array.isArray(c.value)
                        ? c.value.toString().split(',').join(', ')
                        : c.type === 'date'
                            ? dayjs(c.value).format('M/D/YYYY')
                            : c.type === 'boolean'
                                ? c.value === true ? 'yes' : 'no'
                                : c.type === 'city'
                                    ? c.value.label
                                    : c.value;
                    return (
                        <StyledCollapsePanelRecord key={c.id}>
                            <div>{lowerCase(c.name)}:</div>
                            <em>{conditionValue}</em>
                        </StyledCollapsePanelRecord>
                    );
                })}
            </StyledCollapsePanel>
        </StyledCollapse>
    );
};

const StyledCollapse = styled(Collapse)`
    width: 100%;
    color: ${Color.midgrey} !important;
    font-family: ${Font.text} !important;
    
    .ant-collapse-header {
        width: fit-content;
        background-color: transparent;
        color: ${Color.grey} !important;
        padding: 0 ${Spacing._x(30)} 0 0!important;
    }
`;
const StyledCollapsePanel = styled(Collapse.Panel)`
    .ant-collapse-content {
        background-color: #fff !important;
    }
    .ant-collapse-content-box {
        display: flex;
        height: fit-content;
        padding: 0 !important;
        flex-direction: column;
    }
`;
const StyledCollapsePanelRecord = styled(StyledFlexWrapper)`
    width: 100%;
    font-size: 16px;
    text-align: center;
    position: relative;
    flex-direction: row;
    color: ${Color.grey};
    border-top: 1px dashed ${Color.lightgrey};
    
    div {
        width: 50%;
        display: flex;
        font-weight: 400;
        text-align: right;
        align-items: center;
        justify-content: flex-end;
    }
    em {
        width: 50%;
        text-align: left;
        font-weight: 300;
        margin-left: ${Spacing._4};
    }
`;
