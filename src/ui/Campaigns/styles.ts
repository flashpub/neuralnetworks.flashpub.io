import { Map } from 'react-leaflet';
import styled from 'styled-components';
import { Drawer, Popover } from 'antd';
import { CloseOutlined, LeftOutlined, QuestionCircleFilled } from '@ant-design/icons';

import { StyledFlexWrapper } from 'ui/elements/styles';
import { FPButton, FPInput } from 'ui/elements';
import { Color, Font, MaxWidth, Media, Radius, Spacing } from 'assets/styles';

export const StyledCampaignWrapper = styled(StyledFlexWrapper)``;
export const StyledCampaignTitleWrapper = styled(StyledFlexWrapper)``;
export const StyledCampaignTitle = styled(FPButton)`
    font-size: 26px;
    font-family: ${Font.title};

    div {
        font-size: 18px;
        ${Media.tablet} {
            font-size: 26px;
        }
    }

    span {
        margin-top: -6px;
        span {
            font-size: 12px;
        }
    }
`;
export const StyledCampaignResourcesList = styled.div`
    text-align: center;
    background-color: #fff;
    padding: ${Spacing._12};
    border-radius: ${Radius};
    font-family: ${Font.text};
    border: 1px solid ${Color.midgrey};

    div:first-child {
        color: ${Color.grey};
    }
    p {
        font-size: 12px;
        margin-bottom: 0;
        margin-top: ${Spacing._4};
        a {
            margin-top: 0;
            color: ${Color.community};
        }
    }
`;
export const StyledCampaignResourcesListPublishWrapper = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  margin: 0 ${Spacing._12};
  
  a {
    margin-bottom: ${Spacing._4};
  }
  span {
    font-size: 16px;
    color: ${Color.community};
    :hover {
        color: ${Color.grey}
    }
  }
`;
export const StyledMapWrapper = styled.div`
    width: 100vw;
    overflow: hidden;
    position: relative;
    height: calc(100vh - 140px);
`;
export const StyledMapSearchWrapper = styled.div`
    top: 10px;
    left: 60px;
    z-index: 1000;
    position: absolute;
`;
export const StyledMapSearch = styled(FPInput)`
    height: 30px;
    text-align: start;
    background-color: #fff;
    box-shadow: 0 2px 5px 0px rgba(0, 0, 0, 0.45);
`;
export const StyledMap = styled(Map)`
    width: 100%;
    height: 100%;
    .leaflet-popup {
        display: none;
    }
    path {
        stroke: none;
        fill-opacity: 0.7;
    }

    .ant-empty-img-simple-g {
        path {
            stroke: ${Color.midgrey};
        }
    }
`;
export const StyledMapDrawer = styled(Drawer)<{ visible: boolean }>`
    position: absolute;
    // condition that keeps the map from sliding to the side when the drawer opens
    width: ${({ visible }) => (visible ? '375px !important' : 'unset !important')};

    .ant-drawer-content-wrapper {
        top: 10px;
        bottom: 10px;
        height: unset;
        width: 375px !important;
        border-top-left-radius: ${Radius};
        border-bottom-left-radius: ${Radius};

        .ant-drawer-content {
            border-top-left-radius: ${Radius};
            border-bottom-left-radius: ${Radius};
        }
    }
    .ant-drawer-body,
    .ant-drawer-header,
    .ant-drawer-footer {
        padding: ${Spacing._12};
    }

    .ant-drawer-mask {
        display: none;
    }
`;
styled(LeftOutlined)`
    position: absolute;
`;
export const StyledDrawerClose = styled(CloseOutlined)`
    top: 5px;
    left: 5px;
    position: absolute;
`;
export const StyledFooterWrapper = styled(StyledFlexWrapper)`
    align-items: flex-end;
    font-family: ${Font.title};
    
    button:first-child, button:nth-child(2) {
        margin-bottom: ${Spacing._8};
    }
`;
export const StyledHeaderWrapper = styled(StyledFlexWrapper)`
    text-align: center;
    color: ${Color.grey};
    font-family: ${Font.text};
`;
export const StyledHeaderTitle = styled.div`
    font-size: 22px;
    font-weight: 400;
    color: ${Color.community};
    margin-bottom: ${Spacing._8};
`;
export const StyledLegend = styled(QuestionCircleFilled)`
    left: 10px;
    bottom: 10px;
    z-index: 1000;
    font-size: 30px;
    position: absolute;
    color: ${Color.flashpub};
`;
export const StyledLegendPopover = styled(Popover)`
    .ant-popover-inner {
        border-radius: ${Radius};
    }
`;
export const StyledCityStats = styled.div`
    width: 100%;
    text-align: start;
    margin-top: ${Spacing._12};
`;
export const StyledStats = styled.h4`
    font-size: 16px;
    margin: ${Spacing._4} 0;
`;
export const StyledDeathStats = styled(StyledStats)`
    color: #be0200;
`;
export const StyledMapPubRecordWrapper = styled.div`
    border-radius: ${Radius};
    font-family: ${Font.text};
    margin-bottom: ${Spacing._8};
    padding: ${Spacing._4} ${Spacing._x(6)};
    box-shadow: 0 0 5px 2px ${Color.community_t(0.25)};
`;
export const StyledMapPubRecordLink = styled.a``;
export const StyledMapPubRecordTitle = styled.div`
    color: #000;
    line-height: 1;
    font-size: 16px;
    font-family: ${Font.title};
`;
export const StyledMapPubMeta = styled(StyledFlexWrapper)`
    flex-direction: row;
    color: ${Color.grey};
    margin-top: ${Spacing._4};
    justify-content: space-between;

    :last-child {
        font-size: 12px;
        font-weight: 200;
        margin-top: ${Spacing._8};
        font-family: ${Font.text};
    }
`;
export const StyledHeadingWrapper = styled(StyledFlexWrapper)`
    flex-direction: row;
    margin-bottom: ${Spacing._12};
    justify-content: space-between;
`;
export const StyledHeading = styled.div`
    font-size: 18px;
    color: ${Color.community};
    font-family: ${Font.title};
`;
export const StyledSeeAllButton = styled(FPButton)`
    height: 30px;
    font-size: 12px;
    padding-right: 0;
    color: ${Color.midgrey};
    font-family: ${Font.text};
    :hover {
        color: ${Color.grey};
    }
`;

const StyledDashboardContainer = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    max-width: ${MaxWidth};
    padding: ${Spacing._48};
    justify-content: center;
    min-height: 650px;
    position: relative;
`;
const StyledDashboardGridContainer = styled.div`
    width: 100%;
    height: 100%;
    display: grid;
    gap: 20px 20px;
    justify-self: center;
    grid-template-columns: repeat(auto-fill, minmax(250px, 1fr));
`;
const StyledDashboardPartnershipWrapper = styled.div`
    width: 100%;
    display: flex;
    font-family: ${Font.text};
    font-style: italic;
    color: ${Color.community};
    margin-top: 10px;
    align-items: center;
    justify-content: center;
    padding: ${Spacing._24} 0;
`;
const StyledDashboardPartnershipLogo = styled.img`
  height: 60px;
  margin-left: ${Spacing._12};
`;

export const StyledDashboard = {
    Container: StyledDashboardContainer,
    GridContainer: StyledDashboardGridContainer,
    PartnershipWrapper: StyledDashboardPartnershipWrapper,
    PartnershipLogo: StyledDashboardPartnershipLogo,
};

const StyledDashboardTileContainer = styled.div`
  width: 100%;
  height: auto;
  display: flex;
  align-items: center;
  flex-direction: column;
  padding: ${Spacing._24};
`;
const StyledDashboardTileWrapper = styled.div<{ disabled?: boolean }>`
  border-radius: ${Radius};
  border: 2px solid;
  width: 250px;
  height: 215px;
  position: relative;
  cursor: ${({ disabled }) => disabled ? 'default' : 'pointer'};
  border-color: ${({ disabled }) => disabled ? Color.lightgrey : Color.midgrey};
  transition: all .2s ease;
  box-shadow: ${({ disabled }) => disabled ? 'none' : `0px 0px 10px 5px ${Color.community_t15}`};
  :hover {
    transform: ${({ disabled }) => disabled ? 'none' : 'scale(1.02)'};
    border-color: ${({ disabled }) => disabled ? 'none' : Color.community};
  }
`;
const StyledDashboardTileDataWrapper = styled.div`
  padding: ${Spacing._24};
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;
const StyledDashboardTileTitle = styled.h1`
  font-size: 24px;
  text-align: center;
  font-family: ${Font.text};
  font-weight: 300;
  line-height: 1;
  margin-bottom: ${Spacing._4};
`;
const StyledDashboardTileSubTitle = styled.h2`
  font-size: 14px;
  line-height: 1;
  text-align: center;
  font-family: ${Font.text};
  font-style: italic;
  font-weight: 300;
`;

const StyledDashboardTileIndicatorWrapper = styled.div`
  display: flex;
  width: 100%;
  position: relative;
  justify-content: center;
  flex-direction: column;
  align-items: center;
`;
const StyledDashboardTileIndicator = styled.div`
  line-height: 1;
  font-size: 46px;
  font-family: ${Font.text};
  font-weight: 300;
`;
const StyledDashboardTileIndicatorSub = styled.div`
  font-family: ${Font.text};
  font-weight: 300;
`;
const StyledDashboardTileFooterWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  
  div {
      strong {
          font-weight: 800;
      }
      font-family: ${Font.text};
      font-weight: 300;
  }
`;
const StyledDashboardTileSoonOverlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  height: 100%;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: rgba(255,255,255,.7);
`;
const StyledDashboardTileSoonOverlayText = styled.div`
  font-size: 20px;
  color: ${Color.midgrey};
  font-family: ${Font.text};
`;

export const StyledDashboardTile = {
    Title: StyledDashboardTileTitle,
    Wrapper: StyledDashboardTileWrapper,
    SubTitle: StyledDashboardTileSubTitle,
    Container: StyledDashboardTileContainer,
    DataWrapper: StyledDashboardTileDataWrapper,
    IndicatorWrapper: StyledDashboardTileIndicatorWrapper,
    Indicator: StyledDashboardTileIndicator,
    IndicatorSub: StyledDashboardTileIndicatorSub,
    FooterWrapper: StyledDashboardTileFooterWrapper,
    SoonOverlay: StyledDashboardTileSoonOverlay,
    SoonOverlayText: StyledDashboardTileSoonOverlayText,
};

const StyledDashboardAddTileContainer = styled.div``;
const StyledDashboardAddTileWrapper = styled(StyledDashboardTileWrapper)`
    display: flex;
    align-items: center;
    flex-direction: column;
    justify-content: center;
    padding: 0;
    border: none;
      transition: all .2s ease;
      :hover {
        transform: none;
        border-color: ${Color.community};
      }
`;
const StyledDashboardAddTileButton = styled.div`
    height: 100%;
    width: 100%;
    border-radius: ${Radius};
    display: flex;
    align-items: center;
    justify-content: center;
    margin-bottom: 2px;
    flex-direction: column;
    border: 2px dashed ${Color.midgrey};
    transition: all .2s ease;
    :hover {
      transform: scale(1.01);
      border-color: ${Color.community};
    }
    div {
        font-weight: 300;
        color: ${Color.midgrey};
        font-family: ${Font.text};
    }
    div:first-child {
        font-size: 30px;
    }
`;

export const StyledDashboardAddTile = {
    Container: StyledDashboardAddTileContainer,
    Wrapper: StyledDashboardAddTileWrapper,
    Button: StyledDashboardAddTileButton,
};

const StyledDashboardDetailsContainer = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    min-height: 700px;
    padding: 0 ${Spacing._48};
    align-items: center;
    flex-direction: column;
    position: relative;
`;
const StyledDashboardDetailsWrapper = styled.div<{ endorsed?: boolean }>`
    width: 100%;
    max-width: 700px;
    display: flex;
    height: 400px;
    position: relative;
    border-radius: ${Radius};
    padding: ${Spacing._24};
    border: 1px solid ${Color.community};
    box-shadow: 0px 0px 10px 10px ${Color.community_t15};
    
    text {
        font-weight: 400;
        color: ${Color.midgrey} !important;
        font-family: ${Font.text} !important;
    }
    
    circle {
      cursor: pointer;
    }
`;
const StyledDashboardDetailsPubDetailsWrapper = styled.div`
    width: 100%;
    max-width: 700px;
    display: flex;
    height: auto;
    position: relative;
    border-radius: ${Radius};
    padding: ${Spacing._24};
    margin-top: ${Spacing._12};
    border: 1px solid ${Color.community};
    box-shadow: 0px 0px 10px 10px ${Color.community_t15};
`;
const StyledDashboardDetailsPubDetailsInfoWrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    margin-right: ${Spacing._12};
    flex-wrap: wrap;
`;
const StyledDashboardDetailsPubDetailsDataWrapper = styled.div`
  width: fit-content;
  height: fit-content;
  position: relative;
  justify-content: flex-start;
  display: flex;
  flex-direction: column;
  align-items: center;
  border-radius: ${Radius};
  border: 1px solid ${Color.midgrey};
  background-color: ${Color.lightgrey};
`;
const StyledDashboardDetailsPubDetailsTitle = styled.div`
    font-size: 20px;
    color: ${Color.grey};
    line-height: 1;
    font-family: ${Font.title};
`;
const StyledDashboardDetailsPubKeyDetails = styled.div`
    font-weight: 300;
    font-style: italic;
    color: ${Color.grey};
    font-family: ${Font.text};
    line-height: 1;
    font-size: 14px;
    margin: ${Spacing._12} 0;
`;
const StyledDashboardDetailsPubDetailsURL = styled.div`
    font-size: 14px;
    color: ${Color.community};
    font-family: ${Font.text};
    :hover {
      cursor: pointer;
      color: ${Color.flashpub};
    }
`;
const StyledDashboardDetailsDate = styled.div`
    top: 0;
    right: 4px;
    font-weight: 500;
    font-style: italic;
    font-size: 12px;
    position: absolute;
    font-family: ${Font.text};
    color: ${Color.midgrey};
`;
const StyledDashboardDetailsDataTitle = styled.div`
    text-align: center;
    font-family: ${Font.text};
`;
const StyledDashboardDetailsNavigationWrapper = styled.div`
    font-family: ${Font.text};
    margin-bottom: ${Spacing._24};
`;
const StyledDashboardDetailsHeaderWrapper = styled.div`
  top: 0;
  left: 0;
  width: 100%;
  display: flex;
  position: absolute;
  flex-direction: column;
`;
const StyledDashboardDetailsHeaderTopWrapper = styled.div`
  display: flex;
  flex-direction: row;
  padding-top: ${Spacing._8};
  justify-content: space-between;
`;
const StyledDashboardDetailsHeaderBottomWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  font-size: 12px;
  font-family: ${Font.text};
`;
const StyledDashboardDetailsHeaderIndicators = styled.div`
  display: flex;
  transform: scale(0.7);
  position: relative;
  flex-direction: row;
`;
const StyledDashboardDetailsHeaderTopTitle = styled.div`
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    justify-content: center;
    font-size: 26px;
    margin-left: ${Spacing._48};
`;
const StyledDashboardDetailsGraphWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  position: relative;
  align-items: center;
  justify-content: center;
  
  //svg > g {
  //    transform: scale(1.1) translate(100px, 60px)
  //}
`;

const StyledDashboardDetailsLegendWrapper = styled.div`
    position: absolute;
    display: flex;
    flex-direction: column;
    width: fit-content;
    height: fit-content;
    z-index: 1;
    right: 0;
    padding: ${Spacing._4} ${Spacing._8};
    border-radius: ${Radius};
    background-color: ${Color.lightgrey};
`;
const StyledDashboardDetailsLegendItemWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  font-size: 13px;
  font-family: ${Font.text};
`;
const StyledDashboardDetailsLegendItemEndorsed = styled.div`
  width: 12px;
  height: 12px;
  border-radius: 100%;
  background-color: ${Color.flashpub};
  margin-right: ${Spacing._8};
`;
const StyledDashboardDetailsLegendItemInReview = styled(StyledDashboardDetailsLegendItemEndorsed)`
  background-color: #fff;
  border: 1px solid ${Color.flashpub};
`;

export const StyledDashboardDetails = {
    Container: StyledDashboardDetailsContainer,
    Wrapper: StyledDashboardDetailsWrapper,
    LegendWrapper: StyledDashboardDetailsLegendWrapper,
    LegendItemWrapper: StyledDashboardDetailsLegendItemWrapper,
    LegendItemEndorsed: StyledDashboardDetailsLegendItemEndorsed,
    LegendItemInReview: StyledDashboardDetailsLegendItemInReview,
    PubDetailsWrapper: StyledDashboardDetailsPubDetailsWrapper,
    PubDetailsInfoWrapper: StyledDashboardDetailsPubDetailsInfoWrapper,
    PubDetailsTitle: StyledDashboardDetailsPubDetailsTitle,
    PubDetailsURL: StyledDashboardDetailsPubDetailsURL,
    PubKeyDetails: StyledDashboardDetailsPubKeyDetails,
    PubDetailsDataWrapper: StyledDashboardDetailsPubDetailsDataWrapper,
    Date: StyledDashboardDetailsDate,
    GraphWrapper: StyledDashboardDetailsGraphWrapper,
    DataTitle: StyledDashboardDetailsDataTitle,
    HeaderWrapper: StyledDashboardDetailsHeaderWrapper,
    NavigationWrapper: StyledDashboardDetailsNavigationWrapper,
    HeaderTopWrapper: StyledDashboardDetailsHeaderTopWrapper,
    HeaderIndicators: StyledDashboardDetailsHeaderIndicators,
    HeaderTopTitle: StyledDashboardDetailsHeaderTopTitle,
    HeaderBottomWrapper: StyledDashboardDetailsHeaderBottomWrapper,
};
