import styled from 'styled-components';

import { Color, Font, Media, Radius, Spacing } from 'app/assets';

export const FUIClaimItem = styled.span`
    overflow: hidden;
    max-width: 150px;
    border: 1px solid;
    white-space: nowrap;
    display: inline-block;
    color: ${Color.midgrey};
    text-overflow: ellipsis;
    border-radius: ${Radius};
    font-family: ${Font.mono};
    padding: ${Spacing.S} ${Spacing.SM};
    background-color: rgba(255,255,255,.7);
`;
export const FUIPositiveLine = styled.div`
    width: 10px;
    height: 4px;
    margin: 10px 0;
    display: inline-block;
    border-top: 1px solid;
    color: ${Color.midgrey};
    border-bottom: 1px solid;
`;
export const FUIClaim = styled.div`
    display: block;
    cursor: pointer;
    font-size: 14px;
    width: fit-content;
    margin: ${Spacing.M} auto;
    color: ${Color.midgrey};
    font-family: ${Font.text};
    
    &:hover ${FUIClaimItem} {
        color: ${Color.community};
    }
    &:hover ${FUIPositiveLine} {
        color: ${Color.community};
    }
    
    ${Media.desktop} {
        display: inline-block;
        margin: ${Spacing.M};
    }
`;
export const FUINeutralLine = styled(FUIPositiveLine)`
    margin: 8px 0;
    border-bottom: 0;
    border-top: 1px solid ${Color.midgrey};
`;
export const FUINegativeLine = styled(FUIPositiveLine)`
    position: relative;
    
    &::before {
        top: 50%;
        left: 50%;
        content: '/';
        font-size: 10px;
        position: absolute;
        margin-bottom: 10px;
        transform: translate(-50%, -50%);
    }
`;
