import { message } from 'antd';
import { Dispatch } from 'redux';

import * as API from 'api';
import { AppThunk } from 'store';
import { db } from 'configs/firebase';
import * as Publish from 'containers/Publish';

export function createPub(data: any, draft?: string, cb?: any ): AppThunk {
    return async (dispatch: Dispatch) => {
        dispatch(Publish.status.loading());
        try {
            if (draft === 'draft') {
                await db.collection('pubs').doc(data.pub.id).set(data.pub);
            } else {
                await API.pub.create(data);
                dispatch(Publish.step({ order: 4, type: 'SUMMARY' }));
                cb && cb();
            }
            dispatch(Publish.status.success());
            message.success(`Publication ${draft ? 'saved' : 'added'} successfully`);
        } catch (err) {
            dispatch(Publish.status.failure(err.message));
        }
    };
}
