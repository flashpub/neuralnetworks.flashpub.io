import { createGlobalStyle } from 'styled-components';
import { Color, Font, Radius } from 'assets/styles';

export const AntdOverwrites = createGlobalStyle`
    .ant-steps-item-process .ant-steps-item-icon {
        background: ${Color.community};
        border-color: ${Color.community};
    }
    .ant-steps-item-finish .ant-steps-item-icon {
        border-color: ${Color.community};
    }
    .ant-steps-item-finish .ant-steps-item-icon > .ant-steps-icon {
        color: ${Color.community};
    }
    .ant-steps-item-finish > .ant-steps-item-container > .ant-steps-item-content > .ant-steps-item-title::after {
        background-color: ${Color.community};
    }
    .ant-steps .ant-steps-item:not(.ant-steps-item-active):not(.ant-steps-item-process) > .ant-steps-item-container[role='button']:hover .ant-steps-item-icon {
        border-color: ${Color.community};
    }
    .ant-steps .ant-steps-item:not(.ant-steps-item-active):not(.ant-steps-item-process) > .ant-steps-item-container[role='button']:hover .ant-steps-item-icon .ant-steps-icon {
        color: ${Color.community};
    }
    .ant-steps .ant-steps-item:not(.ant-steps-item-active) > .ant-steps-item-container[role='button']:hover .ant-steps-item-title {
        color: ${Color.community};
    }
    .ant-steps-item-process > .ant-steps-item-container > .ant-steps-item-content > .ant-steps-item-title {
        color: ${Color.grey};
    }
    .ant-steps-item-title {
        font-family: ${Font.text};
    }
    .ant-steps-item-wait > .ant-steps-item-container > .ant-steps-item-content > .ant-steps-item-title::after {
        background-color: ${Color.midgrey};
    }
    .ant-steps-item-process > .ant-steps-item-container > .ant-steps-item-content > .ant-steps-item-title::after {
        background-color: ${Color.midgrey};
    }
    .ant-steps-item-disabled {
        cursor: not-allowed;
    }
    .ant-tooltip-inner, .ant-tooltip-arrow::before {
        font-weight: 100;
        font-family: ${Font.text};
        background-color: ${Color.grey};
    }
    .ant-popover-inner {
        border-radius: ${Radius};
        box-shadow: 0px 0px 6px 6px ${Color.community_t(0.25)};
    }
    
    .dropdown-menu{
        width: 99%;
        left: 2px !important;
        right: 2px !important;
    }
    
    .endorse_dialog {
        .ant-modal-content {
            border-radius: ${Radius};
            font-family: ${Font.text};
            
            .ant-modal-confirm-btns {
                button {
                    border-radius: ${Radius};
                }
                .ant-btn:hover, .ant-btn:focus {
                    color: ${Color.community};
                    background-color: #fff;
                    border-color: ${Color.community};
                }
            }
        }
        .ant-modal-confirm-body-wrapper {}
    }
    
    .ant-btn-primary {
        color: unset;
        border-color: unset;
        background-color: unset;
    }
    
    .ant-picker-panel {
        font-family: ${Font.text};
    }
    .ant-picker-today-btn {
        color: ${Color.community} !important;
    }
    
    .ant-select-item-option-selected {
        font-weight: 500 !important;
        color: ${Color.grey} !important;
        background-color: ${Color.community_t(.3)} !important;
    }
    .ant-collapse-borderless {
        background-color: transparent !important;
    }
    .ant-collapse-borderless > .ant-collapse-item {
        border: 0;
    }
    .fp-select-simple-option {
        font-family: ${Font.text};
    }
    
    .fp-share-options {
        padding: 0;
        .ant-popover-inner {
            box-shadow: none;
            background-color: transparent;
        }
        .ant-popover-arrow {
            display: none;
        }
    }
`;
