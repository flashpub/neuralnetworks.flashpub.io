import { Dispatch } from 'redux';

import * as API from 'api';
import { AppThunk } from 'store';
import { userData } from 'helpers';
import * as Access from 'containers/Access';
import * as authentication from 'services/authentication';

export function signinUser(creds: Credentials): AppThunk {
    return async (dispatch: Dispatch) => {
        dispatch(authentication.status.loading());
        try {
            const resp = await API.user.signin(creds);
            if (resp) {
                dispatch(authentication.signin(userData(resp.user)));
                dispatch(authentication.status.success());
                dispatch(Access.set(false));
            }
        } catch (err) {
            dispatch(authentication.status.failure(err.message));
        }
    };
}
export function registerUser(creds: Credentials): AppThunk {
    return async (dispatch: Dispatch) => {
        dispatch(authentication.status.loading());
        try {
            const resp = await API.user.create(creds);
            if (resp) {
                dispatch(Access.step('ORCID'));
                const resp = await API.user.signin(creds);
                if (resp) {
                    dispatch(authentication.signin(userData(resp.user)));
                    dispatch(authentication.status.success());
                }
            }
        } catch (err) {
            dispatch(authentication.status.failure(err.message));
        }
    };
}
export function signoutUser(): AppThunk {
    return async (dispatch: Dispatch) => {
        try {
            await API.user.logout();
            dispatch(authentication.signout());
        } catch (err) {
            dispatch(authentication.status.failure(err.message));
        }
    };
}
