import * as React from 'react';
import styled from 'styled-components';
import { Dialog, Icon } from '@blueprintjs/core';
import capitalize from 'lodash/capitalize';

import { Color, Font, Spacing } from 'app/assets';
import { useWindowWidth, Viewport } from 'app/utils';
import { FUIButton } from 'app/components/Shared/Elements';
import { PortalView } from 'app/utils';

type Props = {
  text: string,
  portalView: PortalView
  cancelButtonText: string,
  onClose(): void,
  onConfirm(): void
}

const ConfirmationDialog: React.FC<Props> = (
  { text, cancelButtonText, onClose, onConfirm, portalView }
) => {
  const isDesktop = useWindowWidth() > Viewport.MOBILE;
  return (
    <ConfirmationDialogContainer
      isOpen={portalView === PortalView.CONFIRMATION}
      canOutsideClickClose={true}
    >
      <Text>{text}</Text>
      <ButtonsContainer>
        <Button buttonType="text" onClick={onClose} text={capitalize(cancelButtonText)} />
        <Button buttonType="outlined" onClick={onConfirm} text={capitalize("ok")} />
      </ButtonsContainer>
      <CloseIcon icon="cross" iconSize={isDesktop ? 20 : 35} onClick={onClose} />
    </ConfirmationDialogContainer>
  )
};

export default ConfirmationDialog;

const Text = styled.p`
  font-size: 17px;
  font-weight: 300;
  text-align: justify;
  color: ${Color.grey};
  font-family: ${Font.text};
  margin-bottom: ${Spacing.XXXL};
`;

const Button = styled(FUIButton)` && {
  display: inline-block;
  cursor: pointer;
  width: 70px;
  padding: ${Spacing.S} ${Spacing.M};
  margin-right: ${Spacing.L};
  font-size: 17px;
  font-weight: 100;
  font-family: ${Font.header_button};
}`;

const ConfirmationDialogContainer = styled(Dialog)` && {
  width: 450px;
  position: relative;
  background-color: #fff;
  padding: ${Spacing.XXL} ${Spacing.XXL} ${Spacing.L} ${Spacing.XXL};
  margin-left: ${Spacing.XL};
  margin-right: ${Spacing.XL};
}`;

const CloseIcon = styled(Icon)` && {
    top: 5px;
    right: 5px;
    cursor: pointer;
    position: absolute;
    color: ${Color.grey};
   
    :hover {
        color: ${Color.flashpub};
    }
}`;

const ButtonsContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: flex-end;
  position: absolute;
  bottom: 10px;
  right: 10px;
`;