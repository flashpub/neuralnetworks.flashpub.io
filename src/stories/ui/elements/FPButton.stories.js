import React from 'react';
import { FPButton } from '../../../ui/elements';
import UserOutlined from '@ant-design/icons/lib/icons/UserOutlined';
import Checkbox from 'antd/es/checkbox';

export default { title: 'ui/elements/FPButton' };

export const Demo = () => {
    const [icon, setIcon] = React.useState(false);
    const [block, setBlock] = React.useState(false);
    const [round, setRound] = React.useState(false);
    const [loading, setLoading] = React.useState(false);
    // const [iconSide, setIconSide] = useState(false);
    // const [buttonType, setButtonType] = useState(false);

    const onChangeIcon = (e) => setIcon(e.target.checked);
    const onChangeBlock = (e) => setBlock(e.target.checked);
    const onChangeRound = (e) => setRound(e.target.checked);
    const onChangeLoading = (e) => setLoading(e.target.checked);

    return (
        <>
            <div>
                <Checkbox onChange={onChangeBlock} checked={block}>
                    block (full width)
                </Checkbox>
                <Checkbox onChange={onChangeLoading} checked={loading}>
                    loading
                </Checkbox>
                <Checkbox onChange={onChangeIcon} checked={icon}>
                    icon
                </Checkbox>
                <Checkbox onChange={onChangeRound} checked={round}>
                    round
                </Checkbox>
            </div>
            <FPButton
                block={block}
                round={round}
                isLoading={loading}
                icon={icon && <UserOutlined />}
                // iconSide={iconSide}
                // buttonType={buttonType}
            />
        </>
    );
};
