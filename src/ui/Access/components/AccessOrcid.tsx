import * as React from 'react';
import { useMedia } from 'react-use';
import { useDispatch } from 'react-redux';
import { navigate } from '@reach/router';

import * as API from 'api';
import { useTypedSelector, envDetector } from 'helpers';
import { MOBILE } from 'assets/styles';
import * as Orcid from 'containers/Orcid';
import * as Access from 'containers/Access';
import { GlobalLoader } from 'ui/Overlays';
import { StyledFlexWrapper } from 'ui/elements/styles';
import {
    StyledAccessText,
    StyledAccessTitle,
    StyledConnectOrcidButton,
    StyledGetOrcidButton,
    StyledOpenOrcidProfile,
} from 'ui/Access/styles';

export const AccessOrcid: React.FC = () => {
    const dispatch = useDispatch();
    const isDesktop = useMedia(`(min-width: ${MOBILE})`);
    const auth = useTypedSelector((state) => state.auth.data);
    const { orcid, code } = useTypedSelector((state) => state.orcid.data);
    const accessStep = useTypedSelector((state) => state.access.data.step);
    const orcidStatus = useTypedSelector((state) => state.orcid.status.type);

    React.useEffect(() => {
        if (isDesktop && accessStep === 'ORCID') {
            window.addEventListener('message', (message) => {
                if (typeof message.data === 'string') {
                    dispatch(Orcid.code(message.data));
                }
            });
        }
        if (isDesktop && code && accessStep === 'ORCID' && auth) {
            dispatch(Orcid.verifyOrcid(code, auth.uid, envDetector('ORCID')));
        }

        // TODO rewrite the code to eliminate the exhaustive-deps warning
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [code]);

    const handelOnConnectOrcidClick = () => (isDesktop ? dispatch(Orcid.desktopToOrcid()) : navigate(API.ORCID));
    const handleOpenOrcidClick = () => window.open(`https://orcid.org/${orcid}`);
    const handleGetOrcidClick = () => window.open('https://orcid.org');

    const renderBeforeConnected = (
        <>
            <StyledAccessText>
                ORCID is a persistent digital identifier that distinguishes you from every other researcher. It's free,
                easy to create, and you'll need it to publish on flash<strong>pub</strong>!
            </StyledAccessText>
            <StyledConnectOrcidButton
                buttonType="link"
                label="Connect your ORCID"
                onClick={handelOnConnectOrcidClick}
            />
            <StyledGetOrcidButton buttonType="link" label="Get an ORCID" onClick={handleGetOrcidClick} />
        </>
    );

    const renderAfterConnected = (
        <>
            <StyledAccessText>Your ORCID is now connected to flashPub!</StyledAccessText>
            <StyledOpenOrcidProfile
                buttonType="link"
                onClick={handleOpenOrcidClick}
                label={`https://orcid.org/${orcid}`}
            />
            <StyledConnectOrcidButton buttonType="link" label="Next" onClick={() => dispatch(Access.step('FINISH'))} />
        </>
    );

    return (
        <>
            <StyledFlexWrapper>
                <StyledAccessTitle>Orcid</StyledAccessTitle>
                {orcid ? renderAfterConnected : renderBeforeConnected}
            </StyledFlexWrapper>
            <GlobalLoader loading={orcidStatus === 'LOADING'} slogan="Connecting you to ORCID..." />
        </>
    );
};
