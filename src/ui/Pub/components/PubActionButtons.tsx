import * as React from 'react';
import { find, isEqual } from 'lodash';
import { message, Modal, Tooltip } from 'antd';
import { useDispatch } from 'react-redux';
import copy from 'clipboard-copy';
import {
    CheckCircleFilled,
    CheckCircleOutlined,
    EditOutlined,
    LoadingOutlined,
    SaveFilled,
    SaveOutlined,
    CloseCircleOutlined,
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    LinkOutlined,
} from '@ant-design/icons';

import * as API from 'api';
import { useTypedSelector, useWindowWidth } from 'helpers';
import * as Pub from 'containers/Pub';
import {
    StyledActionIcon,
    StyledEditModeOverlay,
    StyledEditModeOverlayLoading,
    StyledPubActionButtonsWrapper,
} from 'ui/Pub/styles';
import { StyledSpacer } from 'ui/elements/styles';
import * as Access from 'containers/Access';
import { PubMore } from 'ui/Pub';
import { Viewport } from 'assets/styles';
import PubDataModel from 'models/data/pub';

type Props = {
    showPanel: boolean;
    editMode: boolean;
    pub: PubDefinition;
    setReviewPanel(show: boolean): void;
};

export const PubActionButtons: React.FC<Props> = ({ pub, editMode, showPanel, setReviewPanel }) => {
    const dispatch = useDispatch();
    const [changed, setChanged] = React.useState(false);
    
    const isMobile = useWindowWidth() <= Viewport.TABLET;

    const isAuth = useTypedSelector((state) => state.auth.data);
    const pubData = useTypedSelector((state) => state.pub.data.pub);
    const user = useTypedSelector((state) => state.profile.data.user);
    const draftData = useTypedSelector((state) => state.pub.data.draft);
    const pubStatus = useTypedSelector((state) => state.pub.status.type);

    const hasDataChanged = pubData.id === draftData.id ? !isEqual(pubData, draftData) : false;

    React.useEffect(() => {
        if (isAuth && user && pub && pub.title) {
            const sheet = document.querySelector('#publication_sheet') as HTMLElement;
            const actions = document.querySelector('#pub_actions') as HTMLElement;
            const sheetPos = sheet.getBoundingClientRect();
            actions.style.left = sheetPos.left - (editMode ? 80 : 40) + 'px';
        }
    });

    React.useEffect(() => {
        if (pubStatus === 'SUCCESS') {
            if (hasDataChanged) {
                setChanged(true);
            } else {
                setChanged(false);
            }
        }

        // TODO rewrite the code to eliminate the exhaustive-deps warning
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [hasDataChanged]);

    if (pub && pub.title && isAuth && user) {
        const isOrcidConnected = user.orcid;
        const isLoading = pubStatus === 'LOADING';
        const isUserAuthor = pub.author.orcid === user.orcid;
        const hasUserEndorsed = find(pub.review.acceptedBy, (id) => id === user.orcid);

        const handleFinishEditing = () => {
            dispatch(Pub.editPub());
        };
    
        const handleEndorse = async () => {
            if (!isOrcidConnected) {
                return Modal.confirm({
                    className: 'endorse_dialog',
                    okText: 'Connect Orcid',
                    content: 'You have to connect your ORCID account to continue',
                    async onOk() {
                        dispatch(Access.set(true));
                        dispatch(Access.step('ORCID'));
                    },
                    onCancel() {
                        return null;
                    },
                    centered: true,
                });
            }
            if (hasUserEndorsed) {
                dispatch(Pub.form.cut({ form: 'pub', field: 'review.acceptedBy', value: user?.orcid }));
                await API.pub.updateArrayField(pub.id, 'review.acceptedBy', user.orcid, '-');
                message.warning('Publication rejected');
            } else {
                Modal.confirm({
                    className: 'endorse_dialog',
                    content: 'I verify that this publication sufficiently demonstrates its claims',
                    async onOk() {
                        dispatch(Pub.form.set({ form: 'pub', field: 'review.acceptedBy', value: user?.orcid }));
                        await API.pub.updateArrayField(pub.id, 'review.acceptedBy', user.orcid, '+');
                        message.success('Publication endorsed!');
                    },
                    onCancel() {
                        return null;
                    },
                    centered: true,
                });
            }
        };
    
        const onCopyDoi = () => {
            if (pub?.metadata.doi) {
                copy(pub?.metadata.doi);
                message.info('DOI copied to clipboard');
            } else {
                message.error('Oooops! Something went wrong...');
            }
        };
        const handleSaveChanges = () => {
            if (editMode) {
                handleFinishEditing();
                setChanged(false);
            }
        };
        const handleEditMode = () => {
            if (editMode) {
                dispatch(Pub.draft(PubDataModel));
                dispatch(Pub.edit(false));
            } else {
                dispatch(Pub.draft(pub));
                dispatch(Pub.edit(true));
            }
        };
        
        const handleOpenReviewPanel = () => setReviewPanel(!showPanel);

        return (
            <>
                {editMode && !isMobile && <StyledEditModeOverlay />}
                <StyledPubActionButtonsWrapper id="pub_actions">
                    {isUserAuthor && !pub.review.isPubAccepted && (
                        <Tooltip
                            placement="left"
                            mouseLeaveDelay={0}
                            mouseEnterDelay={1}
                            title={editMode ? 'Close edit mode' : 'Open edit mode'}
                        >
                            <StyledActionIcon onClick={handleEditMode}>
                                {editMode ? <CloseCircleOutlined /> : <EditOutlined />}
                            </StyledActionIcon>
                        </Tooltip>
                    )}

                    {isUserAuthor && editMode && (
                        <>
                            {!isMobile && <StyledSpacer height={10} />}
                            <Tooltip title="Save changes" placement="left" mouseLeaveDelay={0} mouseEnterDelay={1}>
                                <StyledActionIcon onClick={handleSaveChanges} editMode={editMode}>
                                    {isLoading ? <LoadingOutlined /> : changed ? <SaveFilled /> : <SaveOutlined />}
                                </StyledActionIcon>
                            </Tooltip>
                        </>
                    )}

                    {!isUserAuthor && !pub.review.isPubAccepted && (
                        <>
                            {!isMobile && <StyledSpacer height={10} />}
                            <Tooltip
                                placement="left"
                                mouseLeaveDelay={0}
                                mouseEnterDelay={1}
                                title={hasUserEndorsed ? 'Click to reject' : 'Click to endorse'}
                            >
                                <StyledActionIcon onClick={handleEndorse}>
                                    {hasUserEndorsed ? <CheckCircleFilled /> : <CheckCircleOutlined />}
                                </StyledActionIcon>
                            </Tooltip>
                        </>
                    )}
    
                    {!editMode && (
                        <>
                            {!isMobile ? <StyledSpacer height={10} /> : <div style={{ width: 20 }} />}
                            <Tooltip title="Copy DOI" placement="left" mouseLeaveDelay={0} mouseEnterDelay={1}>
                                <StyledActionIcon onClick={onCopyDoi}>
                                    <LinkOutlined />
                                </StyledActionIcon>
                            </Tooltip>
                        </>
                    )}
    
                    {pub.contentOrigin.originalContent && !isUserAuthor && (
                        <>
                            {!isMobile ? <StyledSpacer height={10}/> : <div style={{width: 20}}/>}
                            <Tooltip
                                placement="left"
                                mouseLeaveDelay={0}
                                mouseEnterDelay={1}
                                title={!isMobile && (showPanel ? 'close review panel' : 'open review panel')}
                            >
                                <StyledActionIcon onClick={handleOpenReviewPanel}>
                                    {showPanel ? <MenuFoldOutlined /> : <MenuUnfoldOutlined/>}
                                </StyledActionIcon>
                            </Tooltip>
                        </>
                    )}
                    
                    {!editMode && (
                        <>
                            {!isMobile ? <StyledSpacer height={10} /> : <div style={{ width: 20 }} />}
                            <PubMore editMode={editMode} />
                        </>
                    )}
                </StyledPubActionButtonsWrapper>
                {editMode && isLoading && <StyledEditModeOverlayLoading />}
            </>
        );
    }
    return null;
};
