import * as React from 'react';
import { useLocation } from 'react-use';
import { kebabCase } from 'lodash';
import { useDispatch } from 'react-redux';
import { RouteComponentProps } from '@reach/router';

import { Campaign } from 'ui/Campaigns';
import * as Campaigns from 'containers/Campaigns';
import { useTypedSelector } from 'helpers';
import { GlobalLoader } from 'ui/Overlays';

export const CampaignsScreen: React.FC<RouteComponentProps> = () => {
    const dispatch = useDispatch();
    const location = useLocation();
    const campaigns = useTypedSelector((state) => state.campaigns.data.campaigns);
    const campaignsStatus = useTypedSelector((state) => state.campaigns.status.type);
    const selectedCampaign = useTypedSelector((state) => state.campaigns.data.selected);
    
    const pathTitle = location.pathname?.split('/campaigns/')[1] as string;
    const campaignType = location.state?.campaignType;
    
    React.useEffect(() => {
        if (!selectedCampaign) {
            const match = campaigns.filter((c) => kebabCase(c.title) === pathTitle)[0];
            if (match) {
                dispatch(Campaigns.select(match));
            }
        }
    }, [dispatch, campaigns]);

    return (
        <>
            <Campaign type={campaignType} />
            {!selectedCampaign && <GlobalLoader loading={campaignsStatus === 'LOADING'} slogan="Loading map data" />}
        </>
    );
};
