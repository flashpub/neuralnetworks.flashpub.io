import * as React from 'react';
import { RouteComponentProps } from '@reach/router';

import { DashboardDetails } from 'ui/Campaigns';

export const DashboardDetailsScreen: React.FC<RouteComponentProps> = (props) => {
    const contentTypesDetails = props.location?.state || null;
    
    return <DashboardDetails contentTypesDetails={contentTypesDetails as MidasCampaignData} />;
};
