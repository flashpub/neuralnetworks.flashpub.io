import { createSlice, SliceCaseReducers, ValidateSliceCaseReducers } from '@reduxjs/toolkit';

export const createGenericSlice = <T, Reducers extends SliceCaseReducers<GenericState<T>>>({
    name = '',
    initialState,
    reducers,
}: {
    name: string;
    initialState: GenericState<T>;
    reducers: ValidateSliceCaseReducers<GenericState<T>, Reducers>;
}) => {
    return createSlice({
        name,
        initialState,
        reducers: {
            pending: (state) => {
                state.status = { type: 'PENDING', message: null };
            },
            loading: (state) => {
                state.status = { type: 'LOADING', message: null };
            },
            failure: (state, action) => {
                state.status = { type: 'FAILURE', message: action.payload };
            },
            success: (state) => {
                state.status = { type: 'SUCCESS', message: null };
            },
            ...reducers,
        },
    });
};
