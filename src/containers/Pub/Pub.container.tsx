import find from 'lodash/find';
import * as React from 'react';
import { useUnmount } from 'react-use';
import { useDispatch } from 'react-redux';
import { RouteComponentProps, useLocation } from '@reach/router';

import { useTypedSelector } from 'helpers';
import * as Pub from 'containers/Pub';

export const PubContainer: React.FC<RouteComponentProps> = ({ children }) => {
    const dispatch = useDispatch();
    const location = useLocation();
    const feedList = useTypedSelector((state) => state.feed.data);
    const pubStatus = useTypedSelector((state) => state.pub.status.type);
    const selectedPubId = useTypedSelector((state) => state.pub.data.selected);

    const fetchedPub = find(feedList, (p) => p.id === selectedPubId);

    React.useEffect(() => {
        if (selectedPubId) {
            if (fetchedPub) {
                dispatch(Pub.populate(fetchedPub));
            } else if (pubStatus !== 'LOADING') {
                dispatch(Pub.fetchPub(selectedPubId!));
            }
        } else {
            const pubId = location.pathname.split('_')[1];
            dispatch(Pub.select(pubId));
        }

        // TODO rewrite the code to eliminate the exhaustive-deps warning
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [dispatch, fetchedPub, location.pathname, selectedPubId]);

    useUnmount(() => {
        console.log('clear draft');
        dispatch(Pub.form.clear('draft'));
    });

    return <>{children}</>;
};
