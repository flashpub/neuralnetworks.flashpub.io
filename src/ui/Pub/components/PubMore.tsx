import * as React from 'react';
import { Modal, Popover, Tooltip } from 'antd';
import { useDispatch } from 'react-redux';
import { find } from 'lodash';
import { ExclamationCircleFilled, ExclamationCircleOutlined, MoreOutlined } from '@ant-design/icons';

import * as API from 'api';
import { useTypedSelector, useWindowWidth } from 'helpers';
import { StyledActionIcon, StyledShareWrapper } from 'ui/Pub/styles';
import { StyledSpacer } from 'ui/elements/styles';
import { Viewport } from 'assets/styles';
import { bookmarkFilled, bookmarkOpen } from 'assets/icons';
import * as Pub from 'containers/Pub';


type Props = {
    editMode: boolean;
};

export const PubMore: React.FC<Props> = ({ editMode }) => {
    const dispatch = useDispatch();
    const isMobile = useWindowWidth() <= Viewport.TABLET;
    
    const pub = useTypedSelector((state) => state.pub.data.pub);
    const user = useTypedSelector((state) => state.profile.data.user);
    
    const isUserAuthor = pub.author.orcid === user?.orcid;
    const hasUserBookmarked = find(pub.bookmarkedBy, (id) => id === user?.uid);
    const hasUserReported = find(pub.review.reportedBy, (id) => id === user?.orcid);
    
    const handleBookmark = async () => {
        if (hasUserBookmarked) {
            dispatch(Pub.form.cut({ form: 'pub', field: 'bookmarkedBy', value: user?.uid }));
            await API.pub.updateArrayField(pub.id, 'bookmarkedBy', user?.uid as string, '-');
        } else {
            dispatch(Pub.form.set({ form: 'pub', field: 'bookmarkedBy', value: user?.uid }));
            await API.pub.updateArrayField(pub.id, 'bookmarkedBy', user?.uid as string, '+');
        }
    };
    
    const handleReportPub = async () => {
        if (hasUserReported) {
            dispatch(Pub.form.cut({ form: 'pub', field: 'review.reportedBy', value: user?.orcid }));
            await API.pub.updateArrayField(pub.id, 'review.reportedBy', user?.orcid as string, '-');
        } else {
            Modal.confirm({
                className: 'endorse_dialog',
                content: 'Are you sure you want to report this pub?',
                async onOk() {
                    const email = `mailto:hello@flashpub.io?subject=Reporting /pub/${pub.metadata.fetchId}`;
                    window.open(email);
                    dispatch(Pub.form.set({ form: 'pub', field: 'review.reportedBy', value: user?.orcid }));
                    await API.pub.updateArrayField(pub.id, 'review.reportedBy', user?.orcid as string, '+');
                },
                onCancel() {
                    return null;
                },
                centered: true,
            });
        }
    };
    
    const renderMoreOptions = () => {
        return (
            <StyledShareWrapper mobile={isMobile}>
    
                {!editMode && (
                    <>
                        {!isMobile ? <StyledSpacer height={10} /> : <div style={{ width: 20 }} />}
                        <Tooltip
                            placement="left"
                            mouseLeaveDelay={0}
                            mouseEnterDelay={1}
                            title={hasUserBookmarked ? 'Remove from bookmarks' : 'Add to bookmarks'}
                        >
                            <StyledActionIcon onClick={handleBookmark}>
                                {hasUserBookmarked ? bookmarkFilled : bookmarkOpen}
                            </StyledActionIcon>
                        </Tooltip>
                    </>
                )}
    
                {!isUserAuthor && (
                    <>
                        {!isMobile ? <StyledSpacer height={10} /> : <div style={{ width: 20 }} />}
                        <Tooltip
                            placement="left"
                            mouseLeaveDelay={0}
                            mouseEnterDelay={1}
                            title={hasUserReported ? 'Retract report' : 'Report pub'}
                        >
                            <StyledActionIcon onClick={handleReportPub}>
                                {hasUserReported ? <ExclamationCircleFilled /> : <ExclamationCircleOutlined />}
                            </StyledActionIcon>
                        </Tooltip>
                    </>
                )}
            </StyledShareWrapper>
        );
    };
    
    return (
        <Popover
            trigger="click"
            content={renderMoreOptions()}
            overlayClassName="fp-share-options"
            placement={isMobile ? 'right' : 'left'}
            getPopupContainer={() => document.querySelector('#pub_actions') as HTMLElement}
        >
            <Tooltip title="More" placement="left" mouseLeaveDelay={0} mouseEnterDelay={1}>
                <StyledActionIcon>
                    <MoreOutlined rotate={90} />
                </StyledActionIcon>
            </Tooltip>
        </Popover>
    );
};
