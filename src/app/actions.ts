import { PortalView, Status, action } from 'app/utils';
import { AlertDefinition, CommunityDefinition } from 'app/types';

export const SET_SYSTEM_GATES = action<any>('app/SET_SYSTEM_GATES');
export const SET_AUTH = action<{ isAuth: boolean }>('app/SET_AUTH');
export const SET_APP_STATUS = action<{ status: Status }>('app/SET_APP_STATUS');
export const SET_PORTAL_VIEW = action<{ view: PortalView }>('app/SET_PORTAL_VIEW');
export const SET_MAIN_COMMUNITY = action<CommunityDefinition>('app/SET_MAIN_COMMUNITY');
export const SET_SESAME_OPEN = action<boolean>('app/SET_SESAME_OPEN');

export const ADD_ALERT = action<AlertDefinition>('app/ADD_ALERT');
export const REMOVE_ALERT = action<{ id: Optional<string> }>('app/REMOVE_ALERT');
