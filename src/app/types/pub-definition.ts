import { ClaimDefinition } from './claim-definition';

export interface PubDefinition {
    readonly id: string;
    readonly title: string;
    readonly contentOrigin: ContentOrigin;
    readonly bookmarkedBy: Array<string>;
    readonly acceptedNotification: boolean;
    readonly author: Author;
    readonly description: string;
    readonly claim: Claim;
    readonly contributors: Array<Author>;
    readonly figure: Figure;
    readonly figures: Array<Figure>;
    readonly isDoiActivated: boolean;
    readonly metadata: Metadata;
    readonly review: Review;
}

export type ContentOrigin = {
    readonly source: string;
    readonly url: string;
    readonly originalContent: boolean;
}

export type Author = {
    readonly id: string,
    readonly name: string,
    readonly orcid: string,
    readonly email: string,
}

export type Condition = {
    id: string,
    [key: string]: any,
}

export type Claim = {
    readonly id: string,
    readonly terms: {
        firstItem: string;
        secondItem: string;
        relationship: string;
        type: string;
    };
    readonly conditions: Array<Condition>,
    readonly doc?: Optional<ClaimDefinition>,
}

export type Figure = {
    readonly url: string;
    readonly type: string;
    readonly order: number;
    readonly label?: string;
    readonly file?: File | null;
}

export type Metadata = {
    readonly protocols: Array<Asset>;
    readonly code: Array<Asset>;
    readonly datasets: Array<Asset>;
    readonly references: Array<Asset>;
    readonly datePublished: number;
    readonly dateUpdated: number;
    readonly doi: string;
    readonly fetchId: string;
    readonly allOntologyIds: Array<string>;
}

export type Asset = {
    readonly name: string | 'Supports' | 'Refutes' | 'Extends';
    readonly url: string;
    /**
     * NOT ON BACKEND
     * **/
    readonly id?: string;
}

export type Assets = {
    code: Array<Asset>,
    datasets: Array<Asset>,
    protocols: Array<Asset>,
    references: Array<Asset>,
}

export type Review = {
    readonly isPubAccepted: boolean;
    readonly isPubRetracted: boolean;
    readonly deadline: number;
    readonly acceptedBy: Array<string>;
    readonly quickReviews: { [key: string]: Array<string> };
    readonly reportedBy: Array<string>;
    readonly discussionId: Array<string>;
}
