type ContentContainerState = {
    type: CommunityContentType | null;
    source: 'campaigns' | 'community' | null;
};

type Relationship = { name: string; type: 'positive' | 'negative' | 'neutral' };
type CommunityContentInfo = {
    readonly title: string;
    readonly diagram: string;
    readonly description: string;
};
type CommunityContentClaimItem = {
    readonly label: string;
    readonly filter: string;
    readonly newTermAllowed: boolean;
    readonly setFilterAsItem: boolean;
};
type CommunityContentClaim = {
    readonly item1: CommunityContentClaimItem;
    readonly item2: CommunityContentClaimItem | null;
    readonly relationship: Relationship[] | null;
    readonly type: 'single' | 'relational' | null;
};
type CommunityDefaultCondition = { name: string; type: any };
type CommunityContentType = {
    readonly order: number;
    readonly info: CommunityContentInfo;
    readonly claim: CommunityContentClaim;
    readonly quickReviews: string[];
    readonly figureRequired: boolean;
    readonly contentTypeLabel: string;
    readonly defaultConditions: CommunityDefaultCondition[];
    readonly reviewPeriodEnabled: boolean;
};
