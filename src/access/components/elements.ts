import styled from 'styled-components';

import { Color, Font, Spacing } from 'app/assets';

import { FUIButton } from 'FUI/elements';

export const Title = styled.h1`
    margin-top: 0;
    font-size: 32px;
    font-weight: 100;
    text-align: center;
    color: ${Color.grey};
    margin-bottom: ${Spacing.XXL};
`;
export const Text = styled.p`
    font-size: 17px;
    font-weight: 300;
    text-align: justify;
    color: ${Color.grey};
    font-family: ${Font.text};
    margin-bottom: ${Spacing.XXL};
`;
export const Button = styled(FUIButton)` && {
    padding: 0;
    margin: 0 auto;
    display: block;
    cursor: pointer;
    font-size: 28px;
    font-weight: 100;
    color: ${Color.flashpub};
    font-family: ${Font.text};
    :hover {
        background-color: transparent;
    }
}`;
export const Error = styled.div`
    left: 50%;
    font-size: 12px;
    width: fit-content;
    position: absolute;
    bottom: -${Spacing.SM};
    color: ${Color.flashpub};
    font-family: ${Font.text};
    transform: translateX(-50%);

    @media (max-width: 380px) {
        font-size: 10px;
    }
`;
