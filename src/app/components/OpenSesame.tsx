import * as React from 'react';
import styled from 'styled-components';
import { Radius, Spacing } from '../assets';
import { FUIButton, FUIInput } from '../../FUI/elements';
import { db } from '../../firebase-config';
import { useDispatch, useSelector } from 'react-redux';
import * as AppAction from '../actions';
import { AlertType } from '../types';
import { RootState } from '../store';
import { Status } from '../utils';


type Props = {};

export const OpenSesame: React.FC<Props> = (props: Props) => {
    const [phrase, setPhrase] = React.useState<string>('');
    const dispatch = useDispatch();
    const appStatus = useSelector<RootState, Status>(state => state.app.status);

    const onValueChange = (value: string) => {
        setPhrase(value);
    };

    const sesameGo = async () => {
        try {
            dispatch(AppAction.SET_APP_STATUS.use({ status: Status.LOADING }));
            const resp = await db.collection('system').doc('sesame').get();
            if (resp.exists) {
                const data = resp.data();
                const open = data!.open;
                if (open === phrase) {
                    dispatch(AppAction.SET_SESAME_OPEN.use(true));
                    dispatch(AppAction.SET_APP_STATUS.use({ status: Status.PENDING }));
                } else {
                    dispatch(AppAction.ADD_ALERT.use({
                        type: AlertType.ERROR,
                        message: "Incorrect password!",
                    }));
                    dispatch(AppAction.SET_APP_STATUS.use({ status: Status.FAILURE }));
                }
            }
        } catch (err) {
            dispatch(AppAction.ADD_ALERT.use({
                type: AlertType.ERROR,
                message: err.message,
            }));
            dispatch(AppAction.SET_APP_STATUS.use({ status: Status.FAILURE }));
        }

    };

    return (
        <StyledWrapper>
            {appStatus !== Status.LOADING &&
                <SesameDoor>
                    <FUIInput
                        value={phrase}
                        onChange={onValueChange}
                        placeholder="enter the password"
                    />
                    <SesameButton text="Enter!" buttonType="outlined" onClick={sesameGo} disabled={!phrase} loading={appStatus} />
                </SesameDoor>
            }
        </StyledWrapper>
    );
};

const StyledWrapper = styled.div`
    width: 100vw;
    height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: rgba(0,0,0);
    background-image: url("https://firebasestorage.googleapis.com/v0/b/flashpub-67105.appspot.com/o/surgeon-bg-min.png?alt=media");
`;
const SesameDoor = styled.div`
    width: 300px;
    padding: ${Spacing.L};
    border-radius: ${Radius};
    text-align: center;
    background-color: rgba(255,255,255,.5);
`;
const SesameButton = styled(FUIButton)` && {
    background-color: #fff;
}`;
