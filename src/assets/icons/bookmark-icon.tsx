import * as React from 'react';
import { Color } from 'assets/styles';

const iconWrapper = (path: JSX.Element, isBookmarked: boolean): any => (
    <svg
        className="pt-icon"
        version="1.1"
        id="website-assets"
        xmlns="http://www.w3.org/2000/svg"
        x="0px"
        y="0px"
        width="20px"
        height="20px"
        viewBox="0 0 100 151"
        stroke={`${Color.community}`}
        fill={isBookmarked ? Color.community : '#fff'}
        strokeWidth="14"
    >
        {path}
    </svg>
);

const path = (
    <path
        d="M8,4 C5.790861,4 4,5.790861 4,8 L4,142.078485 C4,143.257358 4.52002222,144.376201
        5.42119308,145.136222 C7.10993453,146.560458 9.63350115,146.346034 11.0577369,144.657292
        L40.0458437,110.28599 C43.4264921,106.369491 46.9890474,104.25282 50.828756,104.359999
        C54.5610843,104.46418 57.9890523,106.558426 61.2295765,110.355248 L88.906236,144.126085
        C89.6659849,145.053124 90.801409,145.590612 92,145.590612 C94.209139,145.590612
        96,143.799751 96,141.590612 L96,8 C96,5.790861 94.209139,4 92,4 L8,4 Z"
    />
);

export const bookmarkFilled = iconWrapper(path, true);

export const bookmarkOpen = iconWrapper(path, false);
