import * as React from 'react';
import { useDispatch } from 'react-redux';
import { RouteComponentProps } from '@reach/router';

import { useTypedSelector } from 'helpers';
import * as authentication from 'services/authentication';
import * as Access from 'containers/Access';
import { AccessForm, StyledAccessTitle } from 'ui/Access';
import { StyledFlexWrapper } from 'ui/elements/styles';

export const AccessRegister: React.FC<RouteComponentProps> = () => {
    const dispatch = useDispatch();
    const authStatus = useTypedSelector((state) => state.auth.status);
    const accessData = useTypedSelector((state) => state.access.data);

    const onDispatch = (value: string) => dispatch(Access.step(value));
    const onSubmitRegisterForm = (creds: Credentials) => dispatch(authentication.registerUser(creds));

    return (
        <>
            <StyledAccessTitle>Registration details</StyledAccessTitle>
            <StyledFlexWrapper>
                <AccessForm
                    label="Register"
                    dispatch={onDispatch}
                    step={accessData.step}
                    onSubmit={onSubmitRegisterForm}
                    isLoading={authStatus.type === 'LOADING'}
                />
            </StyledFlexWrapper>
        </>
    );
};
