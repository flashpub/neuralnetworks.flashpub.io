import shortid from 'shortid';
import * as React from 'react';
import { capitalize } from 'lodash';
import { Popover, Tooltip } from 'antd';
import { useDispatch } from 'react-redux';
import { CloseCircleOutlined } from '@ant-design/icons';

import * as Pub from 'containers/Pub';
import {
    StyledMetadataSelect,
    StyledMetadataAssetUrl,
    StyledMetadataAssetForm,
    StyledMetadataAssetName,
    StyledMetadataAddAssetButton,
    StyledMetadataBox,
    StyledMetadataName,
    StyledMetadataAssetWrapper,
    StyledMetadataAssetLink,
    StyledMetadataWrapper,
    StyledMetadataList,
    StyledAddAsset,
} from 'ui/Publish/styles';

type Props = {
    editMode: boolean;
    pub: PubDefinition;
};

const initialAsset = { name: '', url: '' };

export const PublicationMetadata: React.FC<Props> = ({ pub, editMode }) => {
    const dispatch = useDispatch();
    const [asset, setAsset] = React.useState<typeof initialAsset>(initialAsset);
    const metadata: Metadata = pub.metadata;

    const clearForm = () => setAsset(initialAsset);
    const handleUrlChange = (value: string) => setAsset({ ...asset, url: value });
    const handleNameChange = (value: string) => setAsset({ ...asset, name: value });
    const clearAssetForm = (type: string, id: string) => {
        clearForm();
        dispatch(Pub.form.cut({ form: 'draft', field: `metadata.${type}`, value: id }));
    };

    const renderAssetsForm = (type: string) => {
        const bySource = () => {
            let url = 'url';
            let name = 'name';
            switch (type) {
                case 'protocols':
                    name = 'protocol name';
                    url = 'e.g. doi from protocols.io';
                    break;
                case 'code':
                    name = 'codebase name';
                    url = 'e.g. url from codeocean.com';
                    break;
                case 'datasets':
                    name = 'dataset name';
                    url = 'e.g. doi from figshare.com';
                    break;
                case 'references':
                    name = 'choose a reference';
                    url = 'e.g. doi.org/10.xxxx/2020.03.19.xxxxxxxx';
                    break;
                default:
                    return { name, url };
            }
            return { name, url };
        };
        const onAddAsset = () => {
            const adjustUrl = () => {
                if (asset.url.includes('https://')) return asset.url;
                if (asset.url.includes('doi:')) return `https://doi.org/${asset.url.split('doi:')[1]}`;
                if (asset.url.includes('doi.org')) return `https://${asset.url}`;
                return `https://doi.org/${asset.url}`;
            };
            const newAsset: Asset = {
                id: shortid(),
                url: adjustUrl(),
                name: asset.name,
            };
            dispatch(Pub.form.set({ form: 'draft', field: `metadata.${type}`, value: newAsset }));
            clearForm();
        };
        const isDisabled = !asset.name || !asset.url;
        const assetName = bySource().name;

        return (
            <StyledMetadataAssetForm>
                <StyledMetadataAssetUrl
                    required
                    value={asset.url}
                    onChange={handleUrlChange}
                    placeholder={bySource().url}
                />
                {type === 'references' ? (
                    <StyledMetadataSelect
                        value={asset.name}
                        placeholder={assetName}
                        onChange={(e: any) => handleNameChange(e.target.value as string)}
                    >
                        <option value="" disabled>
                            {assetName}
                        </option>
                        <option value="supports">Supports</option>
                        <option value="refutes">Refutes</option>
                        <option value="extends">Extends</option>
                    </StyledMetadataSelect>
                ) : (
                    <StyledMetadataAssetName
                        required
                        value={asset.name}
                        placeholder={assetName}
                        onChange={handleNameChange}
                    />
                )}
                <StyledMetadataAddAssetButton
                    label="add"
                    onClick={onAddAsset}
                    buttonType="default"
                    disabled={isDisabled}
                />
            </StyledMetadataAssetForm>
        );
    };
    
    const renderMetadataElement = (type: string) => (
        <StyledMetadataBox disabled={!editMode}>
            <StyledMetadataName>{capitalize(type)}:</StyledMetadataName>
            {editMode && (
                <Popover trigger="click" placement="right" content={renderAssetsForm(type)} onVisibleChange={clearForm}>
                    <Tooltip title="add" placement="right">
                        <StyledAddAsset style={{ fontSize: 14 }} />
                    </Tooltip>
                </Popover>
            )}
        </StyledMetadataBox>
    );

    const assetTile = (asset: Asset, action: (id: string) => void, i: number, type?: string) => {
        const prepareHref = (url: string) => {
            if (url.includes('http')) {
                return url;
            } else {
                return `https://${url}`;
            }
        };
        return (
            <StyledMetadataAssetWrapper key={i} disabled={!editMode}>
                {editMode && <CloseCircleOutlined onClick={() => action(asset.id)} style={{ fontSize: 14 }} />}
                <StyledMetadataAssetLink>
                    {`${i + 1}) `}
                    {type === 'references' ? (
                        <a target="_blank" href={prepareHref(asset.url)} rel="noopener noreferrer">
                            {`${asset.url}`}
                        </a>
                    ) : (
                        <a target="_blank" href={prepareHref(asset.url)} rel="noopener noreferrer">
                            {`${asset.name}`}
                        </a>
                    )}
                </StyledMetadataAssetLink>
            </StyledMetadataAssetWrapper>
        );
    };

    const printAssetTiles = (type: string) =>
        metadata[type].map((asset: Asset, i: number) => {
            if (!asset) return null;
            const onRemoveAsset = (id: string) => clearAssetForm(type, id);
            return assetTile(asset, onRemoveAsset, i, type);
        });

    return (
        <StyledMetadataWrapper>
            {(pub.metadata?.references.length !== 0 || editMode) && (
                <StyledMetadataList>
                    {renderMetadataElement('references')}
                    {printAssetTiles('references')}
                </StyledMetadataList>
            )}
            {(pub.metadata?.protocols.length !== 0 || editMode) && (
                <StyledMetadataList>
                    {renderMetadataElement('protocols')}
                    {printAssetTiles('protocols')}
                </StyledMetadataList>
            )}
            {(pub.metadata?.code.length !== 0 || editMode) && (
                <StyledMetadataList>
                    {renderMetadataElement('code')}
                    {printAssetTiles('code')}
                </StyledMetadataList>
            )}
            {(pub.metadata?.datasets.length !== 0 || editMode) && (
                <StyledMetadataList>
                    {renderMetadataElement('datasets')}
                    {printAssetTiles('datasets')}
                </StyledMetadataList>
            )}
        </StyledMetadataWrapper>
    );
};
