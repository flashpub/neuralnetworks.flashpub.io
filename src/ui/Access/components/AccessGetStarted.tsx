import * as React from 'react';
import { useDispatch } from 'react-redux';

import * as Access from 'containers/Access';
import { StyledFlexWrapper } from 'ui/elements/styles';
import { StyledAccessText, StyledAccessTitle, StyledNextButton } from 'ui/Access/styles';

export const AccessGetStarted: React.FC = () => {
    const dispatch = useDispatch();
    const handleNextClick = () => dispatch(Access.step('REGISTER'));

    return (
        <StyledFlexWrapper>
            <StyledAccessTitle>A better way to publish</StyledAccessTitle>
            <StyledAccessText>
                Instead of waiting years to make an impact, micro-publications let you actively lead your community,
                prevent scooping, and help you leave a lasting legacy of robust, verified findings. Welcome to a whole
                new way to publish!
            </StyledAccessText>
            <StyledNextButton buttonType="link" label="Get started!" onClick={handleNextClick} fontSize={28} />
        </StyledFlexWrapper>
    );
};
