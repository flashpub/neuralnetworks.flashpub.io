type Asset = {
    readonly name: string | 'Supports' | 'Refutes' | 'Extends';
    readonly url: string;
    readonly id?: string;
}
type Metadata = {
    readonly protocols: Asset[];
    readonly code: Asset[];
    readonly datasets: Asset[];
    readonly references: Asset[];
    readonly datePublished: number;
    readonly dateUpdated: number;
    readonly doi: string;
    readonly fetchId: string;
    readonly isDeIdentified: boolean;
    readonly allOntologyIds: string[];
}
type Review = {
    readonly isPubAccepted: boolean;
    readonly isPubRetracted: boolean;
    readonly deadline: number;
    readonly acceptedBy: string[];
    readonly quickReviews: { [key: string]: string[]; };
    readonly reportedBy: string[];
    readonly discussionId: string[];
}
type PublishStateForm = {
    readonly id: string;
    readonly title: string;
    readonly description: string;
    readonly claim: FormClaim;
    readonly contributors: Author[];
    readonly figures: Figure[];
    readonly metadata: Partial<Metadata>;
    readonly suggested: {
        readonly reviewer_1: string;
        readonly reviewer_2: string;
        readonly reviewer_3: string;
        readonly reviewer_4: string;
    };
}
type Relationship = { name: string, type: 'positive' | 'negative' | 'neutral' };
type ClaimItem = { id: string, name: string };
type FormClaim = {
    readonly id: string;
    readonly firstItem: ClaimItem;
    readonly firstItemAncestors: string[];
    readonly secondItem: ClaimItem | null;
    readonly secondItemAncestors: string[];
    readonly conditions: Condition[];
    readonly relationship: string | null;
    readonly relationshipType: 'positive' | 'negative' | 'neutral' | null;
}
type Condition = {
    readonly type: any,
    readonly id: string,
    readonly name: string,
    readonly value: any,
}
type Author = {
    readonly id: string,
    readonly name: string,
    readonly orcid: string,
    readonly email: string,
}
type Figure = {
    readonly id: string;
    readonly url: string;
    readonly type: string;
    readonly order: number;
    readonly label?: string;
    readonly preview?: string;
    readonly file?: File | null;
}
type PublishFieldName = keyof FormClaim | keyof Author | keyof Figure;

