import { project } from 'configs';

const TermDataModel: TermDefinition = {
    id: '',
    isCommunity: true,
    ancestors: [],
    metrics: {
        pubCount: 0,
        authorCount: 0,
        dateAdded: Date.now(),
        dateModified: Date.now(),
    },
    description: '',
    name: '',
    longName: '',
    origin: {
        url: project.community.url,
        id: project.community.id,
        tag: '',
    },
    props: null,
    type: '',
    author: {
        email: '',
        name: '',
        id: '',
        orcid: '',
    },
};

export default TermDataModel;
