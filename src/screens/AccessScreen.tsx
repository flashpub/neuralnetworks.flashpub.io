import * as React from 'react';
import { useDispatch } from 'react-redux';
import { RouteComponentProps } from '@reach/router';

import { useTypedSelector } from 'helpers';
import * as Access from 'containers/Access';
import { AccessGetStarted, AccessLobby, AccessRegister, AccessOrcid, AccessFinish } from 'ui/Access';
import { Dialog } from 'ui/Overlays';

export const AccessScreen: React.FC<RouteComponentProps> = () => {
    const dispatch = useDispatch();
    const accessStep = useTypedSelector((state) => state.access.data.step);
    const authStatus = useTypedSelector((state) => state.auth.status.type);
    const isAccessOpen = useTypedSelector((state) => state.access.data.isOpen);

    const handleOnClose = (set: boolean) => dispatch(Access.set(set));
    const isLoading = authStatus === 'LOADING';

    return (
        <Dialog open={isAccessOpen} onClose={handleOnClose} maskClosable={!isLoading} maskStyle={{ backgroundColor: 'rgba(255,255,255,.7' }}>
            {accessStep === 'LOBBY' && <AccessLobby />}
            {accessStep === 'REGISTER' && <AccessRegister />}
            {accessStep === 'GET_STARTED' && <AccessGetStarted />}
            {accessStep === 'ORCID' && <AccessOrcid />}
            {accessStep === 'FINISH' && <AccessFinish />}
        </Dialog>
    );
};
