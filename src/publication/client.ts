import store from 'app/store';
import * as AppAction from 'app/actions';
import api, { API_VERSION } from 'app/api';
import { AlertType, PubDefinition, ClaimDefinition } from 'app/types';

import * as PublicationsAction from 'publication/actions';

const version = API_VERSION.content;

const alertHandler = (type: AlertType, alert: Error | { message: string }) => {
    store.dispatch(AppAction.ADD_ALERT.use({
        type: type,
        message: alert.message,
    }));
    store.dispatch(PublicationsAction.PUBLICATION_OPERATION_FAIL.use());
};

export const publication = {
    get(id: string): Promise<PubDefinition> {
        return api
            .get(`/content/${version}/pub/${id}`)
            .then(response => response.data)
            .catch(error => alertHandler(AlertType.ERROR, error.response.data));
    },
    update(update: PubDefinition): Promise<any> {
        return api
            .put(`/content/${version}/pub/${update.id}`, update)
            .catch(error => alertHandler(AlertType.ERROR, error.response.data));
    },
};

export const claim = {
    get(id: string): Promise<ClaimDefinition> {
        return api
            .get(`/content/${version}/claim/${id}`)
            .then(response => response.data)
            .catch(error => alertHandler(AlertType.ERROR, error.response.data));
    },
};
