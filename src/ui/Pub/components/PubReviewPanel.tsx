import * as React from 'react';
import { find, lowerCase } from 'lodash';
import { useDispatch } from 'react-redux';
import { Checkbox, Badge, message, Modal, Tooltip } from 'antd';

import * as API from 'api';
import { useTypedSelector } from 'helpers';
import {
    StyledReviewPanelWrapper,
    StyledReviewPanelTitle,
    StyledCheckboxWrapper,
    StyledEndorseButton,
    StyledReviewsListWrapper,
    StyledReviewPanelBlocker, StyledCommentButton,
} from 'ui/Pub/styles';
import * as Pub from 'containers/Pub';
import { StyledSpacer } from 'ui/elements/styles';
import * as Access from 'containers/Access';

type Props = {
    showPanel: boolean;
    editMode: boolean;
    pub: PubDefinition;
};

export const PubReviewPanel: React.FC<Props> = ({ pub, editMode, showPanel }) => {
    const dispatch = useDispatch();

    const isAuth = useTypedSelector((state) => state.auth.data);
    const user = useTypedSelector((state) => state.profile.data.user);
    
    React.useEffect(() => {
        if (isAuth && user && pub && pub.title) {
            const sheet = document.querySelector('#publication_sheet') as HTMLElement;
            const reviewPanel = document.querySelector('#pub_review') as HTMLElement;
            const sheetPos = sheet.getBoundingClientRect();
            if (reviewPanel) {
                reviewPanel.style.left = sheetPos.right + 'px';
            }
        }
    });
    
    if (pub && user && !editMode) {
        const isOrcidConnected = user.orcid;
        const quickReviews = pub.review.quickReviews;
        const isUserAuthor = pub.author.orcid === user.orcid;
        const endorsementsCount = pub.review.acceptedBy.length;
        const hasUserEndorsed = find(pub.review.acceptedBy, (id) => id === user?.orcid);
        const hasUserReported = find(pub.review.reportedBy, (id) => id === user?.orcid);
    
        const openConnectOrcidDialog = () => {
            Modal.confirm({
                className: 'endorse_dialog',
                okText: 'Connect Orcid',
                content: 'You have to connect your ORCID account to continue',
                async onOk() {
                    dispatch(Access.set(true));
                    dispatch(Access.step('ORCID'));
                },
                onCancel() {
                    return null;
                },
                centered: true,
            });
        };
    
        const handleEndorse = async () => {
            if (!isOrcidConnected) {
                return openConnectOrcidDialog();
            }
            if (hasUserEndorsed) {
                dispatch(Pub.form.cut({form: 'pub', field: 'review.acceptedBy', value: user?.orcid}));
                await API.pub.updateArrayField(pub.id, 'review.acceptedBy', user.orcid, '-');
                message.warning('Publication rejected');
            } else {
                Modal.confirm({
                    className: 'endorse_dialog',
                    content: 'I verify that this publication sufficiently demonstrates its claims',
                    async onOk() {
                        dispatch(Pub.form.set({form: 'pub', field: 'review.acceptedBy', value: user?.orcid}));
                        await API.pub.updateArrayField(pub.id, 'review.acceptedBy', user.orcid, '+');
                        message.success('Publication endorsed!');
                    },
                    onCancel() {
                        return null;
                    },
                    centered: true,
                });
            }
        };
    
        const renderReviewOptions = Object.keys(quickReviews).map((txt, i) => {
            const reviewText = lowerCase(txt);
            const quickReviewItem = Object.keys(quickReviews)[i];
            const quickReviewItemValue = quickReviews[quickReviewItem];
            const hasUserReviewed = quickReviewItemValue.includes(user.orcid);
            
            const onReviewClick = async () => {
                if (!isOrcidConnected) {
                    return openConnectOrcidDialog();
                }
                if (!isAuth) return message.error('You need to be signed in to do this!');
                if (hasUserReviewed) {
                    dispatch(Pub.form.cut({form: 'pub', field: `review.quickReviews.${txt}`, value: user.orcid}));
                    await API.pub.updateArrayField(pub.id, `review.quickReviews.${txt}`, user.orcid, '-');
                } else {
                    dispatch(Pub.form.set({form: 'pub', field: `review.quickReviews.${txt}`, value: user.orcid}));
                    await API.pub.updateArrayField(pub.id, `review.quickReviews.${txt}`, user.orcid, '+');
                }
            };
        
            return (
                <StyledCheckboxWrapper key={i}>
                    <Checkbox onChange={onReviewClick} checked={hasUserReviewed}>
                        {reviewText}
                    </Checkbox>
                </StyledCheckboxWrapper>
            );
        });
        if (!showPanel || isUserAuthor) {
            return null;
        } else {
            const handleReport = async () => {
                if (hasUserReported) {
                    dispatch(Pub.form.cut({ form: 'pub', field: 'review.reportedBy', value: user?.orcid }));
                    await API.pub.updateArrayField(pub.id, 'review.reportedBy', user?.orcid as string, '-');
                } else {
                    Modal.confirm({
                        className: 'endorse_dialog',
                        content: 'Are you sure you want to report this pub?',
                        async onOk() {
                            const email = `mailto:hello@flashpub.io?subject=Retraction Request for ${pub.metadata.doi}`;
                            window.open(email);
                            dispatch(Pub.form.set({ form: 'pub', field: 'review.reportedBy', value: user?.orcid }));
                            await API.pub.updateArrayField(pub.id, 'review.reportedBy', user?.orcid as string, '+');
                        },
                        onCancel() {
                            return null;
                        },
                        centered: true,
                    });
                }
            };
            const handleComment = () => {
                const email = `mailto:hello@flashpub.io?subject=Reviewer Comment for ${pub.metadata.doi}`;
                window.open(email);
            };
            
            return (
                <StyledReviewPanelWrapper show={showPanel} id="pub_review">
                    <StyledReviewPanelTitle>Review and endorse</StyledReviewPanelTitle>
                    <StyledSpacer height={10} />
                    <StyledReviewsListWrapper>{renderReviewOptions}</StyledReviewsListWrapper>
                    <StyledSpacer height={10} />
                    <StyledCommentButton
                        block
                        fontSize={13}
                        onClick={handleReport}
                        label="Report"
                    />
                    <StyledSpacer height={10} />
                    <StyledCommentButton
                        block
                        fontSize={13}
                        onClick={handleComment}
                        label="Comment"
                    />
                    <StyledSpacer height={10} />
                    <Badge count={endorsementsCount} showZero>
                        <StyledEndorseButton
                            block
                            fontSize={14}
                            onClick={handleEndorse}
                            label={hasUserEndorsed ? 'ENDORSED' : 'ENDORSE'}
                            buttonType={hasUserEndorsed ? 'primary' : 'default'}
                        />
                    </Badge>
                    {isUserAuthor && (
                        <Tooltip
                            placement="top"
                            mouseEnterDelay={0}
                            mouseLeaveDelay={0}
                            title="Endorsements are turned off for pubs' authors"
                        >
                            <StyledReviewPanelBlocker />
                        </Tooltip>
                    )}
                </StyledReviewPanelWrapper>
            );
        }
    }
    return null;
};
