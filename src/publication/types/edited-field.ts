export type EditedField = {
    value: string,
    hadFocus: boolean,
    hadChanged: boolean
}