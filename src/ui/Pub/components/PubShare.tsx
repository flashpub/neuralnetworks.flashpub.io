import * as React from 'react';
import copy from 'clipboard-copy';
import { useLocation } from '@reach/router';
import { TwitterShareButton } from 'react-share';
import { message, Popover, Tooltip } from 'antd';
import { CopyFilled, ShareAltOutlined, TwitterOutlined } from '@ant-design/icons/lib';

import { useTypedSelector, useWindowWidth } from 'helpers';
import { StyledActionIcon, StyledShareWrapper } from 'ui/Pub/styles';
import { StyledSpacer } from 'ui/elements/styles';
import { Viewport } from 'assets/styles';


type Props = {};

export const PubShare: React.FC<Props> = (props: Props) => {
    const location = useLocation();
    const isMobile = useWindowWidth() <= Viewport.TABLET;
    const pub = useTypedSelector((state) => state.pub.data.pub);
    
    const onCopyDoi = () => {
        if (pub?.metadata.doi) {
            copy(pub?.metadata.doi);
            message.info('DOI copied to clipboard');
        } else {
            message.error('Oooops! Something went wrong...');
        }
    };
    
    const renderShareOptions = () => {
        return (
            <StyledShareWrapper mobile={isMobile}>
                <Tooltip title="Copy DOI" placement="right" mouseLeaveDelay={0} mouseEnterDelay={1}>
                    <StyledActionIcon onClick={onCopyDoi}>
                        <CopyFilled />
                    </StyledActionIcon>
                </Tooltip>
                {!isMobile ? <StyledSpacer height={10} /> : <div style={{ width: 20 }} />}
                <Tooltip title="Twitter" placement="right" mouseLeaveDelay={0} mouseEnterDelay={1}>
                    <StyledActionIcon>
                        <TwitterShareButton
                            url={location.href}
                            style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
                        >
                            <TwitterOutlined />
                        </TwitterShareButton>
                    </StyledActionIcon>
                </Tooltip>
            </StyledShareWrapper>
        );
    };
    
    return (
        <Popover
            trigger="click"
            content={renderShareOptions()}
            overlayClassName="fp-share-options"
            placement={isMobile ? 'right' : 'left'}
            getPopupContainer={() => document.querySelector('#pub_actions') as HTMLElement}
        >
            <Tooltip title="Share" placement="left" mouseLeaveDelay={0} mouseEnterDelay={1}>
                <StyledActionIcon>
                    <ShareAltOutlined />
                </StyledActionIcon>
            </Tooltip>
        </Popover>
    );
};
