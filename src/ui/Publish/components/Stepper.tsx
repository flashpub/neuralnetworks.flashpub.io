import { Steps } from 'antd';
import * as React from 'react';
import { useDispatch } from 'react-redux';
import { find, capitalize, lowerCase } from 'lodash';
import { useNavigate, useLocation, RouteComponentProps } from '@reach/router';

import { useTypedSelector } from 'helpers';
import * as Pub from 'containers/Pub';
import * as Publish from 'containers/Publish';
import { StyledStepperWrapper } from 'ui/Publish/styles';

const { Step } = Steps;

export const Stepper: React.FC<RouteComponentProps<{ contentType: string }>> = ({ contentType }) => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const location = useLocation();

    const step = useTypedSelector((state) => state.publish.data.step);
    const claim = useTypedSelector((state) => state.claim.data.claim);
    const dictionary = useTypedSelector((state) => state.dictionary.data);
    const selectedContent = useTypedSelector((state) => state.content.data.type);
    const dictionaryList = useTypedSelector((state) => state.dictionary.data.list);

    const { claimItems, relationship } = claim && claim;
    const isSingle = selectedContent?.claim.type === 'single';
    const termExists = (item: string) => find(dictionaryList, (d) => d.id === item);

    const setPubTitle = () => {
        const firstItemLongName = termExists(claimItems.firstItem.id)?.longName || dictionary.term1.longName;
        const secondItemLongName =
            termExists(claimItems.secondItem?.id as string)?.longName || dictionary.term2.longName;
        const title = isSingle
            ? `${capitalize(selectedContent?.contentTypeLabel)} of ${firstItemLongName}`
            : `${firstItemLongName} ${lowerCase(relationship)} ${secondItemLongName}`;

        dispatch(Pub.form.set({ form: 'draft', field: 'title', value: title }));
    };

    const handleOnStepChange = (current: any) => {
        let path;
        let type;

        switch (current) {
            case 0:
                type = 'INTRO';
                path = `${location.origin}/publish/${contentType}`;
                break;
            case 1:
                type = 'CLAIM';
                path = `${location.origin}/publish/${contentType}/claim`;
                break;
            case 2:
                type = 'PUBLICATION';
                path = `${location.origin}/publish/${contentType}/publication`;
                break;
            case 3:
                type = 'SUBMIT';
                path = `${location.origin}/publish/${contentType}/submit`;
                break;
            default:
                type = 'INTRO';
                path = `${location.origin}/publish/${contentType}`;
        }

        setPubTitle();
        navigate(path);
        dispatch(Publish.step({ order: current, type }));
    };

    const isPublicationDisabled = () => {
        if (isSingle && !claimItems.firstItem.id) return true;
        return !isSingle && (!claimItems.firstItem.id || !claimItems.secondItem?.id || !relationship);
    };

    return (
        <StyledStepperWrapper>
            <Steps size="small" current={step.order} onChange={handleOnStepChange}>
                <Step title="Instructions" />
                <Step title="Claim" />
                <Step title="Publication" disabled={isPublicationDisabled()} />
                <Step title="Submit" disabled />
            </Steps>
        </StyledStepperWrapper>
    );
};
