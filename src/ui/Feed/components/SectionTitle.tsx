import * as React from 'react';
import { RightCircleFilled } from '@ant-design/icons';

import { StyledSectionTitleWrapper, StyledSectionTitle, StyledSectionTitleMore } from 'ui/Feed/styles';

type Props = {
    type: string;
    title: string;
    more?: boolean;
    pubs: PubDefinition[];
};

export const SectionTitle: React.FC<Props> = ({ title, type, pubs, more }) => {
    const topPubs = pubs.slice(0, 3);
    const recentPubs = pubs.slice(3, 10);
    
    if (type === 'top') {
        return (
            <StyledSectionTitleWrapper>
                <StyledSectionTitle>{title}</StyledSectionTitle>
                {topPubs.length === 3 && pubs.length > 3 && more && (
                    <StyledSectionTitleMore
                      iconSide="right"
                      label="more"
                      buttonType="link"
                      icon={<RightCircleFilled />}
                      onClick={() => console.log('view more', title)}
                    />
                )}
            </StyledSectionTitleWrapper>
        );
    } else if (type === 'recent' && recentPubs.length > 0) {
        return (
            <StyledSectionTitleWrapper>
                <StyledSectionTitle>{title}</StyledSectionTitle>
                {recentPubs.length > 7 && more && <StyledSectionTitleMore
                    iconSide="right"
                    label="more"
                    buttonType="link"
                    icon={<RightCircleFilled/>}
                    onClick={() => console.log('view more', title)}
                />}
            </StyledSectionTitleWrapper>
        );
    } else if (type === 'leaders' && pubs.length > 0) {
        return (
            <StyledSectionTitleWrapper>
                <StyledSectionTitle>{title}</StyledSectionTitle>
                {pubs.length > 5 && more && <StyledSectionTitleMore
                  iconSide="right"
                  label="more"
                  buttonType="link"
                  icon={<RightCircleFilled/>}
                  onClick={() => console.log('view more', title)}
                />}
            </StyledSectionTitleWrapper>
        );
    }else if (type === 'campaigns' && pubs.length > 0) {
        return (
            <StyledSectionTitleWrapper>
                <StyledSectionTitle>{title}</StyledSectionTitle>
                {pubs.length > 5 && more && <StyledSectionTitleMore
                  iconSide="right"
                  label="more"
                  buttonType="link"
                  icon={<RightCircleFilled/>}
                  onClick={() => console.log('view more', title)}
                />}
            </StyledSectionTitleWrapper>
        );
    }
    
    return null;
};
