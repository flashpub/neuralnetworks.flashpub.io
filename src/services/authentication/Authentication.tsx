import * as React from 'react';
import { useDispatch } from 'react-redux';

import { userData } from 'helpers';
import { firebase } from 'configs';
import * as authentication from 'services/authentication';
import * as Profile from 'containers/Profile';

import { FirebaseUser } from 'types/firebase';

export const Authentication: React.FC = ({ children }) => {
    const dispatch = useDispatch();

    React.useEffect(() => {
        const authenticateUser = async (user: FirebaseUser) => {
            if (user) {
                dispatch(authentication.signin(userData(user)));
                await dispatch(Profile.getUser(user.uid));
            } else {
                dispatch(authentication.signout());
                dispatch(authentication.status.pending());
                return;
            }
            dispatch(authentication.status.success());
        };
        firebase.auth.onAuthStateChanged(authenticateUser);

        return () => firebase.auth.onAuthStateChanged(authenticateUser)();
    }, [dispatch]);

    return <>{children}</>;
};
