import * as React from 'react';
import { connect } from 'react-redux';
import { Form, Formik, FormikProps } from 'formik';

import { ORCID_API } from 'app/api';
import { RootState } from 'app/store';
import { ReduxProps } from 'app/types';
import * as AppAction from 'app/actions';
import * as Shared from 'app/components/Shared';
import { envDetector, PortalView, sleep, Status, Viewport } from 'app/utils';

import { AccessStep } from 'access/utils';
import * as AccessAction from 'access/actions';
import * as AccessComponents from 'access/components';
import * as AccessAsyncAction from 'access/async-actions';
import { LoginCredentials, ReginCredentials } from 'access/types';

import { auth } from 'firebase-config';

const mapDispatch = {
    loginUser: AccessAsyncAction.loginUser,
    verifyOrcid: AccessAsyncAction.verifyOrcid,
    registerUser: AccessAsyncAction.registerUser,
    resetPassword: AccessAsyncAction.resetPassword,
    
    setAppStatus: AppAction.SET_APP_STATUS.use,
    setPortalView: AppAction.SET_PORTAL_VIEW.use,
    setAccessStep: AccessAction.SET_ACCESS_STEP.use,
    setOrcidWindow: AccessAction.OPEN_ORCID_WINDOW.use,
};

const mapState = (state: RootState) => ({
    gates: state.app.gates,
    open: state.access.open,
    user: state.access.user,
    code: state.access.code,
    portalView: state.app.view,
    appStatus: state.app.status,
    accessStep: state.access.step,
    accessStatus: state.access.status,
});

type Props = ReduxProps<typeof mapState, typeof mapDispatch>;
type State = {
    width: number,
    code: string | null,
}

class AccessContainer extends React.Component<Props, State> {
    state = {
        code: null,
        width: window.innerWidth,
    };

    handleResize = () => this.setState({ width: window.innerWidth });

    componentDidMount(): void {
        window.addEventListener('resize', this.handleResize);
        (this.state.width > Viewport.MOBILE) && this.props.portalView === PortalView.ACCESS &&  this.handleMessageEventListener();
    }

    componentDidUpdate(prevProps: Readonly<Props>): void {
        if (prevProps.code !== this.props.code) {
            this.props.portalView === PortalView.ACCESS && this.props.code && this.handleVerifyOrcid(this.props.code);
        }
    }

    componentWillUnmount(): void {
        window.removeEventListener('resize', this.handleResize);
    }

    handleMessageEventListener = () => {
        window.addEventListener('message', (message) => {
            if (message.data && typeof message.data === 'string') {
                this.setState({ code: message.data });
                this.handleVerifyOrcid(message.data);
            }
        });
    };

    connectOrcidDesktop = () => {
        const setStatus = () => this.props.setAppStatus({ status: Status.PENDING });

        const newWindow = (url: string, width: number, height: number) => {
            const left = ((window as any).visualViewport.width / 2) - ((width / 2));
            const top = ((window as any).visualViewport.height / 2) - ((height / 2) - 50);
            const features = `toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=${width},
                height=${height},left=${left},top=${top},screenX=${left},screenY=${top}`;

            return window.open(url, 'Connect to ORCID', features);
        };
        const orcidWindow = newWindow(ORCID_API, 700, 700);
        const timer = setInterval(() => {
            if (orcidWindow!.closed) {
                !this.state.code && setStatus();
                clearInterval(timer);
            }
        }, 1000);
    };

    connectOrcidResponsive = () => window.open(ORCID_API, '_self');

    handleOnConnectOrcid = () => {
        this.props.setAppStatus({ status: Status.LOADING });

        (this.state.width > Viewport.MOBILE)
            ? this.connectOrcidDesktop()
            : this.connectOrcidResponsive();
    };
    
    handleVerifyOrcid = (code: string) => {
        const uri = envDetector('ORCID');

        auth.onAuthStateChanged(user => {
            if (user && code && uri) {
                this.props.verifyOrcid(code, user.uid, uri);
            }
        });
    };
    
    handleSetAccessStep = (step: AccessStep) => this.props.setAccessStep({ step });
    handleSetPortalView = (view: PortalView) => this.props.setPortalView({ view });
    handleOnLogin = (credentials: LoginCredentials) => this.props.loginUser(credentials);
    handleOnRegister = (credentials: ReginCredentials) => this.props.registerUser(credentials);
    handleOnReset = (email: string) => this.props.resetPassword(email);
    
    handleDialogClose = () => {
        this.handleSetPortalView(PortalView.DEFAULT);
        sleep(200).then(() => this.handleSetAccessStep(AccessStep.DEFAULT));
    };
    
    handleOnReturn = () => {
        switch (this.props.accessStep) {
            case AccessStep.GET_STARTED:
                return this.handleSetAccessStep(AccessStep.DEFAULT);
            case AccessStep.REGISTER:
                return this.handleSetAccessStep(AccessStep.USERNAME);
            case AccessStep.USERNAME:
                return this.handleSetAccessStep(AccessStep.GET_STARTED);
            case AccessStep.RESET_PASSWORD:
                return this.handleSetAccessStep(AccessStep.DEFAULT);
            default:
                return null;
        }
    };

    render() {
        return (
            <Shared.Dialog
                onReturn={this.handleOnReturn}
                onClose={this.handleDialogClose}
                accessStep={this.props.accessStep}
                isOpen={this.props.portalView === PortalView.ACCESS}
                isLoading={this.props.accessStatus === Status.LOADING}
            >
                {this.props.accessStep === AccessStep.DEFAULT &&
                    <AccessComponents.Login
                        gates={this.props.gates}
                        onLogin={this.handleOnLogin}
                        setAccessStep={this.handleSetAccessStep}
                        isAccessLoading={this.props.accessStatus}
                    />
                }

                {this.props.accessStep === AccessStep.GET_STARTED &&
                    <AccessComponents.GetStarted
                        setAccessStep={this.handleSetAccessStep}
                    />
                }

                <Formik
                    initialValues={{ email: '', password: '', name: '' }}
                    onSubmit={(values: ReginCredentials) => this.handleOnRegister(values)}
                    render={(formikBag: FormikProps<ReginCredentials>) => (
                        <Form>
                            {this.props.accessStep === AccessStep.USERNAME &&
                                <AccessComponents.Username
                                    formikBag={formikBag}
                                    setAccessStep={this.handleSetAccessStep}
                                />
                            }

                            {this.props.accessStep === AccessStep.REGISTER &&
                                <AccessComponents.Register
                                    formikBag={formikBag}
                                    setAccessStep={this.handleSetAccessStep}
                                    isAccessLoading={this.props.accessStatus}
                                />
                            }
                        </Form>
                    )}
                />

                {this.props.accessStep === AccessStep.ORCID &&
                    <AccessComponents.Orcid
                        open={this.props.open}
                        user={this.props.user}
                        setAccessStep={this.handleSetAccessStep}
                        onConnectOrcid={this.handleOnConnectOrcid}
                    />
                }
    
                {this.props.accessStep === AccessStep.FINISH &&
                    <AccessComponents.Finish
                        onClose={this.handleDialogClose}
                    />
                }
    
                {this.props.accessStep === AccessStep.RESET_PASSWORD &&
                    <AccessComponents.ResetPassword
                      onReset={this.handleOnReset}
                      onClose={this.handleDialogClose}
                      isAccessLoading={this.props.accessStatus}
                    />
                }

            </Shared.Dialog>
        );
    }
}

// @ts-ignore
export default connect(mapState, mapDispatch)(AccessContainer);
