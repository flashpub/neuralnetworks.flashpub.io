import * as React from 'react';
import { StyledFooterBrandTitle, StyledFooterBrandUrl, StyledFooterISSN } from 'ui/Footer/styles';

import { project } from 'configs';
import { StyledBrandIcon, StyledBrandIconWrapper } from 'ui/elements/styles';

export const FooterBrand: React.FC = () => {
    return (
        <>
            <StyledBrandIconWrapper>
                <StyledBrandIcon symbol={project.community.symbol} size={60} fontSize={28} reversed />
            </StyledBrandIconWrapper>
            <StyledFooterBrandTitle>{project.community.name}</StyledFooterBrandTitle>
            <StyledFooterISSN>
                ISSN 2693-6828
            </StyledFooterISSN>
            <StyledFooterBrandUrl href="https://flashpub.io" target="_blank" rel="noopener noreferrer">
                flashpub.io
            </StyledFooterBrandUrl>
        </>
    );
};
