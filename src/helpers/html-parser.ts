import htmlToDraft from 'html-to-draftjs';
import { ContentState, EditorState } from 'draft-js';

export const parseHtml = (text: string) => {
    const contentBlock = htmlToDraft(text);
    if (contentBlock) {
        const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
        const editorState = EditorState.createWithContent(contentState);
        return editorState.getCurrentContent().getPlainText();
    }
};
