import * as React from 'react';
import htmlToDraft from 'html-to-draftjs';
import draftToHtml from 'draftjs-to-html';
import styled from 'styled-components/macro';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import { PubDefinition } from '../../app/types';
import { Color, Font, Radius, Spacing } from '../../app/assets';


type Props = {
    required: boolean;
    disabled: boolean;
    pub: PubDefinition;
    chars?: number;
    counter?: boolean;
    onChange?(value: string): any;
    placeholder?: string;
};


export const Description: React.FC<Props> = ({ required, disabled, pub, onChange, chars = 300, counter, placeholder }) => {
    const [editorInFocus, setEditorInFocus] = React.useState<boolean>(false);
    const [editorState, setEditorState] = React.useState<any>(EditorState.createEmpty());

    const desc = editorState.getCurrentContent().getPlainText();

    const setEditor = () => {
        const contentBlock = htmlToDraft(pub.description);
        if (contentBlock) {
            const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
            const editorState = EditorState.createWithContent(contentState);
            setEditorState(editorState);
        }
    };

    const handleEditorBlur = () => {
        setEditorInFocus(false);
        const description = draftToHtml(convertToRaw(editorState.getCurrentContent())).replace(/(\r\n|\n|\r)/gm, "");
        if (desc.length === 0) {
            onChange && onChange('');
        } else {
            onChange && onChange(description);
        }
    };

    React.useEffect(() => {
        pub.description && setEditor();
    }, [pub.description]);


    const isRequired = required && !(desc.length > 0);

    return (
        <EditorWrapper required={isRequired} disabled={disabled} focus={editorInFocus}>
            <Editor
                toolbarOnFocus
                readOnly={disabled}
                onBlur={handleEditorBlur}
                placeholder={placeholder}
                editorState={editorState}
                editorClassName="desc_editor"
                toolbarClassName="desc_toolbar"
                onEditorStateChange={setEditorState}
                onFocus={() => !disabled && setEditorInFocus(true)}
                toolbar={{
                    options: ['inline'],
                    // options: ['inline', 'blockType'],
                    inline: {
                        className: 'desc_inline',
                        options: ['italic'],
                        italic: { className: 'desc_inline-italic' },
                    },
                    // blockType: {
                    //     className: 'desc_block',
                    //     dropdownClassName: 'desc_block-dropdown',
                    //     options: ['Normal', 'Blockquote', 'Code'],
                    // },
                }}
            />
            {counter && !disabled && <Counter focus={editorInFocus}>{chars - desc.length}</Counter>}
        </EditorWrapper>
    );
};

const Counter = styled.div<{ focus: boolean }>`
    top: 0;
    right: 0;
    z-index: 10;
    height: 38px;
    display: flex;
    font-size: 14px;
    margin-top: 3px;
    position: absolute;
    align-items: center;
    padding: ${Spacing.S};
    color: ${Color.midgrey};
    border-radius: ${Radius};
    background: ${Color.lightgrey};
    border: 1px solid ${Color.midgrey};
    visibility: ${({ focus }) => focus ? 'visible' : 'hidden'};
`;
const EditorWrapper = styled.div<{ required: boolean, disabled: boolean, focus: boolean }>`
    position: relative;
    .desc_editor {
        z-index: 10;
        font-size: 18px;
        font-weight: 200;
        border: 1px solid;
        position: relative;
        border-radius: 5px;
        color: ${Color.grey};
        background-color: #fff;
        font-family: ${Font.text};
        transition: padding .2s ease;
        padding: ${({ disabled }) => disabled ? 0 : '0 14px'};
        cursor:  ${({ disabled }) => disabled ? 'default' : 'text'};
        border-color: ${({ focus, disabled, required }) => !disabled && focus ? Color.midgrey : required ? Color.midgrey : 'transparent'};
        
        &::before {
            top: 2px;
            left: 4px;
            position: absolute;
            color: ${Color.error};
            content: ${({ required }) => required ? "'*'" : "''"};
        }
        &:focus {
            outline: none;
            border-color: ${({ disabled }) => disabled ? 'transparent' : Color.midgrey};
        }
        &:hover {
            border-color: ${({ disabled }) => disabled ? 'transparent' : Color.midgrey};
        }
        &:active {
            border-color: ${({ disabled }) => disabled ? 'transparent' : Color.midgrey};
        }
    }
    .desc_toolbar {
        padding: 0;
        width: 200px;
        height: 38px;
        margin-top: 5px;
        border-radius: ${Radius};
        background: ${Color.lightgrey};
        border: 1px solid ${Color.midgrey};
        visibility: ${({ disabled }) => disabled ? 'hidden !important' : 'visible'};
    }
    // .desc_block {
    //     width: 120px;
    //     border-radius: ${Radius};
    //     font-family: ${Font.mono};
    //     text-transform: lowercase;
    //     border: 1px solid ${Color.lightgrey};
    //     &:active, &:hover {
    //         box-shadow: unset;
    //     }
    //     span {
    //         color: ${Color.midgrey};
    //     }
    //     .rdw-dropdown-carettoopen, .rdw-dropdown-carettoclose {
    //         top: 50%;
    //         right: 5px;
    //         transform: translateY(-50%);
    //         border-top-color: ${Color.midgrey};
    //         border-bottom-color: ${Color.midgrey};
    //     }
    //     .desc_block-dropdown {
    //         background-color: #fff;
    //         color: ${Color.midgrey};
    //         border-radius: ${Radius};
    //         &:active, &:hover {
    //             box-shadow: unset;
    //         }
    //     }
    // }
    .desc_inline {
        height: 38px;
        .desc_inline-italic, .desc_inline-bold {
            width: 30px;
            height: 18px;
            border-radius: ${Radius};
            margin: 0 ${Spacing.S};
            border: 1px solid ${Color.midgrey};
            &:active, &:hover {
                box-shadow: unset;
            }
            &.rdw-option-active {
                box-shadow: none;
                background: ${Color.midgrey};
            }
        }
    }
`;
