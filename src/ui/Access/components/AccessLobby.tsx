import * as React from 'react';
import { useDispatch } from 'react-redux';
import { RouteComponentProps } from '@reach/router';

import { useTypedSelector } from 'helpers';
import * as Access from 'containers/Access';
import * as authentication from 'services/authentication';
import { StyledCreateAccountButton, AccessForm } from 'ui/Access';
import { StyledBrandIcon, StyledBrandLogo, StyledFlexWrapper, StyledSpacer } from 'ui/elements/styles';

export const AccessLobby: React.FC<RouteComponentProps> = () => {
    const dispatch = useDispatch();
    const authStatus = useTypedSelector((state) => state.auth.status);
    const accessData = useTypedSelector((state) => state.access.data);

    const onDispatch = (value: string) => dispatch(Access.step(value));
    const onCreateAccount = () => dispatch(Access.step('GET_STARTED'));

    const onSubmitLoginForm = (creds: Credentials) => dispatch(authentication.signinUser(creds));

    return (
        <StyledFlexWrapper>
            <StyledBrandIcon symbol="fp" size={80} fontSize={40} main />
            <StyledBrandLogo fontSize={20}>
                flash<strong>pub</strong>
            </StyledBrandLogo>
            <StyledSpacer height={20} />
            <AccessForm
                label="Login"
                dispatch={onDispatch}
                step={accessData.step}
                onSubmit={onSubmitLoginForm}
                isLoading={authStatus.type === 'LOADING'}
            />
            <StyledCreateAccountButton buttonType="link" label="Create flashpub account" onClick={onCreateAccount} />
        </StyledFlexWrapper>
    );
};
