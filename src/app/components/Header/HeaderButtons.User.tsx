import * as React from 'react';
import styled from 'styled-components';
import { Popover, Icon as UIIcon } from '@blueprintjs/core';

import { Color, Font } from 'app/assets';
import { MenuContainer } from 'app/components';

import { FUIButton } from 'FUI/elements';


type Props = {
    orcid: string,
    initials: string,
    isMobile: boolean,
};

const HeaderButtonsUser: React.FC<Props> = (props: Props) => {
    const [isOpen, setIsOpen] = React.useState(false);
    
    const handleUserMenu = (state: boolean) => setIsOpen(state);

    return (
        <Popover
            isOpen={isOpen}
            onInteraction={handleUserMenu}
            popoverClassName="pt-popover-content-sizing"
    // @ts-ignore
            content={<MenuContainer isMobile={props.isMobile} handleUserMenu={handleUserMenu}/>}
        >
            {props.isMobile
                ? <UserIcon icon="user" iconSize={35} orcid={props.orcid}/>
                : <UserButton text={props.initials} buttonType="outlined" orcid={props.orcid}/>
            }
        </Popover>
    );
};

const UserIcon = styled(UIIcon)<{ orcid: string }>` && {
    color: ${Color.community};
    ::after {
        content: ${props => props.orcid ? '' : `''`};
        top: 16px;
        width: 20px;
        color: #fff;
        right: 16px;
        height: 20px;
        position: absolute;
        border-radius: 50%;
        background-color: #fff;
        border: 5px solid ${Color.flashpub};
    }
    }
}`;
const UserButton = styled(FUIButton)<{ orcid: string }>` && {
    min-width: 50px;
    position: relative;
    color: ${Color.grey};
    display: inline-block;
    border-color: ${Color.grey};
    font-family: ${Font.header_button};
    :hover {
        background-color: ${Color.lightgrey};
    }
    ::after {
        content: ${props => props.orcid ? '' : `''`};
        top: -7px;
        width: 14px;
        color: #fff;
        right: -7px;
        height: 14px;
        position: absolute;
        border-radius: 50%;
        background-color: ${Color.flashpub};
    }
}`;

export default HeaderButtonsUser;
