import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle<{ mapMode?: boolean }>` && {
    body, html {
        font-size: 16px;
        min-width: 320px;
        line-height: 1 !important;
    }
    
    
    a:link, a:visited, a:hover {
        text-decoration: none;
    }
    
    .bp3-popover {
        background-color: #fff;
    }
    
    .bp3-popover-arrow {
        display: none;
    }

    
    .content-main {
        padding: 0 !important;
        max-width: 110vw !important;
        margin-left: ${({ mapMode }) => mapMode && 0};
        margin-right: ${({ mapMode }) => mapMode && 0};
    }

    
    .content-main {
        padding: 0 !important;
        max-width: 110vw !important;
        margin-left: ${({ mapMode }) => mapMode && 0};
        margin-right: ${({ mapMode }) => mapMode && 0};
    }

    
    :focus {
        outline: none;
        outline-offset: 0;
        -moz-outline-radius: 0;
    }
}`;
