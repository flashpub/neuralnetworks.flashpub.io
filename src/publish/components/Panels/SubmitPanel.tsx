import * as React from 'react';
import styled from 'styled-components';
import { useHistory } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { Checkbox, Spinner as UISpinner } from '@blueprintjs/core';

import { RootState } from 'app/store';
import { Color, Spacing } from 'app/assets';
import { SET_PORTAL_VIEW } from 'app/actions';
import { PortalView, Status } from 'app/utils';
import { CommunityDefinition, Content } from 'app/types';
import { PublishAction, usePublish } from 'publish';
import { createPub } from 'publish/async-actions';
import { Title } from 'publish/components/Elements';
import { prepareClaimData, prepareDictionaryData, preparePubData } from 'publish/utils';
import { DictionaryDefinition } from 'dictionaries/types';
import { FUIButton, FUIInput } from 'FUI/elements';
import { FUIPanel } from 'FUI/components';


export const SubmitPanel: React.FC = () => {
    const history = useHistory();
    const storeDispatch = useDispatch();
    const [{ panelStack, form }, dispatch] = usePublish();
    const [checked, setChecked] = React.useState<boolean>(form.metadata.isDeIdentified);
    const content = useSelector<RootState, Content | null>(state => state.publish.contentType);
    const community = useSelector<RootState, CommunityDefinition | null>(state => state.app.community);
    const campaign = useSelector<RootState, CommunityDefinition | null>(state => state.campaigns.current);
    const dictionaries = useSelector<RootState, DictionaryDefinition[] | null>(state => state.dictionaries.list);
    const publishStatus = useSelector<RootState, Status | null>(state => state.publish.status);

    if (community && dictionaries) {
        const previousPanel = () => {
            const panel = panelStack[panelStack.length - 2];
            if (panel && panel.order === 0) return panelStack[0];
            return panel;
        };

        const legalExists = campaign ? campaign.hasOwnProperty('legal') : community.hasOwnProperty('legal');

        const isSubmitDisabled = legalExists && !checked;
        const handleOnCheck = (e: any) => {
            setChecked(e.target.checked);
            dispatch(PublishAction.SetPublishFormValue({ name: 'metadata.isDeIdentified', value: e.target.checked }))
        };
        const onMoreInfo = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
            const legalMoreInfo = Object.values(community.legal!).filter(c => c.moreInfo)[0] && Object.values(community.legal!).filter(c => c.moreInfo)[0].moreInfo;
            e.preventDefault();
            window.open(legalMoreInfo);
        };
        // const legalDesc = Object.values(community.legal!).filter(c => c.description)[0].description;
        const legalDesc = () => {
            if (campaign) {
                return legalExists ? Object.values(campaign.legal!).filter(c => c.description)[0] && Object.values(campaign.legal!).filter(c => c.description)[0].description : null;
            } else {
                return Object.values(community.legal!).filter(c => c.description)[0] && Object.values(community.legal!).filter(c => c.description)[0].description;
            }
        };

        const label = <Label>{legalDesc()} (<span onClick={onMoreInfo}>more info</span>)</Label>;

        const revs = ['suggested.reviewer_1','suggested.reviewer_2','suggested.reviewer_3','suggested.reviewer_4'];
        const renderFields = revs.map((r, i) => {
            const handleOnChange = (e: any) => dispatch(PublishAction.SetPublishFormValue({ name: r, value: e }));
            const val = form.suggested[r.split('.')[1]];
            return (
                <Input key={i} placeholder="email..." onChange={handleOnChange} value={val} />
            )
        });

        const onSubmit = async () => {
            const firstItemId = form.claim.firstItem && form.claim.firstItem.id;
            const secondItemId = form.claim.secondItem && form.claim.secondItem.id;
            const firstItemExists = dictionaries.filter(d => d.id === firstItemId)[0];
            const secondItemExists = dictionaries.filter(d => d.id === secondItemId)[0];
            const isSingle = content!.claim.type === 'single';
            const fetchId = `${form.title.split(' ').join('-').toLowerCase()}_${form.id}`;

            localStorage.setItem('fetchId', fetchId);
            localStorage.removeItem('slides');

            if (firstItemExists && !secondItemExists && isSingle) {
                const finished = await storeDispatch(createPub({
                    pub: preparePubData(form),
                    claim: prepareClaimData(form),
                    figures: form.figures,
                }));

                if (finished) {
                    history.push('/');
                    return storeDispatch(SET_PORTAL_VIEW.use({ view: PortalView.SUMMARY }))
                }
            }
            if (!firstItemExists && !secondItemExists && isSingle) {
                const finished = await storeDispatch(createPub({
                    pub: preparePubData(form),
                    claim: prepareClaimData(form),
                    figures: form.figures,
                    newDictionary1: prepareDictionaryData(form, 'first'),
                }));

                if (finished) {
                    history.push('/');
                    return storeDispatch(SET_PORTAL_VIEW.use({ view: PortalView.SUMMARY }))
                }
            }
            if (firstItemExists && secondItemExists && !isSingle) {
                const finished = await storeDispatch(createPub({
                    pub: preparePubData(form),
                    claim: prepareClaimData(form),
                    figures: form.figures,
                }));

                if (finished) {
                    history.push('/');
                    return storeDispatch(SET_PORTAL_VIEW.use({ view: PortalView.SUMMARY }))
                }
            }
            if (firstItemExists && !secondItemExists && !isSingle) {
                const finished = await storeDispatch(createPub({
                    pub: preparePubData(form),
                    claim: prepareClaimData(form),
                    figures: form.figures,
                    newDictionary2: prepareDictionaryData(form, 'second'),
                }));

                if (finished) {
                    history.push('/');
                    return storeDispatch(SET_PORTAL_VIEW.use({ view: PortalView.SUMMARY }))
                }
            }
            if (!firstItemExists && secondItemExists && !isSingle) {
                const finished = await storeDispatch(createPub({
                    pub: preparePubData(form),
                    claim: prepareClaimData(form),
                    figures: form.figures,
                    newDictionary1: prepareDictionaryData(form, 'first'),
                }));

                if (finished) {
                    history.push('/');
                    return storeDispatch(SET_PORTAL_VIEW.use({ view: PortalView.SUMMARY }))
                }
            }
            if (!firstItemExists && !secondItemExists && !isSingle) {
                const finished = await storeDispatch(createPub({
                    pub: preparePubData(form),
                    claim: prepareClaimData(form),
                    figures: form.figures,
                    newDictionary1: prepareDictionaryData(form, 'first'),
                    newDictionary2: prepareDictionaryData(form, 'second'),
                }));

                if (finished) {
                    history.push('/');
                    return storeDispatch(SET_PORTAL_VIEW.use({ view: PortalView.SUMMARY }))
                }
            }
            return null;
        };

        return (
            <FUIPanel
                title="Submit"
                previousPanel={previousPanel()}
                onReturn={() => dispatch(PublishAction.CutPublishPanel())}
            >
                {publishStatus === Status.LOADING
                    ?  <Spinner />
                    : (<>
                        <Title>Ready to Submit?</Title>
                        {legalExists && <Certify>
                            <Checkbox checked={checked} labelElement={label} onChange={handleOnCheck}/>
                        </Certify>}
                        <SuggestedReviewers>
                            <p>Suggested reviewers</p>
                            {renderFields}
                        </SuggestedReviewers>
                        <SubmitButton
                            type="button"
                            text="Publish"
                            onClick={onSubmit}
                            disabled={isSubmitDisabled}
                        />
                    </>)
                }
            </FUIPanel>
        );
    } else return null;
};


const Spinner = styled(UISpinner)` && {
    svg {
        width: 100px;
        height: 100px;
        path.bp3-spinner-head {
            stroke: ${Color.community};
        }
    }
}`;
const Label = styled.span`
    span {
        color: ${Color.community};
    }
`;
const Input = styled(FUIInput)` && {
    width: 300px;
    text-align: start;
    padding: 0 ${Spacing.SM};
}`;
const SuggestedReviewers = styled.div`
    color: ${Color.grey};
    margin-bottom: ${Spacing.XXXL};
    p {
        font-size: 20px;
    }
`;
const SubmitButton = styled(FUIButton)<{ disabled: boolean }>` && {
    width: 350px;
    font-size: 24px;
    color: ${props => props.disabled && '#fff'};
    cursor: ${props => props.disabled && 'not-allowed'};
    background-color: ${props => props.disabled && Color.community_t15};
}`;
const Certify = styled.div`
    color: ${Color.grey};
    margin-bottom: ${Spacing.XXXL};
    
    .bp3-control input:checked ~ .bp3-control-indicator,
    .bp3-control:hover input:checked ~ .bp3-control-indicator {
        background-color: ${Color.community};
    }
`;
