import * as React from 'react';
import { useMedia } from 'react-use';
import { useDispatch } from 'react-redux';
import { RouteComponentProps, useNavigate } from '@reach/router';

import { useTypedSelector, envDetector } from 'helpers';
import { MOBILE } from 'assets/styles';
import * as Access from 'containers/Access';
import * as Orcid from 'containers/Orcid';

export const OrcidScreen: React.FC<RouteComponentProps> = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const auth = useTypedSelector((state) => state.auth.data);
    const isDesktop = useMedia(`(min-width: ${MOBILE})`);

    React.useEffect(() => {
        const search = window && window.location && window.location.search;
        const regex = /[?&]code=(.+)/.exec(search);
        const code = regex && regex[1];
        if (search && code) {
            if (!isDesktop) {
                navigate('/');
                dispatch(Orcid.code(code));
                dispatch(Access.set(true));
                dispatch(Access.step('ORCID'));
                auth && dispatch(Orcid.verifyOrcid(code, auth.uid, envDetector('ORCID')));
            } else {
                window.opener.postMessage(`${code}`);
                window.close();
            }
        }
    });

    return null;
};
