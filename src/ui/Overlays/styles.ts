import { Modal } from 'antd';
import styled from 'styled-components/macro';

import { Color, Font, Media, MOBILE, Radius, Spacing } from 'assets/styles';

export const StyledDialogWrapper = styled(Modal)<{ width?: number}>`
    && {
        width: 100% !important;
        height: 100% !important;
        padding: ${Spacing._24};
        border-radius: ${Radius};
        position: unset !important;
        transform-origin: unset !important;
        background-color: ${Color.notwhite};
        box-shadow: 0px 0px 10px 3px ${Color.community_t75};
        max-width: ${({ width }) => (width ? `${width}px` : '450px')};

        @media (min-width: ${MOBILE}) {
            padding: 0;
        }
        
        ${Media.tablet} {
            height: unset !important;
            background-color: unset;
        }

        .ant-modal-content {
            position: unset !important;
            border-radius: ${Radius} !important;
        }
        .ant-modal-body {
            padding: ${Spacing._24};
            height: fit-content !important;
            @media (min-width: ${MOBILE}) {
                padding: ${Spacing._x(36)} ${Spacing._48};
            }
        }
    }
`;
export const StyledZoomInWrapper = styled(Modal)`
    && {
        cursor: zoom-out;
        max-width: 1200px;
        width: 100% !important;
        padding: ${Spacing._24};

        .ant-modal-content {
            border-radius: ${Radius} !important;
        }
        .ant-modal-body {
            padding: 0 ${Spacing._12} ${Spacing._12};
            @media (min-width: ${MOBILE}) {
                padding: ${Spacing._24};
            }
            img {
                width: 100%;
            }
        }
    }
`;
export const StyledZoomInTitle = styled.div`
    font-size: 20px;
    text-align: center;
    font-family: ${Font.text};
    padding-bottom: ${Spacing._24};
`;
