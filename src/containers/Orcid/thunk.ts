import { Dispatch } from 'redux';

import * as API from 'api';
import { AppThunk } from 'store';
import * as Orcid from 'containers/Orcid';
import * as Profile from 'containers/Profile';

export function verifyOrcid(code: string, uid: string, uri: string): AppThunk {
    return async (dispatch: Dispatch) => {
        dispatch(Orcid.status.loading());
        console.log('am I running?');
        try {
            const resp = await API.orcid.verify(code, uid, uri);
            if (resp.status === 200) {
                dispatch(Orcid.orcid(resp.data));
                dispatch(Orcid.status.success());
                dispatch(Profile.update({ form: 'user', field: 'orcid', value: resp.data }));
            }
        } catch (err) {
            dispatch(Orcid.status.failure(err.message));
        }
    };
}

export function desktopToOrcid(): AppThunk {
    return async (dispatch: Dispatch, getState) => {
        dispatch(Orcid.status.loading());
        const newWindow = (url: string, width: number, height: number) => {
            const left = (window as any).visualViewport.width / 2 - width / 2;
            const top = (window as any).visualViewport.height / 2 - (height / 2 - 50);
            const features = `toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=${width},
                height=${height},left=${left},top=${top},screenX=${left},screenY=${top}`;

            return window.open(url, 'Connect to ORCID', features);
        };
        const orcidWindow = newWindow(API.ORCID, 700, 700);
        const timer = setInterval(() => {
            if (orcidWindow!.closed) {
                !getState().orcid.data.code && dispatch(Orcid.status.pending());
                clearInterval(timer);
            }
        }, 1000);
    };
}
