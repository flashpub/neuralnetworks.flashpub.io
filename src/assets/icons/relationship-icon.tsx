import * as React from 'react';

import { Color } from 'app/assets';

const iconWrapper = (path: JSX.Element) => (
    <svg
        className="pt-icon"
        version="1.1"
        id="website-assets"
        xmlns="http://www.w3.org/2000/svg"
        x="0px"
        y="0px"
        width="20px"
        height="24px"
        viewBox="0 0 20 20"
        stroke={Color.community}
        strokeWidth="1"
        strokeMiterlimit="10"
    >
        {path}
    </svg>
);

export const positiveLine = iconWrapper(
    <path d="M0,10H20" />
);

export const negativeLine = iconWrapper(
    <path d="M0,10H20M5.33,4.76,9.7,15.24m.6-10.48,4.37,10.48" />
);

export const neutralLine = iconWrapper(
    <path d="M0,10H6.7m.24-5.07V15.07M13.06,4.93V15.07M13.23,10h6.69" />
);
