import * as React from 'react';
import {
    StyledCampaignResourcesList,
    StyledCampaignResourcesListPublishWrapper,
    StyledCampaignTitle,
    StyledCampaignTitleWrapper
} from 'ui/Campaigns/styles';
import { Dropdown } from 'antd';
import { CaretDownFilled } from '@ant-design/icons';
import { StyledSpacer } from 'ui/elements/styles';
import { Link } from '@reach/router';
import { kebabCase } from 'lodash';
import { useTypedSelector } from 'helpers';

export const CampaignTitle: React.FC = () => {
    const selectedCampaign = useTypedSelector((state) => state.campaigns.data.selected);
    
    const menu = (
        <StyledCampaignResourcesList>
            <StyledCampaignResourcesListPublishWrapper>
                {selectedCampaign &&
                Object.values(selectedCampaign.contentTypes!)
                    .sort((a, b) => a.order - b.order)
                    .map((c) => (
                            <Link
                                key={c.order}
                                onClick={() => null}
                                to={`/publish/${kebabCase(c.contentTypeLabel)}`}
                            >
                                <span>{c.contentTypeLabel}</span>
                            </Link>
                        )
                    )
                }
            </StyledCampaignResourcesListPublishWrapper>
            <p>Resources:</p>
            {selectedCampaign?.resources.map((r) => (
                <p key={r.url}>
                    <a href={r.url} target="_blank" rel="noopener noreferrer">
                        {r.title}
                    </a>
                </p>
            ))}
        </StyledCampaignResourcesList>
    );
  return (
    
      <StyledCampaignTitleWrapper>
          <Dropdown overlay={menu} trigger={['click']}>
              <div>
                  <StyledCampaignTitle
                      iconSide="right"
                      buttonType="link"
                      icon={<CaretDownFilled />}
                      label={selectedCampaign?.title}
                  />
              </div>
          </Dropdown>
          <StyledSpacer height={10} />
      </StyledCampaignTitleWrapper>
  );
};
