import { Empty } from 'antd';
import * as React from 'react';
import { useDispatch } from 'react-redux';
import { capitalize, kebabCase, difference } from 'lodash';
import { RouteComponentProps, navigate } from '@reach/router';

import { useTypedSelector } from 'helpers';
import { SectionTitle, PubRecord, LeaderRecord } from 'ui/Feed';
import * as Campaigns from 'containers/Campaigns';
import {
    StyledSectionWrapper,
    StyledSectionLeaderRecordsWrapper,
    StyledCampaignsSectionWrapper,
    StyledSectionTopRecordsWrapper,
    StyledCampaignWrapper, StyledCampaignImage,
} from 'ui/Feed/styles';

interface Props extends RouteComponentProps {
    type: SectionType;
}

export const Section: React.FC<Props> = ({ type }) => {
    const dispatch = useDispatch();
    const pubs = useTypedSelector((state) => state.feed.data);
    const campaigns = useTypedSelector((state) => state.campaigns.data.campaigns);

    const pubsSortedPerBookmarkedBy = [...pubs].sort((a, b) => b.bookmarkedBy.length - a.bookmarkedBy.length);
    const topPubs = pubsSortedPerBookmarkedBy.slice(0, 3);
    const recentPubs = difference(pubs, topPubs);
    
    
    const renderSection = () => {
        if (type === 'top') {
            return (
                <StyledSectionTopRecordsWrapper noData={topPubs.length === 0}>
                    {pubs.length > 0
                        ? topPubs.map((pub) => (
                            <PubRecord key={pub.id} pub={pub} />
                        )) : <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
                    }
                </StyledSectionTopRecordsWrapper>
            );
        }
        if (type === 'recent' && recentPubs.length > 0) {
            return (
                <StyledSectionTopRecordsWrapper>
                    {recentPubs.map((pub) => (
                        <PubRecord key={pub.id} pub={pub} />
                    ))}
                </StyledSectionTopRecordsWrapper>
            );
        }
        if (type === 'leaders') {
            return (
                <StyledSectionLeaderRecordsWrapper>
                    <LeaderRecord pubs={pubsSortedPerBookmarkedBy} />
                </StyledSectionLeaderRecordsWrapper>
            );
        }
        if (type === 'campaigns') {
            const handleCampaignSelect = (c: CampaignDefinition) => {
                dispatch(Campaigns.select(c));
                navigate(`/campaigns/${kebabCase(c.title)}`, { state: { campaignType: c.campaignType }});
            };

            return (
                <StyledCampaignsSectionWrapper>
                    {campaigns?.map((c) => (
                        <StyledCampaignWrapper key={c.id} onClick={() => handleCampaignSelect(c)}>
                            <div>{c.title}</div>
                            <StyledCampaignImage src={c.image} />
                        </StyledCampaignWrapper>
                    ))}
                </StyledCampaignsSectionWrapper>
            );
        }
    };

    return (
        <StyledSectionWrapper type={type}>
            <SectionTitle title={capitalize(type)} type={type} pubs={pubsSortedPerBookmarkedBy} />
            {renderSection()}
        </StyledSectionWrapper>
    );
};
