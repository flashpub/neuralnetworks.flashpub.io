import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    lastPath: '',
    currentPath: '',
    penultimatePath: '',
};

export const slice = createSlice({
    name: 'navigation',
    initialState: initialState as typeof initialState,
    reducers: {
        lastPath: (state, action) => {
            state.lastPath = action.payload;
        },
        currentPath: (state, action) => {
            state.currentPath = action.payload;
        },
        penultimatePath: (state, action) => {
            state.penultimatePath = action.payload;
        },
    },
});

export const { lastPath, currentPath, penultimatePath } = slice.actions;

export default slice.reducer;
