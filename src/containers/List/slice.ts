import { createGenericSlice } from 'helpers';

const initialState = {
    data: { list: [], title: 'list' },
    status: { type: 'PENDING', message: null },
};

export const slice = createGenericSlice({
    name: 'list',
    initialState: initialState as GenericState<ListContainerState>,
    reducers: {
        populate: (state, action) => {
            state.data.list = action.payload;
        },
        title: (state, action) => {
            state.data.title = action.payload;
        },
    },
});

export const { populate, title, ...status } = slice.actions;

export default slice.reducer;
