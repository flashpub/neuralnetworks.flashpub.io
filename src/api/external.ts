import { envDetector } from 'helpers';

const ID = 'APP-KOI2PNMKHAWWJR2P';
const URI = envDetector('ORCID');

export const ORCID = `https://orcid.org/oauth/authorize?client_id=${ID}&response_type=code&scope=/authenticate&redirect_uri=${URI}`;
