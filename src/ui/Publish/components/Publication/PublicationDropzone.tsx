import shortid from 'shortid';
import * as React from 'react';
// import Embed from 'react-embed';
import sortBy from 'lodash/sortBy';
import { useDispatch } from 'react-redux';
import { Progress, message, Tooltip } from 'antd';
import styled, { css } from 'styled-components/macro';
import { useDropzone, FileRejection } from 'react-dropzone';
import { DeleteOutlined, FileAddOutlined } from '@ant-design/icons';

import { useTypedSelector } from 'helpers';
import * as Files from 'containers/Files';
import * as Pub from 'containers/Pub';
import { ZoomIn } from 'ui/Overlays';
import { FPInput } from 'ui/elements';
import { StyledFlexWrapper, StyledSpacer } from 'ui/elements/styles';
import { Radius, Color, Font, Spacing } from 'assets/styles';
import { FileSyncOutlined } from '@ant-design/icons/lib';

type Props = {
    label: number;
    hovered: boolean;
    editMode: boolean;
    pub: PubDefinition;
};

export const PublicationDropzone: React.FC<Props> = ({ pub, label, hovered, editMode }) => {
    const dispatch = useDispatch();

    const [isAdded, setIsAdded] = React.useState<boolean>(false);
    // const [isEmbed, setIsEmbed] = React.useState<boolean>(false);
    const [zoomOpen, setZoomOpen] = React.useState<boolean>(false);
    const [figureTitle, setFigureTitle] = React.useState<string>('');

    const uploadStatus = useTypedSelector((state) => state.files.status.type);
    const uploadProgress = useTypedSelector((state) => state.files.data.progress);

    const { getRootProps, getInputProps, isDragAccept, isDragReject, open } = useDropzone({
        noClick: true,
        multiple: false,
        maxSize: 1024000,
        noKeyboard: true,
        accept: 'image/*',
        disabled: !editMode,
        onDropAccepted: (file) => onDropAccepted(file[0]),
        onDropRejected: (reason) => onDropRejected(reason),
    });

    const thisFigure = pub.figures[label];
    const isLoading = uploadStatus === 'LOADING';
    const figureLabel = String.fromCharCode(97 + label);

    React.useEffect(() => {
        if (thisFigure) {
            setIsAdded(true);
            setFigureTitle(thisFigure.label);
        } else {
            setIsAdded(false);
        }
    }, [thisFigure]);

    const onZoomClose = () => setZoomOpen(false);
    const onLabelChange = (value: string) => setFigureTitle(value);
    const onZoomClick = (e: any) => {
        e.stopPropagation();
        setZoomOpen(true);
    };
    const onRemoveClick = (e: any) => {
        e.stopPropagation();
        const filtered: Figure[] = pub.figures.filter((s: Figure) => s.order !== thisFigure.order);
        const newFigures = sortBy(
            filtered.map((s, i) => ({ ...s, order: i })),
            (f) => f.order
        );
        setIsAdded(false);
        setFigureTitle('');
        dispatch(Pub.form.sub({ form: 'draft', field: 'figures', value: newFigures }));
        dispatch(Files.deleteFile(thisFigure.id, pub.id));
    };

    const addFigureTitle = () => {
        const filteredFigures = pub.figures.filter((f) => f.id !== thisFigure.id);
        const updatedFigure = {
            ...thisFigure,
            label: figureTitle,
        };
        const newFigures = sortBy([...filteredFigures, updatedFigure], (f) => f.order);
        dispatch(
            Pub.form.sub({
                form: 'draft',
                field: 'figures',
                value: newFigures,
            })
        );
    };

    const onDropAccepted = (file: File) => {
        const id = shortid();
        const newFile = new File([file], id, { type: file.type });
        const figure = {
            id,
            url: '',
            label: '',
            order: label,
            type: file.type,
        };
        dispatch(Files.uploadFile(newFile, pub.id, figure, thisFigure));
    };

    const onDropRejected = (reason: FileRejection[]) => {
        reason.map((r) =>
            r.errors.map((e) => {
                let error = '';
                switch (e.code) {
                    case 'file-too-large':
                        error = 'File is larger than 1024KB/1MB';
                        break;
                    case 'file-invalid-type':
                        error = 'File is of incorrect type';
                        break;
                    default:
                        error = 'Something went wrong during image upload';
                }
                return message.error(error);
            })
        );
    };

    // const handleVideoEmbed = () => setIsEmbed(true);

    const renderDropzoneContent = () => {
        if (isLoading) {
            return <Progress type="circle" percent={Math.floor(uploadProgress)} strokeColor={Color.success} />;
        }
        if (isDragAccept) {
            return 'Drop figure';
        }
        if (isDragReject) {
            return 'Figure incorrect';
        }
        if (isAdded) {
            return (
                <StyledPreview>
                    {pub.figures.length > 1 && <StyledOrder>{figureLabel}</StyledOrder>}
                    <StyledThumbnail src={thisFigure?.url} added={isAdded} onClick={onZoomClick} />
                    {hovered && editMode && (
                        <StyledPreviewActions>
                            <Tooltip title="replace figure" placement="left">
                                <StyledReplaceFigure onClick={open} />
                            </Tooltip>
                            {pub.figures.length >= 1 && (
                                <Tooltip title="remove figure" placement="left">
                                    <StyledRemoveImage onClick={onRemoveClick} />
                                </Tooltip>
                            )}
                        </StyledPreviewActions>
                    )}
                </StyledPreview>
            );
        }
        // if (isEmbed) {
        //     return <Embed
        //         url='https://www.youtube.com/watch?v=DPE-_3Bj8M0'

        //     />
        // }
        return (
            <div style={{ textAlign: 'center' }}>
                <Tooltip title="add a file" placement="top" mouseEnterDelay={0} mouseLeaveDelay={0}>
                    <StyledAddFigure onClick={open} />
                </Tooltip>
                {/* <Tooltip title="embed a video" placement="right" mouseEnterDelay={0} mouseLeaveDelay={0}>
                    <StyledAddEmbed onClick={handleVideoEmbed} />
                </Tooltip> */}
                <div>*Drag and drop an image or click an icon to upload</div>
                <div>(use PNG for best results)</div>
            </div>
        );
    };

    return (
        <StyledDropzoneWrapper>
            {isAdded && (editMode || figureTitle) ? (
                <div style={{ width: '100%' }}>
                    <StyledFigureTitle
                        counter
                        chars={45}
                        value={figureTitle}
                        disabled={!editMode}
                        onBlur={addFigureTitle}
                        onChange={onLabelChange}
                        placeholder="panel title..."
                    />
                </div>
            ) : (
                <StyledSpacer height={labelHeight} />
            )}

            <StyledDropzone {...getRootProps()} added={isAdded} rejected={isDragReject} accepted={isDragAccept}>
                <input {...getInputProps()} />
                {renderDropzoneContent()}
            </StyledDropzone>

            <ZoomIn open={zoomOpen} onClose={onZoomClose} imgSrc={thisFigure?.url} title={thisFigure?.label} />
        </StyledDropzoneWrapper>
    );
};

const labelHeight = 30;
const StyledFigureTitle = styled(FPInput)<{ value: boolean; disabled: boolean }>`
    margin: 0;
    width: 100%;
    font-size: 16px;
    background-color: #fff;
    height: ${labelHeight}px;
    font-family: ${Font.text};
    border-color: ${({ value }) => (value ? '#fff' : Color.midgrey)};
    &:focus {
        border-color: ${Color.midgrey};
    }
    &:hover {
        border-color: ${({ disabled }) => (disabled ? '#fff' : Color.midgrey)};
    }
`;
const StyledDropzoneWrapper = styled(StyledFlexWrapper)`
    width: 100%;
    height: 400px;
    max-width: 700px;
    position: relative;
`;
const StyledDropzone = styled(StyledFlexWrapper)<any>`
    width: 100%;
    font-weight: 200;
    border-width: 1px;
    position: relative;
    border-style: dashed;
    border-radius: ${Radius};
    height: calc(100% - ${labelHeight}px);
    color: ${({ rejected, accepted }) => (rejected || accepted ? '#fff' : Color.midgrey)};
    border-color: ${({ rejected, added }) => (rejected ? Color.community : added ? 'transparent' : Color.midgrey)};
    background-color: ${({ rejected, accepted }) =>
        rejected ? Color.error : accepted ? Color.success : 'transparent'};
    &:focus {
        outline: none;
    }
`;
const StyledThumbnail = styled.img<{ added: boolean }>`
    margin: 0;
    width: 100%;
    height: 100%;
    display: block;
    cursor: ${({ added }) => (added ? 'zoom-in' : 'default')};
`;
const StyledPreview = styled.div`
    height: 100%;
    overflow: hidden;
    box-sizing: border-box;
`;
const StyledOrder = styled.div`
    left: 2px;
    top: -28px;
    width: 24px;
    height: 24px;
    display: flex;
    font-size: 24px;
    position: absolute;
    align-items: center;
    color: ${Color.grey};
    padding: ${Spacing._4};
    background-color: #fff;
    justify-content: center;
    font-family: ${Font.mono};
    border-bottom-right-radius: ${Radius};
    -moz-border-radius-bottomright: ${Radius};
`;
const StyledPreviewActions = styled.div`
    top: 50%;
    left: 10px;
    display: flex;
    position: absolute;
    align-items: center;
    flex-direction: column;
    justify-content: center;
    transform: translateY(-50%);
`;
const StyledActionIcon = css`
    font-size: 20px;
    border-radius: 100%;
    padding: ${Spacing._8};
    background-color: #fff;
    margin: ${Spacing._8} 0;
    color: ${Color.community};
    box-shadow: 0px 0px 10px 0px ${Color.community_t75};
`;
const StyledRemoveImage = styled(DeleteOutlined)`
    ${StyledActionIcon};
`;
const StyledAddFigure = styled(FileAddOutlined)`
    ${StyledActionIcon};
    font-size: 26px;
`;
const StyledReplaceFigure = styled(FileSyncOutlined)`
    ${StyledActionIcon};
    font-size: 26px;
`;
// const StyledAddEmbed = styled(PlayCircleOutlined)`
//     && {
//         ${StyledActionIcon};
//         font-size: 26px;
//         margin-left: ${Spacing._12};
//     }
// `;
