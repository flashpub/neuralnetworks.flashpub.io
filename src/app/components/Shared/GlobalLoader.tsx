import * as React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Spinner as UISpinner } from '@blueprintjs/core';

import { Status } from 'app/utils';
import { RootState } from 'app/store';
import { ReduxProps } from 'app/types';
import { Color, Font, Spacing } from 'app/assets';

import { AccessStep } from 'access/utils';


const mapState = (state: RootState) => ({
    accessStep: state.access.step,
    slogans: state.app.community && state.app.community.loadingTaglines,
});

type LoaderProps = {
    status: Status;
    orcid?: boolean;
}
type Props = ReduxProps<typeof mapState, any> & LoaderProps;


const GlobalLoader: React.FC<Props> = (
    { accessStep, status, orcid, slogans }
) => {
    const randomQuote = slogans && slogans[Math.floor(Math.random() * slogans.length)];
    return orcid || status
        ? (
            <Overlay>
                <Wrapper>
                    <Slogan>{(accessStep === AccessStep.ORCID || orcid) ? 'Connecting you to ORCID' : randomQuote}</Slogan>
                    <Spinner/>
                </Wrapper>
            </Overlay>
        ) : null;
}

const Overlay = styled.div`
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 1000;
    position: absolute;
    background-color: rgba(255,255,255,0.9);
`;
const Wrapper = styled.div`
    top: 50%;
    left: 50%;
    position: absolute;
    transform: translate(-50%, -50%);
`;
const Slogan = styled.div`
    font-size: 18px;
    font-weight: 200;
    width: fit-content;
    color: ${Color.grey};
    margin: auto auto ${Spacing.XXL};
    font-family: ${Font.pub_title};
    text-align: center;
`;
const Spinner = styled(UISpinner)` && {
    svg {
        width: 100px;
        height: 100px;
        path.bp3-spinner-head {
            stroke: ${Color.community};
        }
    }
}`;

export default connect(mapState, null)(GlobalLoader);
