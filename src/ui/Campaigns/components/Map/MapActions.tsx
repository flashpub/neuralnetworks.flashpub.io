import * as React from 'react';
import {
    StyledLegend,
    StyledLegendPopover,
    StyledMapSearch,
    StyledMapSearchWrapper,
} from 'ui/Campaigns/styles';

type Props = {
    search: string;
    onSearch(value: string): void;
    onOpenPublish(c: CommunityContentType): void;
};

export const MapActions: React.FC<Props> = ({ search, onSearch }) => {
    
    const legendContent = (
        <div>
            <p>Coming soon</p>
        </div>
    );

    return (
        <>
            <StyledMapSearchWrapper>
                <StyledMapSearch value={search} onChange={onSearch} placeholder="Find a city/county" />
            </StyledMapSearchWrapper>
            <StyledLegendPopover content={legendContent} placement="topLeft">
                <StyledLegend />
            </StyledLegendPopover>
        </>
    );
};
