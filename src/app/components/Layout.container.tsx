import * as React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import camelCase from 'lodash/camelCase';
import { RouteComponentProps, withRouter } from 'react-router-dom';

import { system } from 'app/client';
import { project } from 'app/config';
import { RootState } from 'app/store';
import { Header } from 'app/components';
import * as AppAction from 'app/actions';
import * as Shared from 'app/components/Shared';
import { PubDefinition, ReduxProps } from 'app/types';
import { PortalView, sleep, Status } from 'app/utils';

import { AccessContainer } from 'access/components';
import * as AccessAsyncAction from 'access/async-actions';

import { SetDictionary } from 'dictionaries/types';
import * as DictionariesAction from 'dictionaries/actions';
import * as DictionariesAsyncAction from 'dictionaries/async-actions';

import * as FeedAsyncAction from 'feed/async-actions';

import * as PublicationAction from 'publication/actions';
import * as PublicationAsyncAction from 'publication/async-actions';

import { PublishSummary } from 'publish/components';
import { PublishStep } from 'publish/utils';
import * as PublishAction from 'publish/actions';

import { auth } from 'firebase-config';
import { OpenSesame } from './OpenSesame';


const mapDispatch = {
    setAuth: AppAction.SET_AUTH.use,
    setAppStatus: AppAction.SET_APP_STATUS.use,
    setPortalView: AppAction.SET_PORTAL_VIEW.use,
    setSystemGates: AppAction.SET_SYSTEM_GATES.use,
    setPublishStep: PublishAction.SET_PUBLISH_STEP.use,
    setCurrentPub: PublicationAction.GET_PUBLICATION.use,
    setCurrentDictionary: DictionariesAction.SET_CURRENT_DICTIONARY.use,
    setPickedPub: PublicationAction.SET_PICKED_PUBLICATION.use,
    setContentType: PublishAction.SET_CONTENT_TYPE.use,
    setSesameOpen: AppAction.SET_SESAME_OPEN.use,

    getUser: AccessAsyncAction.getUser,
    getPubsPerDictionary: FeedAsyncAction.get,
    getPublication: PublicationAsyncAction.get,
    getDictionariesOrderedBy: DictionariesAsyncAction.get,
};

const mapState = (state: RootState) => ({
    gates: state.app.gates,
    user: state.access.user,
    isAuth: state.app.isAuth,
    portalView: state.app.view,
    appStatus: state.app.status,
    sesameOpen: state.app.sesameOpen,
    pubStatus: state.publication.status,
    pubEditStatus: state.publication.editStatus,
    publishStatus: state.publish.status,
    current: state.dictionaries.current,
    dictionaries: state.dictionaries.list,
    publishStep: state.publish.step,
    community: state.app.community,
    campaign: state.campaigns.current,
    campaigns: state.campaigns.list,
});
type Props = ReduxProps<typeof mapState, typeof mapDispatch> & RouteComponentProps;

class LayoutContainer extends React.Component<Props> {
    navigationProxy = () => {
        const pathname = this.props.location.pathname;
        if (pathname && pathname.includes('pub')) {
            const id = pathname.split('_')[1];
            const fetchId = pathname.split('/pub/')[1];
            if (fetchId) {
                this.props.history.push(fetchId);
                this.props.getPublication(fetchId);
                this.props.setPickedPub({ id });
                this.props.setPortalView({ view: PortalView.PUBLICATION });
                localStorage.setItem('fetchId', fetchId);
            }
        }
        if (pathname && pathname.includes('feed')) {
            const dictionaryName = pathname.split('/feed/')[1];
            const dictionaries = this.props.dictionaries;
            if (dictionaries) {
                const dictionary = dictionaries && dictionaries.filter(d => d.name === dictionaryName)[0];
                const id = dictionary && dictionary.id;
                id && this.handleSetCurrentDictionary({
                    id: id,
                    name: dictionaryName,
                });
            }
        }
        if (pathname && pathname.includes('publish')) {
            let contentTypeLabel = pathname.split('/publish/')[1];
            const community = this.props.community;
            if (contentTypeLabel) {
                if (contentTypeLabel.includes(('_'))) { contentTypeLabel = camelCase(contentTypeLabel) }
                if (community) {
                    this.props.setContentType(community.contentTypes && community.contentTypes[contentTypeLabel]);
                    this.props.setPortalView({ view: PortalView.PUBLISH });
                }
            } else {
                console.log("the number you're trying to reach is currently unavailable");
            }
        }
    };
    componentDidMount(): void | Promise<void> {
        Promise.resolve(this.props.getDictionariesOrderedBy('pubCount'))
            .then(() => this.handleAutoLogin())
            .then(() => system.get().then(doc => this.props.setSystemGates(doc)))
            .then(() => this.handleSetCurrentDictionary({
                id: project.id,
                name: project.name,
            }))
            .then(() => this.navigationProxy())
            .then(() => this.getPubsPerDictionary());
    }

    componentDidUpdate(prevProps: Readonly<Props>): void {
        if (prevProps.current && (prevProps.current !== this.props.current)) {
            this.getPubsPerDictionary();
        }
    }

    getPubsPerDictionary = () => {
        const current = this.props.current;
        const dictionaryId = (current && current.id) || project.id;

        return this.props.getPubsPerDictionary(dictionaryId);
    };

    handleSetPortalView = (view: PortalView) => this.props.setPortalView({ view });
    handleSetPublishStep = (step: PublishStep | null) => this.props.setPublishStep({ step });
    handleSetCurrentPub = (pub: PubDefinition | null) => this.props.setCurrentPub(pub!);
    handleSetCurrentDictionary = (current: SetDictionary | null) => this.props.setCurrentDictionary({ current: current! });
    handleAutoLogin = () => {
        auth.onAuthStateChanged(user => {
            if (user && !this.props.isAuth) {
                this.props.setSesameOpen(true);
                this.props.getUser(user.uid);
            } else {
                sleep(1000).then(() => this.props.setAppStatus({ status: Status.PENDING }))
            }}
        );
    };

    render() {
        const status = this.props.appStatus === Status.LOADING
            || this.props.pubStatus === Status.LOADING
            || this.props.publishStatus === Status.LOADING
            || this.props.pubEditStatus === Status.LOADING;

        return (
            <main>
                <Shared.Alerts />
                <Shared.GlobalLoader status={status} />
                {!this.props.user && !this.props.sesameOpen && project.name === 'surgery'
                    ? !status && <OpenSesame />
                    : <>
                        <Header
                            user={this.props.user}
                            gates={this.props.gates}
                            isAuth={this.props.isAuth}
                            community={this.props.community}
                            campaign={this.props.campaign}
                            campaigns={this.props.campaigns}
                            setCurrentDictionary={this.handleSetCurrentDictionary}
                            publishStep={this.props.publishStep}
                            setCurrentPub={this.handleSetCurrentPub}
                            setPortalView={this.handleSetPortalView}
                            setContentType={this.props.setContentType}
                            setPublishStep={this.handleSetPublishStep}
                        />
                        <Content id="content">
                            <aside>
                                <AccessContainer />
                                <Shared.ConfirmationDialog
                                    onClose={() => this.props.setPortalView({ view: PortalView.DEFAULT })}
                                    onConfirm={() => this.props.setPortalView({ view: PortalView.ACCESS })}
                                    portalView={this.props.portalView}
                                    cancelButtonText={"Cancel"}
                                    text={"This functionality is reserved only for our users, do you want to Login or Register?"}
                                />
                                <PublishSummary />
                            </aside>
                            <Shared.Switch />
                        </Content>
                    </>
                }
            </main>
        );
    }
}

const Content = styled.div`
    margin: 0 auto;
    min-width: 320px;
    max-width: 1100px;
    padding: 0 20px 20px;
    height: fit-content;
`;

// @ts-ignore
export default withRouter(connect(mapState, mapDispatch)(LayoutContainer));
