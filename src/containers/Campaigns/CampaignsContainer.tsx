import * as React from 'react';
import { useDispatch } from 'react-redux';

import * as Campaigns from 'containers/Campaigns';

export const CampaignsContainer = () => {
    const dispatch = useDispatch();

    React.useEffect(() => {
        dispatch(Campaigns.getCampaigns());
    }, [dispatch]);

    return null;
};
