import * as React from 'react';
import uuid from 'uuid/v4';
import styled from 'styled-components';
import lowerCase from 'lodash/lowerCase';
import { useSelector } from 'react-redux';
import capitalize from 'lodash/capitalize';
import { Grid as UIGrid, Row as UIRow} from 'react-styled-flexboxgrid';

import { Content } from 'app/types';
import { Spacing } from 'app/assets';
import { RootState } from 'app/store';

import { PublishAction, usePublish } from 'publish';
import { SingleClaim, RelationalClaim } from 'publish/components';
import { Button, Title } from 'publish/components/Elements/';

import { DictionaryDefinition } from 'dictionaries/types';

import { FUIPanel } from 'FUI/components';
import { ConditionsPanel } from './ConditionsPanel';


export const ClaimPanel: React.FC = () => {
    const [{ form }, dispatch] = usePublish();

    const content = useSelector<RootState, Content | null>(state => state.publish.contentType);
    const dictionaries = useSelector<RootState, DictionaryDefinition[] | null>(state => state.dictionaries.list);

    if (content && dictionaries) {
        const { firstItem, secondItem, relationship } = form.claim;
        const fistItemId = firstItem && firstItem.id;
        const secondItemId = (secondItem && secondItem.id) || '';
        const isSingle = content.claim.type === 'single';

        const getItem = (item: string) => {
            const getFirstItem = dictionaries.filter(d => d.id === fistItemId)[0];
            const getSecondItem = dictionaries.filter(d => d.id === secondItemId)[0];

            const name = item === 'first'
                ? (getFirstItem ? getFirstItem.name : firstItem.name)
                : (getSecondItem ? getSecondItem.name : secondItem && secondItem.name);

            const longName = item === 'first'
                ? (getFirstItem ? getFirstItem.longName : firstItem.name)
                : (getSecondItem ? getSecondItem.longName : secondItem && secondItem.name);

            return { name, longName };
        };

        const renderPubTitle = !secondItem
            ? `${capitalize(content.contentTypeLabel)} of ${getItem('first').longName}`
            : `${getItem('first').longName} ${lowerCase(relationship)} ${getItem('second').longName}`;

        const toNextStep = () => {
            dispatch(PublishAction.SetPublishPanel({ order: 2, title: 'Publication' }));
            dispatch(PublishAction.SetPublishFormValue({ name: 'id', value: uuid() }));
            dispatch(PublishAction.SetPublishFormValue({ name: 'claim.id', value: uuid() }));
            dispatch(PublishAction.SetPublishFormValue({ name: 'title', value: renderPubTitle }));
            localStorage.setItem('title', renderPubTitle);
        };

        const isButtonDisabled = () => {
            if (isSingle && !fistItemId) return true;
            return (!isSingle && (!fistItemId || !secondItemId || !relationship));
        };

        // const previousPanel = () => {
        //     const panel = panelStack[panelStack.length - 2];
        //     if (panel && panel.order === 0) return panelStack[0];
        //     return panel;
        // };

        return (
            <FUIPanel
                title="Claim"
            >
                <Title />
                <Grid>
                    <UIRow center="xs">
                        <SingleClaim />
                        {!isSingle && <RelationalClaim />}
                    </UIRow>
                </Grid>
                <Grid>
                    <UIRow center="xs">
                        <ConditionsPanel disabled={isButtonDisabled()} />
                    </UIRow>
                </Grid>
                <Button
                    type="button"
                    text="Next Step"
                    buttonType="text"
                    onClick={toNextStep}
                    disabled={isButtonDisabled()}
                />
            </FUIPanel>
        );
    } return null;
};


const Grid = styled(UIGrid)` && {
    width: 100%;
    max-width: 800px;
    padding: 0 ${Spacing.M};
}`;
