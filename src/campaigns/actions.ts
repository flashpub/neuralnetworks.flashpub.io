import { action } from 'app/utils';


export const CAMPAIGN_OPERATION_FAIL = action('campaigns/CAMPAIGN_OPERATION_FAIL');
export const CAMPAIGN_OPERATION_START = action('campaigns/CAMPAIGN_OPERATION_START');
export const CAMPAIGN_OPERATION_SUCCESS = action('campaigns/CAMPAIGN_OPERATION_SUCCESS');

export const SET_CAMPAIGNS = action<Array<any>>('campaigns/SET_CAMPAIGNS');
export const SET_CURRENT_CAMPAIGN = action<{ current: any }>('campaigns/SET_CURRENT_CAMPAIGN');
