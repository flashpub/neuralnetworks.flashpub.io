import styled from 'styled-components';

import { Color, Media, Spacing } from 'app/assets';

import { FUIButton } from 'FUI/elements';


export const Button = styled(FUIButton)` && {
    display: flex;
    margin: 0 auto;
    font-size: 24px;
}`;
export const Title = styled.div`
    font-size: 30px;
    font-weight: 300;
    color: ${Color.community};
    margin-bottom: ${Spacing.XXXL};
`;
export const Term = styled.div<{ single?: boolean }>`
    width: 100%;
    margin-top: 24px;
    position: relative;
    display: inline-block;
    margin-bottom: ${Spacing.M};
    
    ${Media.desktop} {
        text-align: start;
        width: ${props => props.single ? '450px' : '290px'};
    }
`;
export const Label = styled.div`
    left: 50%;
    top: -24px;
    width: 100%;
    font-size: 12px;
    max-width: 450px;
    position: absolute;
    text-align: center;
    color: ${Color.grey};
    background-color: #fff;
    transform: translateX(-50%);
`;
