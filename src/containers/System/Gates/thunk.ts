import { Dispatch } from 'redux';

import * as API from 'api';
import { AppThunk } from 'store';
import * as Gates from 'containers/System/Gates';

export function populateSystemGates(): AppThunk {
    return async (dispatch: Dispatch) => {
        dispatch(Gates.status.loading());
        try {
            const resp = await API.system.gates();
            if (resp.exists) {
                dispatch(Gates.populate(resp.data()));
                dispatch(Gates.status.success());
            }
        } catch (err) {
            dispatch(Gates.status.failure(err.message));
        }
    };
}
