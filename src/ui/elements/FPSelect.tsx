import { Select } from 'antd';
import find from 'lodash/find';
import * as React from 'react';
import concat from 'lodash/concat';
import filter from 'lodash/filter';
import { useDebounce } from 'react-use';
import kebabCase from 'lodash/kebabCase';
import startCase from 'lodash/startCase';
import styled from 'styled-components/macro';
import differenceBy from 'lodash/differenceBy';

import { FPButton } from 'ui/elements';
import { Color, Font, Radius, Spacing } from 'assets/styles';

interface SelectProps {
    options: Option[];

    field?: string;
    loading?: boolean;
    required?: boolean;
    creatable?: boolean;
    searchable?: boolean;
    placeholder?: string;
    relationship?: boolean;
    mode?: 'multiple' | 'tags';
    defaultValue?: any;

    onBlur?(): void;
    onFocus?(): void;
    onCreate?(value: any, field?: string): void;
    onChange?(value: any, field?: string): void;
    onSearch?(value: string, field?: string): void;
}

export const FPSelect: React.FC<SelectProps> = (props) => {
    const {
        mode,
        field,
        loading,
        options,
        onChange,
        onSearch,
        onCreate,
        relationship,
        required = false,
        creatable = false,
        searchable = false,
        placeholder = 'placeholder',
        defaultValue = { value: '', key: '', title: '' },
    } = props;

    const [search, setSearch] = React.useState('');
    const [selected, setSelected] = React.useState(defaultValue);
    const [,] = useDebounce(() => onSearch && onSearch(search, field), 300, [search]);

    const parseToUpperFirst = (v: string) => startCase(v);

    const toOption = (o: Option) => ({
        value: o.value,
        key: o.key,
        title: o.title,
    });

    const handleOnChange = (value: any, option: any) => {
        if (onChange) {
            if (mode) {
                const options = option.map((o: Option) => toOption(o));
                setSelected(options);
                return onChange(options, field);
            } else {
                setSelected(toOption(option));
                return onChange(toOption(option), field);
            }
        }
        setSearch('');
    };
    const handleOnSearch = (search: string) => setSearch(search);
    const handleOnCreate = () => {
        const searchOption = {
            key: kebabCase(search),
            value: kebabCase(search),
            title: parseToUpperFirst(search),
        };
        if (onCreate) {
            if (mode && Array.isArray(selected)) {
                setSelected((prevState: Option[]) => [...prevState, searchOption]);
                onCreate([...selected, searchOption], field);
            } else {
                setSelected(searchOption);
                onCreate(searchOption, field);
            }
        }
        setSearch('');
    };

    const renderNotFound =
        creatable && search ? (
            <StyledCreateTermButton
                block
                buttonType="primary"
                onClick={handleOnCreate}
                label={`Create "${parseToUpperFirst(search)}"`}
            />
        ) : (
            <>Not found</>
        );

    // if array, compare the non-existing/newly created option to prop.options, then pull out
    // and concatenate with the prop.options to make sure the new option shows in a dropdown
    const optionsToRender = () => {
        let extendedOptions;
        if (mode && Array.isArray(selected)) {
            extendedOptions = concat(options, differenceBy(selected, options, 'key'));
        } else {
            const termExists = find(options, (d) => d.key === selected.key);
            extendedOptions = !termExists ? [...options, selected] : options;
        }
        return filter(extendedOptions, (o) => o.title !== '');
    };

    const renderInitialValue = mode && Array.isArray(selected) ? selected.map((v: Option) => v.value) : selected.value;

    return (
        <InputWrapper required={required && !selected}>
            <StyledSelect
                mode={mode}
                size="large"
                loading={loading}
                showSearch={searchable}
                onChange={handleOnChange}
                onSearch={handleOnSearch}
                placeholder={placeholder}
                notFoundContent={renderNotFound}
                value={renderInitialValue || undefined}
            >
                {optionsToRender().map((o) => (
                    <StyledOption key={relationship ? `${o.key}_${o.value}` : o.key} value={o.value} title={o.title}>
                        <StyledLongName>{o.title}</StyledLongName>
                        <StyledName>{relationship ? o.key : o.value}</StyledName>
                    </StyledOption>
                ))}
            </StyledSelect>
        </InputWrapper>
    );
};

const InputWrapper = styled.div<{ required?: boolean }>`
    width: 100%;
    position: relative;
    &::before {
        top: -4px;
        left: 2px;
        z-index: 1;
        font-size: 16px;
        position: absolute;
        color: ${Color.error};
        content: ${({ required }) => (required ? "'*'" : "''")};
    }
`;
export const StyledSelect = styled(Select)`
    width: 100%;
    border-radius: ${Radius};
    border: 1px solid ${Color.midgrey};
    background-color: ${Color.notwhite};
    
    .ant-select-selector {
        border: 0 !important;
        border-radius: ${Radius} !important;
        background-color: transparent !important;
    }
`;
export const StyledOption = styled(Select.Option)`
    width: 100%;
    display: flex;
    cursor: pointer;
    align-items: center;
    flex-direction: column;
    padding: ${Spacing._4} 0;
    font-family: ${Font.text};
    &:hover {
        background-color: ${Color.lightgrey};
    }
`;
const StyledLongName = styled.div`
    font-weight: 400;
    font-family: ${Font.text};
`;
const StyledName = styled.div`
    height: 18px;
    font-size: 12px;
    color: ${Color.community};
    font-family: ${Font.mono};
`;
const StyledCreateTermButton = styled(FPButton)`
    font-family: ${Font.text};
`;
