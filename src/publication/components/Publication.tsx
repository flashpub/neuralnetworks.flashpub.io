import * as React from 'react';
import dayjs from 'dayjs';
import Slider from 'react-slick';
import styled from 'styled-components';
import lowerCase from 'lodash/lowerCase';
import { Col, Grid, Row } from 'react-styled-flexboxgrid';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Popover, Position, Tooltip } from '@blueprintjs/core';
import { PopoverInteractionKind } from '@blueprintjs/core/lib/esm/components/popover/popover';

import store from 'app/store';
import { project } from 'app/config';
import * as AppAction from 'app/actions';
import { PortalView, Status, useWindowWidth } from 'app/utils';
import { Color, Font, Media, negativeLine, neutralLine, positiveLine, Radius, Spacing } from 'app/assets';
import { AlertType, Author, ClaimDefinition, PubDefinition, Asset, Assets } from 'app/types';

import { UserDefinition } from 'access/types';

import { CarouselArrow, Metadata } from 'app/components/Shared';

import { DeadlineCounter, PublicationActions, PublicationMetadata, ReviewsPanel, PublicationEditBar } from 'publication/components';

import { SetDictionary } from 'dictionaries/types';
import { FUIClaimItem } from '../../FUI/elements';
import { Description } from '../../publish/components';

type Props = {
    editMode: boolean,
    isAuth: boolean,
    assets: Assets
    pub: PubDefinition | null;
    publicationStatus: Status;
    current: SetDictionary | null;
    user: Optional<UserDefinition> | null;
    claims: Array<ClaimDefinition> | null;
    portalView: PortalView;

    updateBookmark(update: any): void,
    updateQuickReview(update: any): void,
    updateAcceptButton(update: any): void,
    setCurrent(current: SetDictionary | null): void,
    setCurrentPub(pub: PubDefinition | null): void,
    setPortalView(view: PortalView): void,
    setEditMode(): void,
    submitEditedPub(pub: PubDefinition, editedElements: Array<string>): void,
    addAsset(type: string, asset: Asset): void,
    removeAsset(type: string, id: string): void
} & RouteComponentProps;

type PubState = {
    description: {
        value: string,
        hadFocus: boolean,
        hadChanged: boolean
    },
    assetsChanged: {
        code: boolean,
        datasets: boolean,
        protocols: boolean,
        references: boolean
    }
}

const Publication: React.FC<Props> = (
    {
        pub, history, setCurrent, user, updateBookmark, updateQuickReview, updateAcceptButton,
        current, claims, isAuth, editMode, publicationStatus, setCurrentPub, setPortalView, setEditMode,
        submitEditedPub, addAsset, removeAsset, assets, portalView
    }
) => {
    const isMobile = useWindowWidth() <= 1024;
    const [description, editDescription] = React.useState<PubState["description"]>({
        value: "",
        hadFocus: false,
        hadChanged: false
    });

    const [assetsChanged, setAssetsChanged] = React.useState<PubState["assetsChanged"]>({
        code: false,
        datasets: false,
        protocols: false,
        references: false
    });

    if (pub) {
        const handleOnClaimClick = (c: string) => {
            console.log(c);
        };

        const saveDescToStorage = (description: string) => {
            localStorage.setItem('description', description);
        }

        const removeAssetsFromStorage = () => {
            localStorage.removeItem('assets');
        }

        const removeDescFromStorage = () => {
            if (editMode) {
                localStorage.removeItem('description');
                editDescription({ ...description, value: pub.description });
            }
        }

        const handleOnAuthorClick = (orcid: string) => {
            const url = `https://orcid.org/${orcid}`;
            if (!orcid) {
                store.dispatch(AppAction.ADD_ALERT.use({
                    type: AlertType.INFO,
                    message: "This author's ORCID profile isn't connected to flashPub or the author doesn't have an ORCID profile",
                }));
            }
            orcid && window.open(url, "_blank");
        };

        const renderClaimItems = () => {
            if (claims) {
                return pub.claim.terms.secondItem
                    ? (
                        <div onClick={(e) => handleOnClaimClick(pub && pub.claim.terms.firstItem)}>
                            <FUIClaimItem>{pub.claim.terms.firstItem}</FUIClaimItem>
                            <Tooltip content={lowerCase(pub.claim.terms.relationship)} position={Position.TOP}>
                                {pub.claim.terms.type === 'positive'
                                    ? positiveLine
                                    : pub.claim.terms.type === 'neutral'
                                        ? neutralLine
                                        : negativeLine
                                }
                            </Tooltip>
                            <FUIClaimItem>{pub.claim.terms.secondItem}</FUIClaimItem>
                        </div>
                    ) : <FUIClaimItem onClick={(e) => handleOnClaimClick(pub.claim.terms.firstItem)}>{pub.claim.terms.firstItem}</FUIClaimItem>;
            } return null;
        };

        const handleReturnToList = () => {
            if ((current && current.name) === project.name) {
                setCurrent(current);
                setCurrentPub(null);
                history.push(`/`);
            } else {
                setCurrent(current);
                setCurrentPub(null);
                history.push(`/feed/${(current && current.name) || ''}`);
            }
        };

        const printAuthors = pub && pub.contributors.map((c: Author, i) => <span onClick={() => handleOnAuthorClick(c.orcid)} key={i}>, {c.name}</span>);

        const isOverDeadline = dayjs(pub && pub.review.deadline) <= dayjs();

        const pubStatus = () => {
            if (isOverDeadline) return pub && pub.review.isPubAccepted ? 'accepted' : 'awaiting';
            return pub && pub.review.isPubAccepted ? 'countdown' : 'review';
        };

        const printReview = {
            alertText: pubStatus() === 'accepted' ? null : 'IN REVIEW: ',
            counter: () => {
                switch (pubStatus()) {
                    case 'accepted':
                        return null;
                    case 'awaiting':
                        return 'Needs additional accepts';
                    default:
                        return <DeadlineCounter pub={pub} />;
                }
            }
        };

        const printConditions = () => {
            const conditions = pub.claim.conditions;

            if (conditions.length === 0) return <ConditionsList>No conditions</ConditionsList>;

            return (
                <ConditionsList>
                    <p>Claim conditions</p>
                    {conditions.map((con, i) =>
                        <ListItem key={i}>
                            <ListItemKey>{lowerCase(con.name)}: </ListItemKey>
                            <ListItemValue>{con.type === 'city' ? `${con.value.value.city}, ${con.value.value.state}` : con.value}</ListItemValue>
                        </ListItem>
                    )}
                </ConditionsList>
            );
        };

        const isPreviousPathLibrary = () => {
            const path = localStorage.getItem('penultimatePath');
            return path && path.includes('library');
        };

        const handleReturn = () => {
            const toLibrary = () => {
                history.push('/library');
                setPortalView(PortalView.LIBRARY);
            };
            return isPreviousPathLibrary()
                ? toLibrary()
                : publicationStatus !== Status.LOADING && handleReturnToList();
        };

        const COMPONENT_ReviewsPanel = (
            <ReviewsPanel
                pub={pub}
                user={user}
                isAuth={isAuth}
                author={pub.author}
                publicationStatus={publicationStatus}
                updateQuickReview={updateQuickReview}
                updateAcceptButton={updateAcceptButton}
            />
        );

        const COMPONENT_PublicationEditBar = (
            <PublicationEditBar
                pub={pub}
                assetsChanged={assetsChanged}
                description={description}
                assets={assets}
                setAssetsChanged={setAssetsChanged}
                editDescription={editDescription}
                setEditMode={setEditMode}
                removeAssetsFromStorage={removeAssetsFromStorage}
                removeDescFromStorage={removeDescFromStorage}
                submitEditedPub={submitEditedPub}
            />
        );

        const COMPONENT_Carousel = (
            <Carousel>
                <Slider
                    dots={true}
                    swipe={isMobile}
                    prevArrow={<CarouselArrow type="left" />}
                    nextArrow={<CarouselArrow type="right" />}
                    slidesToShow={1}
                    slidesToScroll={1}
                >
                    {pub && pub.figures.map(figure =>
                        <Slide key={figure.url}>
                            <SlideWrapper isMobile={isMobile}>
                                <FigureTitle>{figure.label}</FigureTitle>
                                {pub.figures.length > 1 && <Order isMobile={isMobile}>{String.fromCharCode(97 + figure.order)}</Order>}
                                <img src={figure.url} alt={figure.label} />
                            </SlideWrapper>
                        </Slide>
                    )}
                </Slider>
            </Carousel>
        );

        return (
            <PublicationWrapper>
                <Grid fluid>
                    <Row center="xs">
                        <Col xs md={3} />
                        <Col xs={12} md={6}>
                            <PublicationPage>
                                <PubNavigation>
                                    <Return onClick={handleReturn}>Return to the list</Return>
                                    <Previous>{isMobile ? '<' : '< Previous pub'}</Previous>
                                    <Next>{isMobile ? '>' : 'Next pub >'}</Next>
                                </PubNavigation>
                                {pub && (
                                    <>
                                        {pub.review.isPubRetracted && <Retracted>Retracted</Retracted>}
                                        {!pub.review.isPubAccepted && !pub.review.isPubRetracted && <ReviewAlert>
                                            {printReview.alertText}
                                            {printReview.counter()}
                                        </ReviewAlert>}

                                        {!pub.review.isPubAccepted && isMobile && !pub.review.isPubRetracted && COMPONENT_ReviewsPanel}

                                        {!pub.contentOrigin.originalContent && <ExternalContent>
                                            Originally published in {pub && pub.contentOrigin.source}<br />
                                            <a href={pub && pub.contentOrigin.url} rel="noopener noreferrer" target="_blank">{pub && pub.contentOrigin.url}</a>
                                        </ExternalContent>}
                                        <PubHeader>
                                            <Title>{pub.title}</Title>
                                            <Claim>
                                                <Popover
                                                    hoverOpenDelay={50}
                                                    position={Position.BOTTOM}
                                                    interactionKind={PopoverInteractionKind.HOVER}
                                                >
                                                    {renderClaimItems()}
                                                    {printConditions()}
                                                </Popover>
                                            </Claim>
                                            <Authors>
                                                by <span onClick={() => handleOnAuthorClick(pub.author.orcid)}>{pub.author.name}</span>
                                                {printAuthors}
                                            </Authors>

                                            <PublicationActions
                                                isAuth={isAuth}
                                                author={pub.author}
                                                pubId={pub && pub.id}
                                                userId={user && user.uid}
                                                setEditMode={setEditMode}
                                                setPortalView={setPortalView}
                                                bookmarkedBy={pub.bookmarkedBy}
                                                updateBookmark={updateBookmark}
                                                pubDOI={pub && pub.metadata.doi}
                                                editDescription={editDescription}
                                                setAssetsChanged={setAssetsChanged}
                                                removeDescFromStorage={removeDescFromStorage}
                                                removeAssetsFromStorage={removeAssetsFromStorage}
                                            />
                                        </PubHeader>
                                        {COMPONENT_Carousel}
                                        <Description
                                            counter
                                            required
                                            pub={pub}
                                            chars={3000}
                                            disabled={true}
                                            onChange={value => {
                                                editDescription({ ...description, hadChanged: true, value });
                                                saveDescToStorage(value);
                                            }}
                                        />
                                        {editMode ?
                                            <Metadata
                                                assets={assets}
                                                publication={pub}
                                                editMode={editMode}
                                                addAsset={addAsset}
                                                removeAsset={removeAsset}
                                                assetsChanged={assetsChanged}
                                                setAssetsChanged={setAssetsChanged}
                                            />
                                            : <PublicationMetadata
                                                assets={assets}
                                            />
                                        }
                                    </>
                                )}
                            </PublicationPage>
                        </Col>
                        <Col md={3} style={{ position: 'relative' }}>
                            {!pub.review.isPubAccepted && !isMobile && !pub.review.isPubRetracted && COMPONENT_ReviewsPanel}
                        </Col>
                    </Row>
                </Grid>
                {editMode && COMPONENT_PublicationEditBar}
            </PublicationWrapper>
        );
    } return null;
};

const PublicationWrapper = styled.div`
    left: 0;
    right: 0;
    top: 86px;
    bottom: 0;
    position: absolute;
    color: ${Color.grey};
    
    ${Media.tablet} {
        top: 91px;
    }
`;
const PublicationPage = styled.div`
    padding: 0; 
    width: 100%;
    margin: ${Spacing.ML} auto;
    
    ${Media.tablet} {
        max-width: 800px;
    }
`;
const ReviewAlert = styled.div`
    color: #fff;
    text-align: center;
    padding: ${Spacing.LXL};
    font-family: ${Font.text};
    background-color: ${Color.community};
`;
const Retracted = styled(ReviewAlert)` && {
    background-color: #000;
}`;
const PubHeader = styled.div`
    position: relative;
    padding-top: ${Spacing.XXL};
    
    ${Media.tablet} {
        padding: ${Spacing.XXL} ${Spacing.S} ${Spacing.LXL};
    }
`;
const PubNavigation = styled.div`
    height: 14px;
    font-weight: 300;
    position: relative;
    color: ${Color.midgrey};
    margin-bottom: ${Spacing.XL};
    font-family: ${Font.pub_text};
`;
const Return = styled.div`
    left: 50%;
    position: absolute;
    transform: translateX(-50%);
    :hover {
        cursor: pointer;
        color: ${Color.grey};
    }
`;
const Previous = styled.div`
    position: absolute;
    :hover {
        cursor: not-allowed;
        //color: ${Color.grey};
    }
`;
const Next = styled.div`
    position: absolute;
    right: 0;
    :hover {
        cursor: not-allowed;
        //color: ${Color.grey};
    }
`;
const Title = styled.div`
    display: block;
    font-size: 28px;
    font-weight: 400;
    text-align: center;
    color: ${Color.community};
    font-family: ${Font.pub_title};
    margin: ${Spacing.M} auto ${Spacing.LXL};
    
    ${Media.desktop} {
        box-shadow: none;
        text-align: unset;
        margin: 0 0 ${Spacing.L} 0;
        font-family: ${Font.feed_title};
    }
`;
const ClaimItem = styled.span`
    border: 1px solid;
    color: ${Color.midgrey};
    border-radius: ${Radius};
    padding: ${Spacing.S} ${Spacing.SM};
`;
const Line = styled.hr`
    width: 10px;
    margin-bottom: 2px;
    display: inline-block;
`;
const Claim = styled.div`
    display: block;
    width: fit-content;
    margin: 0 auto;
    padding: ${Spacing.SM};
    font-family: ${Font.mono};

    &:hover ${ClaimItem} {
        cursor: pointer;
        color: ${Color.flashpub};
    }
    
    &:hover ${Line} {
        cursor: pointer;
        color: ${Color.flashpub};
    }
    
    ${Media.desktop} {
        margin: unset;
        padding-left: 0;
        text-align: unset;
        margin-bottom: ${Spacing.L};
    }
`;
const Authors = styled.div`
    width: fit-content;
    font-style: italic;
    margin: ${Spacing.L} auto;
    font-family: ${Font.pub_text};
    
    span:hover {
        cursor: pointer;
        color: ${Color.flashpub};
    }
   
    ${Media.desktop} {
        width: auto;
        margin: unset;
        font-size: 14px;
        overflow: hidden;
        font-style: italic;
        white-space: nowrap;
        color: ${Color.grey};
        display: inline-block;
        text-overflow: ellipsis;
        font-family: ${Font.text};
        padding-top: ${Spacing.S};
        margin-right: ${Spacing.SM};
        padding-right: ${Spacing.S};
    }
`;


const ConditionsList = styled.ul`
    margin: 0;
    list-style: none;
    background: #fff;
    font-weight: 200;
    text-align: center;
    color: ${Color.grey};
    padding: ${Spacing.L};
    border-radius: ${Radius};
    font-family: ${Font.mono};
`;
const ListItem = styled.li`
    margin-bottom: ${Spacing.S};
`;
const ListItemKey = styled.span`
    color: ${Color.community};
`;
const ListItemValue = styled.span`
    font-style: italic;
`;
const ExternalContent = styled.div`
    text-align: center;
    color: ${Color.grey};
    padding: ${Spacing.LXL};
    font-family: ${Font.text};
    background-color: ${Color.lightgrey};
`;

const Carousel = styled.div`
    width: 100%;
    margin-top: ${Spacing.XL};
    .slick-track {
        display: flex ;
        align-items: center;
        justify-content: center; 
    }
`;

const SlideWrapper = styled.div<{ isMobile?: boolean }>`   
    width: auto;
    position: relative;
    height: ${props => props.isMobile ? 'unset' : '400px'};
    overflow: hidden;
`;

const Slide = styled.div`
    display: flex !important;
    flex-wrap: nowrap;
    justify-content: center;
    align-items: center; 
    width: 100%;
    img {
        width: auto;
        height: 100%;
    } 
`;

const Order = styled.div<{ isMobile?: boolean }>`
    font-size: ${props => props.isMobile ? '20px' : '40px'};
    top: 0;
    left: 0;
    width: ${props => props.isMobile ? '20px' : '40px'};
    height: ${props => props.isMobile ? '20px' : '40px'};
    display: flex;
    position: absolute;
    align-items: center;
    color: ${Color.grey};
    padding: ${Spacing.S};
    background-color: #fff;
    justify-content: center;
    font-family: ${Font.mono};
    border-bottom-right-radius: ${Radius};
    -moz-border-radius-bottomright: ${Radius};
`;

const FigureTitle = styled.div`
    text-align: center;
    margin: ${Spacing.S} 0;
    font-family: ${Font.text};
`;

export default withRouter(Publication);
