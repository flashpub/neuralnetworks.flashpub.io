export const Viewport = {
    DESKTOP: 1196,
    TABLET: 576,
};

const customMediaQuery = (maxWidth: number) => `@media (min-width: ${maxWidth}px)`;

export const Media = {
    tablet: customMediaQuery(Viewport.TABLET),
    desktop: customMediaQuery(Viewport.DESKTOP),
};
