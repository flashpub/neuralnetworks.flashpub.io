import { v4 as uuid_v4 } from 'uuid';

const ClaimDataModel: ClaimDefinition = {
    allDictionaryIds: [],
    claimItems: {
        firstItem: {
            id: '',
            name: '',
        },
        secondItem: null,
    },
    id: uuid_v4(),
    linkedPubIds: [],
    relationship: null,
    relationshipType: null,
    isNew: true,
};

export default ClaimDataModel;
