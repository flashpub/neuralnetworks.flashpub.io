import { Empty } from 'antd';
import * as React from 'react';
import { capitalize } from 'lodash';
import { navigate } from '@reach/router';
import { LeftCircleFilled } from '@ant-design/icons';

import { useTypedSelector } from 'helpers';
import { StyledSpacer } from 'ui/elements/styles';
import { StyledListTitle, StyledListWrapper, StyledPubListWrapper } from 'ui/List/styles';
import { PubRecord } from 'ui/Feed';
import { StyledSectionTitleMore, StyledSectionTitleWrapper } from 'ui/Feed/styles';
import { GlobalLoader } from 'ui/Overlays';

type Props = {
    type: ListType;
};

export const List: React.FC<Props> = ({ type }) => {
    const listTitle = useTypedSelector((state) => state.list.data.title);
    
    const renderListTitle = listTitle ? listTitle : capitalize(type);
    const pubList = useTypedSelector((state) => state.list.data.list);
    const pubListStatus = useTypedSelector((state) => state.list.status.type);

    const renderList = pubList.map((p) => <PubRecord key={p.id} pub={p} draft={type === 'drafts'} />);
    
    return (
        <StyledListWrapper>
            <StyledSpacer />
            <StyledSectionTitleWrapper>
                <StyledListTitle>{renderListTitle}</StyledListTitle>
                <StyledSectionTitleMore
                    buttonType="link"
                    label="Back to main"
                    icon={<LeftCircleFilled />}
                    onClick={() => navigate('/')}
                />
            </StyledSectionTitleWrapper>
            <StyledSpacer height={10} />
            <StyledPubListWrapper>
                {pubList.length > 0
                    ? renderList
                    : <Empty
                        image={Empty.PRESENTED_IMAGE_SIMPLE}
                        description={type === 'library' ? 'Bookmark a pub first' : type === 'drafts' ? 'Save a pub first' : 'No Data'}
                    />
                }
                <GlobalLoader loading={pubListStatus === 'LOADING'} />
            </StyledPubListWrapper>
            <StyledSpacer height={80} />
            {pubListStatus === 'LOADING' && <GlobalLoader />}
        </StyledListWrapper>
    );
};
