import validator from 'email-validator';

export const emailValidation = (value: string): string | undefined => {
    let error;
    const isEmailValid = validator.validate(value);
    if (!isEmailValid) { error = 'Invalid email' }
    return error;
};
