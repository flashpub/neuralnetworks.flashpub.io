import dayjs from 'dayjs';
import * as React from 'react';
import { Divider } from 'antd';
import { useDispatch } from 'react-redux';
import { useIntersection } from 'react-use';

import { Color } from 'assets/styles';
import * as Pub from 'containers/Pub';
import { StyledSpacer } from 'ui/elements/styles';
import { FPClaim, FPCollapse, FPPlaceholder } from 'ui/elements';
import {
    StyledDate,
    StyledPublicationSheetWrapper,
    StyledPublicationSheetHeadWrapper,
    StyledPublicationSheetContentWrapper,
    StyledPublicationSheetMetadataWrapper,
} from 'ui/Publish/styles';
import {
    PublicationAuthor,
    PublicationCarousel,
    PublicationDescription,
    PublicationTitle,
    PublicationMetadata,
} from 'ui/Publish';
import Conditions from 'ui/Publish/components/Claim/Conditions';
import { useTypedSelector } from 'helpers';

type Props = {
    pub: PubDefinition;

    editMode?: boolean;
    cb?(show: boolean): void;
};

export const PublicationSheet: React.FC<Props> = ({ pub, editMode = false, cb }) => {
    const dispatch = useDispatch();
    const intersectionRef = React.useRef(null);
    const intersection = useIntersection(intersectionRef, {
        root: null,
        rootMargin: '0px',
        threshold: 0.75,
    });
    const pubStatus = useTypedSelector((state) => state.pub.status.type);
    const isLoading = pubStatus === 'LOADING';
    
    React.useEffect(() => {
        if (!pub.review.isPubAccepted) {
            if (intersection?.intersectionRatio! < 0.75) {
                cb && cb(true);
            } else {
                cb && cb(false);
            }
        }
    }, [intersection, pub.review.isPubAccepted]);

    const handleOnTitleBlur = () => sessionStorage.setItem('storedTitle', pub.title);
    const handleOnTitleChange = (value: string) => dispatch(Pub.form.set({ form: 'draft', field: 'title', value }));
    const handleOnDescriptionBlur = (value: string) =>
        dispatch(dispatch(Pub.form.set({ form: 'draft', field: 'description', value })));
    const handleOnDescriptionChange = (value: string) =>
        dispatch(Pub.form.set({ form: 'draft', field: 'description', value }));

    const renderClaimWithConditions = () => {
        if (pub.claim.conditions.length > 0 && !isLoading) {
            return editMode
                ? <Conditions disabled={!editMode} editMode={editMode} terms={pub.claim.terms} />
                : <FPCollapse conditions={pub.claim.conditions} headerTitle={<FPClaim terms={pub.claim.terms} />} />;
        } else if (isLoading) {
            return <FPPlaceholder height={26} />;
        } else if (pub.claim.conditions.length === 0) {
            return editMode
                ? <Conditions disabled={!editMode} editMode={editMode} terms={pub.claim.terms} />
                : <FPClaim terms={pub.claim.terms} />;
        }
    };
    
    return (
        <StyledPublicationSheetWrapper id="publication_sheet">
            <StyledPublicationSheetHeadWrapper ref={intersectionRef}>
                {((pub.title !== 'Pub title not yet created') || editMode) ? (
                    <PublicationTitle
                        pub={pub}
                        editMode={editMode}
                        onBlur={handleOnTitleBlur}
                        onChange={handleOnTitleChange}
                    />
                ) : (
                    <FPPlaceholder height={54} />
                )}
                <StyledSpacer height={10} />
                {pub.author.id || editMode ? (
                    <PublicationAuthor pub={pub} editMode={editMode} />
                ) : (
                    <FPPlaceholder height={28} />
                )}
                <StyledSpacer height={20} />
                {pub.metadata.fetchId || editMode ? (
                    <StyledDate>{dayjs(pub.metadata.datePublished).format('MMM DD, YYYY')}</StyledDate>
                ) : (
                    <FPPlaceholder height={22} />
                )}
                <StyledSpacer height={20} />
                {renderClaimWithConditions()}
            </StyledPublicationSheetHeadWrapper>
            <StyledSpacer height={20} />
            <StyledPublicationSheetContentWrapper>
                {pub.figures[0] || editMode ? (
                    <PublicationCarousel pub={pub} editMode={editMode} />
                ) : (
                    <FPPlaceholder height={400} />
                )}
                <StyledSpacer height={50} />
                <Divider style={{ margin: 0, background: Color.community_t(0.3) }} />
                <StyledSpacer height={40} id="desc_spacer" />
                {pub.description || editMode ? (
                    <PublicationDescription
                        pub={pub}
                        editMode={editMode}
                        onBlur={handleOnDescriptionBlur}
                        onChange={handleOnDescriptionChange}
                    />
                ) : (
                    <>
                        <StyledSpacer height={20} />
                        <FPPlaceholder height={20} />
                        <StyledSpacer height={4} />
                        <FPPlaceholder height={20} />
                        <StyledSpacer height={4} />
                        <FPPlaceholder height={20} />
                        <StyledSpacer height={4} />
                        <FPPlaceholder height={20} />
                        <StyledSpacer height={4} />
                        <FPPlaceholder height={20} />
                        <StyledSpacer height={4} />
                        <FPPlaceholder height={20} />
                    </>
                )}
            </StyledPublicationSheetContentWrapper>
            <StyledSpacer height={40} />
            <Divider style={{ margin: 0, background: Color.community_t(0.3) }} />
            <StyledSpacer height={40} />
            <StyledPublicationSheetMetadataWrapper>
                <PublicationMetadata pub={pub} editMode={editMode} />
            </StyledPublicationSheetMetadataWrapper>
        </StyledPublicationSheetWrapper>
    );
};
