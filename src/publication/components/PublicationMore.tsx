import React, { Dispatch, SetStateAction } from 'react';
import styled from 'styled-components';
import { Icon, Popover, Menu, Position, MenuItem, Tooltip } from '@blueprintjs/core';

import { Color, Font } from 'app/assets';
import { EditedField } from 'publication/types';

type Props = {
    pubId: string;
    userId: Optional<string> | null;
    authorId: string;
    editDescription: Dispatch<SetStateAction<EditedField>>,
    setEditMode(): void;
    removeDescFromStorage(): void;
    removeAssetsFromStorage(): void,
    setAssetsChanged(assetsChanged: {
        code: boolean,
        datasets: boolean,
        protocols: boolean,
        references: boolean
    }): void;
    pubDOI: string;
};

const PublicationMore: React.FC<Props> = (
    {
        pubId, setEditMode, userId, authorId, removeDescFromStorage, editDescription, setAssetsChanged, removeAssetsFromStorage, pubDOI,
    }
) => {
    const onReportClick = () => {
        const email = `mailto:hello@flashpub.io?Subject=Report%20pub%20${pubId}`;
        window.open(email);
    };

    const onEditClick = () => {
        setEditMode();
        removeDescFromStorage();
        removeAssetsFromStorage();
        editDescription({ value: "", hadChanged: false, hadFocus: false });
        setAssetsChanged({
            code: false,
            datasets: false,
            protocols: false,
            references: false
        });
    };

    const canEdit = () => (authorId === userId);

    const sideMenu = (
        <PubMenu>
            <MenuItem icon="warning-sign" text="report" onClick={onReportClick} />
            {canEdit() && <MenuItem icon="edit" text="edit" onClick={onEditClick} disabled={!canEdit()} />}
        </PubMenu>
    );

    return (
        <MenuWrapper>
            <PubPopover content={sideMenu} position={Position.RIGHT_TOP}>
                <Tooltip content="More" position={Position.RIGHT}>
                    <PubMenuIcon icon="more" iconSize={20} />
                </Tooltip>
            </PubPopover>
        </MenuWrapper>
    );
};

const MenuWrapper = styled.span`
    margin-left: 5px;
`;
const PubMenu = styled(Menu)` && {
    min-width: 50px;
    li {
        margin-bottom: 0;
        color: ${Color.midgrey};
        font-family: ${Font.text};
        :hover {
            color: ${Color.community};
        }
    }
}`;
const PubPopover = styled(Popover)` && {}`;
const PubMenuIcon = styled(Icon)` && {
    cursor: pointer;
    color: ${Color.midgrey};
    :hover {
        color: ${Color.community};
    };
    :focus {
        outline: none;
    };
}`;


export default PublicationMore;
