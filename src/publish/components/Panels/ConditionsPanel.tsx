import * as React from 'react';
import shortid from 'shortid';
import find from 'lodash/find';
import styled from 'styled-components/macro';
import lowerCase from 'lodash/lowerCase';
import camelCase from 'lodash/camelCase';
import { useSelector } from 'react-redux';
import { Icon, Popover, Position, Tooltip } from '@blueprintjs/core';

import { Content } from 'app/types';
import { RootState } from 'app/store';
import { Color, Font, Spacing, Radius } from 'app/assets';

import { usePublish, PublishAction } from 'publish';

import { FUIButton, FUIInput, FUIAsyncField } from 'FUI/elements';

import { cities } from '../../../cities/client';
import { Styles } from 'react-select';


const typeList = ['string', 'date', 'number', 'string[]', 'boolean'];

export const ConditionsPanel: React.FC<{ disabled: boolean }> = ({ disabled }) => {
    const [inputKey, setInputKey] = React.useState<string>('');
    const [inputVal, setInputVal] = React.useState<any>('');
    const [valType, setValType] = React.useState<string | null>(null);

    const [{ form }, dispatch] = usePublish();

    const content = useSelector<RootState, Content | null>(state => state.publish.contentType);
    const isThere = find(content && content.defaultConditions, (c) => lowerCase(c.name) === inputKey);

    const setType = (value: string | null) => {
        let inputType, placeholder;

        switch (value) {
            case 'number':
                inputType = value;
                placeholder = `should be a number`;
                break;
            case 'boolean':
                inputType = null;
                placeholder = `should be a yes/no statement`;
                break;
            case 'string[]':
                inputType = 'text';
                placeholder = `should be a list`;
                break;
            case 'date':
                inputType = 'date';
                placeholder = 'should be a date';
                break;
            default:
                inputType = 'text';
                placeholder = `should be a string`;
        }
        return { inputType, placeholder };
    };

    const setTypeWhenKeyChanges = () => {
        if (isThere) {
            return setValType(isThere.type);
        } else {
            if (inputKey.includes('gender')
                || inputKey.indexOf('gender') > -1
                || inputKey.indexOf('sex') > -1
            ) return setValType(setType('string').inputType);
            return setValType(setType('string').inputType);
        }
    };

    React.useEffect(() => {
        setTypeWhenKeyChanges();
    }, [inputKey]);

    if (content) {
        const conditions: Condition[] = form.claim.conditions;
        const renderConditions = () => {
            if (conditions) {
                return conditions.map((item, i) => {
                    const removeCondition = () => dispatch(PublishAction.CutPublishFormValue({ name: 'claim.conditions', id: item.id }));
                    return (
                        <ConditionBox key={i} id="default">
                            <Key><strong>{lowerCase(item.name)}:</strong></Key>
                            <Value>
                                <em>{item.type === 'city' ? `${item.value.value.city}, ${item.value.value.state}` : item.value}</em>
                                <Tooltip content="Remove condition" position={Position.RIGHT}>
                                    <RemoveIcon onClick={removeCondition} icon="delete" iconSize={18} />
                                </Tooltip>
                            </Value>
                        </ConditionBox>
                    );
                });
            } return null;
        };

        const renderDefaultConditions = content.defaultConditions.map((c, i) => {
            const onClick = () => {
                setValType(c.type);
                setInputVal('');
                setInputKey(lowerCase(c.name));
            };
            return <div onClick={onClick} key={i}>{lowerCase(c.name)}</div>
        });

        const handleAddCondition = () => {
            const condition = {
                id: shortid.generate(),
                name: camelCase(inputKey),
                value: inputVal,
                type: valType,
            };
            dispatch(PublishAction.SetPublishFormValue({ name: 'claim.conditions', value: condition }));
            setInputKey('');
            setInputVal('');
        };

        const onKeyChange = (value: string) => setInputKey(value);
        const onTypeChange = (e: any) => setValType(e.target.value);
        const onValueChange = (value: string) => setInputVal(value);
        const isButtonDisabled = !(inputKey && inputVal);

        const suggestValue = () => {
            return inputVal;
        };

        const onCityNameChange = async (namePrefix: string) => {
            if (namePrefix && namePrefix.length > 2) {
                return (await cities.getForPrefix(namePrefix)).map((x: any) => {
                    return {
                        value: x,
                        label: `${x.city}, ${x.state}`,
                    }
                });
            }
            return [];
        };

        const selectCity = (field: string, value: any) => {
            setInputVal(value);
            setValType('city');
        };

        const renderContent = (
            <ContentWrapper>
                <DefaultConditions>
                    <div>Recommended details:</div>
                    <DefaultList>{renderDefaultConditions}</DefaultList>
                </DefaultConditions>
                <ConditionBox>
                    <Key>
                        <FUIInput
                            type="text"
                            value={inputKey}
                            onChange={onKeyChange}
                            placeholder="detail name"
                        />
                    </Key>
                    {`:`}
                    <Value>
                        {
                            inputKey === 'city' ?
                                <CityPicker
                                    required
                                    styles={styles}
                                    isSearchable={true}
                                    onChange={selectCity}
                                    list={onCityNameChange}
                                    placeholder="enter city name"
                                />
                                :
                                <FUIInput
                                    value={suggestValue()}
                                    onChange={onValueChange}
                                    type={setType(valType).inputType}
                                    placeholder={setType(valType).placeholder.toLowerCase() || `condition value`}
                                />
                        }
                        {inputKey && !isThere && <TypesPicker onChange={onTypeChange} value={valType || 'string'}>
                            {typeList.map(t => <option key={t} value={t}>{t.includes('[]') ? 'list' : t}</option>)}
                        </TypesPicker>}
                    </Value>
                </ConditionBox>
                <AddCondition
                    type="button"
                    text="add condition"
                    buttonType="outlined"
                    disabled={isButtonDisabled}
                    onClick={handleAddCondition}
                />
            </ContentWrapper>
        );

        return (
            <PanelWrapper>
                <Popover
                    autoFocus={true}
                    canEscapeKeyClose={true}
                    position={Position.TOP}
                    content={renderContent}
                >
                    <AddDetail type="button" text="add key detail" buttonType="outlined" disabled={disabled} />
                </Popover>
                <ConditionsList>
                    {renderConditions()}
                </ConditionsList>
            </PanelWrapper>
        );
    } return null;
};

const styles: Styles = {
    valueContainer: () => ({
        flex: 1,
        display: 'flex',
        flexWrap: 'wrap',
        overflow: 'hidden',
        position: 'relative',
        alignItems: 'center',
        boxSizing: 'border-box',
        justifyContent: 'center',
    }),
    control: () => ({
        height: '38px',
        fontSize: '14px',
        border: '1px solid',
        color: Color.midgrey,
        borderRadius: Radius,
    }),
    placeholder: () => ({
        fontWeight: 200,
        fontStyle: 'italic',
        color: Color.midgrey,
    }),
    input: () => ({
        margin: 0,
        padding: 0,
        visibility: 'visible',
        color: 'hsl(0,0%,20%)',
        boxSizing: 'border-box',
    }),
};

const ContentWrapper = styled.div`
    display: flex;
    text-align: center;
    align-items: center;
    flex-direction: column;
    justify-content: center;
    font-family: ${Font.text};
    padding: ${Spacing.XL} ${Spacing.XXXL};
`;
const PanelWrapper = styled.div<{ textAlign?: string }>`
    width: 100%;
    display: flex;
    margin: 0 auto;
    position: relative;
    z-index: 10;
    height: fit-content;
    flex-direction: column;
    text-align: ${({ textAlign }) => textAlign || 'center'};
`;
const RemoveIcon = styled(Icon)`
    cursor: pointer;
    margin-left: 10px;
    visibility: hidden;
    color: ${Color.midgrey};
    :hover {
        color: ${Color.grey};
    }
    :focus {
        outline: none;
    };
`;
const AddDetail = styled(FUIButton) <{ disabled?: any }>` && {
    padding: 0;
    width: 408px;
    height: 30px;
    font-weight: 300;
    margin: ${Spacing.XXL} 0;
    font-family: ${Font.pub_text};
    cursor: ${({ disabled }) => disabled && 'not-allowed'};
    background-color: ${({ disabled }) => disabled && Color.lightgrey};
}`;
const AddCondition = styled(FUIButton) <{ disabled?: any }>` && {
    padding: 0;
    width: 408px;
    height: 30px;
    font-weight: 300;
    margin-top: ${Spacing.L};
    font-family: ${Font.pub_text};
    cursor: ${({ disabled }) => disabled && 'not-allowed'};
    background-color: ${({ disabled }) => disabled && Color.lightgrey};
}`;
const DefaultConditions = styled.div`
    color: ${Color.midgrey};
`;
const DefaultList = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    margin: ${Spacing.L} 0 ${Spacing.XXL};
    
    div {
        cursor: pointer;
        padding: 0 ${Spacing.SM};
        
        &:hover {
            color: ${Color.community};
        }
    }
`;
const ConditionBox = styled.div`
    width: 500px;
    display: flex;
    font-size: 18px;
    font-weight: 200;
    position: relative;
    align-items: center;
    color: ${Color.grey};
    justify-content: center;
    font-family: ${Font.text};
    margin: 0 auto ${Spacing.ML};
    
    &:hover ${RemoveIcon} {
        visibility: visible;
    }
`;
const ConditionsList = styled.div`
    height: fit-content;
    margin-bottom: ${Spacing.XL};
    
    #default {
        height: fit-content
    }
`;
const ConditionInput = styled.div`
    width: 200px;
    height: 38px;
    text-align: end;
    display: inline-block;
    
    input {
        margin: 0;
        height: 38px;
        color: ${Color.grey};
        background-color: #fff;
    }
`;
const Key = styled(ConditionInput)``;
const Value = styled(ConditionInput)` && {
    text-align: start;
    position: relative;
    margin-left: ${Spacing.S};
}`;
const TypesPicker = styled.select`
    top: 0;
    width: 85px;
    right: -84px;
    height: 38px;
    font-size: 14px;
    border: 1px solid;
    position: absolute;
    color: ${Color.midgrey};
    border-radius: ${Radius};
    font-family: ${Font.mono};
    option {
        font-weight: 200;
    }
`;

const CityPicker = styled(FUIAsyncField)` && {
    &.react-select__control {
}`;
