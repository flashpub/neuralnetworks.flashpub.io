import find from 'lodash/find';
import { Dispatch } from 'redux';

import * as API from 'api';
import { AppThunk } from 'store';
import * as Notifications from 'containers/System/Notifications';

export function populateSystemNotifications(userId: string): AppThunk {
    return async (dispatch: Dispatch) => {
        dispatch(Notifications.status.loading());
        try {
            const resp = await API.system.notifications.load();
            if (resp.exists) {
                const findUser = find(resp.data()?.seenBy, (id) => id === userId);
                if (!findUser) {
                    dispatch(Notifications.populate(resp.data()));
                    dispatch(Notifications.status.success());
                }
            }
        } catch (err) {
            dispatch(Notifications.status.failure(err.message));
        }
    };
}
