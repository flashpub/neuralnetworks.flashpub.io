export const Font = Object.freeze({
    title: `'Pridi', serif`,
    text: `'Ubuntu', sans-serif`,
    header: `'Bree Serif', serif`,
    mono: `'Cutive Mono', monospace`,
});
