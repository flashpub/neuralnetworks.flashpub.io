import * as React from 'react';
import { Popover, PopoverInteractionKind, Position } from '@blueprintjs/core';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../app/store';
import styled from 'styled-components';
import { Color, Font, Spacing } from '../../app/assets';
import { campaigns } from '../client';
import * as CampaignsAction from '../actions';
import { FUIButton } from '../../FUI/elements';
import { useHistory } from 'react-router';
import { Placeholder } from '../../app/components/Shared';
import { Status } from '../../app/utils';


type Props = {};

const Campaigns: React.FC<Props> = (props: Props) => {
    const history = useHistory();
    const storeDispatch = useDispatch();
    const [isOpen, setIsOpen] = React.useState(false);
    const campaignsList = useSelector<RootState, any>(state => state.campaigns.list);
    const campaignsStatus = useSelector<RootState, any>(state => state.campaigns.status);
    const appStatus = useSelector<RootState, any>(state => state.app.status);

    React.useEffect(() => {
        !campaignsList && (async () => {
            storeDispatch(CampaignsAction.CAMPAIGN_OPERATION_START.use());
            try {
                const resp = await campaigns.getAll();
                storeDispatch(CampaignsAction.SET_CAMPAIGNS.use(resp));
                storeDispatch(CampaignsAction.CAMPAIGN_OPERATION_SUCCESS.use());
            } catch (err) {
                storeDispatch(CampaignsAction.CAMPAIGN_OPERATION_FAIL.use());
            }
        })();
    }, []);

    const handleDropdown = (state: boolean) => setIsOpen(state);
    const onOpenCampaign = (c: any) => {
        storeDispatch(CampaignsAction.SET_CURRENT_CAMPAIGN.use({ current: c }));
        history.push(`/campaigns/${c.id}`);
    };

    const listExistingCampaigns = campaignsList && campaignsList.map((c: any, i: number) => (
        <MenuItem key={i} onClick={() => onOpenCampaign(c)}>
            {c.title}
        </MenuItem>
    ));
    const campaignTypes = <List>{listExistingCampaigns}</List>;

    return (appStatus === Status.LOADING ||campaignsStatus === Status.LOADING) ? <Placeholder height="36" /> : campaignsList
        ? <CampaignsWrapper>
            {(!campaignsList || campaignsList.length > 1)
                ? <Popover
                    isOpen={isOpen}
                    onInteraction={handleDropdown}
                    popoverClassName="pt-popover-content-sizing"
                    content={campaignTypes}
                    position={Position.BOTTOM}
                    interactionKind={PopoverInteractionKind.HOVER}
                >
                    <CampaignsButton text="Campaigns"/>
                </Popover>
                : <CampaignsButton text={campaignsList[0].title} onClick={() => onOpenCampaign(campaignsList[0])}/>
            }
        </CampaignsWrapper>
        : null;
};

export default Campaigns;

const CampaignsWrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    margin-top: ${Spacing.XL};
    font-family: ${Font.pub_title};
`;
const List = styled.div`
    min-width: 110px;
    text-align: center;
    padding: 0 ${Spacing.M};
`;
const MenuItem = styled.li`
    cursor: pointer;
    font-size: 14px;
    position: relative;
    list-style-type: none;
    color: ${Color.midgrey};
    margin:${Spacing.L};
    font-family: ${Font.text};
    :hover {
        color: ${Color.grey};
    };
`;
const CampaignsButton = styled(FUIButton)` && {
    font-size: 18px;
    padding: ${Spacing.M} ${Spacing.LXL};
}`;
