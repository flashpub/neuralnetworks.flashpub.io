import { uniqWith } from 'lodash';

import { createGenericSlice } from 'helpers';

const initialState = {
    data: {
        campaigns: [],
        selected: null,
        list: [],
    },
    status: { type: 'PENDING', message: null },
};

export const slice = createGenericSlice({
    name: 'campaigns',
    initialState: initialState as GenericState<CampaignsContainerState>,
    reducers: {
        populate: (state, action) => {
            state.data.campaigns = action.payload;
        },
        select: (state, action) => {
            state.data.selected = action.payload;
        },
        list: (state, action) => {
            state.data.list = action.payload.length === 0
                ? action.payload
                : uniqWith([...state.data.list, ...action.payload], (a, b) => a.id === b.id);
        },
    },
});

export const { populate, select, list, ...status } = slice.actions;

export default slice.reducer;
