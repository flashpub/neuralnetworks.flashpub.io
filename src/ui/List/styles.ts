import styled from 'styled-components';

import { Color, Font, MaxWidth, Spacing } from 'assets/styles';
import { StyledFlexWrapper } from 'ui/elements/styles';

export const StyledListWrapper = styled(StyledFlexWrapper)`
    width: 100%;
    margin: 0 auto;
    min-height: 700px;
    align-items: flex-start;
    padding: 0 ${Spacing._24};
    font-family: ${Font.text};
    justify-content: flex-start;
    max-width: calc(${MaxWidth} + ${Spacing._24});
`;
export const StyledListTitle = styled.div`
    font-size: 20px;
    color: ${Color.community};
    font-family: ${Font.title};
    margin-right: ${Spacing._12};
`;
export const StyledPubListWrapper = styled(StyledFlexWrapper)`
    width: 100%;
    min-height: 700px;
    position: relative;
    justify-content: flex-start;
`;
