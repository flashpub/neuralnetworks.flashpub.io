import { Modal } from 'antd';
import * as React from 'react';
import { useDispatch } from 'react-redux';
import { find, capitalize, lowerCase, uniq } from 'lodash';
import { RightCircleOutlined, SaveOutlined } from '@ant-design/icons';
import { RouteComponentProps, useNavigate, useLocation } from '@reach/router';

import { useTypedSelector, matchStepperToUrl } from 'helpers';
import * as Pub from 'containers/Pub';
import { form as ClaimForm } from 'containers/Claim';
import * as Publish from 'containers/Publish';
import * as Dictionary from 'containers/Dictionary';
import { Single, Relational } from 'ui/Publish';
import {
    StyledStepWrapper,
    StyledStepNextButton,
    StyledClaimFieldsWrapper,
    StyledPublicationButtonsWrapper,
} from 'ui/Publish/styles';
import { project } from 'configs';
import Conditions from 'ui/Publish/components/Claim/Conditions';

type Props = RouteComponentProps<{ contentType: string }>;

export const Claim: React.FC<Props> = ({ contentType }) => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const location = useLocation();

    const [firstItemDictionaries, setFirstItemDictionaries] = React.useState<string[]>([]);
    const [secondItemDictionaries, setSecondItemDictionaries] = React.useState<string[]>([]);

    const draft = useTypedSelector((state) => state.pub.data.draft);
    const step = useTypedSelector((state) => state.publish.data.step);
    const claim = useTypedSelector((state) => state.claim.data.claim);
    const dictionary = useTypedSelector((state) => state.dictionary.data);
    const userProfile = useTypedSelector((state) => state.profile.data.user);
    const selectedContent = useTypedSelector((state) => state.content.data.type);
    const dictionaryList = useTypedSelector((state) => state.dictionary.data.list);
    const publishStatus = useTypedSelector((state) => state.publish.status.type);
    const term1 = useTypedSelector((state) => state.dictionary.data.term1);
    const term2 = useTypedSelector((state) => state.dictionary.data.term2);

    const { claimItems, relationship } = claim;
    const isLoading = publishStatus === 'LOADING';
    const isSingle = selectedContent?.claim.type === 'single';
    const termExists = (item: string) => find(dictionaryList, (d) => d.id === item);

    React.useEffect(() => {
        const match = matchStepperToUrl(location.pathname.split(`/${contentType}/`)[1]);
        dispatch(Publish.step({ order: match.order, type: match.type }));
    }, [contentType, location.pathname, dispatch]);

    React.useEffect(() => {
        const allDictionaryIds = uniq([project.community.id, ...firstItemDictionaries, ...secondItemDictionaries]);
        dispatch(ClaimForm.sub({ form: 'claim', field: 'allDictionaryIds', value: allDictionaryIds }));
        dispatch(Pub.form.sub({ form: 'draft', field: 'metadata.allDictionaryIds', value: allDictionaryIds }));
    
        setAuthor();
    }, [dispatch, firstItemDictionaries, secondItemDictionaries]);

    const setPubTitle = () => {
        const firstItemLongName = termExists(claimItems.firstItem.id)?.longName || dictionary.term1.longName;
        const secondItemLongName =
            termExists(claimItems.secondItem?.id as string)?.longName || dictionary.term2.longName;
        const title = isSingle
            ? `${capitalize(selectedContent?.contentTypeLabel)} of ${firstItemLongName}`
            : `${firstItemLongName} ${lowerCase(relationship)} ${secondItemLongName}`;
        const fetchId = `${title.split(' ').join('-').toLowerCase()}_${draft.id}`;
        const storedTitle = sessionStorage.getItem('storedTitle');

        dispatch(Pub.form.set({ form: 'draft', field: 'title', value: storedTitle || title }));
        dispatch(Pub.form.set({ form: 'draft', field: 'metadata.fetchId', value: fetchId }));
    };

    const setAuthor = () => {
        const author = {
            id: userProfile?.uid,
            name: userProfile?.name,
            email: userProfile?.email,
            orcid: userProfile?.orcid,
        };

        dispatch(Pub.form.set({ form: 'draft', field: 'author', value: author }));
        dispatch(Dictionary.form.set({ form: 'term1', field: 'author', value: author }));
        dispatch(Dictionary.form.set({ form: 'term2', field: 'author', value: author }));
    };

    const handleToNextStep = () => {
        setAuthor();
        setPubTitle();
        navigate(`${location.origin}/publish/${contentType}/publication`);
        dispatch(Publish.step({ order: step.order + 1, type: 'PUBLICATION' }));
    };

    const handleSetFirstItemDictionaries = (values: string[]) => setFirstItemDictionaries(values);
    const handleSetSecondItemDictionaries = (values: string[]) => setSecondItemDictionaries(values);

    const isDisabled = () => {
        if (isSingle && !claimItems.firstItem.id) return true;
        return !isSingle && (!claimItems.firstItem.id || !claimItems.secondItem?.id || !relationship);
    };
    
    const saveAsDraft = async () => {
        if (isDisabled()) {
            return Modal.confirm({
                className: 'endorse_dialog',
                content: 'You need to make some changes first',
                onOk() { return null; },
                okCancel: false,
                centered: true,
            });
        }
        
        const pubDraft = {
            ...draft,
            metadata: {
                ...draft.metadata,
                draft: {
                    isDraft: true,
                    contentType: selectedContent?.contentTypeLabel,
                    claim,
                    term1,
                    term2,
                },
            },
        };
        await dispatch(
            Publish.createPub(
                {
                    pub: pubDraft,
                },
                'draft'
            )
        );
    };
    
    return (
        <StyledStepWrapper>
            <StyledClaimFieldsWrapper>
                <Single setDictionaries={handleSetFirstItemDictionaries} />
                {!isSingle && <Relational setDictionaries={handleSetSecondItemDictionaries} />}
            </StyledClaimFieldsWrapper>
            <Conditions disabled={isDisabled()} />
            <StyledPublicationButtonsWrapper>
                <StyledStepNextButton
                    label="Save Draft"
                    buttonType="link"
                    onClick={saveAsDraft}
                    isLoading={isLoading}
                    icon={<SaveOutlined />}
                />
                <StyledStepNextButton
                    iconSide="right"
                    label="Next Step"
                    buttonType="link"
                    disabled={isDisabled()}
                    onClick={handleToNextStep}
                    icon={<RightCircleOutlined />}
                />
            </StyledPublicationButtonsWrapper>
        </StyledStepWrapper>
    );
};
