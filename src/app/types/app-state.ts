import { PortalView, Status } from 'app/utils';
import { AlertDefinition, CommunityDefinition, SystemGatesDefinition } from 'app/types';


export interface AppState {
    readonly sesameOpen: boolean;
    readonly status: Status;
    readonly isAuth: boolean;
    readonly view: PortalView;
    readonly alerts: Array<AlertDefinition>;
    readonly gates: SystemGatesDefinition | null;
    readonly community: CommunityDefinition | null;
}
