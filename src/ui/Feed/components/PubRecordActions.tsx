import { find } from 'lodash';
import * as React from 'react';
import { Popconfirm } from 'antd';
import { useDispatch } from 'react-redux';
import {
    DeleteFilled,
    CheckCircleFilled,
    CheckCircleOutlined,
    ExclamationCircleFilled,
} from '@ant-design/icons';

import { useTypedSelector } from 'helpers';
import { bookmarkFilled, bookmarkOpen } from 'assets/icons';
import * as Pub from 'containers/Pub';
import * as List from 'containers/List';
import * as Files from 'containers/Files';
import { StyledPubRecordActionsWrapper, StyledPubRecordBookmarkIcon, StyledPubRecordEndorseIcon } from 'ui/Feed/styles';


type Props = {
    pub: PubDefinition;
};

export const PubRecordActions: React.FC<Props> = ({ pub }) => {
    const dispatch = useDispatch();
    const drafts = useTypedSelector((state) => state.list.data.list);
    const user = useTypedSelector((state) => state.profile.data.user);
    
    const isUserAuthor = pub.author.orcid === user?.orcid;
    const hasUserBookmarked = find(pub.bookmarkedBy, (id) => id === user?.uid);
    const hasUserEndorsed = find(pub.review.acceptedBy, (id) => id === user?.orcid);
    const hasUserReported = find(pub.review.reportedBy, (id) => id === user?.orcid);
    
    const noPropagation = (e: any) => e.stopPropagation();
    const handleRemovePubRecord = async () => {
        const newList = drafts.filter((p) => p.id !== pub.id);
        const deletePub = await dispatch(Pub.deletePub(pub.id));
        if (pub.figures.length > 0) {
            await dispatch(Files.deleteFileFolder(pub.id));
        }
        if (deletePub === 'OK') {
            dispatch(List.populate(newList));
        }
    };
    
    return (
        <StyledPubRecordActionsWrapper onClick={noPropagation}>
            {!pub.metadata.draft?.isDraft && hasUserReported && (
                <StyledPubRecordEndorseIcon>
                    <ExclamationCircleFilled />
                </StyledPubRecordEndorseIcon>
            )}
            {!pub.metadata.draft?.isDraft && (
                <StyledPubRecordEndorseIcon>
                    {hasUserEndorsed ? <CheckCircleFilled /> : <CheckCircleOutlined />}
                </StyledPubRecordEndorseIcon>
            )}
            {!pub.metadata.draft?.isDraft && (
                <StyledPubRecordBookmarkIcon>
                    {hasUserBookmarked ? bookmarkFilled : bookmarkOpen}
                </StyledPubRecordBookmarkIcon>
            )}
            {isUserAuthor && pub.metadata.draft?.isDraft && (
                <Popconfirm
                    title="Are you sure delete this task?"
                    onConfirm={handleRemovePubRecord}
                    okText="Yes"
                    cancelText="No"
                >
                    <DeleteFilled />
                </Popconfirm>
            )}
        </StyledPubRecordActionsWrapper>
    );
};
