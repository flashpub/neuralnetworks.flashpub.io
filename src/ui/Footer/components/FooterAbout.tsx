import * as React from 'react';
import { StyledFooterSectionTitle, StyledFooterListItemLink } from 'ui/Footer/styles';


export const FooterAbout: React.FC = () => {
    const onContactUs = () => {
        const email = 'mailto:hello@flashpub.io?Subject=Contact Us';
        window.open(email);
    };
    
    return (
        <>
            <StyledFooterSectionTitle>About Us</StyledFooterSectionTitle>
            <StyledFooterListItemLink
                target="_blank"
                href="https://medium.com/@flashpub_io/the-most-valuable-ip-on-the-planet-343a450cd4bb"
            >
                Our story
            </StyledFooterListItemLink>
            <StyledFooterListItemLink onClick={onContactUs}>Contact Us</StyledFooterListItemLink>
        </>
    );
};
