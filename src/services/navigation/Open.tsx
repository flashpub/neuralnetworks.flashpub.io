import * as React from 'react';
import { Router } from '@reach/router';

import { CampaignsScreen, FeedScreen, PubScreen } from 'screens';
import { NotFound } from 'services/navigation';
import { ListScreen } from 'screens/ListScreen';

export const Open: React.FC = () => {
    return (
        <Router primary={false}>
            <NotFound default />
            <FeedScreen path="/" />
            <FeedScreen path="/feed/:name" />
            <PubScreen path="/pub/:id" />
            <CampaignsScreen path="/campaigns/:id" />
            <ListScreen path="/list" />
        </Router>
    );
};
