import { createGenericSlice } from 'helpers';
import { FirebaseUser } from 'types/firebase';

const initialState = {
    data: null,
    status: { type: 'LOADING', message: null },
};

export const slice = createGenericSlice({
    name: 'authentication',
    initialState: initialState as GenericState<FirebaseUser>,
    reducers: {
        signin: (state, action) => {
            state.data = action.payload;
        },
        signout: (state) => {
            state.data = null;
        },
    },
});

export const { signin, signout, ...status } = slice.actions;

export default slice.reducer;
