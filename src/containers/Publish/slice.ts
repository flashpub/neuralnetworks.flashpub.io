import { createGenericSlice } from 'helpers';

const initialState = {
    data: {
        step: { order: 0, type: 'INTRO' },
    },
    status: { type: 'PENDING', message: null },
};

export const slice = createGenericSlice({
    name: 'publish',
    initialState: initialState as GenericState<PublishContainerState>,
    reducers: {
        step: (state, action) => {
            state.data.step = action.payload;
        },
    },
});

export const { step, ...status } = slice.actions;

export default slice.reducer;
