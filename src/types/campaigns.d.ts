interface CampaignsContainerState {
    campaigns: CampaignDefinition[];
    selected: CampaignDefinition | null;
    list: CampaignData[];
}
interface CampaignResource {
    url: string;
    title: string;
}
interface CampaignMetrics {
    userCount: number;
    pubCount: number;
    subCommunityCount: number;
}
type CampaignPubData = {
    fetchId: string;
    id: string;
    title: string;
    date: number;
    author: string;
};
type CampaignData = CampaignCityData & MidasCampaignData;
interface CampaignCityData {
    id: string;
    city: string;
    state: string;
    country: string;
    confirmed: number;
    deaths: number;
    recovered: number;
    ongoing: number;
    latitude: number;
    longitude: number;
    dataDate: number;
    pubCount: number;
    hasCaseCountIncreased: boolean;
    pubArray: CampaignPubData[];
    pubURL: string;
}
interface CampaignDefinition {
    metrics: CampaignMetrics;
    resources: CampaignResource[];
    community: string;
    createdOn: number;
    id: string;
    contentTypes: {
        [key: string]: CommunityContentType;
    };
    updatedOn: number;
    title: string;
    campaignData: CampaignData[];
    description: string;
    image: string;
    campaignType: 'map' | 'dashboard';
}

interface MidasCampaignData {
    id: string;
    pubCount: number;
    pubs: MidasCampaignPubsData[];
    pubsURL: string;
    subtitle: string;
    title: string;
    updatedOn: number;
    value: number;
    valueError: number;
}
type MidasCampaignKeyDetails = {
    age: string;
    dataDate: number;
    region: string;
};

interface MidasCampaignPubsData {
    isEndorsed: boolean;
    keyDetails: MidasCampaignKeyDetails;
    pubURL: string;
    publishedDate: number;
    title: string;
    value: number;
    valueError: number;
}
