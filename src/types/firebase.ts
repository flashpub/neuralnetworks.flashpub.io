import { firebase } from 'configs/firebase';

export type FirebaseUser = firebase.User | null;
