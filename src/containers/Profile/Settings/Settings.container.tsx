import * as React from 'react';

import { StyledFlexWrapper } from 'ui/elements/styles';


type Props = {};

export const SettingsContainer: React.FC<Props> = (props: Props) => {
    return (
        <StyledFlexWrapper>
            [SettingsContainer]
        </StyledFlexWrapper>
    );
};
