import React from 'react';
import styled from 'styled-components';
import capitalize from 'lodash/capitalize';
import { Row, Col } from 'react-styled-flexboxgrid';
import { Position, Tooltip } from '@blueprintjs/core';

import { Asset, Assets } from 'app/types';
import { Color, Font, Media, Spacing } from 'app/assets';

type Props = {
    assets: Assets
}

type AssetType = "references" | "protocols" | "code" | "datasets";

type AssetKey = "url" | "name";

const PublicationMetadata: React.FC<Props> = ({ assets }) => {

    const renderMetadataElement = (assets: Assets, assetType: AssetType, assetKey: AssetKey) => {
        return Object.entries(assets[assetType]).length !== 0 && (
            <MetadataElement>
                {capitalize(assetType)}
                {

                    renderMetadataAssets(assetType, assets, assetKey)
                }
            </MetadataElement>
        );
    };

    const renderMetadataAssets = (assetType: AssetType, metadataAssets: any, assetKey: AssetKey) => {
        return metadataAssets && metadataAssets[assetType].map((asset: Asset, i: number) => (
            <MetadataRecord onClick={() => onMetadataRecordClick(asset.url)} key={i}>
                <Tooltip
                    content={assetType === 'references' ? asset.name : (`${asset.name} | ${asset.url}`)}
                    position={Position.TOP}
                >
                    {assetType === 'references' ? asset.url : asset.name}
                </Tooltip>
            </MetadataRecord>
        ));
    };

    const onMetadataRecordClick = (url: string) => {
        if (url.includes('http')) {
            window.open(url, '_blank');
        } else {
            url && window.open(`https://${url}`, '_blank');
        }
    };

    return (
        <MetadataContainer>
            <Row>
                {assets.references.length !== 0 && (
                    <Col xs={12} sm>
                        {renderMetadataElement(assets, "references", "url")}
                    </Col>
                )}
                {assets.protocols.length !== 0 && (
                    <Col xs={12} sm>
                        {renderMetadataElement(assets, "protocols", "name")}
                    </Col>
                )}
                {assets.code.length !== 0 && (
                    <Col xs={12} sm>
                        {renderMetadataElement(assets, "code", "name")}
                    </Col>
                )}
                {assets.datasets.length !== 0 && (
                    <Col xs={12} sm>
                        {renderMetadataElement(assets, "datasets", "name")}
                    </Col>
                )}
            </Row>
        </MetadataContainer>
    );
};

export default PublicationMetadata;

const MetadataContainer = styled.div`
    padding: 0;
    width: 100%;
    color: ${Color.grey};
    font-family: ${Font.text};
    margin-top: ${Spacing.XXL};
    margin-bottom: ${Spacing.XXL};
`;

const MetadataElement = styled.div`
    font-size: 14px;
    font-weight: 200;
    margin-bottom: ${Spacing.ML};
`;

const MetadataRecord = styled.div`
    font-weight: 200;
    color: ${Color.community};
    margin: ${Spacing.SM} 0 0;
    font-family: ${Font.mono};
    
    span {
        width: 100%;
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden !important;
        
        ${Media.tablet} {
            max-width: 250px;
        }
    }
    
    &:hover {
        color: ${Color.flashpub};
        cursor: pointer;
    }
`;
