interface ListContainerState {
    readonly title: string;
    readonly list: PubDefinition[];
}
