import React, { Dispatch, SetStateAction } from 'react';
import styled from 'styled-components';
import { isEqual } from 'lodash';

import store from 'app/store';
import { ADD_ALERT } from 'app/actions';
import { Color, Font, Media } from 'app/assets';
import { PubDefinition, AlertType, Assets } from 'app/types';
import { EditedField } from 'publication/types';

import { FUIButton } from 'FUI/elements';

type Props = {
    pub: PubDefinition
    assets: Assets,
    assetsChanged: {
        code: boolean,
        datasets: boolean,
        protocols: boolean,
        references: boolean
    },
    description: EditedField,
    editDescription: Dispatch<SetStateAction<EditedField>>,
    setEditMode(): void,
    removeDescFromStorage(): void,
    removeAssetsFromStorage(): void,
    setAssetsChanged(assetsChanged: {
        code: boolean,
        datasets: boolean,
        protocols: boolean,
        references: boolean
    }): void,
    submitEditedPub(pub: PubDefinition, editedElements: Array<string>): void
}

const PublicationEditBar: React.FC<Props> = (
    {
        pub, setEditMode, removeDescFromStorage, submitEditedPub, description, editDescription, assets, assetsChanged,
        setAssetsChanged, removeAssetsFromStorage
    }
) => {

    const handleExitEditMode = () => {
        removeDescFromStorage();
        removeAssetsFromStorage();
        editDescription({ value: "", hadChanged: false, hadFocus: false })
        setAssetsChanged({
            code: false,
            datasets: false,
            protocols: false,
            references: false
        });
        setEditMode();
    }

    const handleSubmitEditedPub = () => {
        const storageDescription = localStorage.getItem('description');
        const storageAssetsString = localStorage.getItem('assets');
        const storageAssets: Assets = storageAssetsString && JSON.parse(storageAssetsString);
        let editedPub: any = pub;
        delete editedPub.asset
        let editedElements = [];

        if (description.hadChanged && description.hadFocus && storageDescription && storageDescription !== pub.description) {
            editedPub = { ...editedPub, description: storageDescription }
            editedElements.push('description');
        }

        if (assets && (assetsChanged.code || assetsChanged.datasets || assetsChanged.protocols || assetsChanged.references)) {
            editedPub = {
                ...editedPub, metadata: {
                    ...editedPub.metadata,
                    code: storageAssets.code,
                    datasets: storageAssets.datasets,
                    protocols: storageAssets.protocols,
                    references: storageAssets.references
                }
            }

            assetsChanged.code && editedElements.push('code');
            assetsChanged.datasets && editedElements.push('datasets');
            assetsChanged.protocols && editedElements.push('protocols');
            assetsChanged.references && editedElements.push('references');

            setAssetsChanged({
                code: false,
                datasets: false,
                protocols: false,
                references: false
            });
        }

        if (!isEqual(pub, editedPub)) {
            submitEditedPub(editedPub, editedElements);
        } else {
            store.dispatch(ADD_ALERT.use({
                type: AlertType.INFO,
                message: "No changes were made"
            }));
            removeDescFromStorage();
            removeAssetsFromStorage();
        }
        setEditMode();
    }

    return (
        <EditBarContainer>
            <EditBar>
                <span>You are now in the edit mode</span>
                <Buttons>
                    <Button type="button" text="Close" onClick={handleExitEditMode} />
                    <Button type="button" buttonType="outlined" text="Save" onClick={handleSubmitEditedPub} />
                </Buttons>
            </EditBar>
        </EditBarContainer>
    );
}

export default PublicationEditBar;

const EditBarContainer = styled.div`
    position: relative;
    width: 100%;
    height: 40px;
    color: white;
`;

const EditBar = styled.div`
    display: flex;
    position: fixed;
    align-items: center;
    justify-content: space-evenly;
    width: 100%;
    height: 40px;
    bottom: 0px;
    left: 0px;
    background-color: ${Color.community};
    font-family: ${Font.text};
    font-size: 13px;

    ${Media.tablet} {
        justify-content: center;
        font-size: 15px;
    }
`;

const Buttons = styled.div`
    display: flex; 
    justify-content: space-evenly;
    position: static;
    
    ${Media.tablet} {
        position: absolute;
        right: 100px;
    }
`;

const Button = styled(FUIButton)` && {
    background-color: ${Color.community};
    color: white;
    margin-left: 20px;
}`;

