import { Dispatch } from 'redux';
import { project } from 'app/config';
import { AxiosResponse } from 'axios';
import * as AppAction from 'app/actions';
import { AlertType, Figure, Thunk } from 'app/types';

import { pub } from 'publish/client';

import * as PublishAction from 'publish/actions';

import * as OntologyAction from 'dictionaries/actions';

import { storage } from '../firebase-config';


export function publishAlertHandler(type: AlertType, alert: Error | { message: string }) {
    return (dispatch: Dispatch) => {
        dispatch(PublishAction.PUBLISH_OPERATION_FAIL.use());
        dispatch(AppAction.ADD_ALERT.use({
            type: type,
            message: alert.message,
        }));
    };
}

export function createPub(data: any): Thunk {
    return async (dispatch) => {
        dispatch(PublishAction.PUBLISH_OPERATION_START.use());

        const addFileToStorage = (file: File) => {
            const uploadTask = storage.ref().child(`figures/${file.name}`).put(file);
            return uploadTask.on(
                'state_changed',
                () => {},
                (error) => dispatch(publishAlertHandler(AlertType.ERROR, error)),
                () => uploadTask.snapshot.ref.getDownloadURL().then(url => url),
            )
        };

        const uploadFiles = Promise.resolve(data.figures.map((f: Figure) => addFileToStorage(f.file as File)));
        const resp = Promise.resolve(pub.create(data));

        return Promise.all([
            uploadFiles.catch(error => dispatch(publishAlertHandler(AlertType.ERROR, error))),
            resp.catch(error => dispatch(publishAlertHandler(AlertType.ERROR, error))),
        ]).then(values => {
            dispatch(AppAction.ADD_ALERT.use({
                type: AlertType.SUCCESS,
                message: 'Publication added successfully',
            }));
            dispatch(PublishAction.SET_PUBLISH_STEP.use({ step: null }));
            dispatch(OntologyAction.SET_CURRENT_DICTIONARY.use({ current: { id: project.id, name: project.name }}));
            dispatch(PublishAction.PUBLISH_OPERATION_SUCCESS.use());
            return (values[1] as AxiosResponse) && (values[1] as AxiosResponse).data === 'OK';
        })
    };
}
