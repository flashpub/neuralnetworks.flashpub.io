import * as React from 'react';
import styled from 'styled-components';
import { Spinner as UISpinner } from '@blueprintjs/core';

import { Status } from 'app/utils';
import { Radius, Font, Spacing, Color } from 'app/assets';


/** this is a flashPub styled button **/
type ButtonProps = {
    text: string,
    loading?: Status,
    onClick?(): void,
    disabled?: boolean,
    buttonType?: 'outlined' | 'text' | undefined,
    type?: 'submit' | 'reset' | 'button' | undefined,
};

const FUIButton: React.FC<ButtonProps> = (props: ButtonProps) => (
    <StyledButton
        {...props}
        onClick={props.onClick}
        disabled={props.disabled}
    >
        {props.loading === Status.LOADING ? <Spinner /> : props.text}
    </StyledButton>
);

const Spinner = styled(UISpinner)` && {
    svg {
        width: 32px;
        height: 32px;
    }
}`;
const StyledButton = styled.button<ButtonProps>`
    cursor: pointer;
    font-size: 16px;
    border-radius: ${Radius};
    padding: ${Spacing.SM} ${Spacing.L};
    transition: background-color .15s ease;
    border: ${props => props.buttonType === 'outlined'
    ? `1px solid`
    : 'none'
};
    color: ${props => {
    if (props.disabled) return Color.midgrey;
    return typeof props.buttonType === 'undefined'
        ? '#fff'
        : Color.community
}};
    background-color: ${props => typeof props.buttonType === 'undefined'
    ? Color.community
    : 'transparent'
};
    :focus {
        outline: none;
        outline-offset: 0;
    }
    :hover {
        background-color: ${props => {
    if (props.disabled) return 'none';
    return typeof props.buttonType === 'undefined'
        ? Color.community_t75
        : Color.community_t15
}}
    }
`;
/** this is a flashPub styled button **/

/** this is a flashPub styled input **/
type InputProps = {
    ref?: any,
    value?: any,
    placeholder?: any,
    onChange?: (e: any) => void,
    type: 'text' | 'password' | string,
};

type Ref = ((instance: HTMLInputElement | null) => void) | React.RefObject<HTMLInputElement> | null | undefined

const FUIInput = React.forwardRef((props: InputProps, ref: Ref) =>
    <StyledInput
        {...props}
        ref={ref}
        value={props.value}
        onChange={props.onChange}
        placeholder={props.placeholder}
    />
);

const StyledInput = styled.input`
    width: 100%;
    height: 40px;
    font-size: 18px;
    box-shadow: none;
    font-weight: 300;
    border-width: 1px;
    text-align: center;
    border-style: solid;
    color: ${Color.grey};
    margin: ${Spacing.M} 0;
    border-radius: ${Radius};
    font-family: ${Font.text};
    border-color: ${Color.grey};
    background-color: ${Color.notwhite};
    :focus {
        outline: none;
    }
    ::placeholder {
        color: ${Color.midgrey};
    }
`;
/** this is a flashPub styled input **/

export {
    FUIInput,
    FUIButton,
}
