import styled from 'styled-components';
import { Color, Font, Media, MOBILE, Radius, Spacing } from 'assets/styles';

export const StyledFlexWrapper = styled.div`
    display: flex;
    align-items: center;
    flex-direction: column;
    justify-content: center;
`;
export const StyledBrandLogo = styled.div<{ fontSize?: number }>`
    font-weight: 200;
    color: ${Color.grey};
    font-family: ${Font.text};
    strong {
        font-weight: 500;
    }
    @media (min-width: ${MOBILE}) {
        font-size: ${({ fontSize }) => (fontSize ? `${fontSize}px` : '14px')};
    }
    a {
        color: ${Color.midgrey};
        :hover {
            color: ${Color.flashpub};
        }
    }
`;
export const StyledBrandIconWrapper = styled(StyledFlexWrapper)`
    align-items: center;
    
    ${Media.desktop} {
        align-items: flex-start;
    }
`;
export const StyledBrandIcon = styled.div<{
    symbol: string;
    size?: number;
    main?: boolean;
    round?: boolean;
    fontSize?: number;
    reversed?: boolean;
    color?: string;
}>`
    display: flex;
    position: relative;
    align-items: center;
    justify-content: center;
    font-family: ${Font.title};
    width: ${({ size }) => (size ? `${size}px` : '40px')};
    height: ${({ size }) => (size ? `${size}px` : '40px')};
    border-radius: ${({ round }) => (round ? '100px' : '6px')};
    background-color: ${({ main, reversed, color }) => (color ? color : main ? Color.flashpub : reversed ? '#fff' : Color.community)};
    &:before {
        content: ${({ symbol }) => `'${symbol}'`};
        color: ${({ reversed }) => (reversed ? Color.community : '#fff')};
        font-size: ${({ fontSize }) => (fontSize ? `${fontSize}px` : '20px')};
    }
`;
export const StyledClaimWrapper = styled(StyledFlexWrapper)`
    flex-direction: row;
`;
export const StyledTermWrapper = styled(StyledFlexWrapper)`
    color: #fff;
    cursor: default;
    font-size: 12px;
    font-weight: 300;
    border-radius: ${Radius};
    font-family: ${Font.text};
    background-color: ${Color.community};
    padding: ${Spacing._x(2)} ${Spacing._8};
`;
export const StyledSpacer = styled.div<{ height?: number }>`
    width: 100%;
    height: ${({ height }) => (height ? `${height}px` : '40px')};
`;
