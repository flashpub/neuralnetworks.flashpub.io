import { createGenericSlice } from 'helpers';

const initialState = {
    data: {
        url: '',
        progress: 0,
    },
    status: { type: 'PENDING', message: null },
};

export const slice = createGenericSlice({
    name: 'upload',
    initialState: initialState as GenericState<UploadContainerState>,
    reducers: {
        url: (state, action) => {
            state.data.url = action.payload;
        },
        progress: (state, action) => {
            state.data.progress = action.payload;
        },
    },
});

export const { url, progress, ...status } = slice.actions;

export default slice.reducer;
