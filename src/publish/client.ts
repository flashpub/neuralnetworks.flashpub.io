import { AxiosResponse } from 'axios';

import api, { API_VERSION } from 'app/api';

const version = API_VERSION.content;

export const pub = {
    create: (body: any): Promise<AxiosResponse> => api.post(`/content/${version}/publish_new`, body),
};
