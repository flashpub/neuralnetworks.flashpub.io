import * as React from 'react';
import styled from 'styled-components';

import { Radius, Font, Spacing, Color } from 'app/assets';


export interface FUIInputRefs {
    getInputFocus(): void;
    getInputValue(): string;

    setInputValue<T>(value: T): void;
}
interface FUIInputProps {
    [propName: string]: any;

    chars?: number;
    counter?: boolean;
    required?: boolean;
    placeholder?: string;
    onChange?(value: string): any;
}
type ChangeEvent = React.ChangeEvent<HTMLInputElement>

export const FUIInput: React.FC<FUIInputProps> = (
    { chars = 100, required, placeholder, onChange, counter = false, value, ...props },
) => {
    const onValueChange = (e: ChangeEvent) => onChange && onChange(e.target.value);

    const isRequired = required && !value;

    return (
        <InputWrapper required={isRequired}>
            <StyledInput
                {...props}
                value={value}
                maxLength={chars}
                required={isRequired}
                onChange={onValueChange}
                placeholder={placeholder}
            />
            {counter && <Counter>{chars - value.length}</Counter>}
        </InputWrapper>
    )
};


const Counter = styled.div`
    top: 50%;
    right: 4px;
    padding: 1px;
    font-size: 14px;
    position: absolute;
    visibility: hidden;
    border-radius: ${Radius};
    color: ${Color.midgrey};
    transform: translateY(-50%);
    background-color: rgba(255,255,255,.9);
`;
const InputWrapper = styled.div<FUIInputProps>`
    position: relative;
    &::before {
        top: 2px;
        left: 4px;
        position: absolute;
        color: ${Color.error};
        content: ${({ required }) => required ? "'*'" : "''"};
    }
    &:hover ${Counter} { visibility: visible }
`;
const StyledInput = styled.input<FUIInputProps>`
    width: 100%;
    height: 40px;
    font-size: 14px;
    box-shadow: none;
    font-weight: 400;
    border-width: 1px;
    text-align: center;
    border-style: solid;
    color: ${Color.grey};
    padding: ${Spacing.M};
    margin: ${Spacing.M} 0;
    border-radius: ${Radius};
    font-family: ${Font.text};
    border-color: ${Color.midgrey};
    background-color: ${Color.notwhite};
    &:hover {
        border-color: ${({ required, disabled }) => required
            ? Color.midgrey
            : disabled
                ? 'transparent'
                : Color.midgrey
        };
    }
    &:focus {
        outline: none;
        border-color: ${Color.midgrey};
    }
    &:focus + ${Counter} { visibility: visible; }
    &::placeholder {
        font-weight: 200;
        font-style: italic;
        color: ${Color.midgrey};
    }
`;
