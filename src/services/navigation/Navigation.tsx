import find from 'lodash/find';
import * as React from 'react';
import { useLocation } from 'react-use';
import { useDispatch } from 'react-redux';

import { useTypedSelector } from 'helpers';
import * as Dictionary from 'containers/Dictionary';

export const Navigation: React.FC = () => {
    const dispatch = useDispatch();
    const location = useLocation();
    const dictionaryList = useTypedSelector((state) => state.dictionary.data.list);

    React.useEffect(() => {
        const path = location.pathname;
        if (path && path.includes('/feed/')) {
            const findTerm = find(dictionaryList, (term) => term.name === path.split('/feed/')[1]);
            dispatch(Dictionary.select({ id: findTerm?.id, name: findTerm?.name }));
        }
    }, [dispatch, location, dictionaryList]);

    return null;
};
