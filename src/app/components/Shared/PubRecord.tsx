import * as React from 'react';
import dayjs from 'dayjs';
import htmlToDraft from 'html-to-draftjs';
import styled from 'styled-components';
import { ContentState, EditorState } from 'draft-js';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Grid as UIGrid, Col as UICol, Row as UIRow } from 'react-styled-flexboxgrid';

import { ClaimDefinition, PubDefinition } from 'app/types';
import { PortalView, useWindowWidth, Viewport } from 'app/utils';
import { Color, Radius, Spacing, Font, Media, positiveLine, neutralLine, negativeLine } from 'app/assets';
import { FUIClaimItem } from '../../../FUI/elements';
import { Position, Tooltip } from '@blueprintjs/core';
import lowerCase from 'lodash/lowerCase';


type Event<T> = React.MouseEvent<T>;
type Props = {
    pub: PubDefinition,
    type: 'top' | 'recent' | 'library',
    isOverDeadline: boolean,
    claims: Array<ClaimDefinition> | null;

    pubStatus(): string,
    setPickedPub(id: string): void,
    setPortalView(view: PortalView): void,
    portalView?: PortalView
} & RouteComponentProps;

const PubRecord: React.FC<Props> = ({ pub, type, setPickedPub, history, setPortalView, claims, pubStatus, isOverDeadline, portalView }) => {
    const isMobile = useWindowWidth() <= Viewport.MOBILE;
    const datePublished = (date: number) => {
        const mm = dayjs(date).format(`MMM. `);
        const DD = dayjs(date).format(`DD`);
        const year = dayjs(date).format(`YYYY`);

        switch (DD.charAt(1)) {
            case '1':
                return `${mm}${DD}st, ${year}`;
            case '2':
                return `${mm}${DD}nd, ${year}`;
            case '3':
                return `${mm}${DD}rd, ${year}`;
            default:
                return `${mm}${DD}th, ${year}`;
        }
    };
    const handleOnClaimClick = (e: Event<HTMLSpanElement>, c: string) => {
        e.stopPropagation();
        console.log(c);
    };
    const handleOnRecordClick = () => {
        localStorage.setItem('penultimatePath', history.location.pathname);
        history.push(`/pub/${pub.metadata.fetchId}`);
        setPortalView(PortalView.PUBLICATION);
        setPickedPub(pub.metadata.fetchId);
    };
    const handleOnAuthorClick = (e: Event<HTMLDivElement>) => {
        e.stopPropagation();
        console.log('author');
    };

    const renderClaimItems = () => {
        if (claims) {
            return pub.claim.terms.secondItem
                ? (
                    <div onClick={(e) => handleOnClaimClick(e, pub && pub.claim.terms.firstItem)}>
                        <FUIClaimItem>{pub.claim.terms.firstItem}</FUIClaimItem>
                        <Tooltip content={lowerCase(pub.claim.terms.relationship)} position={Position.TOP}>
                            {pub.claim.terms.type === 'positive'
                                ? positiveLine
                                : pub.claim.terms.type === 'neutral'
                                    ? neutralLine
                                    : negativeLine
                            }
                        </Tooltip>
                        <FUIClaimItem>{pub.claim.terms.secondItem}</FUIClaimItem>
                    </div>
                ) : <FUIClaimItem onClick={(e) => handleOnClaimClick(e, pub.claim.terms.firstItem)}>{pub.claim.terms.firstItem}</FUIClaimItem>;
        } return null;
    };

    const printStatusTag = pubStatus() === 'accepted'
        ? null
        : <InReviewTag>[IN REVIEW]</InReviewTag>;

    // @ts-ignore
    const parseDescription = () => {
        const contentBlock = htmlToDraft(pub.description);
        if (contentBlock) {
            const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
            const editorState = EditorState.createWithContent(contentState);
            return editorState.getCurrentContent().getPlainText();
        }
    };

    return (
        <RecordContainer fluid onClick={handleOnRecordClick} type={type}>
            <RecordHeader>
                <UICol xs={isMobile ? 12 : 10}>
                    <TitleWrapper>
                        <Title>
                            {printStatusTag}
                            {pub.title}
                        </Title>
                        {portalView !== 'LIBRARY' && <Claims>{renderClaimItems()}</Claims>}
                    </TitleWrapper>
                </UICol>
                <UICol>
                    <Date>{datePublished(pub.metadata.datePublished)}</Date>
                </UICol>
            </RecordHeader>

            <RecordBody type={type}>
                <Body xs={12} sm={9}>
                    <Author onClick={handleOnAuthorClick}>by {pub.author.name} {pub.contributors.length !== 0 && 'et al'}</Author>
                    <Description>{parseDescription()}</Description>
                </Body>
            </RecordBody>
        </RecordContainer>
    );
};

const RecordContainer = styled(UIGrid) <{ type: string }>` && {
    cursor: pointer;
    min-height: 150px;
    position: relative;
    color: ${Color.grey};
    background-color: #fff;
    transition: all .1s ease-in;
    border-bottom: 1px groove ${Color.community_t15};
    padding: ${Spacing.XXL} ${Spacing.S} ${Spacing.XL};
    
    ${Media.desktop} {
        box-shadow: none;
        border-bottom: none;
        border-radius: ${Radius};
        min-height: ${props => props.type === 'top' ? '58px' : '34px'};
        padding: ${props => props.type === 'top' ? `${Spacing.SM} ${Spacing.M}` : `${Spacing.M} ${Spacing.M} 0`};
        :hover {
            transform: scale(1.01);
            background-color: ${Color.lightgrey};
        }
    }
}`;
const RecordHeader = styled(UIRow)` && {
    ${Media.desktop} {
        margin-bottom: ${Spacing.ML};
    }
}`;
const TitleWrapper = styled.div`
    display: block;
    font-size: 18px;
    font-weight: 500;
    text-align: center;
    color: ${Color.grey};
    margin: ${Spacing.M} auto 0;
    font-family: ${Font.feed_excerpt};
    
    ${Media.desktop} {
        margin: 0;
        display: flex;
        font-size: unset;
        box-shadow: none;
        text-align: unset;
        align-items: center;
        font-family: ${Font.feed_title};
    }
`;
const Title = styled.div`
    display: inline-block;
    margin-right: ${Spacing.M};
`;
const ClaimItem = styled.span`
    overflow: hidden;
    max-width: 150px;
    border: 1px solid;
    white-space: nowrap;
    display: inline-block;
    color: ${Color.midgrey};
    text-overflow: ellipsis;
    border-radius: ${Radius};
    font-family: ${Font.mono};
    background-color: rgba(255,255,255,.7);
    padding: 2px ${Spacing.SM} 2px ${Spacing.S};
`;
const Line = styled.hr`
    width: 10px;
    display: inline-block;
    margin-bottom: ${Spacing.M};
`;
const Claims = styled.div`
    display: block;
    font-size: 12px;
    width: fit-content;
    color: ${Color.midgrey};
    font-family: ${Font.text};
    margin: ${Spacing.M} auto 0;

    &:hover ${ClaimItem} {
        cursor: pointer;
        color: ${Color.flashpub};
    }
    
    &:hover ${Line} {
        cursor: pointer;
        color: ${Color.flashpub};
    }
    
    ${Media.desktop} {
        margin: unset;
        display: inline-block;
    }
`;
const Date = styled.div`
    top: 8px;
    right: 4px;
    font-size: 13px;
    position: absolute;
    font-family: ${Font.text};
    
    ${Media.desktop} {
        top: 6px;
        font-size: 14px;
    }
`;
const RecordBody = styled(UIRow) <{ type: string }>` && {
    display: ${props => props.type === 'top' ? null : 'none'};
    
    ${Media.desktop} {
        margin-bottom: ${Spacing.S};
    }
}`;
const Body = styled(UICol)` && {
    ${Media.desktop} {
        height: 16px;
    }
}`;
const Author = styled.div`
    width: fit-content;
    font-style: italic;
    font-family: ${Font.text};
    margin: ${Spacing.M} auto ${Spacing.L};
    :hover {
        cursor: pointer;
        color: ${Color.flashpub};
    }
    
    ${Media.desktop} {
        width: auto;
        margin: unset;
        font-size: 14px;
        overflow: hidden;
        font-style: italic;
        white-space: nowrap;
        color: ${Color.grey};
        display: inline-block;
        text-overflow: ellipsis;
        font-family: ${Font.text};
        margin-right: ${Spacing.SM};
        padding-right: ${Spacing.S};
    }
`;
const Description = styled.div`
    width: 100%;
    font-size: 15px;
    overflow: hidden;
    white-space: nowrap;
    display: inline-block;
    text-overflow: ellipsis;
    font-family: ${Font.text};
    p, em {
        width: inherit;
        margin-bottom: 0;
        margin-right: 2px;
        display: inline-block;
    }
    br {
        display: none;
        margin-right: 2px;
    }
    
    ${Media.desktop} {
        width: 350px;
    }
`;
const InReviewTag = styled.span`
    font-size: 14px;
    font-weight: 500;
    color: ${Color.community};
    font-family: ${Font.text};
    margin-right: ${Spacing.M};
`;


export default withRouter(PubRecord);
