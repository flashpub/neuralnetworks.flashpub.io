import * as React from 'react';
import { v4 as uuidv4 } from 'uuid';
import { useDispatch } from 'react-redux';
import { useEffectOnce } from 'react-use';
import { startCase, intersectionWith, lowerCase, find } from 'lodash';

import { project } from 'configs';
import { useTypedSelector } from 'helpers';
import * as Pub from 'containers/Pub';
import * as Claim from 'containers/Claim';
import * as Dictionary from 'containers/Dictionary';
import { FPSelect } from 'ui/elements';
import { StyledTermWrapper, StyledTermLabel, StyledTerm } from 'ui/Publish/styles';

type Props = {
    setDictionaries: (values: string[]) => void;
};

export const Relational: React.FC<Props> = ({ setDictionaries }) => {
    const dispatch = useDispatch();
    const [search, setSearch] = React.useState<string>('');
    const claim = useTypedSelector((state) => state.claim.data.claim);
    const selectedContent = useTypedSelector((state) => state.content.data.type);
    const dictionaryStatus = useTypedSelector((state) => state.dictionary.status);
    const dictionaryList = useTypedSelector((state) => state.dictionary.data.list);
    const dictionaryAncestors = useTypedSelector((state) => state.dictionary.data.term2.ancestors);

    const { claimItems, relationship, relationshipType } = claim;
    const secondItem = claimItems.secondItem;
    const termExists = (item: string) => find(dictionaryList, (d) => d.id === item);

    useEffectOnce(() => {
        const term = termExists(secondItem?.id as string);
        if (secondItem?.id) {
            if (term) {
                setDictionaries([...term.ancestors, secondItem?.id]);
            } else {
                setDictionaries([...dictionaryAncestors, secondItem?.id]);
            }
        }
    });

    if (dictionaryList && selectedContent) {
        const contentItem2 = selectedContent.claim.item2;
        const contentRelationship = selectedContent.claim.relationship;
        const isCreatable = !termExists(lowerCase(search)) && selectedContent.claim.item2?.newTermAllowed;
        
        const fixedTerm = () => {
            const term = termExists(selectedContent.claim.item2?.filter as string);
            return term ? [{ value: term.name, key: term.id, title: term.longName }] : [];
        };
        
        const options: Option[] = selectedContent.claim.item2?.setFilterAsItem
            ? fixedTerm()
            : dictionaryList
                .filter((term) => term.name !== project.community.name)
                .map((term) => ({ value: term.name, key: term.id, title: term.longName }));
        const relOptions: Option[] = contentRelationship
            ? contentRelationship.map((r: Relationship) => ({ value: r.name, key: r.type, title: lowerCase(r.name) }))
            : [];

        const handleOnChange = (value: any, field?: string) => {
            const term2 = termExists(value.key);
            sessionStorage.removeItem('storedTitle');
            if (field) {
                if (Array.isArray(value)) {
                    const valueArray = value.map((v: Option) => v.key);
                    dispatch(Dictionary.form.sub({ form: 'term2', field, value: valueArray }));
                    setDictionaries([...valueArray, secondItem?.id as string]);
                } else {
                    if (field === 'relationship') {
                        const type = value.key.split('_')[0];
                        dispatch(Claim.form.set({ form: 'claim', field, value: value.value }));
                        dispatch(Claim.form.set({ form: 'claim', field: `${field}Type`, value: type }));
                        dispatch(Pub.form.set({ form: 'draft', field: 'claim.terms.type', value: type }));
                        dispatch(
                            Pub.form.set({ form: 'draft', field: 'claim.terms.relationship', value: value.value })
                        );
                        return;
                    }

                    if (term2) {
                        dispatch(Dictionary.form.clear('term2'));
                        setDictionaries([...term2.ancestors, value.key]);
                    }

                    dispatch(Pub.form.set({ form: 'draft', field: 'claim.terms.secondItem', value: value.value }));
                    dispatch(Claim.form.set({ form: 'claim', field, value: { id: value.key, name: value.value } }));
                }
            }
        };
        const handleOnCreate = (value: any, field?: string) => {
            const termId = uuidv4();
            sessionStorage.removeItem('storedTitle');
            if (field) {
                setDictionaries([termId]);
                dispatch(Dictionary.form.set({ form: 'term2', field: 'id', value: termId }));
                dispatch(Dictionary.form.set({ form: 'term2', field: 'name', value: value.value }));
                dispatch(Dictionary.form.set({ form: 'term2', field: 'longName', value: value.title }));
                dispatch(Claim.form.set({ form: 'claim', field, value: { id: termId, name: value.value } }));
                dispatch(Pub.form.set({ form: 'draft', field: 'claim.terms.secondItem', value: value.value }));
            }
        };
        const handleOnSearch = (input: string) => {
            if (input.length >= 3) {
                setSearch(input);
                // dispatch(Dictionary.searchDictionary(contentItem1.filter, lowerCase(input)));
            }
        };

        const defaultValue = {
            secondItem: () => {
                const term = termExists(secondItem?.id as string);
                const filter = termExists(selectedContent.claim.item2!.filter);
                if (term) {
                    return { value: term.name, key: term.id, title: term.longName };
                } else {
                    if (!selectedContent.claim.item2!.setFilterAsItem) {
                        return {value: secondItem?.name, key: secondItem?.id, title: startCase(secondItem?.name)};
                    } else {
                        return { value: filter?.name, key: filter?.id, title: startCase(filter?.name) };
                    }
                }
            },
            relationship: () => ({ value: relationship, key: relationshipType, title: lowerCase(relationship) }),
            ancestors: () => intersectionWith(options, dictionaryAncestors, (o, id) => o.key === id),
        };
        return (
            <>
                <StyledTermWrapper>
                    <StyledTerm>
                        <StyledTermLabel>What is the relationship</StyledTermLabel>
                        <FPSelect
                            required
                            relationship
                            field="relationship"
                            options={relOptions}
                            onChange={handleOnChange}
                            placeholder="relationship"
                            defaultValue={defaultValue.relationship()}
                        />
                    </StyledTerm>
                </StyledTermWrapper>
                <StyledTermWrapper>
                    <StyledTerm>
                        <StyledTermLabel>
                            {contentItem2?.label || 'Select a term from a dropdown below'}
                        </StyledTermLabel>
                        <FPSelect
                            required
                            searchable
                            options={options}
                            creatable={isCreatable}
                            onChange={handleOnChange}
                            onCreate={handleOnCreate}
                            onSearch={handleOnSearch}
                            field="claimItems.secondItem"
                            defaultValue={defaultValue.secondItem()}
                            loading={dictionaryStatus.type === 'LOADING'}
                            placeholder="pick a term or type in something"
                        />
                    </StyledTerm>
                    {secondItem?.id && !termExists(secondItem?.id) && (
                        <StyledTerm>
                            <FPSelect
                                searchable
                                mode="multiple"
                                field="ancestors"
                                options={options}
                                onChange={handleOnChange}
                                onSearch={handleOnSearch}
                                placeholder="add ancestors"
                                defaultValue={defaultValue.ancestors()}
                            />
                        </StyledTerm>
                    )}
                </StyledTermWrapper>
            </>
        );
    } else return null;
};
