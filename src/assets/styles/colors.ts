import { project } from 'configs';

export const Color = Object.freeze({
    grey: '#808080',
    grey_t75: 'rgba(128, 128, 128, .75)',
    orcid: '#7ed44d',
    midgrey: '#b9b9b9',
    flashpub: '#fb572a',
    flashpub_t15: 'rgba(251, 87, 42, .15)',
    flashpub_t75: 'rgba(251, 87, 42, .75)',
    notwhite: '#fafafa',
    community: project.community.color(),
    lightgrey: '#f2f2f2',
    community_t15: project.community.color(.15),
    community_t75: project.community.color(.75),
    community_t: (t: number) => project.community.color(t),

    info: "#4895ec",
    error: "#ec5f59",
    success: "#67ac5b",
    warning: "#f6c244",
});