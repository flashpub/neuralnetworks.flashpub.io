import lowerCase from 'lodash/lowerCase';

import store from 'app/store';
import { project } from 'app/config';
import { UserDefinition } from 'access/types';
import { DictionaryDefinition } from 'dictionaries/types';
import uniq from 'lodash/uniq';
import { Content } from '../../app/types';


export const prepareDictionaryData = (form: PublishStateForm, item: string) => {
    const state = store.getState();
    const dictionaries: DictionaryDefinition[] | null = state.dictionaries.list;

    const { firstItem, secondItem, firstItemAncestors, secondItemAncestors } = form.claim;
    const user: UserDefinition | null = state.access.user;
    const isFirst = item === 'first';
    const content: Content | null = state.publish.contentType;
    const firstFilter = content && content.claim.item1.filter;
    const secondFilter = content && content.claim.item2.filter;
    const ancestors = isFirst
        ? uniq([...firstItemAncestors, firstFilter, project.id ])
        : uniq([...secondItemAncestors, secondFilter, project.id ]);
    const secondItemId = secondItem && secondItem.id;
    const secondItemName = secondItem && secondItem.name;

    const getItem = () => {
        const getFirstItem = dictionaries!.filter(d => d.id === firstItem.id)[0];
        const getSecondItem = dictionaries!.filter(d => d.id === secondItemId)[0];

        const name = isFirst
            ? (getFirstItem ? getFirstItem.name : (lowerCase(firstItem.name)).split(' ').join(''))
            : (getSecondItem ? getSecondItem.name : (lowerCase(secondItemName!)).split(' ').join(''));

        const longName = isFirst
            ? (getFirstItem ? getFirstItem.longName : firstItem.name)
            : (getSecondItem ? getSecondItem.longName : secondItemName!);

        return { name, longName };
    };

    return {
        id: form.claim[`${item}Item`] && form.claim[`${item}Item`].id,
        isCommunity: false,
        ancestors,
        metrics: {
            authorCount: 0,
            pubCount: 0,
            dateAdded: Date.now(),
            dateModified: Date.now(),
        },
        description: '',
        name: getItem().name,
        longName: getItem().longName,
        origin: project.url,
        type: 'thing',
        props: {},
        author: {
            id: user!.uid,
            name: user!.name,
            orcid: user!.orcid,
            email: user!.email,
        },
    }
};
