import { Dispatch } from 'redux';

import * as AppActions from 'app/actions';
import { AlertType, Thunk } from 'app/types';
import { PortalView, sleep, Status } from 'app/utils';

import { AccessStep } from 'access/utils';
import { orcid, user } from 'access/client';
import * as AccessActions from 'access/actions';
import { LoginCredentials, ReginCredentials } from 'access/types';


export function accessAlertHandler(type: AlertType, alert: Error | { message: string }) {
    return (dispatch: Dispatch) => {
        dispatch(AccessActions.ACCESS_OPERATION_FAIL.use());
        dispatch(AppActions.ADD_ALERT.use({
            type: type,
            message: alert.message,
        }));
        sleep(200)
            .then(() => dispatch(AccessActions.SET_ACCESS_STEP.use({ step: AccessStep.DEFAULT })));
    };
}

export function loginUser(credentials: LoginCredentials): Thunk {
    return (dispatch) => {
        dispatch(AccessActions.ACCESS_OPERATION_START.use());

        return user.login(credentials)
            .catch(error => dispatch(accessAlertHandler(AlertType.ERROR, error)));
    };
}

export function registerUser(credentials: ReginCredentials): Thunk {
    return (dispatch) => {
        dispatch(AccessActions.ACCESS_OPERATION_START.use());
        
        return user.create(credentials)
            .then(response => ((response && response.status) === 200) && dispatch(loginUser(credentials)))
            .catch(error => dispatch(accessAlertHandler(AlertType.ERROR, error)));
    };
}

export function logoutUser(): Thunk {
    return (dispatch) =>
        user.logout()
            .then(() => {
                dispatch(AccessActions.SET_USER.use(null));
                dispatch(AppActions.SET_AUTH.use({ isAuth: false }));
                dispatch(AppActions.SET_PORTAL_VIEW.use({ view: PortalView.DEFAULT }));
                dispatch(AccessActions.SET_ACCESS_STEP.use({ step: AccessStep.DEFAULT }));
            })
            .catch(error => dispatch(accessAlertHandler(AlertType.ERROR, error)));
}

export function getUser(uid: string): Thunk {
    return (dispatch) =>
        user.get(uid)
            .then(user => {
                dispatch(AccessActions.SET_USER.use(user));
                dispatch(AppActions.SET_AUTH.use({ isAuth: true }));
                dispatch(AppActions.SET_APP_STATUS.use({ status: Status.SUCCESS }));
                if (user.orcid) {
                    dispatch(AppActions.SET_PORTAL_VIEW.use({ view: PortalView.DEFAULT }));
                    dispatch(AccessActions.SET_ACCESS_STEP.use({ step: AccessStep.DEFAULT }));
                } else {
                    dispatch(AccessActions.SET_ACCESS_STEP.use({ step: AccessStep.ORCID }));
                }
                dispatch(AccessActions.ACCESS_OPERATION_SUCCESS.use());
            })
            .catch(error => dispatch(accessAlertHandler(AlertType.ERROR, error)));
}

export function verifyOrcid(code: string, uid: string, uri: string): Thunk {
    return (dispatch, getState) =>
        orcid.verify(code, uid, uri)
            .then(orcid => {
                console.log('orcid verify is run', orcid);
                dispatch(AccessActions.ADD_ORCID_TO_USER.use({ orcid }));
                dispatch(AppActions.SET_APP_STATUS.use({ status: Status.SUCCESS }));
            })
            .then(() => getState().access.code && window.location.assign('/'))
            .catch(error => dispatch(accessAlertHandler(AlertType.ERROR, error)));
}

export function resetPassword(email: string): Thunk {
    return (dispatch) => {
        dispatch(AccessActions.ACCESS_OPERATION_START.use());
    
        return user.resetPassword(email)
            .then(() => {
                dispatch(AppActions.SET_PORTAL_VIEW.use({ view: PortalView.DEFAULT }));
                dispatch(accessAlertHandler(AlertType.SUCCESS, {message: 'Password reset successfully' }));
                sleep(4100)
                    .then(() =>
                        dispatch(accessAlertHandler(AlertType.INFO, {
                            message: 'In a few moments, you will receive an email with a link to reset you password'
                        }))
                    );
            })
            .catch(error => dispatch(accessAlertHandler(AlertType.ERROR, error)));
    }
}
