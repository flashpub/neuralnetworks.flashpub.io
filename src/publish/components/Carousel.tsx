import * as React from 'react';
import Slider from 'react-slick';
import sortBy from 'lodash/sortBy';
import { useHover } from 'react-use';
import styled from 'styled-components';

import { Spacing } from 'app/assets';
import { usePublish, PublishAction } from 'publish';
import { Dropzone } from 'publish/components';
import { CarouselArrow } from 'publish/components/Elements';


type CarouselProps = {
    edit: boolean;
}


export const Carousel: React.FC<CarouselProps> = ({ edit }) => {
    const [{ form }, dispatch] = usePublish();

    const handleAddSlide = (slide: Figure) => dispatch(PublishAction.SetPublishFormValue({ name: 'figures', value: slide }));
    const handleRemoveSlide = (order: number) => {
        const filtered: Figure[] = form.figures.filter((s: Figure) => s.order !== order);
        const newSlides = filtered.map((s, i) => ({ ...s, order: i }));
        dispatch(PublishAction.ReplacePublishFormValue({ name: 'figures', value: newSlides }));
    };
    const handleReplaceSlide = (slide: Figure) => {
        const newSlides: Figure[] = [];
        form.figures.map((s: Figure) => s.order === slide.order ? newSlides.push(slide) : newSlides.push(s));
        dispatch(PublishAction.ReplacePublishFormValue({ name: 'figures', value: sortBy(newSlides, s => s.order) }))
    };
    const renderSlides = () => {
        const end = Object.values(form.figures).map(s => s).length + 1;
        let array: number[] = [];
        [...form.figures, undefined].slice(0, end).map((_, i) => array.push(i));

        return array.slice(0, 5).map(s =>
            <Dropzone
                key={s}
                edit={edit}
                order={Number(s)}
                slide={form.figures[s]}
                addSlide={handleAddSlide}
                removeSlide={handleRemoveSlide}
                replaceSlide={handleReplaceSlide}
            />
        )
    };
    const slider = (isHovering: boolean) => (
        <CarouselWrapper>
            <Slider
                dots={true}
                swipe={!edit}
                prevArrow={<CarouselArrow type="left" hover={isHovering} />}
                nextArrow={<CarouselArrow type="right" hover={isHovering} />}
            >
                {renderSlides()}
            </Slider>
        </CarouselWrapper>
    );

    const [carousel, ] = useHover(slider);

    return carousel;
};

const CarouselWrapper = styled.div`
    margin-bottom: ${Spacing.XL};
    padding: ${Spacing.L} ${Spacing.M};
`;
