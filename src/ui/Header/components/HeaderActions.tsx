import * as React from 'react';
import { kebabCase } from 'lodash';
import { useMedia } from 'react-use';
import { Link } from '@reach/router';
import { Divider, Dropdown, message, Modal } from 'antd';
import { useDispatch } from 'react-redux';
import { LoginOutlined, FileAddOutlined } from '@ant-design/icons';

import { useTypedSelector } from 'helpers';
import { MOBILE, Spacing } from 'assets/styles';
import * as authentication from 'services/authentication';
import * as Pub from 'containers/Pub';
import * as Claim from 'containers/Claim';
import * as Access from 'containers/Access';
import * as Content from 'containers/Content';
import * as Dictionary from 'containers/Dictionary';
import { FPButton } from 'ui/elements';
import { StyledBrandIcon } from 'ui/elements/styles';
import {
    StyledMenuWrapper,
    StyledPublishItem,
    StyledPublishButton,
    StyledMenuUserName,
    StyledUserButtonWrapper
} from 'ui/Header/styles';

export const HeaderActions: React.FC = () => {
    const dispatch = useDispatch();
    const isDesktop = useMedia(`(min-width: ${MOBILE})`);
    const isAuth = useTypedSelector((state) => state.auth.data);
    const profile = useTypedSelector((state) => state.profile.data.user);
    const authStatus = useTypedSelector((state) => state.auth.status.type);
    const community = useTypedSelector((state) => state.community.data.community);

    const isLoading = authStatus === 'LOADING';

    const handleOnAccessOpen = () => {
        dispatch(Access.set(true));
        dispatch(Access.step('LOBBY'));
    };
    const handelOpenOrcid = () => {
        dispatch(Access.set(true));
        dispatch(Access.step('ORCID'));
    };
    const handleCommunitySelect = (c: CommunityContentType) => {
        dispatch(Content.populate(c));
        dispatch(Content.source('community'));
        dispatch(Pub.form.clear('pub'));
        dispatch(Pub.form.clear('draft'));
        dispatch(Claim.form.clear('claim'));
        dispatch(Dictionary.form.clear('term1'));
        dispatch(Dictionary.form.clear('term2'));
    };
    const handleUnavailableContentClick = (e: any) => {
        e.preventDefault();
        message.warning('This feature is not yet available');
    };
    const handleOrcidCheck = (e: any) => {
        !profile?.orcid && e.stopPropagation();
        if (!profile?.orcid) {
            Modal.confirm({
                className: 'endorse_dialog',
                okText: 'Connect Orcid',
                content: 'You have to connect your ORCID account to continue',
                async onOk() {
                    dispatch(Access.set(true));
                    dispatch(Access.step('ORCID'));
                },
                onCancel() {
                    return null;
                },
                centered: true,
            });
        } else {
            dispatch(Pub.select(null));
        }
    };
    
    const publishMenu = (
        <StyledMenuWrapper>
            {community &&
                Object.values(community.contentTypes!)
                    .sort((a, b) => a.order - b.order)
                    .map((c) => (
                        <Link
                            key={c.order}
                            onClick={() => handleCommunitySelect(c)}
                            to={`/publish/${kebabCase(c.contentTypeLabel)}`}
                        >
                            <StyledPublishItem>{c.contentTypeLabel}</StyledPublishItem>
                        </Link>
                    ))}
        </StyledMenuWrapper>
    );

    const userMenu = (
        <StyledMenuWrapper>
            <StyledMenuUserName>{profile?.name}</StyledMenuUserName>
            <Divider style={{ margin: `${Spacing._12} 0` }} />
            <Link to="/profile/library">
                <StyledPublishItem>library</StyledPublishItem>
            </Link>
            <Link to="/profile/drafts">
                <StyledPublishItem>drafts</StyledPublishItem>
            </Link>
            <Link to="/help" onClick={handleUnavailableContentClick}>
                <StyledPublishItem>help</StyledPublishItem>
            </Link>
            <Divider style={{ margin: `0 0 ${Spacing._12}` }} />
            <Link to="/profile/settings" onClick={handleUnavailableContentClick}>
                <StyledPublishItem>settings</StyledPublishItem>
            </Link>
            <StyledPublishItem onClick={() => dispatch(authentication.signoutUser())}>logout</StyledPublishItem>
            {!profile?.orcid && (
                <>
                    <Divider style={{ margin: `${Spacing._12} 0` }} />
                    <StyledPublishItem onClick={handelOpenOrcid} showDot>
                        orcid
                    </StyledPublishItem>
                </>
            )}
        </StyledMenuWrapper>
    );

    const renderHeaderActions = () => {
        if (isAuth) {
            if (isDesktop) {
                return (
                    <>
                        <Dropdown
                            trigger={['click']}
                            overlay={publishMenu}
                            placement="bottomCenter"
                        >
                            <div style={{ marginRight: Spacing._12 }}>
                                <StyledPublishButton buttonType="link" label="Publish" onClick={handleOrcidCheck} />
                            </div>
                        </Dropdown>
                        <Dropdown overlay={userMenu} placement="bottomCenter" trigger={['click']}>
                            <StyledUserButtonWrapper showDot={!(profile && profile.orcid)}>
                                <FPButton
                                    round
                                    buttonType="primary"
                                    isLoading={isLoading}
                                    icon={<StyledBrandIcon symbol={profile ? profile.initials : ''} fontSize={18} round />}
                                />
                            </StyledUserButtonWrapper>
                        </Dropdown>
                    </>
                );
            } else {
                return (
                    <>
                        <Dropdown
                            overlay={publishMenu}
                            trigger={['click']}
                            placement="bottomCenter"
                            overlayClassName="dropdown-menu"
                        >
                            <div style={{ marginRight: Spacing._4 }}>
                                <StyledPublishButton buttonType="link" label="Publish" onClick={handleOrcidCheck} />
                            </div>
                        </Dropdown>
                        <Dropdown
                            overlay={userMenu}
                            trigger={['click']}
                            placement="bottomCenter"
                            overlayClassName="dropdown-menu"
                        >
                            <div style={{ marginRight: Spacing._12 }}>
                                <FPButton
                                    round
                                    buttonType="primary"
                                    isLoading={isLoading}
                                    icon={<StyledBrandIcon symbol={profile ? profile.initials : ''} round />}
                                />
                            </div>
                        </Dropdown>
                    </>
                );
            }
        } else {
            if (isDesktop) {
                return (
                    <FPButton
                        label="Login"
                        round={isLoading}
                        buttonType="primary"
                        isLoading={isLoading}
                        onClick={handleOnAccessOpen}
                    />
                );
            } else {
                return <FPButton icon={<LoginOutlined />} isLoading={isLoading} onClick={handleOnAccessOpen} round />;
            }
        }
    };

    return <>{renderHeaderActions()}</>;
};
