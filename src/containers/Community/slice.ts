import { createGenericSlice } from 'helpers';

const initialState = {
    data: {
        community: null,
    },
    status: { type: 'PENDING', message: null },
};

export const slice = createGenericSlice({
    name: 'community',
    initialState: initialState as GenericState<CommunityContainerState>,
    reducers: {
        populate: (state, action) => {
            state.data.community = action.payload;
        },
    },
});

export const { populate, ...status } = slice.actions;

export default slice.reducer;
