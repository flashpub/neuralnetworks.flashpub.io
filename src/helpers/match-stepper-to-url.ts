import { toUpper, indexOf } from 'lodash';

export const matchStepperToUrl = (step: string) => {
    const type = toUpper(step);
    const steps = ['', 'CLAIM', 'PUBLICATION', 'SUBMIT'];
    const current = indexOf(steps, type);
    return {
        type,
        order: current,
    };
};
