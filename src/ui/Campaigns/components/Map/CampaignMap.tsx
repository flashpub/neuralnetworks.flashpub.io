import * as React from 'react';
import { navigate } from '@reach/router';
import { useDispatch } from 'react-redux';
import { kebabCase, uniq, toLower } from 'lodash';
import { TileLayer, CircleMarker, Popup, Viewport } from 'react-leaflet';

import { useTypedSelector } from 'helpers';
import * as Pub from 'containers/Pub';
import * as Claim from 'containers/Claim';
import * as Content from 'containers/Content';
import * as Dictionary from 'containers/Dictionary';
import { StyledMapWrapper, StyledMap } from 'ui/Campaigns/styles';
import { Color } from 'assets/styles';
import { MapActions, MapDrawer } from 'ui/Campaigns/index';

type Props = {
    zoom: number;
    setZoom(zoom: number): void;
};

export const CampaignMap: React.FC<Props> = ({ zoom, setZoom }) => {
    const dispatch = useDispatch();
    const campaignData = useTypedSelector((state) => state.campaigns.data.list);
    const selectedCampaign = useTypedSelector((state) => state.campaigns.data.selected);

    const [search, setSearch] = React.useState('');
    const [drawerVisible, setDrawerVisible] = React.useState(false);
    const [markers, setMarkers] = React.useState<CampaignData[]>([]);
    const [selectedCity, setSelectedCity] = React.useState<CampaignData | null>(null);

    React.useEffect(() => {
        const withPubs = campaignData.filter((c) => c.pubCount > 0);
        if (zoom >= 3) {
            const markerSet = campaignData.filter((c) => c.confirmed > 50000);
            setMarkers(uniq([...markerSet, ...withPubs]));
        }
        if (zoom >= 4) {
            const markerSet = campaignData.filter((c) => c.confirmed > 20000);
            setMarkers(uniq([...markerSet, ...withPubs]));
        }
        if (zoom >= 5) {
            const markerSet = campaignData.filter((c) => c.confirmed > 5000);
            setMarkers(uniq([...markerSet, ...withPubs]));
        }
        if (zoom >= 6) {
            const markerSet = campaignData.filter((c) => c.confirmed > 2000);
            setMarkers(uniq([...markerSet, ...withPubs]));
        }
        if (zoom >= 7) {
            const markerSet = campaignData.filter((c) => c.confirmed > 1000);
            setMarkers(uniq([...markerSet, ...withPubs]));
        }
        if (zoom >= 8) {
            const markerSet = campaignData.filter((c) => c.confirmed > 0);
            setMarkers(uniq([...markerSet, ...withPubs]));
        }
        if (search.length > 2) {
            const match = campaignData.filter((c) => {
                const city = toLower(c.city);
                return city.includes(toLower(search));
            });
            setMarkers(match);
        }
    }, [zoom, search, campaignData]);

    React.useEffect(() => {
        if (campaignData) {
            setDrawerVisible(true);
        }
    }, [campaignData]);

    const cityCampaign = Object.values(selectedCampaign?.contentTypes || {}).filter((t) =>
        t.contentTypeLabel.toLowerCase().includes('city')
    )[0];

    const handleSearch = (value: string) => setSearch(value);
    const handleMarkerClick = (m: CampaignData) => {
        setSelectedCity(m);
        !drawerVisible && setDrawerVisible(true);
    };
    const handleOpenDrawer = () => {
        setSelectedCity(null);
        !drawerVisible && setDrawerVisible(true);
    };
    const handleCloseDrawer = () => setDrawerVisible(false);
    const handleOpenPublish = (c: CommunityContentType) => {
        dispatch(Pub.form.clear('pub'));
        dispatch(Pub.form.clear('draft'));
        dispatch(Claim.form.clear('claim'));
        dispatch(Dictionary.form.clear('term1'));
        dispatch(Dictionary.form.clear('term2'));
        dispatch(Content.populate(c));
        dispatch(Content.source('campaigns'));
        navigate(`/publish/${kebabCase(c?.contentTypeLabel)}`);
    };
    const handleViewportChanged = (viewport: Viewport) => setZoom(viewport.zoom as number);

    const iconSize = (m: number) => {
        if (m < 500) return 12;
        if (m < 1000) return 16;
        if (m < 1500) return 20;
        if (m < 3000) return 26;
        if (m < 5000) return 32;
        if (m < 10000) return 38;
        if (m < 15000) return 44;
        if (m < 30000) return 50;
        return 56;
    };

    const renderMarkers = markers.map((m, i) => {
        if (isNaN(m.latitude) || isNaN(m.longitude)) return null;
        const size = iconSize(m.confirmed);
        const color = m.pubCount > 0 ? Color.flashpub : Color.grey;

        return (
            <CircleMarker key={i} center={[m.latitude, m.longitude]} color={color} radius={size / 2}>
                <Popup onOpen={() => handleMarkerClick(m)}>_</Popup>
            </CircleMarker>
        );
    });

    return (
        <StyledMapWrapper>
            <MapActions search={search} onSearch={handleSearch} onOpenPublish={handleOpenPublish} />
            <StyledMap
                zoom={zoom}
                maxZoom={9}
                center={[20, -10]}
                onclick={handleOpenDrawer}
                onViewportChanged={handleViewportChanged}
            >
                <TileLayer
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                />
                {renderMarkers}
            </StyledMap>
            <MapDrawer
                selectedCity={selectedCity}
                drawerVisible={drawerVisible}
                onOpenPublish={handleOpenPublish}
                onCloseDrawer={handleCloseDrawer}
            />
        </StyledMapWrapper>
    );
};
