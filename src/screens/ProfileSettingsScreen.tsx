import * as React from 'react';
import { RouteComponentProps } from '@reach/router';

import { SettingsContainer } from 'containers/Profile';


export const ProfileSettingsScreen: React.FC<RouteComponentProps> = () => {
    return (
        <>
            <SettingsContainer />
        </>
    );
};
