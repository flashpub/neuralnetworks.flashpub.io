import * as React from 'react';
import { Modal } from 'antd';

type Props = {
    onOk?(): any;
    title?: string;
    onCancel?(): any;
    icon?: JSX.Element;
    content?: string | JSX.Element;
};

export const useConfirmationDialog = ({ title, icon, onOk, onCancel, content, ...props }: Props) => {
    const [modal, setModal] = React.useState<any>(null);
    React.useEffect(() => {
        setModal(
            Modal.confirm({
                title,
                icon,
                content,
                onOk,
                onCancel,
                ...props,
            })
        );
    }, []);

    return modal;
};
