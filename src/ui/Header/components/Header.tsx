import * as React from 'react';
import { Link } from '@reach/router';

import { project } from 'configs';
import { HeaderActions } from 'ui/Header';
import {
    StyledIconWrapper,
    StyledHeaderWrapper,
    StyledCommunityNameWrapper,
    StyledHeaderActionsWrapper,
    StyledHeaderInternalWrapper,
} from 'ui/Header/styles';
import { useWindowWidth } from 'helpers';
import { Viewport } from 'assets/styles';

export const Header: React.FC = () => {
    const isMobile = useWindowWidth() <= Viewport.TABLET;
    
    return (
        <StyledHeaderWrapper>
            <StyledHeaderInternalWrapper>
                {!isMobile && <StyledIconWrapper xs={12}>
                  <div onClick={() => window.open('https://flashpub.io')}>{isMobile ? 'fp' : 'flashpub'}</div>
                </StyledIconWrapper>}
                <StyledCommunityNameWrapper>
                    <Link to="/">{project.community.name}</Link>
                </StyledCommunityNameWrapper>
                <StyledHeaderActionsWrapper xs={12}>
                    <HeaderActions />
                </StyledHeaderActionsWrapper>
            </StyledHeaderInternalWrapper>
        </StyledHeaderWrapper>
    );
};
