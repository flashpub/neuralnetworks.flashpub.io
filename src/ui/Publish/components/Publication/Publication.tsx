import * as React from 'react';
import { useDispatch } from 'react-redux';
import { RightCircleOutlined, SaveOutlined } from '@ant-design/icons';
import { RouteComponentProps, useNavigate, useLocation } from '@reach/router';

import { useTypedSelector, matchStepperToUrl } from 'helpers';
import * as Publish from 'containers/Publish';
import { PublicationSheet } from 'ui/Publish';
import {
    StyledStepWrapper,
    StyledStepNextButton,
    StyledPublicationButtonsWrapper,
} from 'ui/Publish/styles';

type Props = RouteComponentProps<{ contentType: string }>;

export const Publication: React.FC<Props> = ({ contentType }) => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const location = useLocation();
    const draft = useTypedSelector((state) => state.pub.data.draft);
    const step = useTypedSelector((state) => state.publish.data.step);
    const claim = useTypedSelector((state) => state.claim.data.claim);
    const term1 = useTypedSelector((state) => state.dictionary.data.term1);
    const term2 = useTypedSelector((state) => state.dictionary.data.term2);
    const publishStatus = useTypedSelector((state) => state.publish.status.type);
    const selectedContent = useTypedSelector((state) => state.content.data.type);

    const { firstItem, secondItem, relationship } = draft.claim.terms;
    const isSingle = selectedContent?.claim.type === 'single';
    const isLoading = publishStatus === 'LOADING';

    React.useEffect(() => {
        const match = matchStepperToUrl(location.pathname.split(`/${contentType}/`)[1]);
        dispatch(Publish.step({ order: match.order, type: match.type }));
    }, [contentType, location.pathname, dispatch]);

    const handleToNextStep = () => {
        dispatch(Publish.step({ order: step.order + 1, type: 'SUBMIT' }));
        navigate(`${location.origin}/publish/${contentType}/submit`);
    };

    const saveAsDraft = async () => {
        const pubDraft = {
            ...draft,
            metadata: {
                ...draft.metadata,
                draft: {
                    isDraft: true,
                    contentType: selectedContent?.contentTypeLabel,
                    claim,
                    term1,
                    term2,
                },
            },
        };
        await dispatch(
            Publish.createPub(
                {
                    pub: pubDraft,
                },
                'draft'
            )
        );
    };

    const isButtonDisabled =
        !draft.title ||
        (isSingle && !firstItem) ||
        (!isSingle && (!firstItem || !secondItem || !relationship)) ||
        (selectedContent?.figureRequired && !draft.figures[0]?.url) ||
        !draft.description;

    return (
        <StyledStepWrapper>
            <PublicationSheet pub={draft} editMode />
            <StyledPublicationButtonsWrapper>
                <StyledStepNextButton
                    label="Save Draft"
                    buttonType="link"
                    onClick={saveAsDraft}
                    isLoading={isLoading}
                    icon={<SaveOutlined />}
                />
                <StyledStepNextButton
                    iconSide="right"
                    label="Next Step"
                    buttonType="link"
                    onClick={handleToNextStep}
                    disabled={isButtonDisabled}
                    icon={<RightCircleOutlined />}
                />
            </StyledPublicationButtonsWrapper>
        </StyledStepWrapper>
    );
};
