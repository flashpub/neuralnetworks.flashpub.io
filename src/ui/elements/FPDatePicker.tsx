import * as React from 'react';
import { DatePicker } from 'antd';
import styled from 'styled-components';

import { Color, Font, Radius, Spacing } from 'assets/styles';

interface Props {
    onDateChange(date: any): void;
}

export const FPDatePicker: React.FC<Props> = ({ onDateChange }) => (
    <StyledDatePicker
        onChange={onDateChange}
        format="M/D/YYYY"
    />
);

const StyledDatePicker = styled(DatePicker)`
    width: 100%;
    height: 100%;
    box-shadow: none;
    border-radius: ${Radius};
    border-color: ${Color.midgrey};
    background-color: ${Color.notwhite};
    
    :hover {
        border-color: ${Color.midgrey};
    }
    
    .ant-picker-input {
        input {
            text-align: center;
            font-family: ${Font.text};
            margin-right: -${Spacing._x(18)};
        }
        input:hover {
          border-color: ${Color.midgrey};
        }
    }
    .ant-picker-focused {
        box-shadow: none;
        border-color: ${Color.midgrey};
    }
    
`;
