const env = process.env.NODE_ENV;
const port = window && window.location && window.location.port;
const hostname = (window && window.location && window.location.hostname).toLowerCase();

enum EnvType {
    TEST = 'test',
    PRODUCTION = 'production',
    DEVELOPMENT = 'development',
}
enum HostName {
    DEV = 'dev',
    LOCAL = 'localhost',
    STAGING = 'staging',
}

const production = hostname.indexOf(HostName.LOCAL) === -1
    && hostname.indexOf(HostName.STAGING) === -1
    && hostname.indexOf(HostName.DEV) === -1;

export const envDetector = (origin?: string) => {
    let HOST = `${process.env.REACT_APP_DEV_BACKEND_HOST}/`;
    let ORCID = `https://${hostname}/orcid`;
    let CONFIG = 'DEV';
    
    if (hostname.includes(HostName.LOCAL) && env === EnvType.DEVELOPMENT) {
        return origin === 'HOST'
            // ? HOST = `${process.env.REACT_APP_TEST_BACKEND_HOST}/`
            ? HOST = `${process.env.REACT_APP_DEV_BACKEND_HOST}/`
            : origin === 'ORCID'
                ? ORCID = `http://${hostname}:${port}/orcid`
                // : CONFIG = 'TEST';
                : CONFIG = 'DEV';
    }
    if (hostname.includes(HostName.DEV) && env === EnvType.PRODUCTION) {
        return origin === 'HOST'
            ? HOST = `${process.env.REACT_APP_DEV_BACKEND_HOST}/`
            : origin === 'ORCID'
                ? ORCID = `https://${hostname}/orcid`
                : CONFIG = 'DEV';
    }
    if (hostname.includes(HostName.STAGING) && env === EnvType.PRODUCTION) {
        return origin === 'HOST'
            ? HOST = `${process.env.REACT_APP_TEST_BACKEND_HOST}/`
            : origin === 'ORCID'
                ? ORCID = `https://${hostname}/orcid`
                : CONFIG = 'TEST';
    }
    if (production && env === EnvType.PRODUCTION) {
        return origin === 'HOST'
            ? HOST = `${process.env.REACT_APP_PROD_BACKEND_HOST}/`
            : origin === 'ORCID'
                ? ORCID = `https://${hostname}/orcid`
                : CONFIG = 'PROD';
    }
    return origin === 'HOST' ? HOST : origin === 'ORCID' ? ORCID : CONFIG;
};
