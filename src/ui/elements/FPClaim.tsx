import * as React from 'react';

import { StyledClaimWrapper, StyledTermWrapper } from 'ui/elements/styles';

type Props = {
    onClick?([string]: any): void;
    terms: PubClaim['terms'];
};

export const FPClaim: React.FC<Props> = ({ terms, ...props }) => {
    if (terms) {
        return (
            <StyledClaimWrapper {...props}>
                <StyledTermWrapper>{terms.firstItem}</StyledTermWrapper>
                {terms.secondItem ? (
                    <>
                        <div>{terms.relationship}</div>
                        <StyledTermWrapper>{terms.secondItem}</StyledTermWrapper>
                    </>
                ) : null}
            </StyledClaimWrapper>
        );
    } else return null;
};
