import uniq from 'lodash/uniq';
import lowerCase from 'lodash/lowerCase';

import store from 'app/store';
import { Content } from 'app/types';
import { project } from 'app/config';
import { DictionaryDefinition } from 'dictionaries/types';


export const prepareClaimData = (form: PublishStateForm) => {
    const { firstItem, secondItem, relationship, id, relationshipType, firstItemAncestors, secondItemAncestors } = form.claim;
    const state = store.getState();
    const content: Content | null = state.publish.contentType;
    const dictionaries: DictionaryDefinition[] | null = state.dictionaries.list;
    const isSingle = content!.claim.type === 'single';
    const firstFilter = content && content.claim.item1.filter;
    const secondFilter = !isSingle && content && content.claim.item2.filter;
    const ancestors = isSingle
        ? [...firstItemAncestors, firstFilter ]
        : [...firstItemAncestors, ...secondItemAncestors, firstFilter, secondFilter ];
    const secondItemId = secondItem && secondItem.id;
    const secondItemName = secondItem && secondItem.name;

    const getItem = (item: string) => {
        const getFirstItem = dictionaries!.filter(d => d.id === firstItem.id)[0];
        const getSecondItem = dictionaries!.filter(d => d.id === secondItemId)[0];

        const name = item === 'first'
            ? (getFirstItem ? getFirstItem.name : (lowerCase(firstItem.name)).split(' ').join(''))
            : (getSecondItem ? getSecondItem.name : (lowerCase(secondItemName!)).split(' ').join(''));

        const longName = item === 'first'
            ? (getFirstItem ? getFirstItem.longName : lowerCase(firstItem.name))
            : (getSecondItem ? getSecondItem.longName : lowerCase(secondItemName!));

        return { name, longName };
    };

    return {
        id,
        claimItems: {
            firstItem: {
                id: firstItem.id,
                name: getItem('first') ? getItem('first').name : lowerCase(firstItem.name),
            },
            secondItem: !isSingle ? {
                id: secondItemId,
                name: getItem('second') ? getItem('second').name : lowerCase(secondItemName!),
            } : null,
        },
        relationship: isSingle ? null : relationship,
        relationshipType: isSingle ? null : relationshipType,
        linkedPubIds: [form.id],
        allDictionaryIds: isSingle
            ? uniq([...ancestors, project.id, firstItem.id])
            : uniq([...ancestors, project.id, firstItem.id, secondItemId]),
    }
};
