import * as React from 'react';
import styled from 'styled-components';
import { useSelector } from 'react-redux';

import { Font } from 'app/assets';
import { Content } from 'app/types';
import { RootState } from 'app/store';

import {
    InfoPanel,
    ClaimPanel,
    SubmitPanel,
    PublicationPanel,
} from 'publish/components/Panels';
import { usePublish, PublishContainer, PublishAction } from 'publish';


const PublishPanels = () => {
    const [{ panelStack }, dispatch] = usePublish();

    const content = useSelector<RootState, Content | null>(state => state.publish.contentType);
    const contentType = content && content.contentTypeLabel;

    React.useEffect(
        () => {
            dispatch(PublishAction.ClearState());
            localStorage.removeItem('title');
        },
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [contentType]
    );

    const lastPanel = () => {
        const panel = panelStack[panelStack.length - 1];
        if (panel && panel.order === 0) return panelStack[0];
        return panel;
    };

    return (
        <PublishWrapper>
            {lastPanel().order === 0 && <InfoPanel />}
            {lastPanel().order === 1 && <ClaimPanel />}
            {lastPanel().order === 2 && <PublicationPanel />}
            {lastPanel().order === 3 && <SubmitPanel />}
        </PublishWrapper>
    );
};


export const Publish: React.FC = () => (
    <PublishContainer>
        <PublishPanels />
    </PublishContainer>
);


const PublishWrapper = styled.div`
    max-width: 800px;
    margin: auto;
    font-family: ${Font.text};
`;
