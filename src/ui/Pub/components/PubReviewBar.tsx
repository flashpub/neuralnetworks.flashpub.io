import dayjs from 'dayjs';
import * as React from 'react';
import Countdown from 'react-countdown-now';

import { StyledReviewBarWrapper, StyledReviewBarTop, StyledReviewBarBottom } from 'ui/Pub/styles';

type Props = {
    pub: PubDefinition;
};

export const PubReviewBar: React.FC<Props> = ({ pub }) => {
    return (
        <StyledReviewBarWrapper>
            <StyledReviewBarTop>
                <div />
                <span>IN REVIEW:</span>
            </StyledReviewBarTop>
            <StyledReviewBarBottom>
                <Countdown
                    date={dayjs(pub.review.deadline).toDate()}
                    renderer={({ days, hours, minutes, seconds, completed }) => {
                        if (completed) return <span> needs additional endorsements</span>;
                        return <span>{days}d {hours}h {minutes}m {seconds}s</span>;
                    }}
                />
            </StyledReviewBarBottom>
        </StyledReviewBarWrapper>
    );
};
