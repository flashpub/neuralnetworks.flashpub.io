import * as React from 'react';
import copy from 'clipboard-copy';
import styled from 'styled-components';
import { message, Tooltip } from 'antd';
import { useDispatch } from 'react-redux';
import {
    FacebookShareButton, TwitterShareButton,
    FacebookIcon, TwitterIcon, EmailIcon,
} from 'react-share';
import { CopyOutlined } from '@ant-design/icons';

import { project } from 'configs';
import { useTypedSelector } from 'helpers';
import { Color, Font, Spacing } from 'assets/styles';
import * as Publish from 'containers/Publish';
import { Dialog } from 'ui/Overlays';
import { StyledFlexWrapper } from 'ui/elements/styles';
import { FPButton } from 'ui/elements';


interface Props {
    open: boolean;
    peersCount: number;
    reviewTime: number;
}

export const Summary: React.FC<Props> = ({ open, peersCount, reviewTime }) => {
    const dispatch = useDispatch();
    const pub = useTypedSelector((state) => state.pub.data.pub);
    
    const handleOnClose = () => dispatch(Publish.step({ order: 0, type: 'INTRO' }));
    const fetchId = localStorage.getItem('fetchId');
    const url = `${project.community.url}/pub/${fetchId}`;
    const onCopy = () => {
        copy(url);
        message.info('URL copied to clipboard');
    };
    const onEmailShare = () => {
        const email = `mailto:hello@flashpub.io?subject=Shared by ${pub.author.name} with flashpub&body=Pub: ${window.location.origin}/pub/${fetchId}`;
        window.open(email);
    };
    
    return (
        <Dialog open={open}>
            <StyledDialogWrapper>
                <Text>Your article has been submitted.</Text>
                <Title>Congrats!</Title>
                <Text>After submission, your article will be in review for <strong>{reviewTime}</strong> days.</Text>
                <Text>You'll need <strong>{peersCount}</strong> colleagues to endorse the work for it to pass peer review.</Text>
                {fetchId && (
                    <>
                        <Text>Share it with your colleagues!</Text>
                        <Share>
                            <ShareButton>
                                <Tooltip title="Share on Twitter" placement="top">
                                    <TwitterShareButton url={url}>
                                        <TwitterIcon size={40} round />
                                    </TwitterShareButton>
                                </Tooltip>
                            </ShareButton>
                            <ShareButton>
                                <Tooltip title="Share on Facebook" placement="top">
                                    <FacebookShareButton url={url}>
                                        <FacebookIcon size={40} round />
                                    </FacebookShareButton>
                                </Tooltip>
                            </ShareButton>
                            <ShareButton>
                                <Tooltip title="Copy to clipboard" placement="top">
                                    <CopyToClipboard onClick={onCopy}>
                                        <CopyOutlined style={{ fontSize: 24 }} />
                                    </CopyToClipboard>
                                </Tooltip>
                            </ShareButton>
                            <ShareButton>
                                <Tooltip title="Share via Email" placement="top">
                                    <CopyToClipboard onClick={onEmailShare}>
                                        <EmailIcon size={40} round />
                                    </CopyToClipboard>
                                </Tooltip>
                            </ShareButton>
                        </Share>
                    </>
                )}
                <FPButton buttonType='link' onClick={handleOnClose} label="Close" />
            </StyledDialogWrapper>
        </Dialog>
    );
};

const StyledDialogWrapper = styled(StyledFlexWrapper)`
    font-family: ${Font.text};
`;
const Title = styled.div`
    font-size: 30px;
    font-weight: 300;
    color: ${Color.community};
    margin-bottom: ${Spacing._12};
`;
const Text = styled.p`
    font-size: 18px;
    font-weight: 200;
    text-align: center;
    color: ${Color.grey};
    margin-bottom: ${Spacing._12};
    
    strong {
        color: ${Color.community};
    }
`;
const Share = styled.div`
    display: flex;
    justify-content: center;
    margin: ${Spacing._12} 0 ${Spacing._24};
`;
const ShareButton = styled.div`
    margin: 0 ${Spacing._8};
    .SocialMediaShareButton {
        cursor: pointer;
    }
    

`;
const CopyToClipboard = styled.div`
    width: 40px;
    height: 40px;
    display: flex;
    cursor: pointer;
    border-radius: 50%;
    align-items: center;
    justify-content: center;
    background-color: ${Color.grey};
    
    svg {
        fill: #fff !important;
    }
`;
