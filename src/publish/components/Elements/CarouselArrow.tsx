import * as React from 'react';
import styled from 'styled-components';

import { Color, Font, Radius } from 'app/assets';


type Props = {
    type: string;
    hover: boolean;
    onClick?: () => void;
};

export const CarouselArrow: React.FC<Props> = ({ type, onClick, hover }) => {
    const buttonText = type === 'left' ? 'prev' : 'next';

    return (
        <ArrowWrapper type={type} onClick={onClick} hover={hover}>
            <Text>{buttonText}</Text>
        </ArrowWrapper>
    );
};

const ArrowWrapper = styled.div<{ type: string, hover: boolean }>`
    top: 50%;
    width: 20px;
    height: 50%;
    z-index: 10;
    display: flex;
    cursor: pointer;
    overflow: hidden;
    position: absolute;
    color: transparent;
    align-items: center;
    justify-content: center;
    transform: translateY(-50%);
    visibility: ${({ hover }) => hover ? 'visible' : 'hidden'};
    transition: background-color .1s ease, width .1s ease, color .1s ease;
    
    border-top-right-radius: ${({ type }) => type === 'left' && Radius};
    border-bottom-right-radius: ${({ type }) => type === 'left' && Radius};
    -moz-border-radius-topright: ${({ type }) => type === 'left' && Radius};
    -moz-border-radius-bottomright: ${({ type }) => type === 'left' && Radius};
    
    border-top-left-radius: ${({ type }) => type === 'right' && Radius};
    border-bottom-left-radius: ${({ type }) => type === 'right' && Radius};
    -moz-border-radius-topleft: ${({ type }) => type === 'right' && Radius};
    -moz-border-radius-bottomleft: ${({ type }) => type === 'right' && Radius};
    
    background-color: rgba(128,128,128,.5);
    left: ${({ type }) => type === 'left' ? 0 : 'unset' };
    right: ${({ type }) => type === 'right' ? 0 : 'unset' };
    
    &:hover {
        width: 50px;
        color: #fff;
        background-color: ${Color.grey};
    }
    
`;
const Text = styled.div`
    font-family: ${Font.text};
`;
