import { omit } from 'lodash';

import { createFormGenericSlice } from 'helpers';
import ClaimDataModel from 'models/data/claim';

const initialState = {
    data: {
        claim: ClaimDataModel,
    },
    status: { type: 'LOADING', message: null },
};

export const slice = createFormGenericSlice({
    name: 'claim',
    initialState: initialState as GenericState<ClaimContainerState>,
    reducers: {
        populate: (state, action) => {
            state.data.claim = action.payload;
        },
    },
});

const { populate, ...rest } = slice.actions;

const status = omit(rest, ['set', 'sub', 'cut', 'clear']);
const form = omit(rest, ['pending', 'loading', 'failure', 'success']);

export { populate, status, form };

export default slice.reducer;
