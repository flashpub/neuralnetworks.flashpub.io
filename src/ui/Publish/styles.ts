import styled, { css } from 'styled-components/macro';
import {
    SaveOutlined,
    LoadingOutlined,
    UserAddOutlined,
    PlusCircleOutlined,
    CloseCircleOutlined
} from '@ant-design/icons';

import { StyledFlexWrapper } from 'ui/elements/styles';
import { MaxWidth, Spacing, Radius, Color, Font, MOBILE, Media } from 'assets/styles';
import { FPButton, FPInput, FPTextArea } from 'ui/elements';

export const StyledPublishWrapper = styled(StyledFlexWrapper)`
    padding: 0;
    width: 100%;
    height: 100%;
    margin: 0 auto;
    min-height: 700px;
    max-width: ${MaxWidth};
    justify-content: flex-start;
    
    ${Media.tablet} {
        padding: 0 ${Spacing._24};
    }
`;
export const StyledStepperWrapper = styled(StyledFlexWrapper)`
    width: 100%;
    padding: 0 ${Spacing._24};
    border-radius: ${Radius};
    
    ${Media.tablet} {
        height: 40px;
        padding: ${Spacing._8};
        background-color: ${Color.community_t15};
    }
`;
export const StyledStepWrapper = styled(StyledFlexWrapper)`
    width: 100%;
    height: fit-content;
    font-family: ${Font.text};
        padding: 0 ${Spacing._24};
    
    ${Media.tablet} {
        padding: 0 ${Spacing._24};
        margin: ${Spacing._48} 0 ${Spacing._x(96)};
    }
`;
export const StyledStepTitle = styled.div`
    font-size: 30px;
    font-weight: 300;
    text-align: center;
    color: ${Color.community};
    margin-bottom: ${Spacing._48};
    
    ${Media.tablet} {
        text-align: unset;
    }
`;
export const StyledDiagramWrapper = styled.div`
    width: 250px;
    height: 250px;
    margin-bottom: ${Spacing._48};
    
    svg {
        width: 100%;
        height: 250px;
    }
`;
export const StyledStepDescription = styled.p`
    width: 100%;
    font-size: 20px;
    max-width: 800px;
    font-weight: 200;
    text-align: center;
    color: ${Color.grey};
    font-family: ${Font.text};
    margin-bottom: ${Spacing._48};
`;
export const StyledStepNextButton = styled(FPButton)`
    font-size: 24px;
    
    ${Media.tablet} {
        margin: ${Spacing._4} 0;
    }
`;
export const StyledClaimFieldsWrapper = styled(StyledFlexWrapper)`
    width: 100%;
    flex-direction: row;
    align-items: flex-start;
    margin-bottom: ${Spacing._48};
`;
export const StyledTermWrapper = styled(StyledFlexWrapper)`
    width: 100%;
`;
export const StyledTerm = styled(StyledFlexWrapper)<{ single?: boolean }>`
    width: 100%;
    margin-top: ${Spacing._24};
    
    ${Media.tablet} {
        width: ${({ single }) => (single ? '450px' : '300px')};
    }
`;
export const StyledTermLabel = styled(StyledFlexWrapper)<{ single?: boolean }>`
    width: 100%;
    height: 38px;
    font-size: 14px;
    line-height: 16px;
    text-align: center;
    color: ${Color.grey};
    background-color: #fff;
`;
export const StyledConditionsWrapper = styled(StyledFlexWrapper)<{ editMode?: boolean }>`
    width: 100%;
    margin-bottom: ${({ editMode }) => editMode ? 0 : Spacing._48};
    align-items: ${({ editMode }) => editMode ? 'flex-start' : 'center'};
    
    ${Media.tablet} {
        width: ${({ editMode }) => editMode ? '100%' : '450px'};
    }
`;
export const StyledConditionsContentWrapper = styled(StyledFlexWrapper)`
    width: 100%;
    max-width: 500px;
    padding: ${Spacing._12};
    justify-content: flex-start;
    
    p {
        font-size: 16px;
        margin-bottom: 0;
        color: ${Color.midgrey};
        font-family: ${Font.text};
    }
    
    button {
        height: 34px;
        font-size: 14px;
        font-family: ${Font.text};
    }
`;
export const StyledConditionsInputWrapper = styled(StyledFlexWrapper)`
    width: 100%;
    text-align: center;
    margin: ${Spacing._24} 0;

    p {
        margin-top: ${Spacing._4};
    }
    input {
        margin: 0;
        height: 34px;
        text-align: left;
        ::placeholder {
            text-align: center;
        }
    }
    
    ${Media.tablet} {
        text-align: unset;
        flex-direction: row;
        align-items: flex-start;
    }
`;
export const StyledConditionNameInput = styled(FPInput)`
    margin: 0;
    width: 180px;
    height: 34px;
`;
export const StyledConditionValueInputWrapper = styled(StyledFlexWrapper)`
    width: 290px;
    flex-direction: row;
    align-items: flex-start;
`;
export const StyledConditionTypeInput = styled.div`
    width: 110px;
`;
export const StyledConditionValueInput = styled.div<{ fluid?: boolean }>`
    width: 190px;
    height: ${({ fluid }) => fluid ? 'fit-content' : '34px'};
`;
export const StyledAddedConditions = styled(StyledFlexWrapper)<{ editMode?: boolean }>`
    width: 100%;
    font-family: ${Font.text};
    margin-top: ${({ editMode }) => editMode ? 0 :Spacing._24};
`;
export const StyledRemoveCondition = styled(CloseCircleOutlined)`
    visibility: hidden;
    display: inline-block;
    margin-right: ${Spacing._8};
    :hover {
        cursor: pointer;
        color: ${Color.community};
    }
`;
export const StyledCondition = styled(StyledFlexWrapper)<{ editMode?: boolean }>`
    width: 100%;
    line-height: 26px;
    position: relative;
    flex-direction: row;
    color: ${Color.grey};
    border-bottom: 1px dashed ${Color.lightgrey};
    font-size: ${({ editMode }) => editMode ? '16px' : '18px'} !important;
    
    &:hover ${StyledRemoveCondition} {
        visibility: visible;
    }
    
    div {
        width: 50%;
        display: flex;
        text-align: right;
        align-items: center;
        justify-content: flex-end;
    }
    em {
        width: 50%;
        font-weight: 300;
        margin-left: ${Spacing._4};
    }
`;
export const StyledDefaultConditionsWrapper = styled(StyledFlexWrapper)`
    flex-wrap: wrap;
    line-height: 1.3;
    flex-direction: row;
`;
export const StyledDefaultCondition = styled.p`
    cursor: pointer;
    position: relative;
    padding: 0 ${Spacing._x(8)};
    :hover {
        color: ${Color.community};
    }
    ::before, ::after {
        top: 50%;
        width: 2px;
        right: -1px;
        height: 2px;
        content: "";
        position: absolute;
        transform: translateY(-50%);
        background-color: ${Color.grey};
    }
    ::after {
        left: -1px;
    }
`;
export const StyledConditionsEditModeButton = styled.div`
    cursor: pointer;
    font-size: 12px;
    font-weight: 300;
    border-radius: ${Radius};
    color: ${Color.community};
    font-family: ${Font.text};
    border: 1px solid ${Color.community};
    padding: ${Spacing._x(2)} ${Spacing._8};
    :hover {
        background-color: ${Color.community_t15};
    }
`;
export const StyledAuthorsWrapper = styled(StyledFlexWrapper)`
    flex-wrap: wrap;
    font-size: 18px;
    font-weight: 100;
    flex-direction: row;
    font-family: ${Font.text};
    justify-content: flex-start;
    
    div#main-author {
        font-weight: 400;
    }
    
    div:not(:last-child):after {
        content: ",";
        margin-right: ${Spacing._4};
    }
`;
export const StyledRemoveIcon = styled.div`
    top: 50%;
    left: 50%;
    width: 100%;
    height: 1px;
    position: absolute;
    visibility: hidden;
    background-color: #000;
    transform: translate(-50%, -50%);
    :focus {
        outline: none;
    }
`;
export const StyledAuthor = styled.div<{ co?: boolean }>`
    font-size: 14px;
    width: fit-content;
    position: relative;
    display: inline-block;
    border-radius: ${Radius};
    &:hover {
        cursor: pointer;
        color: ${({ co }) => co && Color.midgrey};
        background-color: ${({ co }) => co && Color.lightgrey};
    }
    &:hover ${StyledRemoveIcon} {
        visibility: visible;
    }
    ${Media.tablet} {
        font-size: 18px;
    }
`;
export const StyledFormWrapper = styled.div`
    width: 300px;
`;
export const StyledTitle = styled.div`
    font-size: 14px;
    text-align: center;
    color: ${Color.grey};
    margin: 0 0 ${Spacing._8};
    font-family: ${Font.text};
`;
export const StyledContributorField = styled(FPInput)`
    height: 26px;
    font-size: 12px;
    margin: 0 0 ${Spacing._8};
`;
export const StyledAddContributor = styled(FPButton)<{ disabled?: boolean }>`
    margin: 0;
    padding: 0;
    width: 100%;
    height: 26px;
    font-size: 14px;
    font-weight: 400;
    font-family: ${Font.text};
    cursor: ${({ disabled }) => disabled && 'not-allowed'};
    background-color: ${({ disabled }) => disabled && Color.lightgrey};
`;
export const StyledDate = styled.div`
    font-size: 14px;
    font-family: ${Font.text};
`;
export const StyledPublicationSheetWrapper = styled(StyledFlexWrapper)`
    width: 100%;
    z-index: 11;
    max-width: 796px;
    background-color: #fff;
    border-radius: ${Radius};
    margin-bottom: ${Spacing._4};
    
    ${Media.tablet} {
        padding: ${Spacing._24} ${Spacing._48};
    }
`;
export const StyledPublicationSheetHeadWrapper = styled(StyledFlexWrapper)`
    width: 100%;
    align-items: flex-start;
`;
export const StyledPublicationSheetContentWrapper = styled(StyledFlexWrapper)`
    width: 100%;
`;
export const StyledPublicationSheetMetadataWrapper = styled(StyledPublicationSheetHeadWrapper)``;
export const StyledPublicationSheetTitleWrapper = styled.div`
    width: 100%;
    font-size: 26px;
    max-width: 700px;
    position: relative;
`;
export const StyledPublicationSheetTitle = styled(FPTextArea)<{ value?: string; disabled?: boolean }>`
  && {
    font-size: 20px;
    max-width: 800px;
    text-align: start;
    color: ${Color.community};
    transition: padding 0.2s ease;
    padding: ${({ value }) => (value ? '4px 0' : '4px 8px')};
    cursor: ${({ disabled }) => (disabled ? 'default' : 'text')};
    &:hover,
    &:focus {
        padding: ${({ disabled }) => (disabled ? '4px 0' : '4px 8px')};
    }

    @media (min-width: ${MOBILE}) {
        font-size: 28px;
    }
  }
`;
export const StyledPublicationSheetDescriptionWrapper = styled.div`
    width: 100%;
    position: relative;
`;
export const StyledMetadataAssetForm = styled.div`
    width: 300px;
    font-size: 14px;
`;

export const StyledMetadataAssetField = styled(FPInput)`
    height: 26px;
    font-size: 12px;
    margin: 0 0 ${Spacing._8};
`;
export const StyledMetadataAssetUrl = styled(StyledMetadataAssetField)``;
export const StyledMetadataAssetName = styled(StyledMetadataAssetField)``;
export const StyledMetadataAssetWrapper = styled.div<{ disabled: boolean }>`
    position: relative;
    margin: ${Spacing._4} 0;
    border-radius: ${Radius};
    transition: padding 0.2s ease;
    &:hover {
        cursor: pointer;
        background-color: ${Color.lightgrey};
        padding: ${({ disabled }) => (disabled ? 0 : `0 ${Spacing._4}`)};
    }
    svg:hover {
        fill: ${Color.community};
    }
`;
export const StyledMetadataAssetLink = styled.span`
    font-size: 14px;
    flex: 0 0 auto;
    vertical-align: center;
    margin-left: ${Spacing._4};
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    width: 300px;
    display: inline-block;
    a {
        width: 100%;
        font-weight: 200;
        color: ${Color.community};
        overflow: hidden !important;
        &:hover {
            color: ${Color.flashpub};
        }
    }
    
    ${Media.tablet} {
        overflow: unset;
        white-space: unset;
        text-overflow: unset;
        width: unset;
        display: unset;
    }
`;
export const StyledMetadataSelect = styled.select`
    width: 100%;
    height: 26px;
    padding: 4px;
    color: #808080;
    font-size: 12px;
    margin: 0 0 8px;
    box-shadow: none;
    font-weight: 300;
    border-width: 1px;
    text-align: center;
    border-style: solid;
    border-color: #b9b9b9;
    border-radius: 0.33rem;
    font-family: ${Font.text};
    background-color: #fafafa;
`;
export const StyledMetadataAddAssetButton = styled(FPButton)<{ disabled?: boolean }>`
    margin: 0;
    padding: 0;
    width: 100%;
    height: 26px;
    font-size: 14px;
    font-weight: 400;
    font-family: ${Font.text};
    cursor: ${({ disabled }) => disabled && 'not-allowed'};
    background-color: ${({ disabled }) => disabled && Color.lightgrey};
`;
export const StyledMetadataBox = styled.div<{ disabled: boolean }>`
    display: flex;
`;
export const StyledMetadataName = styled.div`
    width: 80px;
    display: inline-block;
    margin-right: ${Spacing._8};
`;
export const StyledMetadataWrapper = styled.div`
    width: 100%;
    text-align: left;
    color: ${Color.grey};
    font-family: ${Font.text};
    *:focus {
        outline: none;
    }
`;
export const StyledMetadataList = styled.div`
    padding: 0;
    width: 100%;
    border-radius: ${Radius};
    margin-bottom: ${Spacing._12};
`;
const StyledIconButton = css`
    width: 30px;
    height: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 100%;
    background-color: #fff;
    margin-left: ${Spacing._8};
    box-shadow: 0px 0px 10px 0px ${Color.community_t75};

    svg {
        width: 22px;
        height: 22px;
        fill: ${Color.community};
    }

    &:hover {
        background-color: ${Color.community_t15};
    }
`;
export const StyledAddUser = styled(UserAddOutlined)`
    ${StyledIconButton};
    width: 28px;
    height: 28px;
`;
export const StyledAddAsset = styled(PlusCircleOutlined)`
    ${StyledIconButton};
    width: 24px;
    height: 24px;
    svg {
        width: 20px;
        height: 20px;
    }
`;
export const StyledSaveAsDraft = styled(SaveOutlined)`
    ${StyledIconButton};
    width: 40px;
    height: 40px;
    margin-right: ${Spacing._12};
    svg {
        width: 30px;
        height: 30px;
    }
`;
export const StyledSaveAsDraftLoading = styled(LoadingOutlined)`
    ${StyledIconButton};
    width: 40px;
    height: 40px;
    margin-right: ${Spacing._12};
    svg {
        width: 30px;
        height: 30px;
    }
`;
export const StyledSubmitSuggestedReviewers = styled(StyledFlexWrapper)`
    font-size: 14px;
    color: ${Color.grey};
    margin-bottom: ${Spacing._48};
    p {
        font-size: 20px;
    }
`;
export const StyledSubmitSuggestedReviewer = styled(FPInput)`
    width: 300px;
    height: 30px;
`;
export const StyledSubmitCheckboxCertify = styled.div`
    width: 100%;
    max-width: 700px;
    text-align: center;
    color: ${Color.grey};
    margin-bottom: ${Spacing._48};
    .ant-checkbox-checked .ant-checkbox-inner {
        border-color: ${Color.community};
        background-color: ${Color.community};
    }
`;
export const StyledLegalLabel = styled.span`
    span {
        color: ${Color.community};
    }
`;
export const StyledPublicationButtonsWrapper = styled(StyledFlexWrapper)`
    z-index: 20;
    
    ${Media.tablet} {
        flex-direction: row;
    }
`;
