import * as React from 'react';
import { createReducer } from 'yartsul';

import { Actions } from 'publish';
import { FUIPanelProps } from 'FUI/components';


interface PublishState {
    panelStack: FUIPanelProps[];
    form: PublishStateForm;
}

const initialState: PublishState = {
    panelStack: [
        { order: 0, title: 'Introduction' },
    ],
    form: {
        id: '',
        title: '',
        description: '',
        claim: {
            id: '',
            firstItem: {id: '', name: ''},
            firstItemAncestors: [],
            secondItem: null,
            secondItemAncestors: [],
            relationship: '',
            conditions: [],
            relationshipType: null,
        },
        contributors: [],
        figures: [],
        metadata: {
            code: [],
            datasets: [],
            protocols: [],
            references: [],
            isDeIdentified: false,
        },
        suggested: {
            reviewer_1: '',
            reviewer_2: '',
            reviewer_3: '',
            reviewer_4: '',
        }
    },
};

const PublishDispatchContext = React.createContext<any>(undefined);
const PublishStateContext = React.createContext<PublishState | undefined>(undefined);

const PublishAction = Actions;

const publishReducer = createReducer<PublishState>(
    initialState,

    PublishAction.SetPublishPanel.handler((state, payload) => { state.panelStack = [payload, ...state.panelStack].sort((a,b) => a.order! - b.order!) }),
    PublishAction.CutPublishPanel.handler(state => { state.panelStack = state.panelStack.slice(0,-1) }),
    PublishAction.SetPublishFormValue.handler((state, payload) => {
        const split = payload.name.split('.');
        const key = split[0];
        const field = split[1];

        if (field) {
            if (!Array.isArray(state.form[key][field])) {
                state.form = {...state.form, [key]: { ...state.form[key], [field]: payload.value }}
            } else {
                state.form = {...state.form, [key]: {...state.form[key], [field]: [...state.form[key][field], payload.value]}}
            }
        } else {
            if (Array.isArray(state.form[key])) {
                state.form = {...state.form, [key]: [...state.form[key], payload.value]}
            } else {
                state.form = {...state.form, [key]: payload.value}
            }
        }
    }),
    PublishAction.CutPublishFormValue.handler((state, payload) => {
        const split = payload.name.split('.');
        const key = split[0];
        const field = split[1];

        if (field) {
            if (!Array.isArray(state.form[key][field])) {
                state.form = {...state.form, [key]: {
                    ...state.form,
                    [key]: {...state.form[key], [field]: initialState.form[key][field]}
                }}
            } else {
                state.form = {
                    ...state.form,
                    [key]: {...state.form[key], [field]: state.form[key][field].filter((k: any) =>
                        k.id !== payload.id).filter((k: any) => k !== payload.id)}
                }
            }
        } else {
            if (Array.isArray(state.form[key])) {
                state.form = {...state.form, [key]: state.form[key].filter((k: any) =>
                    k.id !== payload.id).filter((k: any) => k !== payload.id)}
            } else {
                state.form = {...state.form, [key]: initialState.form[key]}
            }
        }
    }),
    PublishAction.ReplacePublishFormValue.handler((state, payload) => {
        const split = payload.name.split('.');
        const key = split[0];
        const field = split[1];

        if (field) {
            state.form = {...state.form, [key]: {...state.form[key], [field]: payload.value}}
        } else {
            state.form = {...state.form, [key]: payload.value}
        }
    }),
    PublishAction.ClearState.handler(state => {
        state.form = initialState.form;
        state.panelStack = initialState.panelStack;
    })
);

const usePublishState = () => {
    const context = React.useContext(PublishStateContext);
    if (context === undefined) {
        throw new Error('usePublishState must be used within a PublishContainer')
    }
    return context
};

const usePublishDispatch = () => {
    const context = React.useContext(PublishDispatchContext);
    if (context === undefined) {
        throw new Error('usePublishDispatch must be used within a PublishContainer')
    }
    return context
};

const usePublish = () => [usePublishState(), usePublishDispatch()];

const PublishContainer: React.FC = ({ children }) => {
    const [state, dispatch] = React.useReducer(publishReducer, initialState);
    return (
        <PublishStateContext.Provider value={state}>
            <PublishDispatchContext.Provider value={dispatch}>
                {children}
            </PublishDispatchContext.Provider>
        </PublishStateContext.Provider>
    );
};

export { PublishContainer, usePublish, PublishAction, usePublishDispatch, usePublishState }
