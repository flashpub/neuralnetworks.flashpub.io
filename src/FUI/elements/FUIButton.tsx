import * as React from 'react';
import styled from 'styled-components';
import { Spinner } from '@blueprintjs/core';

import { Status } from 'app/utils';
import { Radius, Spacing, Color } from 'app/assets';


type ButtonProps = {
    text: string,
    loading?: Status,
    onClick?(): void,
    disabled?: boolean,
    buttonType?: 'outlined' | 'text' | undefined,
    type?: 'submit' | 'reset' | 'button' | undefined,
};

export const FUIButton: React.FC<ButtonProps> = props => (
    <StyledButton
        {...props}
        onClick={props.onClick}
        disabled={props.disabled}
    >
        {props.loading === Status.LOADING ? <StyledSpinner /> : props.text}
    </StyledButton>
);


const StyledSpinner = styled(Spinner)` && {
    svg {
        width: 32px;
        height: 32px;
    }
}`;
const StyledButton = styled.button<ButtonProps>`
    cursor: pointer;
    font-size: 16px;
    border-radius: ${Radius};
    padding: ${Spacing.SM} ${Spacing.L};
    transition: background-color .15s ease;
    border: ${props => props.buttonType === 'outlined'
        ? `1px solid`
        : 'none'
    };
    color: ${props => {
        if (props.disabled) return Color.midgrey;
        return typeof props.buttonType === 'undefined'
            ? '#fff'
            : Color.community
    }};
    background-color: ${props => typeof props.buttonType === 'undefined'
        ? Color.community
        : 'transparent'
    };
    
    &:focus {
        outline: none;
        outline-offset: 0;
    }
    
    &:hover {
        background-color: ${props => {
            if (props.disabled) return 'none';
            return typeof props.buttonType === 'undefined'
                ? Color.community_t75
                : Color.community_t15
        }}
    }
`;
