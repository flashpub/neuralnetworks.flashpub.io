import { firebase, project } from "configs";
import { connector } from "api/connector";

const version = "v0";

export const user = {
  signin: (creds: Credentials) =>
    firebase.auth.signInWithEmailAndPassword(creds.email, creds.password),
  create: (creds: Credentials) =>
    connector
      .post(`/access/${version}/users`, creds)
      .then((response) => response),
  get: (uid: string) => firebase.db.collection("users").doc(uid).get(),
  logout: () => firebase.auth.signOut(),
};

export const orcid = {
  verify: (code: string, uid: string, uri: string) =>
    connector
      .post(`/access/${version}/orcid`, { code, uid, uri })
      .then((response) => response),
};

export const dictionary = {
  load: () =>
    firebase.db
      .collection("dictionary")
      .where("ancestors", "array-contains", project.community.id)
      .orderBy("metrics.pubCount", "desc")
      .get(),
  search: (filter: string, input: string) =>
    firebase.db
      .collection("dictionary")
      .where("ancestors", "array-contains", filter)
      .where("name", ">=", input)
      .limit(10)
      .get(),
};

export const pub = {
  load: (id: string) => firebase.db.collection("pubs").doc(id).get(),
  create: (body: PubDefinition) =>
    connector.post(`/content/${version}/publish_new`, body),
  update: (body: PubDefinition) =>
    connector.put(`/content/${version}/pub/${body.id}`, body),
  updateArrayField: (
    pubId: string,
    field: string,
    value: string,
    operation: "+" | "-"
  ) => {
    return firebase.db
      .collection("pubs")
      .doc(pubId)
      .update({
        [field]:
          operation === "+"
            ? firebase.firebase.firestore.FieldValue.arrayUnion(value)
            : firebase.firebase.firestore.FieldValue.arrayRemove(value),
      });
  },
  delete: (id: string) => firebase.db.collection("pubs").doc(id).delete(),
};

export const pubs = {
  load: (filter: string, top: number) =>
    firebase.db
      .collection("pubs")
      .where("metadata.allDictionaryIds", "array-contains", filter)
      .orderBy("metadata.datePublished", "desc")
      .limit(top)
      .get(),
  drafts: (uid: string) =>
    firebase.db
      .collection("pubs")
      .where(
        "metadata.allDictionaryIds",
        "array-contains",
        project.community.id
      )
      .where("author.id", "==", uid)
      .where("metadata.draft.isDraft", "==", true)
      .get(),
  bookmarked: (uid: string) =>
    firebase.db
      .collection("pubs")
      .where("bookmarkedBy", "array-contains", uid)
      .get(),
};

export const community = {
  load: (community: string) =>
    firebase.db.collection("communities").doc(community).get(),
};

export const file = {
  upload: (file: File, pubId: string) =>
    firebase.storage.ref().child(`figures/${pubId}/${file.name}`).put(file),
  deleteAll: (pubId: string) =>
      connector.delete(`/content/${version}/files/${pubId}`),
  delete: (fileName: string, pubId: string) =>
    firebase.storage.ref().child(`figures/${pubId}/${fileName}`).delete(),
};

export const system = {
  gates: () => firebase.db.collection("system").doc("gates").get(),
  notifications: {
    load: () =>
      firebase.db
        .collection("system")
        .doc("notifications")
        .collection("update")
        .doc("version")
        .get(),
    update: (userId: string) =>
      firebase.db
        .collection("system")
        .doc("notifications")
        .collection("update")
        .doc("version")
        .update({
          seenBy: firebase.firebase.firestore.FieldValue.arrayUnion(userId),
        }),
  },
};

export const campaigns = {
  load: (community: string) =>
    firebase.db
      .collection("campaigns")
      .where("community", "==", community)
      .get(),
    data: (campaign: string) =>
        firebase.db
            .collection("campaigns")
            .doc(campaign)
            .collection("campaignData")
            .where("pubCount", ">=", 0)
            .orderBy("pubCount", "asc")
            .get(),
  dataPerPubCount: (campaign: string, pubCount: number) =>
    firebase.db
      .collection("campaigns")
      .doc(campaign)
      .collection("campaignData")
      .where("pubCount", ">", pubCount)
      .get(),
  dataPerCaseCount: (campaign: string, caseCount: number) =>
    firebase.db
      .collection("campaigns")
      .doc(campaign)
      .collection("campaignData")
      .where("confirmed", ">", caseCount)
      .get(),
};
