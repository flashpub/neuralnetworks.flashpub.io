import * as React from 'react';
import shortid from 'shortid';
import styled from 'styled-components';
import { useDebounce } from 'react-use';
import { useSelector } from 'react-redux';
import { useDropzone } from 'react-dropzone';
import { Icon as UIIcon } from '@blueprintjs/core';

import { Content } from 'app/types';
import { RootState } from 'app/store';
import { envDetector } from 'app/utils';
import { Color, Font, Radius, Spacing } from 'app/assets';
import { usePublish } from 'publish';
import { FUIInput } from 'FUI/elements';

type DropzoneProps = {
    edit: boolean;
    order: number;
    slide: Figure;
    addSlide(slide: Figure): void;
    removeSlide(order: number): void;
    replaceSlide(slide: Figure): void;
}


export const Dropzone: React.FC<DropzoneProps> = ({ slide, order, addSlide, removeSlide, replaceSlide, edit }) => {
    const [{ form },] = usePublish();
    const [isAdded, setIsAdded] = React.useState<boolean>(!!(slide && slide.preview));
    const [label, setLabel] = React.useState<string>((slide && slide.label) || '');
    const content = useSelector<RootState, Content | null>(state => state.publish.contentType);
    const { getRootProps, getInputProps, isDragActive, isDragAccept, isDragReject } = useDropzone({
        disabled: !edit,
        multiple: false,
        maxSize: 2621440,
        accept: 'image/*',
        onDropAccepted: (file) => onDropAccepted(file),
        onDropRejected: () => console.log('drop rejected'),
    });

    const isFigureRequired = content!.figureRequired && !isAdded && !(form.figures[0] && form.figures[0].preview);

    const addLabel = () => {
        const newSlide = {...slide, label};
        replaceSlide(newSlide)
    };

    const [,] = useDebounce(() => addLabel(), 500, [label]);

    React.useEffect(() => {
        return () => {
            if (slide && slide.preview) {
                setIsAdded(true);
            } else {
                setLabel('');
                setIsAdded(false);
            }
        }
    });

    const onDropAccepted = (file: File[]) => {
        let url: string;

        switch (envDetector()) {
            case 'DEV':
                url = `https://firebasestorage.googleapis.com/v0/b/flashpub-dev.appspot.com/o/figures%2F${file[0].name}?alt=media`;
                break;
            case 'TEST':
                url = `https://firebasestorage.googleapis.com/v0/b/flashpub-staging-489e3.appspot.com/o/figures%2F${file[0].name}?alt=media`;
                break;
            case 'PROD':
                url = `https://firebasestorage.googleapis.com/v0/b/flashpub-67105.appspot.com/o/figures%2F${file[0].name}?alt=media`;
                break;
            default:
                url = `https://firebasestorage.googleapis.com/v0/b/flashpub-dev.appspot.com/o/figures%2F${file[0].name}?alt=media`;
        }

        const id = shortid();
        // const split = file[0].name.split('.');
        // const type = split[split.length - 1];
        // const newName = `${id}.${type}`;
        // const updatedFile = {
        //     ...file[0],
        //     path: newName,
        //     name: newName,
        //     lastModified: file[0].lastModified,
        //     size: file[0].size,
        //     type: file[0].type,
        // };
        const newSlide = {
            ...file[0],
            id,
            url,
            file: file[0],
            // file: updatedFile,
            order,
            type: file[0].type,
            label: '',
            preview: URL.createObjectURL(file[0]),
        };

        if (isAdded) {
            replaceSlide(newSlide);
        } else {
            addSlide(newSlide);
            setIsAdded(true);
        }
    };

    const onRemoveImage = () => {
        removeSlide(order);
        setLabel('');
        setIsAdded(false);
    };

    const onLabelChange = (value: string) => setLabel(value);

    const preview = (
        <Preview>
            <Order>{String.fromCharCode(97 + order)}</Order>
            <Thumbnail src={slide && slide.preview}/>
            <RemoveArea onClick={onRemoveImage}>
                <Icon icon="trash" iconSize={30} />
            </RemoveArea>
        </Preview>
    );

    return (
        <DropzoneWrapper>
            {isAdded ? <Label
                counter
                chars={45}
                value={label}
                onChange={onLabelChange}
                placeholder="figure title..."
            /> : <div style={{ height: labelHeight }} />}
            <DropArea
                {...getRootProps()}
                added={isAdded}
                active={isDragActive}
                rejected={isDragReject}
                accepted={isDragAccept}
                required={isFigureRequired}
            >
                <input {...getInputProps()} />
                <Text active={isDragActive}>
                    {(slide && slide.preview) ? '' : isDragAccept
                        ? 'Drop figure'
                        : isDragReject
                            ? 'Incorrect format!'
                            : <div style={{ textAlign: 'center' }}>
                                <div>*Drag and drop an image or click to upload</div>
                                <div>(use 1000x800px PNG for best results)</div>
                            </div>
                    }
                </Text>
                {preview}
            </DropArea>
        </DropzoneWrapper>
    );
};


const labelHeight = 30;

const DropzoneWrapper = styled.div`
    height: 400px;
`;
const Label = styled(FUIInput)<{ value: boolean }>` && {
    margin: 0;
    font-size: 16px;
    background-color: #fff;
    height: ${labelHeight}px;
    font-family: ${Font.text};
    border-color: ${({ value }) => value ? '#fff' : Color.midgrey};
    &:focus { border-color: ${Color.midgrey} }
    &:hover { border-color: ${Color.midgrey} }
}`;
const DropArea = styled.div<{ required: boolean, rejected: boolean, active: boolean, accepted: boolean, added: boolean }>`
    width: 100%;
    display: grid;
    cursor: pointer;
    border-width: 1px;
    position: relative;
    align-items: center;
    justify-items: center;
    border-radius: ${Radius};
    height: calc(100% - ${labelHeight}px);
    border-style: ${({ rejected, active }) => rejected || active ? 'solid' : 'dashed'};
    border-color: ${({ rejected, active, added }) => rejected || active ? Color.community : added ? 'transparent' : Color.midgrey};
    background-color: ${({ rejected, accepted }) => rejected ? Color.error : accepted ? Color.success : 'transparent' };
    &:focus { outline: none }
    &::before {
        top: 2px;
        left: 4px;
        position: absolute;
        color: ${Color.error};
        content: ${({ required }) => required ? "'*'" : "''"};
    }
`;
const Order = styled.div`
    top: 0;
    left: 0;
    width: 30px;
    height: 30px;
    display: flex;
    font-size: 30px;
    position: absolute;
    align-items: center;
    color: ${Color.grey};
    padding: ${Spacing.S};
    background-color: #fff;
    justify-content: center;
    font-family: ${Font.mono};
    border-bottom-right-radius: ${Radius};
    -moz-border-radius-bottomright: ${Radius};
`;
const Icon = styled(UIIcon)` && {
    svg { fill: ${Color.grey} !important }
}`;
const RemoveArea = styled.div`
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    display: none;
    position: absolute;
    background-color: rgba(255,255,255,.75);
`;
const Preview = styled.div`
    height: 100%;
    overflow: hidden;
    position: relative;
    box-sizing: border-box;
    
    &:hover ${RemoveArea} {
        display: flex;
        align-items: center;
        justify-content: center;
    }
`;
const Thumbnail = styled.img`
    margin: 0;
    width: auto;
    height: 100%;
    display: block;
`;
const Text = styled.div<{ active: boolean }>`
    top: 50%;
    left: 50%;
    font-weight: 200;
    position: absolute;
    transform: translate(-50%, -50%);
    color: ${({ active }) => active ? `#fff` : Color.midgrey};
`;
