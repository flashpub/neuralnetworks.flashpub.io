import * as React from 'react';
import { RouteComponentProps, useLocation } from '@reach/router';

import { ListContainer } from 'containers/List';
import { List } from 'ui/List';


export const ListScreen: React.FC<RouteComponentProps> = () => {
    const location = useLocation();
    const [listType, setListType] = React.useState<ListType>('list');

    React.useEffect(() => {
        const isProfile = location.pathname.includes('/profile/');
        if (isProfile) {
            const path = location.pathname.split('/profile/')[1] as ListType;
            setListType(path);
        } else {
            const path = location.pathname.split('/')[1] as ListType;
            setListType(path);
        }
    }, [location.pathname]);

    return (
        <ListContainer type={listType}>
            <List type={listType} />
        </ListContainer>
    );
};
