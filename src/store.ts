// import logger from 'redux-logger';
import thunk, { ThunkAction } from 'redux-thunk';
import { Action, combineReducers, configureStore } from '@reduxjs/toolkit';

import * as API from 'api';
import auth from 'services/authentication/slice';
import navigation from 'services/navigation/slice';

import gates from 'containers/System/Gates/slice';
import notifications from 'containers/System/Notifications/slice';

import pub from 'containers/Pub/slice';
import feed from 'containers/Feed/slice';
import list from 'containers/List/slice';
import claim from 'containers/Claim/slice';
import files from 'containers/Files/slice';
import orcid from 'containers/Orcid/slice';
import access from 'containers/Access/slice';
import content from 'containers/Content/slice';
import profile from 'containers/Profile/slice';
import publish from 'containers/Publish/slice';
import campaigns from 'containers/Campaigns/slice';
import community from 'containers/Community/slice';
import dictionary from 'containers/Dictionary/slice';

const preloadedState = {};
const thunkExtras = { API };
const rootReducer = combineReducers({
    auth,
    navigation,

    gates,
    notifications,

    pub,
    feed,
    list,
    claim,
    files,
    orcid,
    access,
    content,
    profile,
    publish,
    campaigns,
    community,
    dictionary,
});
export type RootState = ReturnType<typeof rootReducer>;
export type AppThunk = ThunkAction<void, RootState, typeof thunkExtras, Action<string>>;

const middleware = [thunk.withExtraArgument(thunkExtras)];
// const middleware = [...getDefaultMiddleware<RootState>(), thunk.withExtraArgument(thunkExtras)];
// const middleware = [...getDefaultMiddleware<RootState>(), thunk.withExtraArgument(thunkExtras), logger];

const store = configureStore({
    reducer: rootReducer,
    middleware,
    devTools: process.env.NODE_ENV !== 'production',
    preloadedState,
});

export default store;
