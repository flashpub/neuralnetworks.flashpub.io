import dayjs from 'dayjs';
import * as React from 'react';
import { kebabCase, compact } from 'lodash';
import { useDispatch } from 'react-redux';
import { useNavigate } from '@reach/router';

import { parseHtml, useTypedSelector } from 'helpers';
import * as Pub from 'containers/Pub';
import * as Claim from 'containers/Claim';
import * as Content from 'containers/Content';
import * as Publish from 'containers/Publish';
import * as Dictionary from 'containers/Dictionary';
import {
    StyledPubRecordWrapper,
    StyledPubRecordTag,
    StyledPubRecordContent,
    StyledPubRecordTitle,
    StyledPubRecordUpper,
    StyledPubRecordDate,
    StyledPubRecordDescription,
    StyledPubRecordLower,
    StyledPubRecordClaim,
    StyledPubRecordAuthor,
    StyledPubRecordInnerWrapper,
    StyledAuthoredDateWrapper,
} from 'ui/Feed/styles';
import { FPClaim } from 'ui/elements';
import { PubRecordActions } from 'ui/Feed/components/PubRecordActions';
import { StyledSpacer } from 'ui/elements/styles';

type Props = {
    draft?: boolean;
    pub?: PubDefinition;
};

export const PubRecord: React.FC<Props> = ({ pub, draft = false }) => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const community = useTypedSelector((state) => state.community.data.community);
    const campaigns = useTypedSelector((state) => state.campaigns.data.campaigns);
    
    if (pub) {
        const handleOpenPub = () => {
            if (draft) {
                const draftContentType = kebabCase(pub.metadata.draft?.contentType);

                // match draftContentType name with a value inside community list
                const matchCommunityContent = Object.values((community as CommunityDefinition)?.contentTypes).filter(
                    (ct) => kebabCase(ct.contentTypeLabel) === draftContentType
                )[0];
                const matchCampaignContent = compact(campaigns.map((c) => Object.values(c.contentTypes)
                    .map((ct) => kebabCase(ct.contentTypeLabel) === draftContentType && ct)[0]
                ))[0];
                
                const matchContent = matchCommunityContent || matchCampaignContent;

                if (matchContent) {
                    dispatch(Pub.draft(pub));
                    dispatch(Claim.populate(pub.metadata.draft?.claim));
                    dispatch(Dictionary.term1(pub.metadata.draft?.term1));
                    dispatch(Dictionary.term2(pub.metadata.draft?.term2));
                    dispatch(Content.populate(matchContent));
                    dispatch(Publish.step({ order: 1, type: 'CLAIM' }));
                    navigate(`/publish/${draftContentType}/claim`, { state: { pass: true } });
                }
            } else {
                dispatch(Pub.select(pub.id));
                navigate(`/pub/${pub.metadata.fetchId}`);
            }
        };
        
        const renderAuthors = pub.contributors.length > 0
            ? `by ${pub?.author.name} et al on`
            : `by ${pub?.author.name} on`;

        return (
            <StyledPubRecordWrapper onClick={handleOpenPub} id="pub_record">
                {!pub?.review.isPubAccepted && !pub?.metadata.draft && (
                    <StyledPubRecordTag>IN REVIEW</StyledPubRecordTag>
                )}
                <StyledPubRecordInnerWrapper>
                    <StyledPubRecordContent>
                        <StyledPubRecordUpper>
                            <StyledPubRecordTitle>{pub?.title}</StyledPubRecordTitle>
                            <PubRecordActions pub={pub} />
                        </StyledPubRecordUpper>
                        <StyledSpacer height={10} />
                        <StyledPubRecordDescription>{parseHtml(pub?.description as string)}</StyledPubRecordDescription>
                        <StyledPubRecordLower>
                            <StyledPubRecordClaim>
                                <FPClaim terms={pub.claim.terms} />
                            </StyledPubRecordClaim>
                            <StyledAuthoredDateWrapper>
                                <StyledPubRecordAuthor>{renderAuthors}</StyledPubRecordAuthor>
                                <StyledPubRecordDate>
                                    {dayjs(pub?.metadata.datePublished).format('M/D/YYYY')}
                                </StyledPubRecordDate>
                            </StyledAuthoredDateWrapper>
                        </StyledPubRecordLower>
                    </StyledPubRecordContent>
                </StyledPubRecordInnerWrapper>
            </StyledPubRecordWrapper>
        );
    } else return null;
};
