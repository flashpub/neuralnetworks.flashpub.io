type ClaimContainerState = {
    claim: ClaimDefinition & PublicationFormClaim & PubClaim;
};
type ClaimItem = {
    readonly id: string;
    readonly name: string;
};
type ClaimItems = {
    readonly firstItem: ClaimItem;
    readonly secondItem: ClaimItem | null;
};
type ClaimDefinition = {
    readonly id: string;
    readonly claimItems: ClaimItems;
    readonly linkedPubIds: string[];
    readonly allDictionaryIds: string[];
    readonly relationship: 'single' | 'relational' | null;
    readonly relationshipType: 'positive' | 'negative' | 'neutral' | null;

    readonly [key: string]: any;
};
type PubTerms = {
    firstItem: string;
    type: string | null;
    secondItem: string | null;
    relationship: string | null;
};
type PubClaim = {
    readonly id: string;
    readonly conditions: PubClaimCondition[];
    readonly terms: PubTerms;
};
type PublicationFormClaim = {
    readonly id: string;
    readonly firstItem: string;
    readonly firstItemId: string;
    readonly secondItem: string;
    readonly secondItemId: string;
    readonly conditions: PubClaimCondition[];
    readonly relationship: string;
    readonly firstItemAncestors: string[];
    readonly secondItemAncestors: string[];
    readonly relationshipType: 'positive' | 'negative' | 'neutral' | null;
};
type PubClaimCondition = {
    readonly id: string;
    readonly value: any;
    readonly name: string;
    readonly type: string;
};
