import React, { Dispatch, SetStateAction } from 'react';
import styled from 'styled-components';

import { Author } from 'app/types';
import { Spacing } from 'app/assets';
import { PortalView } from 'app/utils';

import { EditedField } from 'publication/types';
import { PublicationBookmark, PublicationMore, PublicationShare } from 'publication/components';

type Props = {
    pubId: string;
    author: Author;
    isAuth: boolean;
    bookmarkedBy: Array<string>;
    userId: Optional<string> | null;
    editDescription: Dispatch<SetStateAction<EditedField>>,
    setPortalView(portalView: PortalView): void,
    updateBookmark(update: any): void;
    setEditMode(): void;
    removeDescFromStorage(): void;
    removeAssetsFromStorage(): void,
    setAssetsChanged(assetsChanged: {
        code: boolean,
        datasets: boolean,
        protocols: boolean,
        references: boolean
    }): void;
    pubDOI: string;
};

const PublicationActions: React.FC<Props> = (
    {
        author, bookmarkedBy, userId, updateBookmark, setEditMode, isAuth, pubId, setPortalView,
        removeDescFromStorage, editDescription, removeAssetsFromStorage, setAssetsChanged, pubDOI
    }
) => (
        <Actions>
            <PublicationShare
                pubDOI={pubDOI}
            />
            <PublicationBookmark
                isAuth={isAuth}
                userId={userId}
                author={author}
                bookmarkedBy={bookmarkedBy}
                updateBookmark={updateBookmark}
                setPortalView={setPortalView}
            />
            <PublicationMore
                pubId={pubId}
                pubDOI={pubDOI}
                setEditMode={setEditMode}
                editDescription={editDescription}
                setAssetsChanged={setAssetsChanged}
                removeDescFromStorage={removeDescFromStorage}
                removeAssetsFromStorage={removeAssetsFromStorage}
                userId={userId}
                authorId={author.id}
            />
        </Actions>
    );

const Actions = styled.div`
    top: 0;
    right: 0;
    position: absolute;
    padding: ${Spacing.S};
`;

export default PublicationActions;
