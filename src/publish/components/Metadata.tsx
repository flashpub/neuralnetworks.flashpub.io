import * as React from 'react';
import shortid from 'shortid';
import styled from 'styled-components';
import capitalize from 'lodash/capitalize';
import { Icon, Popover, Position, Tooltip } from '@blueprintjs/core';

import { Asset } from 'app/types';
import { Color, Font, Radius, Spacing } from 'app/assets';
import { PublishAction, usePublish } from 'publish';

import { FUIButton, FUIInput } from 'FUI/elements';


const initialAsset = { name: '', url: '' };

export const Metadata: React.FC = () => {
    const [{ form }, dispatch] = usePublish();
    const [asset, setAsset] = React.useState<typeof initialAsset>(initialAsset);
    const metadata: Metadata = form.metadata;

    const handleUrlChange = (value: string) => setAsset({...asset, url: value});
    const handleNameChange = (value: string) => setAsset({...asset, name: value});

    const clearAssetForm = () => {
        setAsset(initialAsset);
    };
    const renderAssetsForm = (type: string) => {
        const bySource = () => {
            let name = 'name', url = 'url';
            switch (type) {
                case 'protocols':
                    name = 'protocol name';
                    url = 'e.g. doi from protocols.io';
                    break;
                case 'code':
                    name = 'codebase name';
                    url = 'e.g. url from codeocean.com';
                    break;
                case 'datasets':
                    name = 'dataset name';
                    url = 'e.g. doi from figshare.com';
                    break;
                case 'references':
                    name = 'choose a reference';
                    url = 'doi';
                    break;
                default:
                    return { name, url };
            }
            return { name, url };
        };
        const onAddAsset = () => {
            const newAsset: Asset = {
                id: shortid(),
                url: asset.url,
                name: asset.name,
            };
            dispatch(PublishAction.SetPublishFormValue({ name: `metadata.${type}`, value: newAsset }));
            setAsset(initialAsset);
            clearAssetForm();
        };
        const isDisabled = !asset.name || !asset.url;
        const assetName = bySource().name;

        return (
            <AssetForm>
                <AssetUrl
                    required
                    onChange={handleUrlChange}
                    placeholder={bySource().url}
                />
                    {type === 'references'
                        ? (<Select
                            placeholder={assetName}
                            defaultValue={'DEFAULT'}
                            onChange={(e) => handleNameChange(e.target.value as string)}
                        >
                            <option value="DEFAULT" disabled>{assetName}</option>
                            <option value="supports">Supports</option>
                            <option value="refutes">Refutes</option>
                            <option value="extends">Extends</option>
                        </Select>) : (<AssetName
                            required
                            placeholder={assetName}
                            onChange={handleNameChange}
                        />)
                    }
                <AddAssetButton
                    text="add"
                    type="button"
                    onClick={onAddAsset}
                    disabled={isDisabled}
                    buttonType="outlined"
                />
            </AssetForm>
        );
    };

    const renderMetadataElement = (type: string) => (
        <Box>
            <MetadataName>{capitalize(type)}</MetadataName>
            <Popover
                autoFocus={true}
                canEscapeKeyClose={true}
                position={Position.RIGHT}
                content={renderAssetsForm(type)}
            >
                <Tooltip content="add" position={Position.RIGHT}>
                    <AddAssetIcon iconSize={14} icon="add"/>
                </Tooltip>
            </Popover>
        </Box>
    );
    const assetTile = (
        asset: Asset,
        action: (id: string | undefined) => void,
        i: number,
        type?: string
    ) => {
        const prepareHref = (url: string) => {
            if (url.includes('http')) { return url }
            else { return `https://${url}` }
        };
        return (
            <AssetWrapper key={i}>
                <AssetRemoveIcon onClick={() => action(asset.id)} icon="delete" iconSize={14}/>
                <AssetLink>
                    {`${i + 1}) `}
                    <a target="_blank" href={prepareHref(asset.url)} rel="noopener noreferrer">
                        {`[${asset.name}] | ${asset.url}`}
                    </a>
                </AssetLink>
            </AssetWrapper>
        );
    };

    const printAssetTiles = (type: string) => metadata[type].map((asset: Asset, i: number) => {
        if (!asset) return null;
        const onRemoveAsset = (id: string | undefined) =>
            dispatch(PublishAction.CutPublishFormValue({ name: `metadata.${type}`, id }));
        return assetTile(asset, onRemoveAsset, i, type);
    });


    return (
        <MetadataWrapper>
            <MetadataList>
                {renderMetadataElement('references')}
                {printAssetTiles('references')}
            </MetadataList>
            <MetadataList>
                {renderMetadataElement('protocols')}
                {printAssetTiles('protocols')}
            </MetadataList>
            <MetadataList>
                {renderMetadataElement('code')}
                {printAssetTiles('code')}
            </MetadataList>
            <MetadataList>
                {renderMetadataElement('datasets')}
                {printAssetTiles('datasets')}
            </MetadataList>
        </MetadataWrapper>
    );
};


const MetadataWrapper = styled.div`
    text-align: left;
    color: ${Color.grey};
    padding: ${Spacing.M} 0;
    font-family: ${Font.text};
    margin-bottom: ${Spacing.XL};
    *:focus { outline: none };
`;
const AddAssetIcon = styled(Icon)`
    cursor: pointer;
    color: ${Color.midgrey};
    margin-left: ${Spacing.S};
    &:hover { color:  ${Color.grey}}
`;
const AssetRemoveIcon = styled(Icon)`
    cursor: pointer;
    margin-right: 2px;
    visibility: hidden;
    color: ${Color.midgrey};
    vertical-align: top !important;
    &:hover { color: ${Color.grey} }
    
    @media (max-width: 576px) {
        visibility: visible;
    }
`;
const MetadataList = styled.div`
    margin-bottom: ${Spacing.L};
`;
const AddAssetButton = styled(FUIButton)<{ disabled?: boolean }>` && {
    width: 100%;
    height: 26px;
    font-size: 14px;
    font-weight: 400;
    font-family: ${Font.pub_text};
    padding: ${Spacing.S} ${Spacing.L} ${Spacing.SM};
    cursor: ${({ disabled }) => disabled && 'not-allowed'};
    background-color: ${({ disabled }) => disabled && Color.lightgrey};
}`;
const AssetForm = styled.div`
    width: 300px;
    padding: ${Spacing.ML};
`;
const AssetField = styled(FUIInput)` && {
    height: 26px;
    font-size: 12px;
    margin: 0 0 ${Spacing.M};
}`;
const AssetUrl = styled(AssetField)``;
const AssetName = styled(AssetField)``;
const AssetWrapper = styled.div`
    position: relative;
    margin: ${Spacing.M} 0;
    border-radius: ${Radius};
    &:hover ${AssetRemoveIcon} {
        visibility: visible;
    }
`;
const AssetLink = styled.span`
    flex: 0 0 auto;
    vertical-align: top;
    a {
        width: 100%;
        font-weight: 200;
        color: ${Color.community};
        overflow: hidden !important;
        &:hover { color:  ${Color.flashpub}}
    }
`;
const MetadataName = styled.div`
    width: 80px;
    display: inline-block;
`;
const Box = styled.div`
    border-radius: ${Radius};
    padding: ${Spacing.S} ${Spacing.M};
    &:hover {
        cursor: pointer;
        background-color: ${Color.lightgrey};
    }
`;
const Select = styled.select`
    width: 100%;
    padding: 4px;
    color: #808080;
    box-shadow: none;
    height: 26px;
    font-size: 12px;
    margin: 0 0 8px;
    font-weight: 300;
    border-width: 1px;
    text-align: center;
    border-style: solid;
    border-color: #b9b9b9;
    border-radius: 0.33rem;
    background-color: #fafafa;
    font-family: 'Ubuntu',sans-serif;
`;
