import * as React from 'react';
import { StyledPubMobileBarWrapper } from 'ui/Pub/styles';

import { PubActionButtons } from 'ui/Pub/components/PubActionButtons';
import { useWindowWidth } from 'helpers';
import { Viewport } from 'assets/styles';



type Props = {
    showPanel: boolean;
    editMode: boolean;
    pub: PubDefinition;
    setReviewPanel(show: boolean): void;
};

export const PubMobileBar: React.FC<Props> = ({ pub, editMode, showPanel, setReviewPanel }) => {
    const isMobile = useWindowWidth() <= Viewport.TABLET;
    
    return (
        <StyledPubMobileBarWrapper>
            <PubActionButtons editMode={editMode} pub={pub} showPanel={showPanel} setReviewPanel={setReviewPanel} />
        </StyledPubMobileBarWrapper>
    );
};
