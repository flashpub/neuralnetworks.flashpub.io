import { defineAction } from 'yartsul';

import { action } from 'app/utils';
import { Content } from 'app/types';
import { PublishStep } from 'publish/utils';
import { DictionaryDefinition } from 'dictionaries/types';
import { FUIPanelProps } from 'FUI/components';


export const Actions = {
    ClearState: defineAction('Publish/ClearState'),
    CutPublishPanel: defineAction('Publish/CutPublishPanel'),
    SetPublishPanel: defineAction<FUIPanelProps>('Publish/SetPublishPanel'),
    SetPublishFormValue: defineAction<{ name: string, value: any }>('Publish/SetPublishFormValue'),
    CutPublishFormValue: defineAction<{ name: string, id?: string }>('Publish/CutPublishFormValue'),
    ReplacePublishFormValue: defineAction<{ name: string, value: any }>('Publish/ReplacePublishFormValue'),
};

export const PUBLISH_OPERATION_FAIL = action('publish_old/PUBLISH_OPERATION_FAIL');
export const PUBLISH_OPERATION_START = action('publish_old/PUBLISH_OPERATION_START');
export const PUBLISH_OPERATION_SUCCESS = action('publish_old/PUBLISH_OPERATION_SUCCESS');

export const SET_CONTENT_TYPE = action<Content>('app/SET_CONTENT_TYPE');
export const SET_PUBLISH_STEP = action<{ step: PublishStep | null }>('publish_old/SET_PUBLISH_STEP');
export const SET_PUB_CLAIM = action<{ claim?: DictionaryDefinition | null }>('publish_old/SET_PUB_CLAIM');
