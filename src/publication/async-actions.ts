import { Dispatch } from 'redux';

import * as AppAction from 'app/actions';
import { AlertType, PubDefinition, Thunk } from 'app/types';

import { publication } from 'publication/client';
import * as PublicationAction from 'publication/actions';


export function publicationAlertHandler(type: AlertType, alert: Error | { message: string }) {
    return (dispatch: Dispatch) => {
        dispatch(AppAction.ADD_ALERT.use({
            type: type,
            message: alert.message,
        }));
        dispatch(PublicationAction.PUBLICATION_OPERATION_FAIL.use());
    };
}

export function get(pubId: string): Thunk {
    return (dispatch) => {
        dispatch(PublicationAction.PUBLICATION_OPERATION_START.use());

        return publication.get(pubId)
            .then(response => {
                dispatch(PublicationAction.GET_PUBLICATION.use(response));
                dispatch(PublicationAction.PUBLICATION_OPERATION_SUCCESS.use());
            })
            .catch(error => dispatch(publicationAlertHandler(AlertType.ERROR, error)));
    };
}

export function update(pub: PubDefinition): Thunk {
    return (dispatch) => {
        dispatch(PublicationAction.PUBLICATION_OPERATION_START.use());

        return publication.update(pub)
            .then(() => dispatch(PublicationAction.PUBLICATION_OPERATION_SUCCESS.use()))
            .catch(error => dispatch(publicationAlertHandler(AlertType.ERROR, error)));
    };
}

export function edit(pub: PubDefinition, editedElements: Array<string>): Thunk {
    return (dispatch) => {
        dispatch(PublicationAction.EDIT_OPERATION_START.use());

        return publication.update(pub)
            .then(() => {
                dispatch(PublicationAction.EDIT_OPERATION_SUCCESS.use());
                dispatch(publicationAlertHandler(AlertType.SUCCESS, {
                    message: `Changes applied to 
                    ${editedElements.reduce((prev, current, index) => prev + (index === editedElements.length - 1 ? ' and ' : ', ') + current)} 
                    were saved`
                }))
            })
            .catch(error => {
                dispatch(AppAction.ADD_ALERT.use({
                    type: AlertType.ERROR,
                    message: error.message,
                }));
                dispatch(PublicationAction.EDIT_OPERATION_FAIL.use());
            });
    };
}
