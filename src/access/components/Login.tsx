import * as React from 'react';
import styled from 'styled-components';
import { Field, FieldProps, Form, Formik, FormikProps } from 'formik';

import { Status } from 'app/utils';
import { Color, Font, Spacing } from 'app/assets';
import { SystemGatesDefinition } from 'app/types';

import { AccessStep } from 'access/utils';
import { LoginCredentials } from 'access/types';
import { Button, Title as Text } from 'access/components/elements';
import { FUIInput } from '../../app/components/Shared';


type Props = {
    isAccessLoading: Status,
    gates: SystemGatesDefinition | null,
    setAccessStep(step: AccessStep): void,
    onLogin(credentials: LoginCredentials): void,
};

const Login: React.FunctionComponent<Props> = (props: Props) => {
    const handleOnLogin = (credentials: LoginCredentials) => props.onLogin(credentials);

    const renderFieldInput = ({ field }: FieldProps<LoginCredentials>) => {
        const inputType = field.name !== 'password' ? 'text' : 'password';
        return <FUIInput placeholder={field.name} type={inputType} {...field} />;
    };

    const handleNext = () => props.setAccessStep(AccessStep.GET_STARTED);

    return (
        <LoginContainer>
            <Title>flashpub</Title>

            <Formik
                initialValues={{ email: '', password: '' }}
                onSubmit={(values: LoginCredentials) => handleOnLogin(values)}
                render={(formikBag: FormikProps<LoginCredentials>) => {
                    const disableButton = formikBag.values.email === '' || formikBag.values.password === '';
                    return (
                        <Form>
                            <Field name="email" render={renderFieldInput} />
                            <Field name="password" render={renderFieldInput} />
                            <Reset
                                buttonType="text"
                                text="Reset password"
                                type="button"
                                onClick={() => props.setAccessStep(AccessStep.RESET_PASSWORD)}
                            />
                            <LoginButton
                                text="Login"
                                type="submit"
                                buttonType="text"
                                disabled={disableButton}
                                loading={props.isAccessLoading}
                            />
                        </Form>
                    )
                }}
            />

            <RegisterButton
                buttonType="text"
                text="Create a flashPub account"
                disabled={!(props.gates && props.gates.isRegOpen)}
                onClick={handleNext}
            />
        </LoginContainer>
    );
};

const LoginContainer = styled.div`
    text-align: center;
`;
const Title = styled(Text)` && {
    font-family: ${Font.heading};
}`;
const Reset = styled(Button)` && {
    font-size: 12px;
    color: ${Color.grey};
    text-transform: uppercase;
    :hover {
        color: ${Color.flashpub};
    }
}`;
const LoginButton = styled(Button)<{ disabled?: boolean }>` && {
    margin: 24px auto ${Spacing.XXL};
    cursor: ${props => props.disabled ? 'not-allowed' : 'pointer'};
    color: ${props => props.disabled ? Color.grey : Color.flashpub};
}`;
const RegisterButton = styled(Button)<{ disabled: boolean }>` && {
    font-size: 16px;
    color: ${Color.grey};
    margin-bottom: ${Spacing.M};
    cursor: ${props => props.disabled ? 'not-allowed' : 'pointer'};
    :hover {
        color: ${props => !props.disabled && Color.flashpub};
    }
}`;

export default Login;
