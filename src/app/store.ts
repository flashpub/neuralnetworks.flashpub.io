import { createBrowserHistory } from 'history';
import thunk, { ThunkDispatch } from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import {
    Store,
    createStore,
    applyMiddleware,
    combineReducers,
    Action,
} from 'redux';
import { connectRouter } from 'connected-react-router';

import { StateType } from 'app/types';

import app from 'app/reducer';
import feed from 'feed/reducer';
import access from 'access/reducer';
import publish from 'publish/reducer';
import campaigns from 'campaigns/reducer';
import publication from 'publication/reducer';
import dictionaries from 'dictionaries/reducer';

export const history = createBrowserHistory();

const rootReducer = combineReducers({
    router: connectRouter(history),
    app,
    feed,
    access,
    publish,
    campaigns,
    publication,
    dictionaries,
});

const middleware = [thunk];

export type RootState = StateType<typeof rootReducer>;

type StoreExtensions = { dispatch: ThunkDispatch<RootState, void, Action> };
type ExtendedStore = Store<RootState, Action> & StoreExtensions;

const configureStore = (initialState?: RootState): ExtendedStore => {
    return createStore(
        rootReducer,
        initialState,
        composeWithDevTools(applyMiddleware(...middleware)),
    );
};

const store = configureStore();

export default store;
