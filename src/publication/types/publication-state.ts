import { Status } from 'app/utils';
import { ClaimDefinition, PubDefinition } from 'app/types';


export interface PublicationState {
    readonly editStatus: Status;
    readonly status: Status;
    readonly pickedPub: string | null;
    readonly pub: PubDefinition | null;
    readonly claim: ClaimDefinition | null;
    readonly origPub: PubDefinition | null;
}
