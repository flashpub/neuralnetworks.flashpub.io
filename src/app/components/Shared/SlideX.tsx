import * as React from 'react';
import styled from 'styled-components';
import { Transition  } from 'react-transition-group';


type Props = {
    isMenuOpen: boolean,
    children: React.ReactNode,
};

const SlideX = (props: Props) => (
    <Transition in={props.isMenuOpen} timeout={300}>
        {state => (
            <StyledWrapper style={{ ...transitionStyles[state] }}>
                {props.children}
            </StyledWrapper>
        )}
    </Transition>
);

const transitionStyles = {
    entering: { transform: 'translateX(0)' },
    entered: { transform: 'translateX(0)' },
    exiting: { transform: 'translateX(100vw)' },
    exited: { transform: 'translateX(100vw)' },
};

const StyledWrapper = styled.div`
    transform: translateX(100vw);
    transition: transform .3s ease-in-out;
`;

export default SlideX;