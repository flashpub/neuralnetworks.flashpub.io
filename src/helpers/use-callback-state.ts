import React from 'react';

export function useCallbackState(defaultState: any) {
    const [value, setValue] = React.useState(defaultState);
    const firstMount = React.useRef<boolean>(true);
    const callback = React.useRef<any>(null);

    const setState = (state: any, fn?: any): void => {
        if (fn === undefined) {
            setValue(state);
        } else {
            setValue(state);
            callback.current = fn;
        }
    };

    React.useEffect(() => {
        if (firstMount.current) {
            firstMount.current = false;
            return;
        }

        if (callback.current) {
            callback.current();
        }
    }, [value]);

    return [value, setState];
}
