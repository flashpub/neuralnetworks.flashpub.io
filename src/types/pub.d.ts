type PubContainerState = {
    edit: boolean;
    pub: PubDefinition;
    draft: PubDefinition;
    selected: string | null;
};
type ContentOrigin = {
    readonly url: string;
    readonly source: string;
    readonly originalContent: boolean;
};
type Figure = {
    readonly id: string;
    readonly url: string;
    readonly type: string;
    readonly order: number;
    readonly label: string;
    readonly name?: string;
};
type Asset = {
    readonly id: string;
    readonly url: string;
    readonly name: 'Supports' | 'Refutes' | 'Extends' | string;
};
type Draft = {
    readonly isDraft: boolean;
    readonly contentType: string;
    readonly claim: ClaimDefinition;
    readonly term1?: TermDefinition;
    readonly term2?: TermDefinition;
};
type Review = {
    readonly isPubAccepted: boolean;
    readonly isPubRetracted: boolean;
    readonly discussionId: string[];
    readonly deadline: number;
    readonly acceptedBy: string[];
    readonly reportedBy: string[];
    readonly suggestedReviewers: string[];
    readonly quickReviews: { [key: string]: string[] };
};
type Metadata = {
    readonly protocols: Asset[];
    readonly code: Asset[];
    readonly datasets: Asset[];
    readonly references: Asset[];
    readonly datePublished: number;
    readonly dateUpdated: number;
    readonly doi: string;
    readonly fetchId: string;
    readonly isDeIdentified: boolean;
    readonly isPrivate: boolean;
    readonly draft: Draft | null;
    readonly allDictionaryIds: string[];

    [key: string]: any;
};
interface PubDefinition {
    readonly id: string;
    readonly title: string;
    readonly contentOrigin: ContentOrigin;
    readonly acceptedNotification: boolean;
    readonly author: Author;
    readonly contributors: Author[];
    readonly description: string;
    readonly figures: Figure[];
    readonly metadata: Metadata;
    readonly review: Review;
    readonly bookmarkedBy: string[];
    readonly claim: PubClaim;

    [key: string]: any;
}
