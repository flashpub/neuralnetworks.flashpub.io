interface AccessContainerState {
    isOpen: boolean;
    step: AccessStep;
    code: string | null;
    orcid: string | null;
}
interface Credentials {
    name: string;
    email: string;
    password: string;
}
type AccessStep =
    | 'LOGIN'
    | 'ORCID'
    | 'ERROR'
    | 'FINISH'
    | 'LOBBY'
    | 'SUMMARY'
    | 'USERNAME'
    | 'REGISTER'
    | 'GET_STARTED'
    | 'RESET_PASSWORD';
