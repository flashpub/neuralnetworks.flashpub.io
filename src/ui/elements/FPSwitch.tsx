import { Switch } from 'antd';
import * as React from 'react';
import styled from 'styled-components';

import { Color, Radius } from 'assets/styles';
import { StyledFlexWrapper } from 'ui/elements/styles';

interface Props {
    onSwitchChange(value: any): void;
}

export const FPSwitch: React.FC<Props> = ({ onSwitchChange }) => (
    <StyledSwitchWrapper>
        <StyledSwitch
            checkedChildren="Yes"
            unCheckedChildren="No"
            onChange={onSwitchChange}
        />
    </StyledSwitchWrapper>
);

const StyledSwitchWrapper = styled(StyledFlexWrapper)`
    width: 100%;
    height: 34px;
    border-radius: ${Radius};
    border: 1px solid ${Color.midgrey};
    background-color: ${Color.notwhite};
    .ant-switch-checked {
        background-color: ${Color.community};
    }
`;
const StyledSwitch = styled(Switch)`
    height: fit-content !important;
`;
