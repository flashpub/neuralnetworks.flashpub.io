// export const clinic = {
//     url: 'ai.flashpub.io',
//     name: 'ai',
//     longName: 'Artificial Intelligence',
//     id: 'dcbb3983-4515-4754-b927-0992633de904',
//     color: (a: number) => `rgba(67, 85, 165, ${a})`,
// };

// @ts-ignore
const clinic = {
    url: 'clinic.flashpub.io',
    name: 'clinic',
    longName: 'Clinic',
    id: '9f39c304-9c67-4251-83d9-86f7837c0135',
    // filterOneId: '8810c4ed-1db1-4c85-901b-b6e46966e910',
    // filterTwoId: 'adabe023-24ec-4a02-892f-8cfdcbc43f30',
    color: (a?: number) => `rgba(208, 2, 27, ${a || 1})`,
};
// @ts-ignore
const ai = {
    url: 'ai.flashpub.io',
    name: 'ai',
    longName: 'Artificial Intelligence',
    id: 'dcbb3983-4515-4754-b927-0992633de904',
    // filterOneId: '8810c4ed-1db1-4c85-901b-b6e46966e910',
    // filterTwoId: 'adabe023-24ec-4a02-892f-8cfdcbc43f30',
    color: (a?: number) => `rgba(67, 85, 165, ${a || 1})`,
};
// @ts-ignore
const surgery = {
    url: 'surgery.flashpub.io',
    name: 'surgery',
    longName: 'Surgery',
    id: '1e8bd2b5-b762-41cd-a79d-d04af88bbbde',
    // filterOneId: '8810c4ed-1db1-4c85-901b-b6e46966e910',
    // filterTwoId: '1e8bd2b5-b762-41cd-a79d-d04af88bbbde',
    color: (a?: number) => `rgba(208, 2, 27, ${a || 1})`,
};
// @ts-ignore
const outbreak = {
    url: 'outbreak.flashpub.io',
    name: 'outbreak',
    longName: 'Outbreak',
    id: 'a09615d2-4299-4d36-90dd-7648b97589ea',
    color: (a?: number) => `rgba(106, 151, 186, ${a || 1})`,
};

export const query = {
    size: 10,
};

export const project = outbreak;
