type UploadContainerState = {
    url: string;
    progress: number;
}
