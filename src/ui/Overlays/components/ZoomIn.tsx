import * as React from 'react';

import { StyledZoomInTitle, StyledZoomInWrapper } from 'ui/Overlays';

type Props = {
    open: boolean;
    title: string;
    imgSrc: string;
    onClose: (open: boolean) => void;
};

export const ZoomIn: React.FC<Props> = ({ open, onClose, imgSrc, title }) => {
    const handleOnClose = () => onClose && onClose(false);

    return (
        <StyledZoomInWrapper centered footer={null} visible={open} closable={false} onCancel={handleOnClose}>
            <StyledZoomInTitle>{title}</StyledZoomInTitle>
            <img alt="zoomed in figure" src={imgSrc} onClick={handleOnClose} />
        </StyledZoomInWrapper>
    );
};
