import * as React from 'react';
import styled from 'styled-components';

import { Color, Font, MOBILE, Radius, Spacing } from 'assets/styles';

interface InputProps {
    [propName: string]: any;

    chars?: number;
    counter?: boolean;
    disabled?: boolean;
    required?: boolean;
    placeholder?: string;
    onChange?(value: string): any;
    onBlur?(): void;
}
type ChangeEvent = React.ChangeEvent<HTMLInputElement>;

export const FPInput: React.FC<InputProps> = ({
    value,
    disabled,
    required,
    onBlur,
    onChange,
    placeholder,
    chars = 100,
    counter = false,
    ...props
}) => {
    const onValueChange = (e: ChangeEvent) => onChange && onChange(e.target.value);

    const isRequired = required && !value;

    return (
        <InputWrapper required={isRequired}>
            <StyledInput
                {...props}
                value={value}
                onBlur={onBlur}
                maxLength={chars}
                disabled={disabled}
                required={isRequired}
                onChange={onValueChange}
                placeholder={placeholder}
            />
            {counter && !disabled && <Counter>{chars - value?.length}</Counter>}
        </InputWrapper>
    );
};

const Counter = styled.div`
    top: 50%;
    right: 4px;
    padding: 1px;
    font-size: 14px;
    position: absolute;
    visibility: hidden;
    color: ${Color.midgrey};
    border-radius: ${Radius};
    transform: translateY(-50%);
    background-color: rgba(255, 255, 255, 0.9);
`;
const InputWrapper = styled.div<InputProps>`
    width: 100%;
    position: relative;
    &::before {
        top: -2px;
        left: 4px;
        font-size: 16px;
        position: absolute;
        color: ${Color.error};
        content: ${({ required }) => (required ? "'*'" : "''")};
    }
    &:hover ${Counter} {
        visibility: visible;
    }
`;
const StyledInput = styled.input<InputProps>`
    width: 100%;
    height: 30px;
    font-size: 14px;
    box-shadow: none;
    font-weight: 400;
    border-width: 1px;
    text-align: center;
    border-style: solid;
    color: ${Color.grey};
    padding: ${Spacing._8};
    border-radius: ${Radius};
    font-family: ${Font.text};
    margin-bottom: ${Spacing._8};
    border-color: ${Color.midgrey};
    background-color: ${Color.notwhite};
    &:hover {
        border-color: ${({ required, disabled }) =>
            disabled ? 'transparent' : required ? Color.midgrey : Color.midgrey};
    }
    &:focus {
        outline: none;
        border-color: ${Color.midgrey};
    }
    &:focus + ${Counter} {
        visibility: visible;
    }
    &::placeholder {
        font-weight: 200;
        font-style: italic;
        color: ${Color.midgrey};
        @media (min-width: ${MOBILE}) {
            height: 40px;
            font-size: 14px;
        }
    }
    @media (min-width: ${MOBILE}) {
        height: 40px;
    }
`;
