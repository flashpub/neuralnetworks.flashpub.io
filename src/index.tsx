import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import store from 'store';
import { GlobalStyle, AntdOverwrites } from 'assets/styles';
import { Authentication } from 'services/authentication';
import { GatesContainer } from 'containers/System/Gates';
import { NotificationsContainer } from 'containers/System/Notifications';
import { CampaignsContainer } from 'containers/Campaigns';
import { CommunityContainer } from 'containers/Community';
import { DictionaryContainer } from 'containers/Dictionary';
import { Layout } from 'ui/Layout';

import * as serviceWorker from './serviceWorker';

import 'normalize.css';
import 'antd/dist/antd.css';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import 'typeface-pridi';
import 'typeface-ubuntu';
import 'typeface-bree-serif';
import 'typeface-cutive-mono';

ReactDOM.render(
    <Provider store={store}>
        <GlobalStyle />
        <AntdOverwrites />

        <GatesContainer>
            <Authentication>
                <DictionaryContainer />
                <NotificationsContainer />
                <CampaignsContainer />
                <CommunityContainer />
                <Layout />
            </Authentication>
        </GatesContainer>
    </Provider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
