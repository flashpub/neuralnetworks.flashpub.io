import * as React from 'react';
import { Link } from '@reach/router';

import { StyledDictionaryTerm } from 'ui/Feed/DictionaryBar/styles';
import { Color } from 'assets/styles';

export const DictionaryTerm: React.FC<any> = (props) => (
    <StyledDictionaryTerm>
        <Link
            {...props}
            getProps={({ isCurrent }) => {
                return {
                    style: {
                        fontWeight: isCurrent ? 400 : 200,
                        color: isCurrent ? Color.community : Color.community_t75,
                        borderColor: isCurrent ? Color.community : 'transparent',
                    },
                };
            }}
        />
    </StyledDictionaryTerm>
);
