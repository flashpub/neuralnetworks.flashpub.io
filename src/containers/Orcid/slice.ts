import { createGenericSlice } from 'helpers';

const initialState = {
    data: {
        code: null,
        orcid: null,
    },
    status: { type: 'PENDING', message: null },
};

export const slice = createGenericSlice({
    name: 'orcid',
    initialState: initialState as GenericState<OrcidContainerState>,
    reducers: {
        orcid: (state, action) => {
            state.data.orcid = action.payload;
        },
        code: (state, action) => {
            state.data.code = action.payload;
        },
    },
});

export const { code, orcid, ...status } = slice.actions;

export default slice.reducer;
