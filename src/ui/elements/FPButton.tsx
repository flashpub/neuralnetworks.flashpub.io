import * as React from 'react';
import styled, { css } from 'styled-components/macro';
import { LoadingOutlined } from '@ant-design/icons';

import { Color, MOBILE, Radius, Spacing } from 'assets/styles';

interface Props extends React.DOMAttributes<HTMLButtonElement> {
    size?: number;
    block?: boolean;
    round?: boolean;
    fontSize?: number;
    icon?: JSX.Element;
    disabled?: boolean;
    isLoading?: boolean;
    iconSide?: 'left' | 'right';
    label?: string | JSX.Element;
    buttonType?: 'primary' | 'link' | 'default' | 'dashed';
}

export const FPButton: React.FC<Props> = ({
    icon,
    block,
    children,
    disabled,
    size = 40,
    round = false,
    fontSize = 18,
    iconSide = 'left',
    isLoading = false,
    label = 'Click Me',
    buttonType = 'default',
    ...props
}) => (
    <StyledButton
        size={size}
        block={block}
        round={round}
        disabled={disabled}
        fontSize={fontSize}
        buttonType={buttonType}
        isLoading={isLoading && !icon}
        {...props}
    >
        {iconSide === 'left' && icon && (
            <Icon icon={icon} iconSide={iconSide} isLoading={isLoading && !icon} round={round}>
                {isLoading && icon ? <LoadingOutlined /> : icon}
            </Icon>
        )}
        {round ? (
            isLoading && !icon && <LoadingOutlined />
        ) : (
            <div>
                {isLoading && !icon && <LoadingOutlined />}
                {!children && label}
                {children}
            </div>
        )}
        {iconSide === 'right' && (
            <Icon icon={icon} iconSide={iconSide} isLoading={isLoading && !icon} round={round}>
                {isLoading && icon ? <LoadingOutlined /> : icon}
            </Icon>
        )}
    </StyledButton>
);
const dashedButton = () => css`
    border: 1px dashed;
    cursor: not-allowed;
    color: ${Color.midgrey};
    background-color: ${Color.lightgrey};
    &:hover {
        background-color: ${Color.lightgrey};
    }
`;
const primaryButton = () => css`
    border: 0;
    color: #fff;
    background-color: ${Color.community};
    :hover {
        color: #fff;
        background-color: ${Color.community_t75};
    }
    &:active {
        border: 0;
    }
`;
const linkButton = () => css`
    border: none;
    background-color: #fff;
    color: ${Color.community};
    &:active {
        border: 0;
    }
`;
const defaultButton = () => css`
    background-color: #fff;
    &:hover {
        background-color: ${Color.community_t15};
    }
`;

const StyledButton = styled.button<Partial<Props>>`
    cursor: pointer;
    border: 1px solid;
    align-items: center;
    display: inline-flex;
    justify-content: center;
    color: ${Color.community};
    transition: width 0.15s linear;
    padding: 0 ${Spacing._x(19)};
    height: ${({ size }) => size && `${size}px`};
    border-radius: ${({ round }) => (round ? '100px' : Radius)};
    width: ${({ round, size, block }) => (block ? '100%' : round ? `${size}px` : 'unset')};
    ${({ buttonType }) => {
        if (buttonType === 'link') return linkButton();
        if (buttonType === 'dashed') return dashedButton();
        if (buttonType === 'default') return defaultButton();
        if (buttonType === 'primary') return primaryButton();
    }};
    ${({ disabled, buttonType }) => {
        if (disabled) {
            return css<Partial<Props>>`
                color: ${({ disabled }) => disabled && Color.midgrey};
                background-color: ${({ disabled, buttonType }) =>
                    disabled && buttonType === 'link' ? 'transparent' : disabled && Color.lightgrey};

                &:hover {
                    filter: ${({ disabled }) => disabled && `none`};
                    color: ${({ disabled }) => disabled && Color.midgrey};
                    cursor: ${({ disabled }) => disabled && 'not-allowed'};
                    background-color: ${({ disabled, buttonType }) =>
                        disabled && buttonType === 'link' ? 'transparent' : disabled && Color.lightgrey};
                }
            `;
        }
    }};
    div {
        position: relative;
        align-items: center;
        display: inline-flex;
        justify-content: center;
        font-size: ${({ fontSize }) => `${fontSize}px`};
        color: ${({ isLoading }) => isLoading && 'transparent'};
        span {
            position: absolute;
            color: ${({ buttonType }) => {
                if (buttonType === 'primary') return '#fff';
                if (buttonType === 'dashed') return `${Color.midgrey}`;
                return `${Color.community}`;
            }};
        }
    }
    :focus {
        outline: none;
    }
    @media (min-width: ${MOBILE}) {
        font-size: 18px;
    }
`;
const Icon = styled.span<Partial<Props>>`
    margin-left: ${({ iconSide, isLoading, round }) => (isLoading || round ? 0 : iconSide === 'right' && Spacing._8)};
    margin-right: ${({ iconSide, isLoading, round }) => (isLoading || round ? 0 : iconSide === 'left' && Spacing._8)};
    display: ${({ icon, isLoading }) => (isLoading ? 'inline-block' : icon ? 'inline-block' : 'none')};
    @media (min-width: ${MOBILE}) {
        margin-top: 0;
    }
`;
