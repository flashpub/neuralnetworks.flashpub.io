import * as React from 'react';
import { useDispatch } from 'react-redux';

import { useTypedSelector } from 'helpers';
import * as Notifications from 'containers/System/Notifications';

export const NotificationsContainer: React.FC = () => {
    const dispatch = useDispatch();
    const auth = useTypedSelector((state) => state.auth.data);

    React.useEffect(() => {
        auth && dispatch(Notifications.populateSystemNotifications(auth.uid));
    }, []);

    return null;
};
