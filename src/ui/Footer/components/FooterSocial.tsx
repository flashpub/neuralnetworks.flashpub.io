import * as React from 'react';
import {
    StyledSlackIcon,
    StyledMediumIcon,
    StyledTwitterIcon,
    StyledFooterSectionTitle,
    StyledFooterSocialWrapper, StyledFooterListItemLink,
} from 'ui/Footer/styles';

export const FooterSocial: React.FC = () => {
    return (
        <>
            <StyledFooterSectionTitle>Connect with us</StyledFooterSectionTitle>
            <StyledFooterSocialWrapper>
                <StyledFooterListItemLink
                    target="_blank"
                    href="https://join.slack.com/t/flashpubcommunity/shared_invite/zt-9kqlkgyd-zp0q5eJEO3~oPKHnx4DTqw"
                >
                    <StyledSlackIcon />
                </StyledFooterListItemLink>
                <StyledFooterListItemLink
                    target="_blank"
                    href="https://medium.com/@flashpub_io/the-most-valuable-ip-on-the-planet-343a450cd4bb"
                >
                    <StyledMediumIcon />
                </StyledFooterListItemLink>
                <StyledFooterListItemLink
                    target="_blank"
                    href="https://twitter.com/flashpub_io?s=20"
                >
                    <StyledTwitterIcon />
                </StyledFooterListItemLink>
            </StyledFooterSocialWrapper>
        </>
    );
};
