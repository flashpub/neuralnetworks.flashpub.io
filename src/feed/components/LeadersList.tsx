import * as React from 'react';
import sortBy from 'lodash/sortBy';
import styled from 'styled-components';

import { PubDefinition } from 'app/types';
import * as Shared from 'app/components/Shared';
import { Color, Font, Media, Spacing, Radius } from 'app/assets';
import { Status, useWindowWidth, Viewport } from 'app/utils';

import { AuthorDefinition } from 'feed/types';
import { LeaderRecord } from 'feed/components';
import { Container as MyContainer, Heading as MyHeading } from 'feed/components/elements';


type Props = {
    feedStatus: Status,
    pubs: Optional<Array<PubDefinition>> | null,
};

const LeadersList: React.FC<Props> = ({ pubs, feedStatus }) => {
    const isMobile = useWindowWidth() <= Viewport.MOBILE;

    const onLeaderRecordClick = (orcid: string) => {
        const url = `https://orcid.org/${orcid}`;
        orcid && window.open(url, "_blank");
    };

    const printAuthorsSortedPerPubCount = () => {
        if (pubs) {
            let authors = {};
            let leaders: Array<AuthorDefinition> = [];

            pubs.map((pub: PubDefinition) => {
                const pubId = pub.id;
                const authorId = pub.author.id;
                const authorName = pub.author.name;
                const split = pub.author.email !== undefined ? pub.author.email.split('@') : "";
                const authorAffiliation = '@'.concat(split[split.length - 1]);
                const authorInitials = authorName.match(/\b\w/g)!.join('');
                const orcid = pub.author.orcid;

                if (authors[authorId]) {
                    return authors[authorId] = {
                        ...authors[authorId],
                        pubs: [...authors[authorId].pubs, pubId],
                    }
                } else {
                    return authors[authorId] = {
                        id: authorId,
                        pubs: [pubId],
                        name: authorName,
                        initials: authorInitials,
                        affiliation: authorAffiliation,
                        orcid: orcid
                    }
                }
            });

            Object.values(authors).map(author => leaders.push(author as AuthorDefinition));

            return sortBy(leaders, leader => leader.pubs.length)
                .reverse()
                .splice(0, 3)
                .map((leader: AuthorDefinition, i) => (
                    <LeaderContainer key={i} onClick={() => onLeaderRecordClick(leader.orcid)}>
                        <LeaderRecord leader={leader} />
                    </LeaderContainer>
                ));
        } else {
            return [];
        }
    };

    const placeholder = [0, 1, 2].map((i) =>
        <LeaderContainer key={i} isLoading={feedStatus === Status.LOADING}>
            <Shared.Placeholder height={isMobile ? '67px' : '50px'} />
        </LeaderContainer>
    );

    return (
        <Container>
            <Heading show={printAuthorsSortedPerPubCount().length !== 0}>Leaders</Heading>
            <LeadersWrapper>
                {feedStatus === Status.LOADING ? placeholder : printAuthorsSortedPerPubCount()}
            </LeadersWrapper>
        </Container>
    );
};
const Container = styled(MyContainer)` && {
    height: fit-content;
}`;
const Heading = styled(MyHeading)<{ show?: boolean | null }>`
    color: ${props => props.show ? Color.community : 'transparent'};
    
    ${Media.desktop} {
        color: ${props => props.show ? Color.grey : 'transparent'};
    }
`;
const LeadersWrapper = styled.div`
    display: grid;
    grid-auto-flow: row;
    justify-items: center;
    padding: ${Spacing.M} 0;
    grid-template-columns: 1fr;
    grid-column-gap: ${Spacing.M};
    
    ${Media.tablet} {
        justify-items: unset;
        grid-template-columns: repeat(3, minmax(240px, 1fr));
    }
`;
const LeaderContainer = styled.div<{ isLoading?: boolean }>`
    width: 100%;
    display: grid;
    color: ${Color.grey};
    padding: ${Spacing.ML} 0;
    border-radius: ${Radius};
    transition: all .1s ease-in;
    grid-template-columns: auto;
    font-family: ${Font.heading};
     
    ${Media.tablet} {
        padding: ${Spacing.M};
        grid-template-columns: ${props => props.isLoading ? 'auto' : '50px auto'};
        
        :hover {
            cursor: pointer;
            transform: scale(1.03);
            background-color: ${Color.lightgrey};
        }
    }
`;

export default LeadersList;
