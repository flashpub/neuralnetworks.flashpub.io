import * as React from 'react';
import styled from 'styled-components';
import snakeCase from 'lodash/snakeCase';
import { RouteComponentProps, withRouter } from 'react-router';
import { Popover } from '@blueprintjs/core';

import { PortalView } from 'app/utils';
import { Color, Font, Spacing } from 'app/assets';
import { CommunityDefinition, Content } from 'app/types';

import { PublishStep } from 'publish/utils';

import { FUIButton } from 'FUI/elements';


type Props = {
    publishStep: PublishStep | null,
    community: CommunityDefinition | null,
    campaign: any,
    campaigns: any,

    setContentType(type: Content): void,
    setPublishStep(step: PublishStep): void,
    openPublishView(view: PortalView): void,
} & RouteComponentProps;

const HeaderButtonsPublish: React.FC<Props> = ({ setContentType, openPublishView, history, campaigns, campaign, community }) => {
    const [isOpen, setIsOpen] = React.useState(false);
    
    const handleUserMenu = (state: boolean) => setIsOpen(state);

    const onSetContentType = (type: Content) => {
        const label = type.contentTypeLabel;
        handleUserMenu(false);
        setContentType(type);
        openPublishView(PortalView.PUBLISH);
        history.push(`/publish/${snakeCase(label)}`);
        localStorage.removeItem('conditions');
    };

    const publishMenuItems = () => {
        if (campaign && !(community && community.contentTypes)) {
            return campaign.contentTypes
                ? Object.values(campaign.contentTypes)
                    .sort((a: any, b: any) => a.order - b.order)
                    .map((type: any, i) => (
                        <MenuItem key={i} onClick={() => onSetContentType(type)}>
                            {type.contentTypeLabel}
                        </MenuItem>
                    ))
                : [];
        } else if (campaigns && !(community && community.contentTypes)) {
            return campaigns.map((c: any) => Object.values(c.contentTypes).map((type: any, i: number) =>
                <MenuItem key={i} onClick={() => onSetContentType(type)}>
                    {type.contentTypeLabel}
                </MenuItem>
            ));
        } else {
            return community && community.contentTypes
                ? Object.values(community.contentTypes)
                    .sort((a, b) => a.order - b.order)
                    .map((type, i) => (
                        <MenuItem key={i} onClick={() => onSetContentType(type)}>
                            {type.contentTypeLabel}
                        </MenuItem>
                    ))
                : [];
        }
    };

    const publishMenu = <Menu>{publishMenuItems()}</Menu>;

    return (
        <Popover
            isOpen={isOpen}
            onInteraction={handleUserMenu}
            popoverClassName="pt-popover-content-sizing"
            content={publishMenu}
        >
            <PublishButton text="Publish" buttonType="text"/>
        </Popover>
    );
};
const Menu = styled.div`
    max-width: 140px;
    min-width: 110px;
    text-align: center;
    padding: 0 ${Spacing.M};
`;
const MenuItem = styled.li`
    cursor: pointer;
    font-size: 14px;
    position: relative;
    list-style-type: none;
    color: ${Color.midgrey};
    font-family: ${Font.pub_title};
    margin:${Spacing.L};
    :hover {
        color: ${Color.grey};
    };
`;
const PublishButton = styled(FUIButton)`
    padding-top: 7px;
    padding-bottom: 7px;
    display: inline-block;
    margin: 0 ${Spacing.L};
    font-family: ${Font.header_button};

    @media (max-width: 650px) {
        display: none;
    }
`;

export default withRouter(HeaderButtonsPublish);
