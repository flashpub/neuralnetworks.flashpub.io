type StatusType = 'LOADING' | 'PENDING' | 'FAILURE' | 'SUCCESS';
type SectionType = 'top' | 'recent' | 'drafts' | 'library' | 'list' | 'leaders' | 'campaigns';
type ListType = SectionType;
interface Status {
    type: StatusType;
    message: Error | string | null;
}
interface GenericState<T> {
    data: T;
    status: Status;
}
type SelectionTerm = {
    id: string;
    name: string;
};
// interface Selection {
//     pub: string | null;
//     term: SelectionTerm;
// }
type OrArray<T> = T | T[];
type Option = {
    readonly value: string;
    readonly key: string;
    readonly title: string;
};
interface Author {
    readonly email: string;
    readonly name: string;
    readonly id: string;
    readonly orcid: string;
}
interface Leader {
    readonly id: string;
    readonly name: string;
    readonly initials: string;
    readonly pubs: string[];
    readonly affiliation: string;
    readonly orcid: string;
    readonly bookmarks: string[];
    readonly endorsements: string[];
}


declare module 'medium-draft';
