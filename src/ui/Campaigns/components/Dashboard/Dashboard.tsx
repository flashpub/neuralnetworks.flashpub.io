import * as React from 'react';
import { useDispatch } from 'react-redux';

import { useTypedSelector } from 'helpers';
import { StyledDashboard } from 'ui/Campaigns/styles';
import * as Campaigns from 'containers/Campaigns/thunk';
import { DashboardTile } from 'ui/Campaigns';

export const Dashboard: React.FC = () => {
    const dispatch = useDispatch();
    
    const campaignData: CampaignData[] = useTypedSelector((state) => state.campaigns.data.list);
    
    React.useEffect(() => {
        if (campaignData.length === 0) {
            console.log('runs');
            dispatch(Campaigns.getCampaignData('Outbreak-Covid-19-characteristics'));
        }
    }, [dispatch, campaignData]);
    
    const renderDashboardTiles = [...campaignData]
        .sort((a,b) => b.pubCount - a.pubCount)
        .map((d) => <DashboardTile key={d.id} tileData={d as MidasCampaignData} />);
    
    return (
        <>
        <StyledDashboard.Container>
            <StyledDashboard.GridContainer>
                {renderDashboardTiles}
                {/*<DashboardAddTile />*/}
            </StyledDashboard.GridContainer>
        </StyledDashboard.Container>
        <StyledDashboard.PartnershipWrapper>
            {/*in partnership with*/}
            {/*<StyledDashboard.PartnershipLogo src="https://firebasestorage.googleapis.com/v0/b/flashpub-staging-489e3.appspot.com/o/partnerships%2Fmidas_logo.jpg?alt=media&token=41a5ce51-9516-43a8-8f78-f1efbed8d465" />*/}
        </StyledDashboard.PartnershipWrapper>
        </>
    );
};
