import * as React from 'react';
import uuid from 'uuid/v4';
import { lowerCase } from 'lodash';
import { Styles } from 'react-select';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import { Col as UICol } from 'react-styled-flexboxgrid';

import { Content } from 'app/types';
import { RootState } from 'app/store';
import { PublishAction, usePublish } from 'publish';
import { Label, Term } from 'publish/components/Elements';
import { DictionaryDefinition } from 'dictionaries/types';
import { FUIAsyncField } from 'FUI/elements';


type Option = { value: string, label: string, [key: string]: string }
type OrArray<T> = T[] extends undefined ? T[] : T


export const RelationalClaim: React.FC = () => {
    const [{ form }, dispatch] = usePublish();

    const content = useSelector<RootState, Content | null>(state => state.publish.contentType);
    const dictionaries = useSelector<RootState, DictionaryDefinition[] | null>(state => state.dictionaries.list);

    const secondItem = form.claim.secondItem;
    const secondItemId = secondItem && secondItem.id;
    const clearAncestors = () => dispatch(PublishAction.ReplacePublishFormValue({ name: 'claim.secondItemAncestors', value: [] }));

    React.useState(() => {
        const forcedTerm = content && content.claim.item2.setFilterAsItem;
        if (forcedTerm && dictionaries) {
            const item = dictionaries.filter(d => d.id === (content && content.claim.item2.filter))[0];
            dispatch(PublishAction.SetPublishFormValue({ name: 'claim.secondItem', value: { id: item.id, name: item.name }}));
        }
    });

    if (content && dictionaries) {
        const item2 = content.claim.item2;
        const relationship = content.claim.relationship;
        const forcedTerm = content.claim.item2.setFilterAsItem;
        const isNew = !dictionaries.filter(d => d.id === secondItemId)[0];
        const secondItemDictionaryAncestors = dictionaries && dictionaries.filter(d => d.ancestors.includes(item2.filter));

        const filterNames = (input: string) => secondItemDictionaryAncestors
            .filter(d => d.longName.toLowerCase().includes(input.toLowerCase()))
            .map(d => ({ value: d.id, label: d.longName }));

        const relationshipTypes = () => (relationship || []).map(r => ({ value: r.name, label: lowerCase(r.name), type: r.type, }));

        const onFieldChange = (field: string, value: OrArray<Option>) => {
            if (value) {
                if (Array.isArray(value)) {
                    const update = value.map(o => o.value);
                    dispatch(PublishAction.ReplacePublishFormValue({ name: field, value: update }));
                } else {
                    const exists = dictionaries.filter(d => d.id === value.value)[0];
                    const isRel = field === 'claim.relationship';
                    if (exists) { clearAncestors() }
                    if (isRel) {
                        dispatch(PublishAction.SetPublishFormValue({ name: field, value: value.value }));
                        dispatch(PublishAction.SetPublishFormValue({ name: 'claim.relationshipType', value: value.type }));
                    } else {
                        dispatch(PublishAction.SetPublishFormValue({
                            name: field,
                            value: { id: exists ? value.value : uuid(), name: exists ? exists.name : value.label },
                        }));
                    }
                }
            }
        };

        const setDefaultValue = (name: string) => {
            const item = form.claim[name];

            if (item && name === 'relationship') {
                return ({ value: item, label: lowerCase(item) });
            }

            if (item) {
                if (Array.isArray(item)) {
                    return item
                        .map(x => dictionaries.filter(d => d.id === x)[0])
                        .map(x => ({ value: x.id, label: x.longName }));
                } else {
                    const exists = dictionaries.filter(d => d.id === item.id)[0];
                    if (forcedTerm) return dictionaries.filter(d => d.id === content.claim.item2.filter).map(d => ({ value: d.id, label: d.longName }))[0];
                    return exists
                        ? dictionaries.filter(d => d.id === item.id).map(d => ({ value: d.id, label: d.longName }))[0]
                        : ({ value: item.id, label: item.name});
                }
            } return null
        };

        return (
            <>
                <Col xs={12} sm>
                    <RelationshipTerm>
                        <RelationshipLabel>What is the relationship</RelationshipLabel>
                        <FUIAsyncField
                            required
                            styles={styles}
                            isSearchable={false}
                            onChange={onFieldChange}
                            list={relationshipTypes}
                            field="claim.relationship"
                            placeholder="relationship"
                            defaultValue={setDefaultValue('relationship')}
                        />
                    </RelationshipTerm>
                </Col>
                <Col xs={12} sm>
                    <Term>
                        <Label>{item2.label}</Label>
                        <FUIAsyncField
                            required
                            styles={styles}
                            list={filterNames}
                            field="claim.secondItem"
                            onChange={onFieldChange}
                            isCreatable={item2.newTermAllowed}
                            placeholder="pick a term or type in something"
                            defaultValue={setDefaultValue('secondItem')}
                        />
                    </Term>
                    {secondItem && isNew &&
                        <Term>
                            <FUIAsyncField
                                isMulti
                                styles={styles}
                                list={filterNames}
                                onChange={onFieldChange}
                                placeholder="add ancestors"
                                field="claim.secondItemAncestors"
                                defaultValue={setDefaultValue('secondItemAncestors')}
                            />
                        </Term>
                    }
                </Col>
            </>
        );
    } return null;
};


const styles: Styles = {
    valueContainer: () => ({
        flex: 1,
        display: 'flex',
        flexWrap: 'wrap',
        padding: '2px 8px',
        overflow: 'hidden',
        position: 'relative',
        alignItems: 'center',
        boxSizing: 'border-box',
        justifyContent: 'center',
    }),
};
const Col = styled(UICol)` && {
    padding: 0;
}`;
const RelationshipTerm = styled(Term)` && {
    @media (min-width: 768px) {
        width: 120px;
    }
}`;
const RelationshipLabel = styled(Label)` && {
    display: block;

    @media (min-width: 768px) {
        display: none;
    }
}`;
