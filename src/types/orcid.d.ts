interface OrcidContainerState {
    code: string | null;
    orcid: string | null;
}
