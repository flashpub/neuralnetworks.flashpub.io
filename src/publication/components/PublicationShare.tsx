import * as React from 'react';
import styled from 'styled-components';
import copy from 'clipboard-copy';
import { Icon, Popover, Menu, Position, Tooltip, MenuItem } from '@blueprintjs/core';
import { TwitterShareButton } from 'react-share';
import { Color, Font, Spacing } from '../../app/assets';
import store from '../../app/store';
import * as AppAction from '../../app/actions';
import { AlertType } from '../../app/types';
import { project } from '../../app/config';


type Props = {
    pubDOI: string;
};

const PublicationShare: React.FC<Props> = ({ pubDOI = '' }) => {
    const fetchId = localStorage.getItem('fetchId');
    const url = `https://${project.url}/pub/${fetchId}`;
    const onCopy = () => {
        copy(pubDOI);
        store.dispatch(AppAction.ADD_ALERT.use({
            type: AlertType.INFO,
            message: "DOI copied to clipboard",
        }));
    };

    const sideMenu = (
        <PubMenu>
            <Tooltip content="Copy to clipboard" position={Position.RIGHT} openOnTargetFocus={false} autoFocus={false}>
                <MenuItem onClick={onCopy} text="Copy DOI" popoverProps={{ autoFocus: false }} />
            </Tooltip>
            <TwitterShareButton url={url} style={{ textAlign: 'center' }}>
                <Tooltip content="Share on Twitter" position={Position.RIGHT}>
                    <MenuItem text="Twitter" />
                </Tooltip>
            </TwitterShareButton>
        </PubMenu>
    );

    return (
        <MenuWrapper>
            <Popover content={sideMenu} position={Position.RIGHT_TOP}>
                <Tooltip content="Share" position={Position.RIGHT}>
                    <PubMenuIcon icon="share" iconSize={16} />
                </Tooltip>
            </Popover>
        </MenuWrapper>
    );
};

const MenuWrapper = styled.span`
    margin-left: 5px;
`;
const PubMenu = styled(Menu)` && {
    min-width: 50px;
    li {
        margin-bottom: 0;
        color: ${Color.midgrey};
        font-family: ${Font.text};
        :hover {
            color: ${Color.community};
        }
    }
    .styled-twitter {
        cursor: pointer;
    }
    .item-text {
        text-align: center;
        color: ${Color.midgrey};
        border-radius: 2px;
        font-family: ${Font.text};
        padding: ${Spacing.S} ${Spacing.M};
        :hover {
            color: ${Color.community};
            background-color: rgba(167, 182, 194, 0.3);
        }
    }
    *:focus {
        outline: none;
    }`;
const PubMenuIcon = styled(Icon)` && {
    cursor: pointer;
    color: ${Color.midgrey};
    margin-right: ${Spacing.M};
    :hover {
        color: ${Color.community};
    };
    :focus {
        outline: none;
    };
}`;

export default PublicationShare;
