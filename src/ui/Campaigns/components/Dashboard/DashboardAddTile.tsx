import * as React from 'react';
import { kebabCase } from 'lodash';
import { useNavigate } from '@reach/router';

import { useTypedSelector } from 'helpers';
import { FPSimpleSelect } from 'ui/elements';
import { StyledDashboardTile, StyledDashboardAddTile } from 'ui/Campaigns/styles';

interface Props {}

export const DashboardAddTile: React.FC<Props> = (props) => {
    const navigate = useNavigate();
    const [contentTypeLabel, setContentTypeLabel] = React.useState<string>('');
    
    const selectedCampaign = useTypedSelector((state) => state.campaigns.data.selected);
    
    const options = selectedCampaign &&
        Object.values(selectedCampaign.contentTypes!)
            .sort((a, b) => a.order - b.order)
            .map((c) => c.contentTypeLabel);
    
    
    const handleOnChange = (value: any) => {
        setContentTypeLabel(value);
    };
    
    const handleOnAdd = (contentTypeLabel: string) => {
        navigate(`/publish/${kebabCase(contentTypeLabel)}`);
    };
    
    return (
        <StyledDashboardTile.Container>
            <StyledDashboardAddTile.Wrapper>
                <StyledDashboardAddTile.Button onClick={() => handleOnAdd(contentTypeLabel || options![0])}>
                    <div>Add</div>
                    <div>{contentTypeLabel || options![0]}</div>
                </StyledDashboardAddTile.Button>
                {options &&
                    <FPSimpleSelect
                      onChange={handleOnChange}
                      options={options as string[]}
                        value={contentTypeLabel || options[0]}
                    />
                }
            </StyledDashboardAddTile.Wrapper>
        </StyledDashboardTile.Container>
    );
};
