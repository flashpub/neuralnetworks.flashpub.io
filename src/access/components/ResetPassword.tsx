import * as React from 'react';
import styled from 'styled-components';
import { ErrorMessage, Field, FieldProps, Form, Formik, FormikProps } from 'formik';

import { Status } from 'app/utils';
import { Color, Spacing } from 'app/assets';

import { emailValidation } from 'access/utils';
import { Button, Text as MyText, Error } from 'access/components/elements';

import { FUIInput } from '../../app/components/Shared';


type Props = {
    onClose(): void,
    isAccessLoading: Status,
    onReset(email: string): void,
};

const ResetPassword: React.FC<Props> = (props: Props) => {
    const renderFieldInput = ({ field }: FieldProps<{ email: string }>) => {
        const inputType = field.name !== 'password' ? 'text' : 'password';
        return <FUIInput placeholder={field.name} type={inputType} {...field}/>;
    };
    const handleOnReset = (email: string) => props.onReset(email);
    
    return (
        <>
            <Text>Type in your email address to reset your password</Text>
            
            <Formik
                initialValues={{ email: '' }}
                onSubmit={(values: { email: string }) => handleOnReset(values.email)}
                render={(formikBag: FormikProps<{ email: string }>) => {
                    const disableButton = formikBag.values.email === '' || formikBag.errors.email === 'Invalid email';
                    return (
                        <Form>
                            <InputWrapper>
                                <Field name="email" render={renderFieldInput} validate={emailValidation}/>
                                <ErrorMessage name="email">{error => <Error>{error}</Error>}</ErrorMessage>
                            </InputWrapper>
                            <ResetButton
                                text="Reset password"
                                type="submit"
                                buttonType="text"
                                disabled={disableButton}
                                loading={props.isAccessLoading}
                            />
                        </Form>
                    )
                }}
            />
        </>
    );
};

const Text = styled(MyText)` && {
    text-align: center;
}`;
const InputWrapper = styled.div`
    position: relative;
`;
const ResetButton = styled(Button)<{ disabled?: boolean }>` && {
    margin: ${Spacing.XXL} auto ${Spacing.M};
    cursor: ${props => props.disabled ? 'not-allowed' : 'pointer'};
    color: ${props => props.disabled ? Color.grey : Color.flashpub};
}`;

export default ResetPassword;
