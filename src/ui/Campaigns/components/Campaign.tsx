import * as React from 'react';

import { useTypedSelector } from 'helpers';
import { StyledCampaignWrapper } from 'ui/Campaigns/styles';
import { Map } from 'ui/Campaigns/components/Map/Map';
import { Dashboard } from 'ui/Campaigns/components/Dashboard/Dashboard';
import { GlobalLoader } from 'ui/Overlays';
import { CampaignTitle } from 'ui/Campaigns/components/CampaignTitle';

type Props = {
    type: 'map' | 'dashboard';
};

export const Campaign: React.FC<Props> = ({ type }) => {
    const campaignStatus = useTypedSelector((state) => state.campaigns.status.type);
    const selectedCampaign = useTypedSelector((state) => state.campaigns.data.selected);
    
    if (selectedCampaign) {
        const renderCampaign = () => {
            if (type === 'dashboard') {
                return <Dashboard />;
            }
            if (type === 'map') {
                return <Map />;
            }
            return null;
        };
        
        return (
            <StyledCampaignWrapper>
                <CampaignTitle />
                {renderCampaign()}
                {campaignStatus === 'LOADING' && <GlobalLoader />}
            </StyledCampaignWrapper>
        );
    }
    return null;
};
