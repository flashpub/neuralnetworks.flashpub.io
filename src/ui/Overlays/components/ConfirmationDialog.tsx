import { Modal } from 'antd';
import * as React from 'react';

type Props = {
    title: string;
    icon?: JSX.Element;
    content: string | JSX.Element;
    onOk(): any;
    onCancel(): any;
};

export const ConfirmationDialog: React.FC<Props> = ({ children, ...props }) => {
    const showConfirm = () => {
        Modal.confirm({
            title: props.title,
            icon: props.icon,
            content: props.content,
            onOk: props.onOk,
            onCancel: props.onCancel,
        });
    };

    return <div onClick={showConfirm}>{children}</div>;
};
