import * as React from 'react';

import { useTypedSelector } from 'helpers';
import { AccessScreen } from 'screens';
import { Summary } from 'ui/Overlays';

export const Overlays: React.FC = () => {
    const [summaryOpen, setSummaryOpen] = React.useState(false);
    const publishStep = useTypedSelector((state) => state.publish.data.step);
    
    React.useEffect(() => {
        if (publishStep.type === 'SUMMARY') {
            setSummaryOpen(true);
        } else {
            setSummaryOpen(false);
        }
    }, [publishStep]);
    
    return (
        <>
            <AccessScreen />
            <Summary open={summaryOpen} peersCount={3} reviewTime={3} />
        </>
    );
};
