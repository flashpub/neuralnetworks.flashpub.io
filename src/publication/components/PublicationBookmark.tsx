import * as React from 'react';
import styled from 'styled-components';
import { Tooltip, Position, Button } from '@blueprintjs/core';

import { PortalView } from 'app/utils';
import { Author } from 'app/types';
import { bookmarkFilled, bookmarkOpen, Color, Font, Spacing } from 'app/assets';

type Props = {
    author: Author,
    isAuth: boolean,
    bookmarkedBy: Array<string>,
    userId: Optional<string> | null,
    setPortalView(portalView: PortalView): void,
    updateBookmark(update: any): void,
};

const PublicationBookmark: React.FC<Props> = ({ author, bookmarkedBy, userId, updateBookmark, isAuth, setPortalView }) => {
    const isAuthor = author.id === userId;
    const isBookmarked = bookmarkedBy && bookmarkedBy.includes(userId!);
    const showBookmarksNum = (bookmarkedBy && bookmarkedBy.length) || 0;
    const checkIfBookmarked = isBookmarked
        ? bookmarkedBy.filter((value: string) => value !== userId)
        : (bookmarkedBy || []).concat(userId!);

    const handleSetBookmark = () => {
        if (!isAuth) {
            return setPortalView(PortalView.CONFIRMATION);
        }
        return updateBookmark(checkIfBookmarked);
    };

    return (
        <>
            <BookmarkIcon
                onClick={handleSetBookmark}
                bookmarked={isBookmarked ? 'bookmarked' : null}
                icon={isBookmarked ? bookmarkFilled : bookmarkOpen}
            />
            {isAuthor &&
                <Counter>
                    <Tooltip content={`Bookmarked by ${showBookmarksNum} users`} position={Position.RIGHT}>
                        <strong>{showBookmarksNum}</strong>
                    </Tooltip>
                </Counter>
            }
        </>
    );
};
const Counter = styled.div`
    margin-left: -9px;
    width: fit-content;
    display: none;
    color: ${Color.community};
    padding: 0 ${Spacing.S} 0;
    font-family: ${Font.text};
    border-top-right-radius: 2px;
    border-bottom-right-radius: 2px;
    background-color: ${Color.community_t15};
    strong {
        cursor: pointer;
    }
`;
const BookmarkIcon = styled(Button) <{ bookmarked: string | null }>` && {
    padding-top: 0;
    cursor: pointer;
    padding-left: 0;
    padding-right: 0;
    box-shadow: none !important;
    background-color: transparent !important;
    color: ${props => props.bookmarked ? Color.community : Color.grey};
    :focus {
        outline: none;
    };
}`;

export default PublicationBookmark;
