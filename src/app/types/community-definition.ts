export type CommunityMetrics = {
    readonly pubCount: number,
    readonly userCount: number,
    readonly subCommunityCount: number,
}

export type ContentInfo = {
    readonly title: string;
    readonly diagram: string;
    readonly description: string;
}

export type ContentClaimItem = {
    label: string;
    filter: string;
    newTermAllowed: boolean;
    setFilterAsItem: boolean;
}

export type ContentClaim = {
    type: 'single' | 'relational';
    relationship: Array<Relationship> | null;
    item1: ContentClaimItem;
    item2: ContentClaimItem;
}

export type DefCon = { name: string, type: any }
export type Content = {
    readonly contentTypeLabel: string;
    readonly order: number;
    readonly info: ContentInfo;
    readonly claim: ContentClaim;
    readonly quickReviews: Array<string>;
    readonly figureRequired: boolean;
    readonly defaultConditions: Array<DefCon>;
}

export type ContentType = {
    readonly [key: string]: Content,
}
export type Legal = {
    [key: string]: {
        readonly description?: string;
        readonly moreInfo?: string;
    }
}

export interface CommunityDefinition {
    readonly name: string;
    readonly dictionaryId: string;
    readonly createdOn: number;
    readonly metrics: CommunityMetrics;
    readonly contentTypes: ContentType;
    readonly loadingTaglines: Array<string>;
    readonly topSubCommunities: Array<string>;
    readonly quickReviewTemplates: Array<string>;

    readonly legal?: Legal;
}
