import * as React from 'react';
import styled from 'styled-components';
import htmlToDraft from 'html-to-draftjs';
import draftToHtml from 'draftjs-to-html';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertToRaw, ContentState } from 'draft-js';

import { StyledFlexWrapper } from 'ui/elements/styles';
import { Radius, Color, Spacing, Font } from 'assets/styles';

import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

interface TextEditorProps {
    pub: PubDefinition;
    editMode?: boolean;
    required?: boolean;
    placeholder?: string;
    onBlur?(value: string): void;
    onFocus?(value: string): void;
    onChange?(value: string): void;
}

const toolbar = {
    options: ['inline'],
    inline: {
        options: ['italic'],
    },
};

export const TextEditor: React.FC<TextEditorProps> = ({
    pub,
    onFocus,
    required,
    editMode,
    onChange,
    placeholder = 'Start typing...',
}) => {
    const [editorInFocus, setEditorInFocus] = React.useState<boolean>(false);
    const [editorState, setEditorState] = React.useState<EditorState>(EditorState.createEmpty());

    const description = editorState.getCurrentContent().getPlainText();
    const isRequired = required && !(description.length > 0);

    React.useEffect(() => {
        const setEditor = () => {
            const contentBlock = htmlToDraft(pub.description);
            if (contentBlock) {
                const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
                const editorState = EditorState.createWithContent(contentState);
                setEditorState(editorState);
            }
        };
        pub.description && setEditor();
    }, [pub.description]);

    React.useEffect(() => {
        if (editMode) {
            const selection = window.getSelection() as Selection;
            const editor = document.querySelector('.desc_editor') as HTMLElement;
            const toolbar = document.querySelector('.desc_toolbar') as HTMLElement;
    
            const placeToolbar = () => {
                const editorPos = editor.getBoundingClientRect();
                const toolbarPos = toolbar.getBoundingClientRect();
                const selectionPos = selection.getRangeAt(0).getBoundingClientRect();
                const top = selectionPos.top - editorPos.top - 40;
                const left = selectionPos.width / 2 - toolbarPos.width / 2 + selectionPos.left - editorPos.left;
                toolbar.style.display = 'flex';
                toolbar.style.top = top + 'px';
                toolbar.style.left = left + 'px';
            };
    
            editor.addEventListener('mouseup', () => {
                if (!selection.isCollapsed) {
                    placeToolbar();
                }
            });
    
            editor.addEventListener('mousedown', () => {
                toolbar.style.display = 'none';
            });
    
            if (selection.type !== 'Range') {
                toolbar.style.display = 'none';
            }
        }
    });

    const handleEditorBlur = () => {
        setEditorInFocus(false);
        const parsedEditorState = draftToHtml(convertToRaw(editorState.getCurrentContent())).replace(/(\r\n|\n|\r)/gm, '');
        if (description.length === 0) {
            onChange && onChange('');
        } else {
            onChange && onChange(parsedEditorState);
        }
    }
    const handleEditorFocus = () => {
        if (editMode) {
            onFocus && onFocus('');
            setEditorInFocus(true);
        }
    };

    return (
        <StyledEditorWrapper required={isRequired} disabled={!editMode} focus={editorInFocus}>
            <Editor
                toolbar={toolbar}
                readOnly={!editMode}
                onBlur={handleEditorBlur}
                placeholder={placeholder}
                editorState={editorState}
                onFocus={handleEditorFocus}
                editorClassName="desc_editor"
                toolbarClassName="desc_toolbar"
                onEditorStateChange={setEditorState}
            />
        </StyledEditorWrapper>
    );
};

const StyledEditorWrapper = styled(StyledFlexWrapper)<{ required?: boolean; disabled?: boolean; focus?: boolean }>`
    width: 100%;
    position: relative;

    &:after {
        top: -2px;
        left: 4px;
        z-index: 1;
        font-size: 16px;
        position: absolute;
        color: ${Color.error};
        content: ${({ required }) => (required ? "'*'" : "''")};
    }

    .rdw-editor-wrapper {
        width: 100%;
        position: relative;
    }
    .desc_editor {
        z-index: 10;
        font-size: 18px;
        font-weight: 200;
        border: 1px solid;
        line-height: 22px;
        border-radius: 5px;
        color: ${Color.grey};
        background-color: #fff;
        font-family: ${Font.text};
        transition: padding 0.2s ease;
        cursor: ${({ disabled }) => (disabled ? 'default' : 'text')};
        padding: ${({ required, focus }) => (required || focus ? `0 ${Spacing._8}` : 0)};
        border-color: ${({ focus, disabled, required }) =>
            !disabled && focus ? Color.midgrey : required ? Color.midgrey : 'transparent'};

        &:focus {
            outline: none;
            border-color: ${Color.midgrey};
        }
        &:hover {
            padding: ${({ disabled }) => (disabled ? 0 : `0 ${Spacing._8}`)};
            border-color: ${({ required, disabled }) =>
                required ? Color.midgrey : disabled ? 'transparent' : Color.midgrey};
        }

        blockquote {
            font-size: 16px;
            font-weight: 100;
            font-style: italic;
            padding: ${Spacing._4} ${Spacing._8};
            border-left: 2px solid ${Color.grey};
            background-color: ${Color.lightgrey};
            div {
                margin: 0;
            }
        }
    }
    .desc_toolbar {
        padding: 0;
        z-index: 11;
        display: none;
        position: absolute;
        align-items: center;
        justify-content: center;
        border-radius: ${Radius};
        border: 1px solid ${Color.community};
        :after {
            width: 0;
            top: 30px;
            left: 50%;
            height: 0;
            content: '';
            display: block;
            margin-left: -6px;
            position: absolute;
            border-style: solid;
            border-width: 6px 6px 0;
            border-color: ${Color.community} transparent transparent;
        }
    }
    .rdw-inline-wrapper,
    .rdw-block-wrapper {
        margin: 0;
    }
    .rdw-option-wrapper {
        margin: 0;
        width: 30px;
        height: 30px;
        border: none;
        display: flex;
        border-radius: 0;
        align-items: center;
        background: transparent;
        justify-content: center;
        border-right: 1px solid ${Color.community_t75};
        :last-child {
            border-right: none;
        }
        :hover {
            box-shadow: none;
        }
        :active {
            background: ${Color.community_t15};
        }
    }
    .rdw-option-active {
        box-shadow: none;
        background: ${Color.community_t(0.35)};
    }

    .rdw-dropdown-wrapper {
        border: 0;
        margin: 0;
        border-radius: ${Radius};
        :hover {
            box-shadow: none;
        }
    }
    .rdw-dropdown-selectedtext {
        margin: 0;
        height: 0;
        border-left: 1px solid ${Color.community_t75};
        border-right: 1px solid ${Color.community_t75};

        :last-child,
        :nth-last-child(2) {
            border-right: none;
        }
        span {
            color: ${Color.community};
        }
        .rdw-dropdown-carettoopen,
        .rdw-dropdown-carettoclose {
            top: 52%;
            right: 8%;
            transform: translateY(-50%);
        }
        .rdw-dropdown-carettoopen {
            border-top: 6px solid ${Color.community_t75};
        }
        .rdw-dropdown-carettoclose {
            border-bottom: 6px solid ${Color.community_t75};
        }
    }
    .rdw-dropdown-optionwrapper {
        width: 100%;
        box-shadow: none;
        border: 1px solid ${Color.community_t75};
        li {
            :hover {
                background: ${Color.community_t15};
            }
        }
    }
`;
