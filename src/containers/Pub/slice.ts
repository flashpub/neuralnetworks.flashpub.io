import { omit } from 'lodash';

import { createFormGenericSlice } from 'helpers';
import PubDataModel from 'models/data/pub';

const initialState = {
    data: {
        edit: false,
        selected: null,
        pub: PubDataModel,
        draft: PubDataModel,
    },
    status: { type: 'PENDING', message: null },
};

export const slice = createFormGenericSlice({
    name: 'pub',
    initialState: initialState as GenericState<PubContainerState>,
    reducers: {
        populate: (state, action) => {
            state.data.pub = action.payload;
        },
        select: (state, action) => {
            state.data.selected = action.payload;
        },
        edit: (state, action) => {
            state.data.edit = action.payload;
        },
        draft: (state, action) => {
            state.data.draft = action.payload;
        },
    },
});

const { populate, select, edit, draft, ...rest } = slice.actions;

const status = omit(rest, ['set', 'sub', 'cut', 'clear']);
const form = omit(rest, ['pending', 'loading', 'failure', 'success']);

export { populate, select, edit, draft, status, form };

export default slice.reducer;
