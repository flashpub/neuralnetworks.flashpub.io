import * as React from 'react';
import { StyledFooterSectionTitle, StyledFooterListItemLink } from 'ui/Footer/styles';


export const FooterResources: React.FC = () => {
    return (
        <>
            <StyledFooterSectionTitle>Resources</StyledFooterSectionTitle>
            <StyledFooterListItemLink
                target="_blank"
                href="https://midasnetwork.us/covid-19/"
            >
                Funding opportunities
            </StyledFooterListItemLink>
            <StyledFooterListItemLink
                target="_blank"
                href="https://join.slack.com/t/flashpubcommunity/shared_invite/zt-9kqlkgyd-zp0q5eJEO3~oPKHnx4DTqw"
            >
                Connect and discuss outbreak science
            </StyledFooterListItemLink>
            <StyledFooterListItemLink
                target="_blank"
                href="https://coronavirus.jhu.edu/"
            >
                Johns Hopkins COVID-19 database
            </StyledFooterListItemLink>
        </>
    );
};
