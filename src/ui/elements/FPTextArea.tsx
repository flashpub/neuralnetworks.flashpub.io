import { Input } from 'antd';
import * as React from 'react';
import styled from 'styled-components';

import { Color, Font, MOBILE, Radius, Spacing } from 'assets/styles';

interface InputProps {
    [propName: string]: any;

    rows?: number;
    chars?: number;
    counter?: boolean;
    required?: boolean;
    editable?: boolean;
    disabled?: boolean;
    placeholder?: string;
    onBlur?(): void;
    onFocus?(): void;
    onChange?(value: string): void;
}
type Event = React.ChangeEvent<HTMLTextAreaElement>

export const FPTextArea: React.FC<InputProps> = ({
    value,
    onBlur,
    onFocus,
    disabled,
    required,
    onChange,
    placeholder,
    chars = 300,
    counter = false,
    editable = true,
    ...props
}) => {
    const isRequired = required && !value;
    const onValueChange = (e: Event) => onChange && onChange(e.target.value);
    const handleOnFocus = () => onFocus && onFocus();

    return (
        <TextAreaWrapper required={isRequired}>
            <StyledTextArea
                {...props}
                value={value}
                onBlur={onBlur}
                maxLength={chars}
                disabled={disabled}
                required={isRequired}
                onFocus={handleOnFocus}
                onChange={onValueChange}
                placeholder={placeholder}
                autoSize={{ minRows: 1, maxRows: 3 }}
            />
            {counter && !disabled && <Counter>{chars - value.length}</Counter>}
        </TextAreaWrapper>
    );
};

const Counter = styled.div`
    top: 10px;
    right: 4px;
    padding: 1px;
    line-height: 0;
    font-size: 14px;
    position: absolute;
    visibility: hidden;
    color: ${Color.midgrey};
    border-radius: ${Radius};
    transform: translateY(-50%);
    background-color: rgba(255, 255, 255, 0.9);
`;
const TextAreaWrapper = styled.div<InputProps>`
    position: relative;
    &::before {
        top: -2px;
        left: 4px;
        z-index: 1;
        font-size: 16px;
        position: absolute;
        color: ${Color.error};
        content: ${({ required }) => (required ? "'*'" : "''")};
    }
    &:hover ${Counter} {
        visibility: visible;
    }
`;
const StyledTextArea = styled(Input.TextArea)<{ required?: boolean; disabled?: boolean }>`
    && {
        width: 100%;
        resize: none;
        cursor: default;
        font-size: 14px;
        box-shadow: none;
        font-weight: 400;
        border-width: 1px;
        text-align: center;
        border-radius: 5px;
        border-style: solid;
        color: ${Color.grey};
        background-color: #fff;
        font-family: ${Font.text};
        padding: ${({ disabled }) => (disabled ? 0 : `4px ${Spacing._8}`)};
        border-color: ${({ required }) => (required ? Color.midgrey : '#fff')};
        &:focus {
            outline: none;
            border-color: ${Color.midgrey};
        }
        &:hover {
            border-color: ${({ required, disabled }) =>
                required ? Color.midgrey : disabled ? 'transparent' : Color.midgrey};
        }
        &:focus + ${Counter} {
            visibility: visible;
        }
        &::placeholder {
            font-weight: 200;
            font-style: italic;
            color: ${Color.midgrey};
        }
        @media (min-width: ${MOBILE}) {
            padding: ${({ disabled }) => (disabled ? `${Spacing._4} 0` : `4px ${Spacing._8}`)};
        }
    }
`;
