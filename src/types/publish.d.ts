type PublishStep = 'INTRO' | 'CLAIM' | 'PUBLICATION' | 'SUBMIT' | 'SUMMARY';
type PublishContainerState = {
    step: {
        order: number;
        type: PublishStep;
    }
}