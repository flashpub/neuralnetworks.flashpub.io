import * as React from 'react';
import copy from 'clipboard-copy';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Dialog as UIDialog, Icon, Position, Tooltip } from '@blueprintjs/core';
import {
    FacebookShareButton, TwitterShareButton, EmailShareButton,
    FacebookIcon, TwitterIcon, EmailIcon,
} from 'react-share';

import store from 'app/store';
import { project } from 'app/config';
import { RootState } from 'app/store';
import { AlertType } from 'app/types';
import { PortalView } from 'app/utils';
import * as AppAction from 'app/actions';
import { Color, Font, Radius, Spacing } from 'app/assets';

import { FUIButton } from 'FUI/elements';


const mapDispatch = {
    setPortalView: AppAction.SET_PORTAL_VIEW.use,
};

const mapState = (state: RootState) => ({
    portalView: state.app.view,
});

type Props = {
    portalView: PortalView;
    setPortalView({ view }: any): void;
};


const PublishSummary: React.FC<Props> = (
    { portalView, setPortalView, }
) => {
    const fetchId = localStorage.getItem('fetchId');
    const url = `https://${project.url}/pub/${fetchId}`;
    const onClose = () => setPortalView({ view: PortalView.DEFAULT });
    const onCopy = () => {
        copy(url);
        store.dispatch(AppAction.ADD_ALERT.use({
            type: AlertType.INFO,
            message: "URL copied to clipboard",
        }));
    };
    return (
        <Modal
            onClose={onClose}
            canEscapeKeyClose={false}
            canOutsideClickClose={false}
            isOpen={portalView === PortalView.SUMMARY}
        >
            <Text>Your article has been submitted.</Text>
            <Title>Congrats!</Title>
            <Text>After submission, your article will be in review for <strong>7</strong> days.</Text>
            <Text>You'll need <strong>3</strong> colleagues to endorse the work for it to pass peer review.</Text>
            {fetchId && (
                <>
                    <Text>Share it with your colleagues!</Text>
                    <Share>
                        <ShareButton>
                            <Tooltip content="Share on Twitter" position={Position.RIGHT}>
                                <TwitterShareButton url={url}>
                                    <TwitterIcon size={40} round />
                                </TwitterShareButton>
                            </Tooltip>
                        </ShareButton>
                        <ShareButton>
                            <Tooltip content="Share on Facebook" position={Position.RIGHT}>
                                <FacebookShareButton url={url}>
                                    <FacebookIcon size={40} round />
                                </FacebookShareButton>
                            </Tooltip>
                        </ShareButton>
                        <ShareButton>
                            <Tooltip content="Copy to clipboard" position={Position.RIGHT}>
                                <CopyToClipboard onClick={onCopy}>
                                    <Icon icon="clipboard" iconSize={20} />
                                </CopyToClipboard>
                            </Tooltip>
                        </ShareButton>
                        <ShareButton>
                            <Tooltip content="Share via Email" position={Position.RIGHT}>
                                <EmailShareButton url={url}>
                                    <EmailIcon size={40} round />
                                </EmailShareButton>
                            </Tooltip>
                        </ShareButton>
                    </Share>
                </>
            )}
            <FinishButton text="Close" onClick={onClose} buttonType="text" />
        </Modal>
    );
};

const Modal = styled(UIDialog)` && {
    width: 450px;
    position: relative;
    text-align: center;
    background-color: #fff;
    padding: ${Spacing.XXXL};
    border-radius: ${Radius};
    font-family: ${Font.text};
    margin-left: ${Spacing.XL};
    margin-right: ${Spacing.XL};
}`;
const Title = styled.div`
    font-size: 30px;
    font-weight: 300;
    color: ${Color.community};
    margin-bottom: ${Spacing.XL};
`;
const Text = styled.p`
    font-size: 18px;
    font-weight: 200;
    text-align: center;
    color: ${Color.grey};
    margin-bottom: ${Spacing.XL};
    
    strong {
        color: ${Color.community};
    }
`;
const Share = styled.div`
    display: flex;
    justify-content: center;
    margin: ${Spacing.XL} 0 ${Spacing.XXL};
`;
const ShareButton = styled.div`
    margin: 0 ${Spacing.ML};
    .SocialMediaShareButton {
        cursor: pointer;
    }
    

`;
const CopyToClipboard = styled.div`
    width: 40px;
    height: 40px;
    display: flex;
    cursor: pointer;
    border-radius: 50%;
    align-items: center;
    justify-content: center;
    background-color: ${Color.grey};
    
    svg {
        fill: #fff !important;
    }
`;
const FinishButton = styled(FUIButton)` && {
    font-size: 24px;
}`;

// @ts-ignore
export default connect(mapState, mapDispatch)(PublishSummary);
