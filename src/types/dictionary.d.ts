type DictionaryContainerState = {
    list: TermDefinition[];
    term1: TermDefinition;
    term2: TermDefinition;
    selected: { id: string; name: string };
};
type Metrics = {
    readonly pubCount: number;
    readonly dateAdded: number;
    readonly authorCount: number;
    readonly dateModified: number;
};
type Origin = {
    readonly url: string;
    readonly id: string;
    readonly tag: string;
};
interface TermDefinition {
    readonly id: string;
    readonly isCommunity: boolean;
    readonly ancestors: string[];
    readonly metrics: Metrics;
    readonly description: string;
    readonly name: string;
    readonly longName: string;
    readonly origin: Origin;
    readonly props: any | null;
    readonly type: string;
    readonly author: Author;
    
    [key: string]: any;
}
