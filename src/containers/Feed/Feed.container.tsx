import find from 'lodash/find';
import * as React from 'react';
import { useDispatch } from 'react-redux';

import * as API from 'api';
import * as Feed from 'containers/Feed';
import { useTypedSelector } from 'helpers';
import { project } from 'configs';

export const FeedContainer: React.FC = ({ children }) => {
    const dispatch = useDispatch();
    const dictionaryList = useTypedSelector((state) => state.dictionary.data.list);
    const selectedTerm = useTypedSelector((state) => state.dictionary.data.selected);

    // TODO filter out drafts
    
    React.useEffect(() => {
        const path = window.location.pathname;
        const populateFeed = async (termId: string) => {
            dispatch(Feed.status.loading());
            try {
                const pubs: PubDefinition[] = [];
                const snapshot = await API.pubs.load(termId, 20);
                snapshot.forEach((snap) => pubs.push(snap.data() as PubDefinition));
                dispatch(Feed.populate(pubs.filter((p) => !p.metadata.draft)));
                dispatch(Feed.status.success());
            } catch (err) {
                dispatch(Feed.status.failure(err.message));
            }
        };

        if (path) {
            if (path.includes('/feed/')) {
                const pathTerm = path.split('/feed/')[1];
                const findTerm = find(dictionaryList, (term) => term.name === pathTerm);
                if (findTerm) {
                    populateFeed(findTerm.id);
                }
            } else if (path === '/') {
                populateFeed(project.community.id);
            } else {
                populateFeed(selectedTerm.id);
            }
        }
    }, [dispatch, dictionaryList, selectedTerm]);

    return <>{children}</>;
};
