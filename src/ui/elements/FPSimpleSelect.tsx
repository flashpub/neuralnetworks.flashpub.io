import * as React from 'react';
import styled from 'styled-components/macro';

import { Color, Font, Radius, Spacing } from 'assets/styles';
import { StyledOption, StyledSelect } from 'ui/elements/FPSelect';


interface Props {
    value?: string;
    options: string[];
    mobilebar?: string;
    onChange(value: any): void;
}

export const FPSimpleSelect: React.FC<Props> = ({ options, onChange, value, mobilebar }) => {
    return (
        <StyledSimpleSelect onChange={onChange} value={value === 'string[]' ? 'list' : value} mobilebar={mobilebar}>
            {options.map((option, i) => (
                <StyledSimpleOption
                    key={i}
                    title={option}
                    value={option}
                    className="fp-select-simple-option"
                >
                    {option}
                </StyledSimpleOption>
            ))}
        </StyledSimpleSelect>
    );
};

const StyledSimpleSelect = styled(StyledSelect)<{ mobilebar?: string }>`
    border: ${({ mobilebar }) => mobilebar === 'true' ? 0 : 1} !important;
    text-align: ${({ mobilebar }) => mobilebar === 'true' ? 'center' : 'unset'} !important;
    background-color: ${({ mobilebar }) => mobilebar === 'true' ? Color.community : 'transparent'} !important;
    .ant-select-selector {
        border: 0;
        border-radius: ${Radius};
        padding: 0 ${Spacing._8} !important;
        background-color: ${({ mobilebar }) => mobilebar === 'true' ? Color.community : 'transparent'} !important;
    }
    .ant-select-selection-item {
        font-family: ${Font.text};
        padding-top: ${Spacing._x(1)} !important;
        font-weight: ${({ mobilebar }) => mobilebar === 'true' ? 500 : 300} !important;
        color: ${({ mobilebar }) => mobilebar === 'true' ? '#fff' : Color.grey} !important;
    }
    .ant-select-arrow {
        color: ${({ mobilebar }) => mobilebar === 'true' ? "#fff" : 'unset'} !important;
    }
    .ant-select-selection-item {
        padding: ${({ mobilebar }) => mobilebar === 'true' ? 0 : 'unset'} !important;
    }
`;
const StyledSimpleOption = styled(StyledOption)`
    font-family: ${Font.text} !important;
`;
