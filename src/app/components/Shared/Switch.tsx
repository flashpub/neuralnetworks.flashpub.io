import * as React from 'react';
import { connect } from 'react-redux';
import { Switch as Router, Route } from 'react-router';

import { RootState } from 'app/store';
import { ReduxProps } from 'app/types';
import { OrcidContainer } from 'app/components';

import { FeedContainer } from 'feed/components';

import { PublicationContainer } from 'publication/components';

import { Publish } from 'publish/components/Publish';

import { LibraryContainer } from 'library/components';
import { Campaign } from 'campaigns';


const mapDispatch = {};
const mapState = (state: RootState) => ({
    current: state.dictionaries.current,
    origPub: state.publication.origPub,
});

type Props = ReduxProps<typeof mapState, typeof mapDispatch>;

const Switch: React.FC<Props> = ({ current, origPub }) => (
    <Router>
        <Route exact path="/" component={FeedContainer} />
        <Route path="/orcid" component={OrcidContainer} />
        <Route path={`/feed/${current && current.name}`} component={FeedContainer} />
        <Route path={`/pub/${origPub && origPub.metadata.fetchId}`} component={PublicationContainer} />
        <Route path="/publish" component={Publish} />
        <Route path="/library" component={LibraryContainer} />
        <Route path="/campaigns" component={Campaign} />
    </Router>
);

export default connect(mapState, mapDispatch)(Switch);
