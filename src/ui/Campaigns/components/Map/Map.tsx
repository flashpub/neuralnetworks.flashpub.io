import * as React from 'react';
import { usePrevious } from 'react-use';
import { useDispatch } from 'react-redux';

import { useTypedSelector } from 'helpers';
import * as Campaigns from 'containers/Campaigns';
import { CampaignMap } from 'ui/Campaigns/components/Map/CampaignMap';

type Props = {};

export const Map: React.FC<Props> = () => {
    const dispatch = useDispatch();
    const [currentMapZoom, setCurrentMapZoom] = React.useState(3);
    const [lastMapZoom, setLastMapZoom] = React.useState(3);
    const prevMapZoom = usePrevious(currentMapZoom);
    
    const campaignData = useTypedSelector((state) => state.campaigns.data.list);
    
    React.useEffect(() => {
        // TODO consider whether to run this only when there's no data fetched or every time
        // find if there's a way to refresh the component every x time
        if (campaignData.length === 0) {
            dispatch(Campaigns.getCampaignDataPerPubCount('Outbreak-COVID-19-city', 0));
            dispatch(Campaigns.getCampaignDataPerCaseCount('Outbreak-COVID-19-city', 50000));
        } else {
            if (prevMapZoom) {
                if (currentMapZoom >= 4 && currentMapZoom < 5 && currentMapZoom > prevMapZoom && currentMapZoom > lastMapZoom) {
                    dispatch(Campaigns.getCampaignDataPerCaseCount('Outbreak-COVID-19-city', 20000));
                    setLastMapZoom(currentMapZoom);
                }
                if (currentMapZoom >= 5 && currentMapZoom < 6 && currentMapZoom > prevMapZoom && currentMapZoom > lastMapZoom) {
                    dispatch(Campaigns.getCampaignDataPerCaseCount('Outbreak-COVID-19-city', 5000));
                    setLastMapZoom(currentMapZoom);
                }
                if (currentMapZoom >= 6 && currentMapZoom < 7 && currentMapZoom > prevMapZoom && currentMapZoom > lastMapZoom) {
                    dispatch(Campaigns.getCampaignDataPerCaseCount('Outbreak-COVID-19-city', 2000));
                    setLastMapZoom(currentMapZoom);
                }
                if (currentMapZoom >= 7 && currentMapZoom < 8 && currentMapZoom > prevMapZoom && currentMapZoom > lastMapZoom) {
                    dispatch(Campaigns.getCampaignDataPerCaseCount('Outbreak-COVID-19-city', 1000));
                    setLastMapZoom(currentMapZoom);
                }
                if (currentMapZoom >= 8 && currentMapZoom < 9 && currentMapZoom > prevMapZoom && currentMapZoom > lastMapZoom) {
                    dispatch(Campaigns.getCampaignDataPerCaseCount('Outbreak-COVID-19-city', 0));
                    setLastMapZoom(currentMapZoom);
                }
            }
        }
    }, [currentMapZoom]);
    
    React.useEffect(() => {
        const footer = document.querySelector('#footer') as HTMLElement;
        if (footer) {
            footer.style.display = 'none';
        }
        
        return () => {
            footer.style.display = 'flex';
        };
    });
    
    const handleOnZoom = (zoom: number) => setCurrentMapZoom(zoom);
    
    return <CampaignMap zoom={currentMapZoom} setZoom={handleOnZoom} />;
};
