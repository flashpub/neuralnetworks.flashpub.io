import { omit } from 'lodash';

import { project } from 'configs';
import { createFormGenericSlice } from 'helpers';
import TermDataModel from 'models/data/term';

const initialState = {
    data: {
        list: [],
        term1: TermDataModel,
        term2: TermDataModel,
        selected: { id: project.community.id, name: project.community.name },
    },
    status: { type: 'LOADING', message: null },
};

export const slice = createFormGenericSlice({
    name: 'dictionary',
    initialState: initialState as GenericState<DictionaryContainerState>,
    reducers: {
        populate: (state, action) => {
            state.data.list = action.payload;
        },
        select: (state, action) => {
            state.data.selected = action.payload;
        },
        search: (state, action) => {
            state.data.list = [...action.payload, ...state.data.list];
        },
        term1: (state, action) => {
            state.data.term1 = action.payload;
        },
        term2: (state, action) => {
            state.data.term2 = action.payload;
        },
    },
});

const { populate, select, search, term1, term2, ...rest } = slice.actions;

const status = omit(rest, ['set', 'sub', 'cut', 'clear']);
const form = omit(rest, ['pending', 'loading', 'failure', 'success']);

export { populate, select, search, term1, term2, status, form };

export default slice.reducer;
